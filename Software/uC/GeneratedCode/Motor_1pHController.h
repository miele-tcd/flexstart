/*
 * File: Motor_1pHController.h
 *
 * Code generated for Simulink model 'Motor_1pHController'.
 *
 * Model version                  : 1.467
 * Simulink Coder version         : 9.1 (R2019a) 23-Nov-2018
 * C/C++ source code generated on : Thu Feb 17 15:09:33 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. Traceability
 *    3. Safety precaution
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_Motor_1pHController_h_
#define RTW_HEADER_Motor_1pHController_h_
#include <string.h>
#ifndef Motor_1pHController_COMMON_INCLUDES_
# define Motor_1pHController_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                /* Motor_1pHController_COMMON_INCLUDES_ */

#include "Motor_1pHController_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

/* Block signals for system '<Root>/Interrupt_8k' */
typedef struct {
  real_T SFunction_o17;                /* '<S2>/StateMachine8K' */
  real_T SFunction_o18;                /* '<S2>/StateMachine8K' */
  real_T SFunction_o19;                /* '<S2>/StateMachine8K' */
  real_T SFunction_o20;                /* '<S2>/StateMachine8K' */
  real_T SFunction_o21;                /* '<S2>/StateMachine8K' */
  real_T SFunction_o22;                /* '<S2>/StateMachine8K' */
  real_T SFunction_o23;                /* '<S2>/StateMachine8K' */
  real_T SFunction_o24;                /* '<S2>/StateMachine8K' */
  real_T SFunction_o25;                /* '<S2>/StateMachine8K' */
  real_T SFunction_o26;                /* '<S2>/StateMachine8K' */
} B_Interrupt_8k_Motor_1pHContr_T;

/* Block states (default storage) for system '<Root>/Interrupt_8k' */
typedef struct {
  uint32_T temporalCounter_i1;         /* '<S2>/StateMachine8K' */
  uint32_T previousTicks;              /* '<S2>/StateMachine8K' */
  int16_T rotor_position_reference;    /* '<S2>/StateMachine8K' */
  uint16_T MaskFlag;                   /* '<S2>/StateMachine8K' */
  uint8_T is_active_c1_Motor_1pHControlle;/* '<S2>/StateMachine8K' */
  uint8_T is_c1_Motor_1pHController;   /* '<S2>/StateMachine8K' */
  uint8_T is_Motor_On;                 /* '<S2>/StateMachine8K' */
} DW_Interrupt_8k_Motor_1pHCont_T;

/* Block states (default storage) for system '<Root>/Interrupt_1k' */
typedef struct {
  uint8_T is_active_c3_Motor_1pHControlle;/* '<S1>/StateMachine1K' */
  uint8_T is_c3_Motor_1pHController;   /* '<S1>/StateMachine1K' */
  uint8_T is_Motor_On;                 /* '<S1>/StateMachine1K' */
} DW_Interrupt_1k_Motor_1pHCont_T;

/* Block signals (default storage) */
typedef struct {
  ControlModeState controller_mode_m;  /* '<Root>/RT19' */
  ControlModeState controller_mode;    /* '<S1>/StateMachine1K' */
  ControlFlag controller_flag;         /* '<S2>/StateMachine8K' */
  int16_T speed_reference_c;           /* '<Root>/RT3' */
  int16_T voltage_reference_k;         /* '<Root>/RT2' */
  int16_T speed_reference;             /* '<S1>/StateMachine1K' */
  int16_T voltage_reference;           /* '<S1>/StateMachine1K' */
  int16_T current_reference_g;         /* '<S1>/StateMachine1K' */
  B_Interrupt_8k_Motor_1pHContr_T Interrupt_8k;/* '<Root>/Interrupt_8k' */
} B_Motor_1pHController_T;

/* Block states (default storage) for system '<Root>' */
typedef struct {
  ControlFlag RT9_Buffer;              /* '<Root>/RT9' */
  ControlModeState RT19_Buffer0;       /* '<Root>/RT19' */
  int16_T RT3_Buffer0;                 /* '<Root>/RT3' */
  int16_T RT2_Buffer0;                 /* '<Root>/RT2' */
  int16_T RT20_Buffer;                 /* '<Root>/RT20' */
  DW_Interrupt_1k_Motor_1pHCont_T Interrupt_1k;/* '<Root>/Interrupt_1k' */
  DW_Interrupt_8k_Motor_1pHCont_T Interrupt_8k;/* '<Root>/Interrupt_8k' */
} DW_Motor_1pHController_T;

/* Constant parameters (default storage) */
typedef struct {
  /* Expression: Controller.CosFcn.y
   * Referenced by: '<S22>/1-D Lookup Table'
   */
  int16_T uDLookupTable_tableData[256];

  /* Expression: Controller.CosFcn.x
   * Referenced by: '<S22>/1-D Lookup Table'
   */
  int16_T uDLookupTable_bp01Data[256];
} ConstP_Motor_1pHController_T;

/* External inputs (root inport signals with default storage) */
typedef struct {
  boolean_T Direction;                 /* '<Root>/Direction' */
  boolean_T motor_on;                  /* '<Root>/motor_on' */
  int16_T speed_command;               /* '<Root>/speed_command' */
  boolean_T fault_clear;               /* '<Root>/fault_clear' */
  int16_T current_command;             /* '<Root>/current_command' */
  uint16_T align_delay;                /* '<Root>/align_delay' */
  int16_T ramp_command;                /* '<Root>/ramp_command' */
  int16_T boost_command_pos;           /* '<Root>/boost_command_pos' */
  int16_T boost_command_neg;           /* '<Root>/boost_command_neg' */
  boolean_T fault1;                    /* '<Root>/fault1' */
  boolean_T fault2;                    /* '<Root>/fault2 ' */
  int16_T speed_command_vf_pos;        /* '<Root>/speed_command_vf_pos' */
  int16_T speed_command_vf_neg;        /* '<Root>/speed_command_vf_neg' */
  boolean_T start_ctr;                 /* '<Root>/start_ctr' */
  int16_T start_angle_pos;             /* '<Root>/start_angle_pos' */
  int16_T start_angle_neg;             /* '<Root>/start_angle_neg' */
  int16_T vdc_adc;                     /* '<Root>/vdc_adc' */
} ExtU_Motor_1pHController_T;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  int16_T Vs_s;                        /* '<Root>/Vs_s' */
  uint16_T PWM_OFF;                    /* '<Root>/PWM_OFF' */
} ExtY_Motor_1pHController_T;

/* Real-time Model Data Structure */
struct tag_RTM_Motor_1pHController_T {
  const char_T * volatile errorStatus;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    uint32_T clockTick0;
    struct {
      uint8_T TID0_1;
    } RateInteraction;
  } Timing;
};

/* Block signals (default storage) */
extern B_Motor_1pHController_T Motor_1pHController_B;

/* Block states (default storage) */
extern DW_Motor_1pHController_T Motor_1pHController_DW;

/* External inputs (root inport signals with default storage) */
extern ExtU_Motor_1pHController_T Motor_1pHController_U;

/* External outputs (root outports fed by signals with default storage) */
extern ExtY_Motor_1pHController_T Motor_1pHController_Y;

/* Constant parameters (default storage) */
extern const ConstP_Motor_1pHController_T Motor_1pHController_ConstP;

/* Model entry point functions */
extern void Motor_1pHController_initialize(void);
extern void Motor_1pHController_step0(void);
extern void Motor_1pHController_step1(void);
extern void Motor_1pHController_terminate(void);

/* Real-time Model object */
extern RT_MODEL_Motor_1pHController_T *const Motor_1pHController_M;

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<S6>/Data Type Duplicate' : Unused code path elimination
 * Block '<S6>/Data Type Propagation' : Unused code path elimination
 * Block '<S23>/Data Type Duplicate' : Unused code path elimination
 * Block '<S23>/Data Type Propagation' : Unused code path elimination
 * Block '<Root>/Scope' : Unused code path elimination
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'Motor_1pHController'
 * '<S1>'   : 'Motor_1pHController/Interrupt_1k'
 * '<S2>'   : 'Motor_1pHController/Interrupt_8k'
 * '<S3>'   : 'Motor_1pHController/Interrupt_1k/StateMachine1K'
 * '<S4>'   : 'Motor_1pHController/Interrupt_1k/StateMachine1K/alignment_1k'
 * '<S5>'   : 'Motor_1pHController/Interrupt_1k/StateMachine1K/vf_1k'
 * '<S6>'   : 'Motor_1pHController/Interrupt_1k/StateMachine1K/vf_1k/Saturation Dynamic1'
 * '<S7>'   : 'Motor_1pHController/Interrupt_1k/StateMachine1K/vf_1k/Voltage Amplitude  Calculation '
 * '<S8>'   : 'Motor_1pHController/Interrupt_8k/StateMachine8K'
 * '<S9>'   : 'Motor_1pHController/Interrupt_8k/StateMachine8K/SpeedToAngle'
 * '<S10>'  : 'Motor_1pHController/Interrupt_8k/StateMachine8K/VFDone_Flag'
 * '<S11>'  : 'Motor_1pHController/Interrupt_8k/StateMachine8K/VFStart'
 * '<S12>'  : 'Motor_1pHController/Interrupt_8k/StateMachine8K/fault.Protections'
 * '<S13>'  : 'Motor_1pHController/Interrupt_8k/StateMachine8K/fault.fault'
 * '<S14>'  : 'Motor_1pHController/Interrupt_8k/StateMachine8K/modulation'
 * '<S15>'  : 'Motor_1pHController/Interrupt_8k/StateMachine8K/state_variable_init'
 * '<S16>'  : 'Motor_1pHController/Interrupt_8k/StateMachine8K/SpeedToAngle/Angle Calculation SH (int16)'
 * '<S17>'  : 'Motor_1pHController/Interrupt_8k/StateMachine8K/VFDone_Flag/Enumerated_Constant'
 * '<S18>'  : 'Motor_1pHController/Interrupt_8k/StateMachine8K/VFDone_Flag/Enumerated_Constant1'
 * '<S19>'  : 'Motor_1pHController/Interrupt_8k/StateMachine8K/VFDone_Flag/Speed>Speed_Limit'
 * '<S20>'  : 'Motor_1pHController/Interrupt_8k/StateMachine8K/fault.Protections/Enumerated_Constant'
 * '<S21>'  : 'Motor_1pHController/Interrupt_8k/StateMachine8K/fault.Protections/Enumerated_Constant1'
 * '<S22>'  : 'Motor_1pHController/Interrupt_8k/StateMachine8K/modulation/Modulation Controller V3 (int16)1'
 * '<S23>'  : 'Motor_1pHController/Interrupt_8k/StateMachine8K/modulation/Modulation Controller V3 (int16)1/Saturation Dynamic'
 */
#endif                                 /* RTW_HEADER_Motor_1pHController_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
