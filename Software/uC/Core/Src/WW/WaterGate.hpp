/** @file WaterGate.h
 *  @brief contains the Watergate control class
 *
 *  @date 17.10.2019
 *  @author dewernt
 */

#ifndef WW_H_
#define WW_H_

#include <gsl/gsl>
#include <algorithm>

#include "../System.hpp"
#include "FlexStart.hpp"
#include "misc/Taster.hpp"

/**
 * @brief controls the positioning of the watergate
 *
 * for correct operation it needs to be implemented in this way
 *  - navigate to Position must be executed every 10ms
 *  - <WaterGate instance>.m_motor.m_phase.zero_crossing_handler needs to be executed on positive zero crossing of grid
 *  - <WaterGate instance>.m_motor.m_phase.on_ch1_event_handler needs to be executed on timer interrupt of configured
 * on_channel */
template <uint8_t num_of_channels>
class WaterGate {
 public:
  /** nuckle positions */
  enum class Position { safe_1, mitte, unten, oben, safe_2 };
  /** state mashine operation mode */
  enum class State { done, operating };

  struct Error {
    static constexpr uint8_t timeout = 1;
    static constexpr uint8_t position_lost = 2;
    static constexpr uint8_t no_init = 4;
    uint8_t state;
  };

  struct ErrorCounter {
    uint32_t CW = 0;
    uint32_t CCW = 0;
    uint32_t trials = 0;
  };

  WaterGate<num_of_channels>(Taster& switch_ref, FlexStart<num_of_channels>& flex_start_ref)
      : m_init(flex_start_ref, switch_ref),
        m_measure_nuckle(switch_ref),
        m_switch{switch_ref},
        m_motor{flex_start_ref} {
    m_error.state = 0;
  }

  ~WaterGate<num_of_channels>() { m_motor.stop_motor(); }

  /** @brief runtime function for watergate. should be executed every 10 ms
   * @param time_ms absolute time since startup or reset */
  void interrupt_handler_zero_crossing(uint64_t time_ms) {
    if (!m_error.state) {
      // navigate to position
      navigate_to_position(time_ms);

      // timeout handling
      timeout_handling(time_ms);
    }

    // error handling
    if (m_error.state & Error::no_init || m_error.state & Error::position_lost) {
    	m_num_of_errors++;
      // reset error states
      m_error.state = gsl::narrow_cast<uint8_t>(~Error::no_init & m_error.state);
      m_error.state = gsl::narrow_cast<uint8_t>(~Error::position_lost & m_error.state);
      // reset state mashine
      reset(true, time_ms);
    } else if (m_error.state & Error::timeout) {
    	m_num_of_errors++;
      m_motor.stop_motor();
    }

    m_motor.zero_crossing_event_handler();
  }

  /** sets new target position
   * @param position new target position */
  void set_target_position(Position position) {
  	if(static_cast<int>(position) > num_nuckles_on_disk){return;} // ignore errand input
    m_target_position = position;
  }

  State get_state() const { return m_state; }

  bool is_initialized() const {return m_init.get_state() == Init::State::done;}

  uint8_t get_error() const { return m_error.state; }

  const ErrorCounter& get_dir_error() const { return m_dir_err; }

  /**returns current position */
  Position get_position() const { return m_position_now; }

  /** returns the number of errors that occured since runtime */
  uint32_t get_number_of_errors() const {
  	return m_num_of_errors;
  }

  /** resets the state mashine
   * @param hard if true, also init state mashine is reset
   * @param current_system_time_ms system time to reset timeout counter properly */
  void reset(bool hard, uint64_t current_system_time_ms = System::global_time_ms) {
    if (hard) {
      m_init.reset();
    }
    m_error.state = 0;
    m_state_navigation = NavigationState::init;
    m_timeout_timestamp = current_system_time_ms;
  }

  /**
   * resets counter values
   * */
  void reset_com() {
    m_dir_err.CW = 0;
    m_dir_err.CCW = 0;
    m_dir_err.trials = 0;
  }

  /**
   * @brief change alignment parameters in flex start motor control
   * @param pause1 number of periods for first pause
   * @param align_full number of periods for full power alignment
   * @param pause2 number of periods for second pause
   * @param align_decline number of periods for declining alignment pulses
   * @param pause3 number of periods for pause after declining alignment
   * */
  void set_flexstart_parameters(uint8_t pause1, uint8_t align_full, uint8_t pause2, uint8_t align_decline,
                                uint8_t pause3) {
    m_motor.set_parameters(pause1, align_full, pause2, align_decline, pause3);
  }

 private:
  /** nuckle measurement tolerance in ms */
  constexpr static Milliseconds measure_tolerance = 300;
  /** table of nuckle times in milliseconds */
  constexpr static Milliseconds nuckle_times[] = {
      1160,  // safe2
      3420,  // mitte
      4410,  // unten
      370,   // oben
      10420  // safe1
  };
  /** table of dent times in milliseconds */
  constexpr static Milliseconds dent_times[] = {
      840,  // Tal1
      840,  // Tal2
      840,  // Tal3
      840,  // Tal4
      840   // Tal5
  };

  // compile time evaluation of max timeout
  constexpr static Milliseconds timeout_calc(){
  	const Milliseconds max_nuckle_time = *std::max_element(std::begin(nuckle_times), std::end(nuckle_times));
  	const Milliseconds max_dent_time = *std::max_element(std::begin(dent_times), std::end(dent_times));
  	return (max_nuckle_time + max_dent_time + measure_tolerance) * 2;
  }

  /** timeout var in [100ms] that helps to avoid being stuck in initial while loop */
  constexpr static Milliseconds timeout_limit = timeout_calc();

  constexpr static Microseconds phase_timer_period = 20'000;

 public:
  /** amount of nuckles on disk */
  constexpr static uint8_t num_nuckles_on_disk = sizeof(nuckle_times) / sizeof(nuckle_times[0]);

 private:
  enum class NavigationState { init, decide_direction, measure_nuckle, wait_for_middle_position, done };

  /**
   * @brief object that measures a nuckle length in time
   *
   * execute measure_nuckle_time_ms at least as often as you wish your nuckle length tolerance to be. recommended is
   * 10ms
   * @param microswitch switch instance to get states from input pins */
  class MeasureNuckle {
   public:
    enum class State { find_nuckle, measure_nuckle, done };

    MeasureNuckle(Taster& microswitch) : m_switch{microswitch} {}

    Milliseconds get_nuckle_time() const { return gsl::narrow_cast<Milliseconds>(m_time_end - m_time_start); }

    State get_state() const { return m_measure_nuckle_state; }

    void reset() { m_measure_nuckle_state = State::find_nuckle; }

    /** internal method to measure a nuckle time. requires running motor
     * @param time_ms absolute time since reset in milliseconds
     * @return true for done, false for operating */
    bool measure_nuckle_time_ms(int64_t time_ms) {
      switch (m_measure_nuckle_state) {
        case State::find_nuckle:
          // wait for nuckle to come
          if (m_switch.get_event()) {
            // arrived at nuckle
            m_time_start = time_ms;  // take time stamp
            m_measure_nuckle_state = State::measure_nuckle;
          }
          break;
        case State::measure_nuckle:
          // wait for nuckle to end
          if (!m_switch.get_state()) {
            // return nuckle time in ms
            m_measure_nuckle_state = State::done;
            m_time_end = time_ms;
          }
          break;
        case State::done:
          return true;
          break;
      }
      return false;
    }

   private:
    /** microswitch driver instance */
    Taster& m_switch;
    /** state mashine index */
    State m_measure_nuckle_state = State::find_nuckle;
    /** storage variable do measure time delta */
    int64_t m_time_start = 0;
    /** result value */
    int64_t m_time_end = 0;
  };

  class Init {
   public:
    enum class State { init, measure_nuckle_1, measure_nuckle_2, done, error };

    Init(FlexStart<num_of_channels>& motor_reference, Taster& microswitch_ref)
        : m_motor{motor_reference}, m_measure_nuckle(microswitch_ref) {}

    State get_state() const { return m_init_state; }

    WaterGate::Position get_position() const { return m_position; }

    void reset() { m_init_state = State::init; }

    /** @brief finds Position after loss or for initialization
     *
     * 1. Init - start motor
     * 2. wait for dent
     * 3. measure first nuckle time
     * 4. measure second nuckle time
     * 5. calculate current Position
     *
     * @param time_ms absolute time since reset in milliseconds */
    void find_position(uint64_t time_ms) {
      switch (m_init_state) {
        case State::init:
          // start motor
          m_motor.start_motor(FlexStart<num_of_channels>::CW);
          m_init_state = State::measure_nuckle_1;
          __attribute__((fallthrough));  // indicates that fallthough is disired and break is left intentionally

        case State::measure_nuckle_1:
          if (m_measure_nuckle.measure_nuckle_time_ms(time_ms)) {
            m_nuckle1_time = m_measure_nuckle.get_nuckle_time();
            m_measure_nuckle.reset();                // init measurement instance
            m_init_state = State::measure_nuckle_2;  // next step
          }
          break;

        case State::measure_nuckle_2:
          if (m_measure_nuckle.measure_nuckle_time_ms(time_ms)) {
            m_nuckle2_time = m_measure_nuckle.get_nuckle_time();
            m_measure_nuckle.reset();  // init measurement instance
            WaterGate::Position pos_1, pos_2;
            // determine Position
            pos_1 = WaterGate::find_position_in_table(m_nuckle1_time, m_motor.get_grid_period());
            pos_2 = WaterGate::find_position_in_table(m_nuckle2_time, m_motor.get_grid_period());

            // interpret results: if error happened, return error code
            // since motor rotation is not safe, truth table has to respect
            // both directions alike
            m_position = calc_position(pos_1, pos_2);
            if (static_cast<int>(m_position) < 0 ||
                static_cast<int>(m_position) > static_cast<int>(WaterGate::num_nuckles_on_disk)) {
              m_init_state = State::error;
            }
            m_init_state = State::done;
          }
          break;
        case State::done:

          break;

        case State::error:

          break;
      }
    }

   private:
    /** derives the position out of the difference of the two given measured array positions
     *
     * to identify the new position out of measurements and correct overflow errors
     * occuring from disk turning over position 0 or max give your positions to the function
     *
     * @param first_position position that was measured first
     * @param second_position position that was measured second
     * @param pointer to a direction indicator variable. this one is optional
     * @return current position */
    WaterGate::Position calc_position(WaterGate::Position first_position, WaterGate::Position second_position) {
      const int pos_1_int = static_cast<int>(first_position);
      const int pos_2_int = static_cast<int>(second_position);

      // motor turned CCW
      if ((pos_1_int + 1 == pos_2_int) || ((pos_1_int == num_nuckles_on_disk - 1) && (pos_2_int == 0))) {
        m_direction = FlexStart<num_of_channels>::CCW;
        return static_cast<WaterGate::Position>((pos_2_int + 1) % (num_nuckles_on_disk));
      }
      // motor turned CW
      else if ((pos_1_int == pos_2_int + 1) || ((pos_1_int == 0) && (pos_2_int == num_nuckles_on_disk - 1))) {
        m_direction = FlexStart<num_of_channels>::CW;
        return static_cast<WaterGate::Position>(pos_2_int);
      } else {
        return static_cast<WaterGate::Position>(-1);  // return error code
      }
    }

    /** motor control instance reference */
    FlexStart<num_of_channels>& m_motor;
    /** measure nuckle instance */
    MeasureNuckle m_measure_nuckle;
    /** state machine status */
    State m_init_state;
    /** nuckle time of first nuckle */
    uint32_t m_nuckle1_time;
    /** nuckle time of second nuckle */
    uint32_t m_nuckle2_time;
    /** resulted position */
    WaterGate::Position m_position = Position::safe_1;
    /** direction indicator */
    typename FlexStart<num_of_channels>::Direction m_direction = FlexStart<num_of_channels>::STOP;
  };

  /** current state of operation */
  State m_state = State::operating;
  /** error state */
  Error m_error;

  /** takes a measured nuckle time in ms and finds the corresponding value in the nuckle time table
   * @param nuckle_time nuckle time in ms
   * @param grid_period grid period time in us (50Hz -> 20000)
   * @return Position in table or -1 for error */
  static Position find_position_in_table(Milliseconds nuckle_time, Microseconds grid_period) {
    int32_t diff;

    for (int8_t n = 0; n < num_nuckles_on_disk; n++) {
      // determine Position. table values were measured on 50Hz (20ms). If other grid freqency is applied,
      // parameters have to be adapted
      diff = std::abs(((nuckle_time * phase_timer_period / grid_period) - nuckle_times[n]));
      if (diff < measure_tolerance) {
        return static_cast<Position>(n);
      }  // return Position
    }
    // no position found
    return static_cast<WaterGate::Position>(-1);
  }

  /** @brief routes the water gate to designated Position
   *
   * 1. decide direction if new target is placed
   * 2. wait for nuckle
   * 3. meausre nuckle time
   * 4. compare nuckle time with table value and determine Position
   * 5. identify turning direction and errors
   * 6. redo process until Position equails target Position
   *
   * @param time_ms time in milliseconds since reset */
  void navigate_to_position(int64_t time_ms) {
    switch (m_state_navigation) {
      case NavigationState::init:
        m_state = State::operating;
        // check if initialized. either move on or initialize first
        if (m_init.get_state() == Init::State::done) {
          // already initialized
          m_state_navigation = NavigationState::decide_direction;
          m_position_old = m_position_now;
        } else {
          // yet to be initialized
          m_init.find_position(time_ms);  // determine position
          if (m_init.get_state() == Init::State::done) {
            // if results are ready copy position
            m_position_now = m_init.get_position();
          } else if (m_init.get_state() == Init::State::error) {
            m_error.state |= Error::no_init;
          }
        }
        break;

      case NavigationState::decide_direction:
        // check on reference == current position
        if (m_position_now != m_target_position) {
          uint32_t time_cw = 0,
                   time_ccw = 0;  // store time in ms for turn of wheel to designated point in cw or ccw direction

          // calc time to reach new Position
          uint8_t i = static_cast<uint8_t>(m_position_now);
          while (i != static_cast<uint8_t>(m_target_position)) {
            time_ccw += WaterGate::nuckle_times[i] + WaterGate::dent_times[i];
            i++;
            i %= (WaterGate::num_nuckles_on_disk);  // modulo
          }
          // calc sum of wheel
          uint32_t sum = 0;
          for (uint16_t i = 0; i < WaterGate::num_nuckles_on_disk; i++) {
            sum += WaterGate::nuckle_times[i] + WaterGate::dent_times[i];
          }
          time_cw = sum - time_ccw;  // calc CW time

          // decide faster direction
          // check if microswitch is in right state
          if (m_switch.get_state() == true) {
            // switch not in dent --> initialization lost
            m_error.state |= Error::no_init;
            return;
          } else {
            m_motor.start_motor(time_ccw <= time_cw ? FlexStart<num_of_channels>::CCW : FlexStart<num_of_channels>::CW);
            m_state_navigation = NavigationState::measure_nuckle;
          }
        } else {
          // target and position is equal
          m_state_navigation = NavigationState::done;  // do nothing
        }
        break;

      case NavigationState::measure_nuckle:
        // measure nuckle time
        if (m_measure_nuckle.measure_nuckle_time_ms(time_ms)) {
          // determine Position and direction
          typename FlexStart<num_of_channels>::Direction dir;  // direction variable
          WaterGate<num_of_channels>::Position pos = find_position_in_table(m_measure_nuckle.get_nuckle_time(), m_motor.get_grid_period());
          m_position_now = derive_position(pos, dir);
          // check direction errors
          if ((m_motor.get_motor_direction() == FlexStart<num_of_channels>::CCW) &&
              (dir == FlexStart<num_of_channels>::CW)) {
            // turned left instead of right
            m_dir_err.CW++;
          } else if ((m_motor.get_motor_direction() == FlexStart<num_of_channels>::CW) &&
                     (dir == FlexStart<num_of_channels>::CCW)) {
            // turned right instead of left error
            m_dir_err.CCW++;
          }

          m_position_old = m_position_now;  // update old position

          // start next wait process
          m_state_navigation = NavigationState::wait_for_middle_position;
          // init dent timer with current time
          m_dent_timer = time_ms;
          // reset nuckle measurement
          m_measure_nuckle.reset();
        }
        break;

      case NavigationState::wait_for_middle_position: {
        // check for middle of dent
        uint8_t pos_index = static_cast<uint8_t>(m_position_now);
        if (time_ms - m_dent_timer >= (WaterGate::dent_times[pos_index] / 2)) {
          // middle of dent reached
          m_state_navigation =
              m_position_now != m_target_position ? NavigationState::decide_direction : NavigationState::done;
          // indicate success
          if (m_state_navigation == NavigationState::done) {
            m_dir_err.trials++;
          }
        }
        break;
      }

      case NavigationState::done:
      	m_state = State::done; // indicate that navigation is through
        m_motor.stop_motor();
        if(m_target_position != m_position_now){
        	m_state = State::operating;
        	m_state_navigation = NavigationState::decide_direction;
          m_position_old = m_position_now;
        }
        break;
      default:
        reset(true, time_ms);
        break;
    }
  }

  /** initialization class */
  Init m_init;
  /** measure nuckle instance */
  MeasureNuckle m_measure_nuckle;
  /** micro switch instance */
  Taster& m_switch;
  /** motor instance */
  FlexStart<num_of_channels>& m_motor;
  /** error class */

  /** navigation state index */
  NavigationState m_state_navigation = NavigationState::init;
  /** old position indicator */
  Position m_position_old = Position::safe_1;
  /** set point Position */
  Position m_target_position = Position::safe_1;
  /** current Position */
  Position m_position_now = Position::safe_1;
  /** timer value to measure time until switch positioned in center of dent */
  int64_t m_dent_timer = 0;

  /** direction error counter */
  ErrorCounter m_dir_err;
  /** timeout counter */
  uint64_t m_timeout_timestamp = 0;
  // number of errors occured since runtime
  uint32_t m_num_of_errors = 0;

  /** derives the position out of the difference of the two given measured array positions
   *
   * to identify the new position out of measurements and correct overflow errors
   * occuring from disk turning over position 0 or max give your positions to the function
   *
   * @param first_position position that was measured first
   * @param second_position position that was measured second
   * @param pointer to a direction indicator variable. this one is optional
   * @return current position */
  Position derive_position(Position new_position, typename FlexStart<num_of_channels>::Direction& direction_ref) {
    const int new_position_int = static_cast<int>(new_position);
    const int old_position_int = static_cast<int>(m_position_old);

    // check input
    if (new_position_int < 0 || new_position_int >= num_nuckles_on_disk) {
      m_error.state |= Error::position_lost;
      return static_cast<WaterGate::Position>(-1);
    }
    // motor turned CCW
    if (old_position_int == new_position_int) {
      direction_ref = FlexStart<num_of_channels>::CCW;
      return static_cast<Position>((new_position_int + 1) % (num_nuckles_on_disk));
    }
    // motor turned CW
    else if ((old_position_int == new_position_int + 1) ||
             ((old_position_int == 0) && (new_position_int == num_nuckles_on_disk - 1))) {
      direction_ref = FlexStart<num_of_channels>::CW;
      return static_cast<Position>(new_position_int);
    } else {
      m_error.state |= Error::position_lost;
      return static_cast<WaterGate::Position>(-1);
    }
  }

  void timeout_handling(uint64_t timestamp_ms) {
    // do timeout handling
    if (m_state != State::done) {
      if (timestamp_ms - m_timeout_timestamp >= timeout_limit) {
        m_error.state |= Error::timeout;
      }
    } else {
      // we're done. copy system time to reset timeout counter
      m_timeout_timestamp = timestamp_ms;
    }
  }
};

template <uint8_t num_of_channels>
const Milliseconds WaterGate<num_of_channels>::nuckle_times[];
template <uint8_t num_of_channels>
const Milliseconds WaterGate<num_of_channels>::dent_times[];
template <uint8_t num_of_channels>
const Microseconds WaterGate<num_of_channels>::phase_timer_period;

#endif /* WW_H_ */
