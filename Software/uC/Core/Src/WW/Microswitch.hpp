/** @file Microswitch.h
 *
 *  @date 17.10.2019
 *  @author dewernt
 */

#ifndef WW_MICROSWITCH_H_
#define WW_MICROSWITCH_H_

#include "Gpio.h"
#include "hw_cfg.h"

/** defines a switch. given is the port identification and the characteristic
 *
 * @param input_pin peripheral pin definition
 * @param init_state defines the default state of the button. false for low, true for high
 */
class Microswitch {
 public:
  Microswitch(Pin input_pin, bool init_state)
      : m_gpio{GpioPin(input_pin.port, input_pin.pin, Gpio::mode_Input, Gpio::outtype_OpenDrain, Gpio::speed_HighSpeed,
                       Gpio::pull_PullDown, input_pin.altfun)},
        m_state{init_state},
        m_low_state{init_state} {}

  ~Microswitch() {}

  bool get_state(void) { return m_gpio.get_state() ^ m_low_state; }

  bool get_event(void) {
    if (get_state()) {
      if (m_state == (true ^ m_low_state)) {
        m_state = (false ^ m_low_state);
        return true;
      }
    } else {
      m_state = (true ^ m_low_state);
    }
    return false;
  }

 private:
  GpioPin m_gpio;
  bool m_state;
  const bool m_low_state;
};

#endif /* WW_MICROSWITCH_H_ */
