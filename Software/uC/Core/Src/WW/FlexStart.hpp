/**
 * @file FlexStart.h
 * @brief contains flex start control for synchronous micro drives
 * @author Timo Werner
 * @date 30.7.20
 * @copyright Miele&Cie KG 2020
 */

#ifndef MOT_H_
#define MOT_H_

#include "misc/PhaseCut.hpp"
#include "misc/TimeMeas.hpp"

// nameless namespace to avoid pollution of global namespace with internally used alignment class
namespace {
template <uint8_t num_of_channels>
class Alignment {
 public:
  typedef enum { pause_1, align_1, pause_2, align_2, pause_3, done } AlignState;

  Alignment<num_of_channels>(PhaseCut<num_of_channels>& phase_cut_ref, uint8_t channel)
      : m_aligned{false},
        m_phase{phase_cut_ref},
        m_state{pause_1},
        m_channel{channel} {

        };

  /**
   * @brief change alignment paramters
   * @param pause1 number of periods for first pause
   * @param align_full number of periods for full power alignment
   * @param pause2 number of periods for second pause
   * @param align_decline number of periods for declining alignment pulses
   * @param pause3 number of periods for pause after declining alignment */
  void set_parameters(uint8_t pause1, uint8_t align_full, uint8_t pause2, uint8_t align_decline, uint8_t pause3) {
    m_pause1 = static_cast<uint8_t>(pause1 * 2);
    m_pause2 = static_cast<uint8_t>(pause2 * 2);
    m_pause3 = static_cast<uint8_t>(pause3 * 2);
    m_full_pulse = static_cast<uint8_t>(align_full * 2);
    m_cutted_pulse = static_cast<uint8_t>(align_decline * 2);
  }

  /**
   * @brief alignes the motor with dc current
   * @return 1 for success ; 0 for operation */
  bool align_motor() {
    switch (m_state) {
      case pause_1:
        if (align_pause(m_period_count, m_pause1)) {
          m_state = align_1;
          m_period_count = 0;
        }
        break;
      case align_1:
        if (align_pulse(m_period_count, m_full_pulse, 1_deg, 1_deg)) {
          m_state = pause_2;
          m_period_count = 0;  // reset number for next step
        }
        break;
      case pause_2:
        if (align_pause(m_period_count, m_pause2)) {
          m_state = align_2;
          m_period_count = 0;
        }
        break;
      case align_2:
        if (align_pulse(m_period_count, m_cutted_pulse, 1_deg, 170_deg)) {
          m_state = pause_3;
          m_period_count = 0;  // reset number for next step
        }
        break;
      case pause_3:
        if (align_pause(m_period_count, m_pause3)) {
          m_state = done;
          m_aligned = true;
          m_period_count = 0;  // reset period count value
        }
        break;
      case done:

        return true;
      default:
        break;
    }

    m_period_count++;  // increment half wave counter
    return false;
  }

  /** reset alignment state mashine */
  void reset() {
    m_aligned = false;
    m_state = pause_1;
  }

  bool get_alignment_state() const { return m_aligned; }

 private:
  /** default parameters for alignment */
  constexpr static uint8_t num_of_hw_pause1_default = 16;
  constexpr static uint8_t num_of_hw_pause2_default = 16;
  constexpr static uint8_t num_of_hw_pause3_default = 16;
  constexpr static uint8_t num_of_hw_align1_default = 18;
  constexpr static uint8_t num_of_hw_align2_default = 48;

  /** indicator of alignment state */
  bool m_aligned;
  /** number of pause periods */
  uint8_t m_pause1 = num_of_hw_pause1_default;
  /** number of pause 2 periods */
  uint8_t m_pause2 = num_of_hw_pause2_default;
  /** number of pause 3 periods */
  uint8_t m_pause3 = num_of_hw_pause3_default;

  /** number of full pulses in alignment */
  uint8_t m_full_pulse = num_of_hw_align1_default;
  /** number of cutted pulses in alignment */
  uint8_t m_cutted_pulse = num_of_hw_align2_default;
  /** instance of phase cut object */
  PhaseCut<num_of_channels>& m_phase;
  /** indicates state mashine status */
  AlignState m_state;

  /** period counter for alignment */
  uint8_t m_period_count = 0;

  /** number of motor that is operated */
  uint8_t m_channel;

  /**@brief starts pulse output to Alignment motor
   @param number_of_period number of interrupt that happened
   @param pulse_amount amount of zero crossing interrupts necessary to pulse
   @param angle_start angle of first pulse
   @param angle_end angle of last pulse
   @return true for reached number, false for nothing	 */
  bool align_pulse(uint8_t number_of_period, uint8_t pulse_amount, Angle angle_start, Angle angle_end) {
    // avoid divide by 0 error
    if (pulse_amount == 0) {
      return true;  // consider done if amount is 0
    }

    if (m_phase.get_half_wave_counter() & 1) {
      // positive wave --> align
      // linear adaption of angle
      int32_t angle_delta = std::abs(static_cast<int32_t>(angle_end) - static_cast<int32_t>(angle_start));

      Angle angle = static_cast<Angle>(angle_start + angle_delta * number_of_period / pulse_amount);
      // fire on positive half wave, ignore negative
      m_phase.set_angle(m_channel, angle);
      m_phase.enable_output(true, m_channel);
    } else {
    	//negative half wave -> no output
      m_phase.enable_output(false, m_channel);
    }
    return number_of_period >= pulse_amount;  // return success if all pulses done
  }

  /**@brief returns true if pause is over
   @param number_of_nd_u8 number of interrupt that happened
   @param pause_length_u8 amount of zero crossing interrupts necessary to pause
   @return true for reached number, false for nothing */
  bool align_pause(uint8_t number_of_period, uint8_t pause_length) {
    m_phase.enable_output(false, m_channel);  // deactivate
    return pause_length <= number_of_period;  // return 1 if done counting
  }
};
}

/**
 * control class for driving one motor. selection of channel on peripherals is done by channel argument */
template <uint8_t num_of_channels>
class FlexStart {
 public:
  typedef enum { align, stop, startup, run } MotorState;

  typedef enum { STOP, CW, CCW } Direction;

  /**
   * @param phase_cut_ref reference of phase cut module
   * @param channel specifies on which channel it is operating */
  FlexStart<num_of_channels>(PhaseCut<num_of_channels>& phase_cut_ref, uint8_t channel)
      : m_phase{phase_cut_ref},
        m_align(phase_cut_ref, channel),
        m_motor_state{align},
        m_motor_state_ref{STOP},
        m_cnt_start_cw{0},
        m_cnt_start_ccw{0},
        m_channel{channel} {}

  /** getter for count variable on cw starts */
  uint16_t get_count_start_cw() const { return m_cnt_start_cw; }
  /** returns number of ccw starts */
  uint16_t get_count_start_ccw() const { return m_cnt_start_ccw; }
  /** return current direction */
  Direction get_motor_direction() const { return m_motor_state_ref; }
  /** resets number of ccw starts */
  void reset_count_start() {
    m_cnt_start_ccw = 0;
    m_cnt_start_cw = 0;
  }
  /** returns current motor state */
  MotorState get_motor_state() const { return m_motor_state; }

  /** stop routine for motor */
  void stop_motor() {
    m_motor_state_ref = STOP;  // indicate stop
    if (m_motor_state != stop) {
      m_motor_state = align;
    }
  }

  /**@brief Starts motor in indicated direction
   @param direction specifies the direction that should be started */
  void start_motor(Direction direction) {
    // check if status changed
    if (direction != m_motor_state_ref) {
      m_motor_state_ref = direction;
      m_motor_state = align;
    }
  }

  /** return grid period in microseconds */
  Microseconds get_grid_period() const { return m_phase.get_grid_period(); }

  /** zero crossing handler */
  void zero_crossing_event_handler() {
    // control is only executed on full period: here only on uneven counts
    control_motor();  // go to next state
  }

  /**
   * @brief change alignment paramters
   * @param pause1 number of periods for first pause
   * @param align_full number of periods for full power alignment
   * @param pause2 number of periods for second pause
   * @param align_decline number of periods for declining alignment pulses
   * @param pause3 number of periods for pause after declining alignment
   * */
  void set_parameters(uint8_t pause1, uint8_t align_full, uint8_t pause2, uint8_t align_decline, uint8_t pause3) {
    m_align.set_parameters(pause1, align_full, pause2, align_decline, pause3);
  }

 private:
  static constexpr Angle angle_cw = 90_deg;  // startup on negative wave
  static constexpr Angle angle_ccw = 0_deg;  // startup on negative wave

  /** phase control instance */
  PhaseCut<num_of_channels>& m_phase;
  /** alignment instance */
  Alignment<num_of_channels> m_align;

  /** motor status */
  MotorState m_motor_state;
  /** motor status to be*/
  Direction m_motor_state_ref;
  /**Counter: Start CW*/
  uint16_t m_cnt_start_cw = 0;
  /**Counter: Start CCW*/
  uint16_t m_cnt_start_ccw = 0;
  /** indicates the motor to be operated on*/
  uint8_t m_channel;

  /** changes motor states according to reference
   *
   * according to Reference direction, motor is aligned after stop, or adjusted to turn in CCW or CW mode*/
  void control_motor() {
    switch (m_motor_state) {
      case align:
        if (m_align.get_alignment_state() == true) {
          if (m_motor_state_ref == STOP) {
            m_motor_state = stop;
          } else {
            m_motor_state = startup;
          }
        } else {
          m_align.align_motor();
        }
        break;
      case stop:
        m_phase.enable_output(false, m_channel);
        // go to alignment if not aligned
        if (m_align.get_alignment_state() == false) {
          m_motor_state = align;
        }
        // go to startup if movement is desired
        if (m_motor_state_ref == CCW || m_motor_state_ref == CW) {
          m_motor_state = startup;
        }
        break;
      case startup:
        // check half wave orientation
        if (m_phase.get_half_wave_counter() & 1) {
          // positive wave -> do nothing
          m_phase.enable_output(false, m_channel);
        } else {
          // config channel for rising edge depending on direction
          if (m_motor_state_ref == CCW) {
            m_cnt_start_ccw++;
            m_phase.set_angle(m_channel, angle_ccw);
            m_phase.enable_output(true, m_channel);
          } else {
            m_cnt_start_cw++;
            m_phase.set_angle(m_channel, angle_cw);
            m_phase.enable_output(true, m_channel);
          }

          m_align.reset();      // reset alignment
          m_motor_state = run;  // go to next state
        }
        break;
      case run:
        m_phase.set_angle(m_channel, 0);  // run motor
        m_phase.enable_output(true, m_channel);
        break;
    }
  }
};

#endif /* MOT_H_ */
