/** @file Com.h
 *  @brief com high level class for i2c communication
 *
 *  @date 17.10.2019
 *  @author dewernt
 */

#ifndef WW_COM_H_
#define WW_COM_H_

#include <array>

#include "WaterGate.hpp"
#include "hw/hw_cfg.h"
#include "peripherals/Gpio.hpp"
#include "peripherals/I2C.hpp"

template <uint8_t number_of_channels>
class Com {
 public:
  typedef uint32_t Milliseconds;

  typedef enum : uint8_t { reset, set_position, get_status, get_data, get_position, set_flexstart_parameters } OPCode;

  enum IN_Frame : uint8_t { IN_ww_id, IN_opcode, IN_data };

  enum OUT_Frame : uint8_t { OUT_ww_id, OUT_opcode, OUT_status, OUT_data };

  enum OPStatus : uint8_t { ok = 0xAC, error = 0xFF };

  enum OPData : uint8_t { trials_high, trials_low, err_CW_high, err_CW_low, err_CCW_high, err_CCW_low };

  /** maximum length for incoming data*/
  constexpr static uint8_t message_length_rx_max = 10;
  /** maximum length for output data */
  constexpr static uint8_t message_length_tx_max = 10;
  /** slave address */
  constexpr static uint8_t slave_address = 0x50;
  /** config struct for i2c driver */
  constexpr static I2C::Config i2c_cfg{
      true, 5, false, 8'000'000, I2C::speed_standart_100kHz, 32768, I2C::add_7Bit, slave_address, 0, false, 0, false};

  /**
   * @param i2c_peri_address address of the peripheral in the uC
   * @param Gpio indicators
   * @param watergate instances to control by com */
  Com<number_of_channels>(I2C_TypeDef* const i2c_peri_address, std::array<GpioPin::Pin, 2> pins,
                          std::array<WaterGate<number_of_channels>, number_of_channels>& watergate_reference,
                          Milliseconds timeout = 2000)
      : m_i2c{i2c_peri_address, i2c_cfg},
        m_watergate{watergate_reference},
        m_pins{GpioPin(pins[0], pin_cfg), GpioPin(pins[1], pin_cfg)},
        m_timeout{timeout} {
    m_i2c.enable_interrupt(true, I2C::int_rx_not_empty);
  }

  ~Com<number_of_channels>() { m_i2c.enable_interrupt(false, I2C::int_rx_not_empty); }

  void i2c_interrupt_handler(uint64_t time) {
    // copy timestamp
    timestamp = time;

    if (m_i2c.get_status(I2C::status_address_matched)) {
      m_i2c.clear_interrupt_flag(I2C::flag_address_matched);
    }
    if (m_i2c.get_status(I2C::status_rx_full)) {
      // byte received
      rx_handler();
    } else if (m_i2c.get_status(I2C::status_tx_need_new_data)) {
      // send next byte
      send();
    }
  }

  void reset_handler(uint64_t time) {
    if (time - timestamp > 2000) {
      // most likely no more transfers
      tx_reset();
      rx_reset();
    }
  }

 private:
  /** hardware i2c instance */
  I2C m_i2c;
  /** rx message length counter */
  uint8_t m_rx_message_length = 0;
  /** tx message length counter */
  uint8_t m_tx_message_length = 0;
  /** tx buffer index pointing to the next data to be sent */
  uint8_t m_tx_buffer_index = 0;
  /** rx buffer */
  std::array<uint8_t, message_length_rx_max> m_rx_buffer;
  /** tx buffer */
  std::array<uint8_t, message_length_tx_max> m_tx_buffer;
  /** watergate references */
  std::array<WaterGate<number_of_channels>, number_of_channels>& m_watergate;
  /** hardware pins */
  const std::array<GpioPin, 2> m_pins;
  /** reset timestamp */
  uint64_t timestamp = 0;
  /** timeout in [ms] */
  Milliseconds m_timeout = 2000;

  static constexpr GpioPin::Config pin_cfg = {GpioPin::Mode::Alternate, GpioPin::OutputType::OpenDrain,
                                              GpioPin::Speed::High, GpioPin::Pull::Up};

  void rx_handler() {
    tx_reset();
    // store content (read clears rx interrupt flag)
    m_rx_buffer.at(m_rx_message_length) = m_i2c.read();
    // new length of rx buffer
    m_rx_message_length++;

    // check if opcode already was sent
    if (m_rx_message_length > IN_opcode) {
      op_code_handler(static_cast<OPCode>(m_rx_buffer[IN_opcode]));
    }
  }

  void op_code_handler(OPCode op) {
    switch (op) {
      case reset:
        ww_reset();
        break;

      case set_position:
        ww_set_position();
        break;

      case get_status:
        ww_get_status();
        break;

      case get_data:
        ww_get_count_data();
        break;

      case get_position:
        ww_get_position();
        break;

      case set_flexstart_parameters:
        ww_set_flexstart_parameters();
        break;

      default:
        rx_reset();
        break;
    }
  }

  void ww_reset() {
    // check if data is already available
    if (m_rx_message_length > IN_data) {
      uint8_t is_hard_reset = m_rx_buffer.at(IN_data);
      // interpret sanitized input
      m_watergate[m_rx_buffer[IN_ww_id]].reset_com();
      if (is_hard_reset) {
        // reset watergate completely
        m_watergate[m_rx_buffer[IN_ww_id]].reset(true, timestamp);
      }

      respond(true, 0);
    }
  }

  void ww_set_position() {
    // check if data is already available
    if (m_rx_message_length > IN_data) {
      typename WaterGate<number_of_channels>::Position pos =
          static_cast<typename WaterGate<number_of_channels>::Position>(m_rx_buffer.at(IN_data));
      if (static_cast<int>(pos) < static_cast<int>(WaterGate<number_of_channels>::num_nuckles_on_disk)) {
        respond(true, 0);
        m_watergate[m_rx_buffer[IN_ww_id]].set_target_position(pos);
      } else {
        // error
        respond(false, 0);
      }
    }
  }

  void ww_get_status() {
    m_tx_buffer[OUT_data] = static_cast<uint8_t>(m_watergate[m_rx_buffer[IN_ww_id]].get_state());
    m_tx_buffer[OUT_data + 1] = m_watergate[m_rx_buffer[IN_ww_id]].get_error();

    respond(true, 2);
  }

  void ww_get_count_data() {
    m_tx_buffer[OUT_data + trials_high] =
        static_cast<uint8_t>(m_watergate[m_rx_buffer[IN_ww_id]].get_dir_error().trials >> 8);
    m_tx_buffer[OUT_data + trials_low] =
        static_cast<uint8_t>(m_watergate[m_rx_buffer[IN_ww_id]].get_dir_error().trials & 0xFF);
    m_tx_buffer[OUT_data + err_CW_high] =
        static_cast<uint8_t>(m_watergate[m_rx_buffer[IN_ww_id]].get_dir_error().CW >> 8);
    m_tx_buffer[OUT_data + err_CW_high] =
        static_cast<uint8_t>(m_watergate[m_rx_buffer[IN_ww_id]].get_dir_error().CW & 0xFF);
    m_tx_buffer[OUT_data + err_CCW_high] =
        static_cast<uint8_t>(m_watergate[m_rx_buffer[IN_ww_id]].get_dir_error().CCW >> 8);
    m_tx_buffer[OUT_data + err_CCW_high] =
        static_cast<uint8_t>(m_watergate[m_rx_buffer[IN_ww_id]].get_dir_error().CCW & 0xFF);

    respond(true, 6);
  }

  void ww_get_position() {
    m_tx_buffer[OUT_data] = static_cast<uint8_t>(m_watergate[m_rx_buffer[IN_ww_id]].get_position());

    respond(true, 1);
  }

  void ww_set_flexstart_parameters() {
    if (m_rx_message_length > IN_data + 4) {
      // extract data from message
      uint8_t pause1 = m_rx_buffer[IN_data];
      uint8_t align_full = m_rx_buffer[IN_data + 1];
      uint8_t pause2 = m_rx_buffer[IN_data + 2];
      uint8_t align_decline = m_rx_buffer[IN_data + 3];
      uint8_t pause3 = m_rx_buffer[IN_data + 4];

      // set data in watergate
      m_watergate[m_rx_buffer[IN_ww_id]].set_flexstart_parameters(pause1, align_full, pause2, align_decline, pause3);

      respond(true, 0);
    }
  }

  void rx_reset() {
    m_rx_message_length = 0;  // reset length indicator
  }

  void tx_reset() {
    m_tx_message_length = 0;
    m_tx_buffer_index = 0;
    m_i2c.enable_interrupt(false, I2C::int_tx_empty);
  }

  void respond(bool is_ok, uint8_t data_length) {
    m_tx_buffer[OUT_ww_id] = m_rx_buffer[IN_ww_id];
    m_tx_buffer[OUT_opcode] = m_rx_buffer[IN_opcode];
    m_tx_buffer[OUT_status] = is_ok ? ok : error;

    tx_reset();
    m_tx_message_length = static_cast<uint8_t>(data_length + 3);
    rx_reset();

    m_i2c.enable_interrupt(true, I2C::int_tx_empty);
    send();  // send answer
  }

  void send() {
    if (m_tx_buffer_index < m_tx_message_length) {
      m_i2c.write(m_tx_buffer[m_tx_buffer_index]);
      m_tx_buffer_index++;
    } else {
      tx_reset();
    }
  }
};

template <uint8_t number_of_channels>
const I2C::Config Com<number_of_channels>::i2c_cfg;

template <uint8_t number_of_channels>
constexpr GpioPin::Config Com<number_of_channels>::pin_cfg;

#endif /* WW_COM_H_ */
