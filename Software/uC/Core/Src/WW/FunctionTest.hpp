/*
 * FunctionTest.h
 *
 *  Created on: 13.11.2019
 *      Author: dewernt
 */

#ifndef WW_FUNCTIONTEST_H_
#define WW_FUNCTIONTEST_H_

#include "WaterGate.hpp"
#include "hw/hw_cfg.h"
#include "peripherals/Gpio.hpp"

template <uint8_t num_of_channels>
class FunctionTest {
 public:
  enum class State { init, turning, done, error };

  /**@param gate reference to gate control object
   * @param goto_final_position activate final position setting after full test turn
   * @param final_position sets final position after full turn is done */
  FunctionTest<num_of_channels>(
      WaterGate<num_of_channels>& gate, bool goto_final_position = false,
      typename WaterGate<num_of_channels>::Position final_position = WaterGate<num_of_channels>::Position::unten)
      : m_watergate{gate},
        m_active_final_position{goto_final_position},
        m_final_position{final_position},
        leds{GpioPin(led_pins[0], led_pin_cfg), GpioPin(led_pins[1], led_pin_cfg)} {
    // disable all leds in the beginning
    for (auto& led : leds) {
      led.set_state(true);
    }
  }

  /** put this handler in a 10ms routine*/
  void on_10ms_handler() { state_machine(); }

 private:
  State m_state = State::init;
  typename WaterGate<num_of_channels>::Position m_init_position;
  uint8_t m_position_iteration = 0;
  WaterGate<num_of_channels>& m_watergate;
  bool m_active_final_position;
  typename WaterGate<num_of_channels>::Position m_final_position;

  using Position = typename WaterGate<num_of_channels>::Position;

  enum class Leds { red, green };

  static constexpr std::array<GpioPin::Pin, 2> led_pins = {
      {{GPIOB, 10, GpioPin::AlternateFunction::AF0}, {GPIOB, 11, GpioPin::AlternateFunction::AF0}}};
  static constexpr GpioPin::Config led_pin_cfg = {GpioPin::Mode::Output, GpioPin::OutputType::OpenDrain,
                                                  GpioPin::Speed::Low, GpioPin::Pull::None};
  std::array<GpioPin, 2> leds;

  bool check_error() const {
    return m_watergate.get_number_of_errors() > 0 || m_watergate.get_dir_error().CCW > 0 || m_watergate.get_dir_error().CW > 0;
  }
  /**
   * @brief moves through all positions from watergate to test */
  void state_machine() {
    switch (m_state) {
      // find initial position
      case State::init:
        // wait for initialization to pass
        if (m_watergate.is_initialized()) {
          // activate both LEDs on turning operation
          leds[static_cast<int>(Leds::green)].set_state(false);
          leds[static_cast<int>(Leds::red)].set_state(false);

          // init done. next step
          m_state = State::turning;
          // safe initial position
          m_init_position = m_watergate.get_position();
          // select next knuckle as target position. make sure to prevent overflow.
          Position new_pos = static_cast<Position>((static_cast<int>(m_init_position) + 1) %
                                                   WaterGate<num_of_channels>::num_nuckles_on_disk);
          m_watergate.set_target_position(new_pos);
        }

        // check for errors
        if (check_error()) {
          m_state = State::error;
        }
        break;

      // iterate through positions
      case State::turning:
        // check if error occured or wrong direction was driven
        if (check_error()) {
          m_state = State::error;
        }

        // check if next knuckle is reached
        if (m_watergate.get_state() == WaterGate<num_of_channels>::State::done) {
          // check if we're all around
          if (m_watergate.get_position() == m_init_position) {
            // we're through
            m_state = State::done;
            break;
          } else {
            // select next knuckle as target position. make sure to prevent overflow.
            Position new_pos = static_cast<Position>((static_cast<int>(m_watergate.get_position()) + 1) %
                                                     WaterGate<num_of_channels>::num_nuckles_on_disk);
            m_watergate.set_target_position(new_pos);
          }
        }
        break;

      // if desired, drive to final position
      case State::done:
        // green led indicates OK
        leds[static_cast<int>(Leds::red)].set_state(true);
        leds[static_cast<int>(Leds::green)].set_state(false);
        if (m_active_final_position) {
          m_watergate.set_target_position(m_final_position);
        }
        break;

      case State::error:
        // red led indicates error
        leds[static_cast<int>(Leds::red)].set_state(false);
        leds[static_cast<int>(Leds::green)].set_state(true);
        break;
    }
  }
};

template <uint8_t num_of_channels>
constexpr GpioPin::Config FunctionTest<num_of_channels>::led_pin_cfg;
template <uint8_t num_of_channels>
constexpr std::array<GpioPin::Pin, 2> FunctionTest<num_of_channels>::led_pins;

#endif /* WW_FUNCTIONTEST_H_ */
