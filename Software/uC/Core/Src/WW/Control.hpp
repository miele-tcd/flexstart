/** @file Control.h
 *  @brief control wrapper module for watergates
 *
 *  @date 31.8.2020
 *  @author dewernt
 *  @copyright Miele&Cie KG
 */

#ifndef WW_CONTROL_H_
#define WW_CONTROL_H_

#include <array>

#include "Com.hpp"
#include "FlexStart.hpp"
#include "FunctionTest.hpp"
#include "Watergate.hpp"
#include "misc/MovingAverage.hpp"
#include "misc/PhaseCut.hpp"
#include "misc/Taster.hpp"
#include "misc/TimeMeas.hpp"
#include "peripherals/Gpio.hpp"


template <uint8_t num_of_channels>
class Control {
 public:
  Control<num_of_channels>(Hz f_apbus)
      : phase_cut({phasecut_cfg.output_timer_address, phasecut_cfg.input_timer_address, f_apbus, phasecut_cfg.output,
                   phasecut_cfg.filter},
                  pins_motor, pin_motor_cfg, pin_timer),
        com(I2C1, i2c_pins, watergate, 2000)
  {
  	// init tests
  	int i = 0;
  	for(auto& t : test){
  		t = new FunctionTest<num_of_channels>(watergate[i]);
  		i++;
  	}
  }

  void com_interrupt_handler(uint64_t system_time) { com.i2c_interrupt_handler(system_time); }

  void time_measurement_interrupt_handler(uint64_t system_time) {
    // handle watergate stuff every 10ms
    for (uint8_t i = 0; i < num_of_channels; i++) {
      // check watergate stuff
      watergate[i].interrupt_handler_zero_crossing(system_time);
    }

    // handle phase cut issues
    phase_cut.zero_crossing_handler();
  }

  void set_watergate_position(uint8_t channel, typename WaterGate<num_of_channels>::Position pos) {
    watergate[channel].set_target_position(pos);
  }
  typename WaterGate<num_of_channels>::Position get_watergate_position(uint8_t channel) const {return watergate[channel].get_position();}

  typename WaterGate<num_of_channels>::State get_state(uint8_t channel) const {return watergate[channel].get_state();}

 private:
  static constexpr GpioPin::Config pin_motor_cfg = {GpioPin::Mode::Alternate, GpioPin::OutputType::OpenDrain,
                                              GpioPin::Speed::High, GpioPin::Pull::None};

  static constexpr std::array<GpioPin::Pin, num_of_channels> pins_motor = {{GPIOA, 8, GpioPin::AlternateFunction::AF2}};

  static constexpr GpioPin::Pin pin_timer = {GPIOA, 6, GpioPin::AlternateFunction::AF5};

  static constexpr std::array<GpioPin::Pin, num_of_channels> pins_switch = {
      {GPIOB, 12, GpioPin::AlternateFunction::AF0}};

  static constexpr GpioPin::Config pins_switch_cfg = {GpioPin::Mode::Input, GpioPin::OutputType::OpenDrain,
                                                      GpioPin::Speed::Low, GpioPin::Pull::Down};

  static constexpr std::array<GpioPin::Pin, 2> i2c_pins = {
      {{GPIOB, 8, GpioPin::AlternateFunction::AF1}, {GPIOB, 9, GpioPin::AlternateFunction::AF1}}};

  static constexpr typename PhaseCut<num_of_channels>::Config phasecut_cfg = {
      TIM1, TIM16, 0, PhaseCut<num_of_channels>::Output::inverted, TimeMeas<1>::Filter::_4};


  // phase cutting object
  PhaseCut<num_of_channels> phase_cut;

  // flexstart object
  std::array<FlexStart<num_of_channels>, num_of_channels> motor = {FlexStart<num_of_channels>(phase_cut, 0)};

  // switches
  std::array<Taster, num_of_channels> mic = {Taster(pins_switch[0], pins_switch_cfg, false)};

  // water gate control
  std::array<WaterGate<num_of_channels>, num_of_channels> watergate = {WaterGate<num_of_channels>(mic[0], motor[0])};

  // i2c communication
  Com<num_of_channels> com;

  // function testing
  std::array<FunctionTest<num_of_channels>*, num_of_channels> test;
};

template <uint8_t num_of_channels>
constexpr GpioPin::Config Control<num_of_channels>::pin_motor_cfg;

template <uint8_t num_of_channels>
constexpr std::array<GpioPin::Pin, num_of_channels> Control<num_of_channels>::pins_switch;

template <uint8_t num_of_channels>
constexpr std::array<GpioPin::Pin, num_of_channels> Control<num_of_channels>::pins_motor;

template <uint8_t num_of_channels>
constexpr std::array<GpioPin::Pin, 2> Control<num_of_channels>::i2c_pins;

template <uint8_t num_of_channels>
constexpr typename PhaseCut<num_of_channels>::Config Control<num_of_channels>::phasecut_cfg;

template <uint8_t num_of_channels>
constexpr GpioPin::Config Control<num_of_channels>::pins_switch_cfg;

template <uint8_t num_of_channels>
constexpr GpioPin::Pin Control<num_of_channels>::pin_timer;

#endif /* WW_CONTROL_H_ */
