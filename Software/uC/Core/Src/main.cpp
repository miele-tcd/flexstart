/** @file main.cpp
 *
 *  @date 17.10.2019
 *  @author dewernt
 */

#include "System.hpp"
#include "WW/Control.hpp"
#include "WW/FunctionTest.hpp"
#include "misc/Types.hpp"
#include "misc/Taster.hpp"

// cpu frequency
constexpr Hz cpu_frequency = 48'000'000;
constexpr uint8_t num_of_channels = 1; // defines the number of channels that are operated on

// init system
static System sys(cpu_frequency);

// watergate control
static Control<num_of_channels> control(sys.clock.get_f_apbus());

class Blink {
public:
	Blink(GpioPin::Pin pin, GpioPin::Config cfg, uint64_t blink_time) :
			blink_time { blink_time }, led { GpioPin(pin, cfg) } {
		led.set_state(true); //disable led
	}

	void init(uint64_t new_timestamp, uint8_t num_of_blinks) {
		blink_ref = num_of_blinks;
		timestamp = new_timestamp;
		active = true;
	}

	void deinit() {
		active = false;
	}

	void blink_handler(uint64_t system_time) {
		if (active) {
			// enabled blinking
			if (system_time < blink_ref * blink_time * 2 + timestamp) {
				led.set_state(((system_time - timestamp) / blink_time) % 2);
			} else {
				led.set_state(false); // enable led
			}
		} else {
			// disabled blinking
			led.set_state(true); // disable led
		}
	}

	bool is_active() const {return active;}

private:
	uint64_t timestamp;
	uint64_t blink_time = 200;
	uint8_t blink_ref = 0;
	bool active = false;
	GpioPin led;
};

int main() {
	// create button interface for position control
	Taster up( { GPIOB, 13, GpioPin::AlternateFunction::AF0 }, {
			GpioPin::Mode::Input, GpioPin::OutputType::OpenDrain, GpioPin::Speed::Low,
			GpioPin::Pull::Down }, false);
	Taster down( { GPIOB, 14, GpioPin::AlternateFunction::AF0 }, {
			GpioPin::Mode::Input, GpioPin::OutputType::OpenDrain, GpioPin::Speed::Low,
			GpioPin::Pull::Down }, false);

	// highest priority for phase cut instance
	NVIC_SetPriority(TIM16_IRQn, 0);
	// enable interrupt for time measurement
	NVIC_EnableIRQ(TIM16_IRQn);
	// enable i2c communication
	NVIC_EnableIRQ(I2C1_IRQn);

	Blink blink({ GPIOB, 15, GpioPin::AlternateFunction::AF0 }, {
			GpioPin::Mode::Output, GpioPin::OutputType::OpenDrain,
			GpioPin::Speed::Low, GpioPin::Pull::None }, 200);


	while (1) {
		// check buttons
		if (up.get_event()) {
			control.set_watergate_position(0,
					static_cast<typename WaterGate<num_of_channels>::Position>((static_cast<int>(control.get_watergate_position(
							0)) + 1) % 5));
		} else if (down.get_event()) {
			if (control.get_watergate_position(0)
					== WaterGate<num_of_channels>::Position::safe_1) {
				control.set_watergate_position(0,
						WaterGate<num_of_channels>::Position::safe_2);
			} else {
				control.set_watergate_position(0,
						static_cast<typename WaterGate<num_of_channels>::Position>(static_cast<int>(control.get_watergate_position(
								0)) - 1));
			}
		}

		// indicate movement
		if (control.get_state(0) == WaterGate<num_of_channels>::State::operating) {
			blink.deinit();
		} else {
			if(!blink.is_active()){
				blink.init(System::global_time_ms, static_cast<int>(control.get_watergate_position(0)) + 1);
			}
		}

		blink.blink_handler(System::global_time_ms);

		__WFI();
	}
}

// interrupts
extern "C" {
// com interrupt
void I2C1_IRQHandler(void) {
	control.com_interrupt_handler(System::global_time_ms);
}

// handler for flex start
void TIM16_IRQHandler(void) {
	control.time_measurement_interrupt_handler(System::global_time_ms);
}

void SysTick_Handler(void) {
	System::global_time_ms++;
}

/** @brief This function handles Non maskable interrupt. */
void NMI_Handler(void) {
}

/** @brief This function handles Hard fault interrupt. */
void HardFault_Handler(void) {
	while (1) {
	}
}

/** @brief This function handles System service call via SWI instruction. */
void SVC_Handler(void) {
}

/** @brief This function handles Pendable request for system service. */
void PendSV_Handler(void) {
}
}
