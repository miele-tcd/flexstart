/**
 * @file System.h
 * @brief hardware control wrapper
 *
 * @author dewernt
 * @date 16.7.2020
 * @copyright Miele & Cie KG 2020
 */

#ifndef HW_SYSTEM_H_
#define HW_SYSTEM_H_

#include "hw/hw_cfg.h"
#include "system_periphery/Clock.hpp"
#include "system_periphery/Exti.hpp"
#include "system_periphery/Flash.hpp"
#include "system_periphery/SysConfig.hpp"
#include "system_periphery/SysPower.hpp"
#include "system_periphery/SysTick.hpp"

class System {
 public:
  System(Hz cpu_frequency)
      : f_cpu(cpu_frequency),
        flash{cpu_frequency},
        systick(cpu_frequency, 1000),
        clock(cpu_frequency, cpu_frequency, cpu_frequency) {
    // init peripheral clocking for RTC
    clock.enable_LSI(true);
    clock.enable_APBus_peripheral(true, Clock::APB::SYSCFG_1);
    clock.enable_APBus_peripheral(true, Clock::APB::PWR_1);
    clock.enable_APBus_peripheral(true, Clock::APB::TIM_1);
    clock.enable_APBus_peripheral(true, Clock::APB::TIM_16);

    clock.enable_AHBus_peripheral(true, Clock::AHB::GPIO_A);
    clock.enable_AHBus_peripheral(true, Clock::AHB::GPIO_B);
  }

  /** software id */
  static constexpr uint16_t version = 10;
  /** software hash */
  static constexpr uint64_t git_hash = 0x123456789abcdef;
  /** repository name */
  static constexpr char name[] = "FlexStart";
  /** preconfigurated cpu frequency. real value can be seen in clock class */
  const Hz f_cpu;
  /** global system time incremented by systick */
  volatile static uint64_t global_time_ms;
  /** controller temperature */
  static Temperature controller_temperature;

  Flash flash;
  SystemTimer systick;
  Clock clock;
  SysConfig sysconf;
  Exti exti;
  SysPower power;
};

const char System::name[];
const uint64_t System::git_hash;
const uint16_t System::version;
volatile uint64_t System::global_time_ms = 0;
Temperature System::controller_temperature = 0;

#endif /* HW_SYSTEM_H_ */
