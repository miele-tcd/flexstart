
//#include "HAL_DATATYPE.h"
#include "rtwtypes.h"

#ifndef SciComm_H
#define	SciComm_H

#define	READY		1
#define	BUSY		1
#define	EMPTY		0
#define	FULL		1
#define	TIMEOUT		1000000



#define	FALSCHER_BEFEHL			 170
#define	READCOMMAND			 1111
#define	WRITECOMMAND			 2222
#define	RESTART_CODE			 0x4444
#define	SCI_STARTCODE			 9999
#define	SCI_ENDCODE			 85
#define	SCI_BUF_GR			 500 


#define	CommStateHeaderWord1		 1
#define	CommStateHeaderWord2		 2
#define	CommStateHeaderWord2_2		 22
#define	CommStateHeaderWord3		 3
#define	CommStateHeaderWord4		 4
#define	CommStateHeaderWord5		 5
#define	CommStateHeaderWord6		 6
#define	CommStateHeaderWord7 		 7
#define	CommStateReadData		11
#define	CommStateWriteToRam		13
#define	CommStateHeaderTimeout		77


#define	CHANMAX			6     	/* es koennen maximal chanmax Kanaele 	*/
								/* aufgezeichnet werde					*/
								/* Aufzeichnung (on chip RAM)			*/ 
#define	BUFFERSIZE		 1048  //4000




//typedef unsigned short WORD;
//typedef uint8_t  BYTE;


typedef struct {	
	uint8_T 		CommState;
	uint8_T  		WordReady;
	uint8_T		        SCI_CHECK;
	uint8_T		        PC_CHECK;
	uint8_T		        DSPIC_CHECK;
	uint8_T 		SCIRxDataLow;
	uint8_T 		SCIRxDataHigh;
	uint8_T 		SCITxDataLow;
	uint8_T 		SCITxDataHigh;
	uint16_T		SCIRxData;
	uint16_T		SCITxData;
	uint32_T		Time;
	uint16_T		SciType;          /* Datenart: Nutzdaten/Befehldaten		*/
	uint16_T		SciCommand;		/* Befehl: lese in den/schreibeaus dem 	DSP-Speicher							*/
	uint16_T	        SciNumber;            /* Anzahl der Daten die zu lesen aus    */
	uint32_T	        SciAdress;           /* Anzahl der Daten die zu lesen        */
	uint16_T		ScibufferIndex;
	uint16_T		Scibuffer[SCI_BUF_GR];
	} tSciCommParm;


void 	SciReadWord(void);
void	SciWriteWord(void);
void    InitUSART0_v(void);
void	ReadFromRS232WriteToRAM(void);
void	ReadFromRAMSendToRS232(void);
void	Record(void);
void	RecordLoop(void);
void 	Rx1BufferFull(void);
#endif


