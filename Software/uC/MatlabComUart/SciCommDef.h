#include "SciComm.h"

tSciCommParm	SciCommParm;

uint32_T	Adressen_u32[CHANMAX]={0x00,0x00,0x00,0x00,0x00,0x00};
											/* Buffer beinhaltet die Adressen der 	*/
											/* Variablen, die aufgezeichnet werden  */
											/* muessen								*/
uint16_T	Kanalanz_u16=1;         	/* Anzahl der Variablen, die            */
											/* aufgezeichnet werden muessen			*/
uint16_T	Datenanz_u16=0;         	/* Anzahl der Daten die gesendet werden */
											/* muessen								*/
uint16_T	Abtast_u16=1;          		/* Abtastperiode der Aufzeichnung		*/
uint16_T	ChanZaehler_u16=1;      	/*															*/
uint16_T	RecordPointer_u16=0;		/* Zeiger auf die Adresse im Buffer in  */
											/* der geschrieben wird 				*/
volatile uint16_T	RecordBuffer_u16[BUFFERSIZE];
uint16_T                BufferAdresse_u16=500;
uint16_T	        *ZeigerAufAdresse;

