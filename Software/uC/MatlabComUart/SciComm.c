/** ###################################################################*/
/**     Filename  : SciComm.C	*/
/** ###################################################################*/


  
/* MODULE SciComm */
#include "SciComm.h"
#include "SciCommDef.h"
#include "fsl_gpio.h"
#include "fsl_uart.h"
#include "fsl_clock.h"


#include "pin_mux.h"
#include "peripherals.h"
#include "rtwtypes.h"


#include <stdio.h>

//void Rx1BufferFull(void)
#ifdef MATLAB
void UART_HANDLER(void)
{
	uint8_T rx=0;
 if ((kUART_RxDataRegFullFlag) & UART_GetStatusFlags(UART_BASEADDR))
	{

		//UART_ClearStatusFlags(UART_BASEADDR, kUART_RxDataRegFullFlag | kUART_RxOverrunFlag);
		UART_ClearStatusFlags(UART_BASEADDR, kUART_RxDataRegFullFlag);
		//UART_ClearStatusFlags(UART_BASEADDR, kUART_RxOverrunFlag);

    if(SciCommParm.WordReady==0)
    {		
     /*clear RDRF by reading data reg */
    //SciCommParm.SCIRxDataHigh = (uint16_t) (UART_ReadByte(UART_BASEADDR) & 0xFF);
	UART_ReadBlocking(UART_BASEADDR,&rx,1u);
    SciCommParm.SCIRxDataHigh  =  rx & 0xFF;
    SciCommParm.SCI_CHECK= (uint16_t) (SciCommParm.SCI_CHECK ^ SciCommParm.SCIRxDataHigh);
    SciCommParm.WordReady=1;
    } 
    else
    {
      //SciCommParm.SCIRxDataLow=(uint16_t) (UART_ReadByte(UART_BASEADDR) & 0xFF);
      UART_ReadBlocking(UART_BASEADDR,&rx,1u);
      SciCommParm.SCIRxDataLow  =  rx & 0xFF;
      SciCommParm.SCIRxData= (uint16_t) SciCommParm.SCIRxDataLow+(uint16_t)(SciCommParm.SCIRxDataHigh<<8);
      SciCommParm.WordReady=0;
      SciCommParm.SCI_CHECK=(uint16_t) (SciCommParm.SCI_CHECK ^ SciCommParm.SCIRxDataLow);
      switch(SciCommParm.CommState)
      {
        case	CommStateHeaderWord1:
        {
          if(SciCommParm.SCIRxData==SCI_STARTCODE)
          {
            SciCommParm.CommState=CommStateHeaderWord2;
          } 
          else
          {
            SciCommParm.CommState=CommStateHeaderTimeout;
          }
          break;
        }
        case	CommStateHeaderWord2:
        {
          SciCommParm.SciAdress= ((uint32_t) (SciCommParm.SCIRxData)<<16);
          SciCommParm.CommState=CommStateHeaderWord2_2;
          break;
        }
        case	CommStateHeaderWord2_2:
        {
          SciCommParm.SciAdress= SciCommParm.SciAdress + (uint32_t)(SciCommParm.SCIRxData);
          SciCommParm.CommState=CommStateHeaderWord3;
          break;
        }
        case	CommStateHeaderWord3:
        {
          SciCommParm.SciNumber=SciCommParm.SCIRxData;
          SciCommParm.CommState=CommStateHeaderWord5;
          SciCommParm.DSPIC_CHECK=SciCommParm.SCI_CHECK;
          break;
        }
        case	CommStateHeaderWord4:
        {
          SciCommParm.SciCommand=SciCommParm.SCIRxData;
          SciCommParm.CommState=CommStateHeaderWord5;
          SciCommParm.DSPIC_CHECK=SciCommParm.SCI_CHECK;
        }
        break;
        case	CommStateHeaderWord5:
        {
          SciCommParm.PC_CHECK=SciCommParm.SCIRxDataLow;				
          if(SciCommParm.PC_CHECK==SciCommParm.DSPIC_CHECK)
          {
            SciCommParm.CommState=CommStateHeaderWord6;
          }
          else
          {
            SciCommParm.CommState=CommStateHeaderTimeout;
            SciCommParm.SCI_CHECK=0;
          } 
          break;
        }

        case	CommStateHeaderWord6:
        {
          SciCommParm.SciCommand=SciCommParm.SCIRxData;
          if (SciCommParm.SciCommand==READCOMMAND)
          {
            SciCommParm.SCI_CHECK=0;
            while (SciCommParm.SciNumber>0)
            {                              
              SciCommParm.SciNumber--;
              SciCommParm.SCITxData= (* (uint16_T *)  (SciCommParm.SciAdress));
              SciCommParm.SciAdress= SciCommParm.SciAdress+2;		  
              SciWriteWord();                    
            }	
          SciCommParm.SCITxData = ((uint16_T) (SciCommParm.SCI_CHECK)<<8)+SCI_ENDCODE;
          SciWriteWord();
          SciCommParm.CommState=CommStateHeaderWord1;
          SciCommParm.SCI_CHECK=0;	
          }
          if (SciCommParm.SciCommand==WRITECOMMAND)
          {
            SciCommParm.SCI_CHECK=0;
            SciCommParm.ScibufferIndex=0;
            SciCommParm.CommState=CommStateWriteToRam;
          }
          break;		
        }
        case	CommStateWriteToRam:
        {
          if (SciCommParm.ScibufferIndex<SciCommParm.SciNumber)
          {              
            SciCommParm.Scibuffer[SciCommParm.ScibufferIndex]=SciCommParm.SCIRxData;
            SciCommParm.ScibufferIndex++;
            SciCommParm.DSPIC_CHECK=SciCommParm.SCI_CHECK;
          } 
          else
          {
            if (SciCommParm.DSPIC_CHECK==SciCommParm.SCIRxData)
            {
              SciCommParm.SCITxData = SCI_ENDCODE;
              SciWriteWord();	
              SciCommParm.ScibufferIndex=0;
              while (SciCommParm.ScibufferIndex<(SciCommParm.SciNumber))
              {
                *(uint16_T *)(SciCommParm.SciAdress+(SciCommParm.ScibufferIndex<<1))	=
                SciCommParm.Scibuffer[SciCommParm.ScibufferIndex] ;   
                SciCommParm.ScibufferIndex+=1;         
              }
            }
            SciCommParm.CommState=CommStateHeaderWord1;
            SciCommParm.SCI_CHECK=0;
          }	
          break;		
        }

        case CommStateHeaderTimeout:
        {
          if(SciCommParm.SCIRxData==RESTART_CODE)	
          {
            SciCommParm.SCITxData = SCI_ENDCODE;
            SciWriteWord();	
            SciCommParm.CommState=CommStateHeaderWord1;
            SciCommParm.SCI_CHECK=0;
            SciCommParm.Time=0;
          }	
          break;				
        }

        default:
        {
          SciCommParm.CommState=CommStateHeaderTimeout;
          SciCommParm.SCI_CHECK=0;
          break;
        }
      }	// end of switch (SciCommParm.CommState)							/* Checksumme reinitialisieren          */	     
    }
 }
}
#endif


void SciWriteWord(void)
{        
	
  uint8_T Tx=0;
  SciCommParm.SCITxDataLow =(uint16_t)(SciCommParm.SCITxData&0xff);
  SciCommParm.SCITxDataHigh=(uint16_t)(SciCommParm.SCITxData>>8);	
	
  /* Loop until USART0 DR register is empty */
  /*while(!((kUART_TxDataRegEmptyFlag) & UART_GetStatusFlags(UART_BASEADDR)))
  {
  }*/

  /* Send one byte from USART0  */
  Tx = SciCommParm.SCITxDataHigh;
  UART_WriteBlocking(UART_BASEADDR, &Tx,1u);

  /* Loop until USART0 DR register is empty */
  /*while(!((kUART_TxDataRegEmptyFlag) & UART_GetStatusFlags(UART_BASEADDR)))
  {
  }*/

  /* Send one byte from USART0  */
  Tx = SciCommParm.SCITxDataLow;
  UART_WriteBlocking(UART_BASEADDR, &Tx,1u);
}

void Record (void)
{
  unsigned int	KanalZaehler_u16;

  if  (ChanZaehler_u16>=Abtast_u16)
  {	
    ChanZaehler_u16=1;
	
    if (Datenanz_u16>(BUFFERSIZE-1))     
    {
      Datenanz_u16=BUFFERSIZE-1;  
    }
	
    if  (RecordPointer_u16<Datenanz_u16)
    {   
      for (KanalZaehler_u16=0;KanalZaehler_u16<Kanalanz_u16;KanalZaehler_u16++)                      
      {	
	ZeigerAufAdresse = (uint16_T *) (Adressen_u32[KanalZaehler_u16]);
	RecordBuffer_u16[RecordPointer_u16]=(*ZeigerAufAdresse);
	RecordPointer_u16=RecordPointer_u16+1;
	ChanZaehler_u16=1;
      }		
    }			
  }	
  else
  {                 
    ChanZaehler_u16++;
  }
}

uint16_t RecordLoopStart_u16=0;

void RecordLoop(void)
{
  unsigned  int	KanalZaehler_u16;

  if (RecordLoopStart_u16 == 1)
  {
    if  (ChanZaehler_u16>=Abtast_u16)
    {	
      ChanZaehler_u16=1;		
      for (KanalZaehler_u16=0;KanalZaehler_u16<Kanalanz_u16;KanalZaehler_u16++)                      
      {	
	ZeigerAufAdresse=(uint16_T *) (Adressen_u32[KanalZaehler_u16]);
	RecordBuffer_u16[RecordPointer_u16]=(*ZeigerAufAdresse);
	RecordPointer_u16=(RecordPointer_u16+1) & (BUFFERSIZE-1);  // BUFFERSIZE muss ein 2^n-1 Zahl sein
	ChanZaehler_u16=1;
      }					
    }	
    else
    {                    
      ChanZaehler_u16++;
    }
  }
}


/* END SciComm */
