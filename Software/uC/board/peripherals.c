/*
 * Copyright 2017-2018 NXP
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* TEXT BELOW IS USED AS SETTING FOR TOOLS *************************************
 !!GlobalInfo
 product: Peripherals v1.0
 * BE CAREFUL MODIFYING THIS COMMENT - IT IS YAML SETTINGS FOR TOOLS **********/

/**
 * @file    peripherals.c
 * @brief   Peripherals initialization file.
 */

/* This is a template for board specific configuration created by MCUXpresso IDE Project Wizard.*/

#include "peripherals.h"
#include "fsl_ftm.h"
#include "fsl_gpio.h"
#include "fsl_pdb.h"
#include "fsl_adc16.h"
#include "fsl_cmp.h"
#include "fsl_uart.h"


/*******************************************************************************
 * Code
 ******************************************************************************/

void BOARD_InitBootPeripherals(void) {
	/* The user initialization should be placed here */
	InitPWM();
	InitPDB();
	InitADC();
	InitCMP();
	systick_kv10z_init();
	InitUart();
}

/* The Flextimer Initialization (FTM) */
void InitPWM() {
// number of channels in pwm output
#define NUM_OF_CHANNELS 4
	/* Struct Defintion in fsl_ftm.c */
	ftm_chnl_pwm_signal_param_t ftmParam[NUM_OF_CHANNELS]; /* 4 channels */

	/* fill FTM config Modulation structure */
	for(uint8_t i = 0; i < NUM_OF_CHANNELS; i++){
		ftmParam[i].chnlNumber = kFTM_Chnl_2 + i;
		ftmParam[i].dutyCyclePercent = 0; //100;
		ftmParam[i].firstEdgeDelayPercent = 0;
		ftmParam[i].level = kFTM_LowTrue;
		//ftmParam[i].level = kFTM_HighTrue;
	}

	// fill timer config struct

	ftm_config_t ftmInfo;
	ftmInfo.prescale = kFTM_Prescale_Divide_1;
	ftmInfo.bdmMode = kFTM_BdmMode_0;
	ftmInfo.pwmSyncMode = kFTM_SoftwareTrigger;
	/*The FTM registers (FTM_CNTIN, FTM_MOD, FTM_CV, ...) are updated when the counter reaches the maximum value */
	ftmInfo.reloadPoints = kFTM_CntMax;
	/*Fault control is enabled for all channels, and the selected mode is the manual fault clearing.*/
	ftmInfo.faultMode = kFTM_Fault_AllChnlsMan;
	ftmInfo.faultFilterValue = 0;
	ftmInfo.deadTimePrescale = kFTM_Deadtime_Prescale_4;
	 /* 72 MHz / 4 * 1 usec*/
	ftmInfo.deadTimeValue =  18;
	/*Generate External Trigger when counter is updated with CNTIN*/
	ftmInfo.extTriggers = kFTM_InitTrigger;
	ftmInfo.chnlInitState = 0;
	ftmInfo.chnlPolarity = 0;
	ftmInfo.useGlobalTimeBase = false;

	// status varialbe for display
	status_t stat = 0; // init as OK

	/* Initialize FTM module */
	stat = FTM_Init(FTM_BASEADDR, &ftmInfo);

	/* Configuration_
	 * two FTM0 pairs of channels
	 * 8KHz output period
	 * Center aligned waveform  */
	stat = FTM_SetupPwm(FTM_BASEADDR, ftmParam, NUM_OF_CHANNELS, kFTM_CenterAlignedPwm, 8000, FTM_SOURCE_CLOCK);

	/* Enable complementary output on the channel pair 1,2 */
	FTM_SetComplementaryEnable(FTM_BASEADDR, FTM_CHANNEL_PAIR1, true);
	FTM_SetComplementaryEnable(FTM_BASEADDR, FTM_CHANNEL_PAIR2, true);

	/* Enable Deadtime insertion on the channel pair */
	FTM_SetDeadTimeEnable(FTM_BASEADDR, FTM_CHANNEL_PAIR1, true);
	FTM_SetDeadTimeEnable(FTM_BASEADDR, FTM_CHANNEL_PAIR2, true);

	/* Enable Fault Control insertion on the channel pair */
	FTM_SetFaultControlEnable(FTM_BASEADDR, FTM_CHANNEL_PAIR1, true);
	FTM_SetFaultControlEnable(FTM_BASEADDR, FTM_CHANNEL_PAIR2, true);

	/* FTM Fault config */
	ftm_fault_param_t ftmFaultParam;
	ftmFaultParam.enableFaultInput = false; /* no fault input */
	ftmFaultParam.faultLevel = false; /* '1' indicates a fault*/
	ftmFaultParam.useFaultFilter = false; /* not filtered */
	/*Init FTM fault 0 input pin*/
	FTM_SetupFault(FTM_BASEADDR, kFTM_Fault_0, &ftmFaultParam);
	/*Start Timer*/
	FTM_StartTimer(FTM_BASEADDR, kFTM_SystemClock);

#undef NUM_OF_CHANNELS
}

/* The PDB Initialization */
void InitPDB() {
	pdb_config_t pdbConfigStruct;
	pdb_adc_pretrigger_config_t pdbAdcPreTriggerConfigStruct;

	/* Get Default Configuration */
	PDB_GetDefaultConfig(&pdbConfigStruct);

	/*
	 * The default values are as follows.
	 *   config->loadValueMode = kPDB_LoadValueImmediately;
	 *   config->prescalerDivider = kPDB_PrescalerDivider1;
	 *   config->dividerMultiplicationFactor = kPDB_DividerMultiplicationFactor1;
	 *   config->triggerInputSource = kPDB_TriggerSoftware;
	 *   config->enableContinuousMode = false;
	 */

	/* Config PDB */
	pdbConfigStruct.triggerInputSource = kPDB_TriggerInput8; /* FTM0 Initialization input trigger */
	pdbConfigStruct.enableContinuousMode = false;
	PDB_Init(PDB_BASE, &pdbConfigStruct);

	/* Configure the delay interrupt. */
	/* Value between delay (0) and PWM Period (125 us)
	 * 1/(24MHz / 1535) = 64 us */
	PDB_SetModulusValue(PDB_BASE, 1535U);

	/* Configure the ADC Pre-Trigger. */
	PDB_SetADCPreTriggerDelayValue(PDB_BASE, PDB_TRIGGER_CHANNEL0,
			kPDB_ADCPreTrigger0, 0U);

	/* Enable back-to-back operation. */
	pdbAdcPreTriggerConfigStruct.enableBackToBackOperationMask =
			PDB_PRETRIGGER_CHANNEL1_MASK; /* Enable channel 0 pretrigger 1 to  back to back mode */
	pdbAdcPreTriggerConfigStruct.enablePreTriggerMask =
			(PDB_PRETRIGGER_CHANNEL0_MASK | PDB_PRETRIGGER_CHANNEL1_MASK);
	pdbAdcPreTriggerConfigStruct.enableOutputMask =
			(PDB_PRETRIGGER_CHANNEL0_MASK | PDB_PRETRIGGER_CHANNEL1_MASK);
	PDB_SetADCPreTriggerConfig(PDB_BASE, PDB_TRIGGER_CHANNEL0,
			&pdbAdcPreTriggerConfigStruct);

	pdbAdcPreTriggerConfigStruct.enableBackToBackOperationMask =
			(PDB_PRETRIGGER_CHANNEL0_MASK | PDB_PRETRIGGER_CHANNEL1_MASK); /* Enable channel 1 pretrigger 0 and channel 1 pretrigger 1 to  back to back mode */
	PDB_SetADCPreTriggerConfig(PDB_BASE, PDB_TRIGGER_CHANNEL1,
			&pdbAdcPreTriggerConfigStruct);

	/* Load PDB register, LDOK bit */
	PDB_DoLoadValues(PDB_BASE);
}

/* The ADC Initialization */
void InitADC() {
	adc16_config_t adc16ConfigStruct;
	adc16_channel_config_t adc16ChannelConfigStruct;
	adc16_hardware_compare_config_t adc16HardwareCompareConfigStruct;

	/* Get Default Configuration */
	ADC16_GetDefaultConfig(&adc16ConfigStruct);
	/*
	 * The default values are as follows.
	 * @code
	 *   config->referenceVoltageSource     = kADC16_ReferenceVoltageSourceVref;
	 *   config->clockSource                = kADC16_ClockSourceAsynchronousClock;
	 *   config->enableAsynchronousClock    = true;
	 *   config->clockDivider               = kADC16_ClockDivider8;
	 *   config->resolution                 = kADC16_ResolutionSE12Bit;
	 *   config->longSampleMode             = kADC16_LongSampleDisabled;
	 *   config->enableHighSpeed            = false;
	 *   config->enableLowPower             = false;
	 *   config->enableContinuousConversion = false;
	 */

	/* Config ADC */
	adc16ConfigStruct.clockSource = kADC16_ClockSourceAlt2;
	adc16ConfigStruct.clockDivider = kADC16_ClockDivider1;
	adc16ConfigStruct.enableAsynchronousClock = false;
	adc16ConfigStruct.enableHighSpeed = true;

	/* Init ADC */
	ADC16_Init(ADC_BASE0, &adc16ConfigStruct);
	ADC16_Init(ADC_BASE1, &adc16ConfigStruct);

	/* Conversion trigger selection.*/
	ADC16_EnableHardwareTrigger(ADC_BASE0, true);
	ADC16_EnableHardwareTrigger(ADC_BASE1, true);

#if defined(FSL_FEATURE_ADC16_HAS_DIFF_MODE) && FSL_FEATURE_ADC16_HAS_DIFF_MODE
	adc16ChannelConfigStruct.enableDifferentialConversion = false;
#endif /* FSL_FEATURE_ADC16_HAS_DIFF_MODE */

	/*Enable ADC Interrupt On Conversion Completed*/
	adc16ChannelConfigStruct.enableInterruptOnConversionCompleted = true;

	/*Config ADC Channels*/
	adc16ChannelConfigStruct.channelNumber = ADC0_USER_CHANNEL2;
	ADC16_SetChannelConfig(ADC_BASE0, ADC_CHANNEL_GROUP0,
			&adc16ChannelConfigStruct);

	adc16ChannelConfigStruct.channelNumber = ADC0_USER_CHANNEL1;
	ADC16_SetChannelConfig(ADC_BASE0, ADC_CHANNEL_GROUP1,
			&adc16ChannelConfigStruct);

	adc16ChannelConfigStruct.channelNumber = ADC1_USER_CHANNEL2;
	ADC16_SetChannelConfig(ADC_BASE1, ADC_CHANNEL_GROUP0,
			&adc16ChannelConfigStruct);

	adc16ChannelConfigStruct.channelNumber = ADC1_USER_CHANNEL8;
	ADC16_SetChannelConfig(ADC_BASE1, ADC_CHANNEL_GROUP1,
			&adc16ChannelConfigStruct);

	/*Enable the NVIC*/
	EnableIRQ(ADC1_IRQn);
	/*Set Interrupt Priority*/
	NVIC_SetPriority(ADC1_IRQn, 0);
}

/* The Comparator Initialization */
void InitCMP() {
	cmp_config_t mCmpConfigStruct;
	cmp_dac_config_t mCmpDacConfigStruct;

	/* Get Default Configuration */
	CMP_GetDefaultConfig(&mCmpConfigStruct);
	/*
	 * This function initializes the user configuration structure to these default values.
	 *   config->enableCmp           = true;
	 *   config->hysteresisMode      = kCMP_HysteresisLevel0;
	 *   config->enableHighSpeed     = false;
	 *   config->enableInvertOutput  = false;
	 *   config->useUnfilteredOutput = false;
	 *   config->enablePinOut        = false;
	 *   config->enableTriggerMode   = false;
	 */

	/* Init the CMP0 comparator. */
	CMP_Init(CMP0_BASE, &mCmpConfigStruct);

	/* Configure the DAC channel for over current detect. */
	mCmpDacConfigStruct.referenceVoltageSource = kCMP_VrefSourceVin2; /* VCC. */
	mCmpDacConfigStruct.DACValue = 57; /* 89% voltage of logic high level. 1.3A*/
	CMP_SetDACConfig(CMP0_BASE, &mCmpDacConfigStruct);

	CMP_SetInputChannels(CMP0_BASE, CMP_USER_CHANNEL_OVC, CMP_DAC_CHANNEL_OVC);
}

/* The Systick Initialization */
void systick_kv10z_init() {
	/*System Tick Configuration*/
	SysTick_Config(71000);
	/*Set Interrupt Priority*/
	NVIC_SetPriority(SysTick_IRQn, 1);
}

void InitUart(void) {
	uint32_t baudRate_Bps = UART_BAUD_RATE;
	uint32_t uartSourceClockInHz = UART_SOURCE_CLOCK; /* System Clock */

	/* Structure of initialize UART */
	uart_config_t config;

	/* Get Default Configuration */
	UART_GetDefaultConfig(&config);
	/*
	 * The default values are as follows.
	 *   uartConfig->baudRate_Bps = 115200U;
	 *   uartConfig->bitCountPerChar = kUART_8BitsPerChar;
	 *   uartConfig->parityMode = kUART_ParityDisabled;
	 *   uartConfig->stopBitCount = kUART_OneStopBit;
	 *   uartConfig->txFifoWatermark = 0;
	 *   uartConfig->rxFifoWatermark = 1;
	 *   uartConfig->idleType = kUART_IdleTypeStartBit;
	 *   uartConfig->enableTx = false;
	 *   uartConfig->enableRx = false;
	 */

	/* Configure UART */
	config.baudRate_Bps = baudRate_Bps;
	config.enableTx = true;
	config.enableRx = true;

	/* Initializes a UART instance with a user configuration structure and peripheral clock */
	UART_Init(UART_BASEADDR, &config, uartSourceClockInHz);

	/* Enables UART interrupts when kUART_RxDataRegFullFlag */
	UART_EnableInterrupts(UART_BASEADDR, kUART_RxDataRegFullInterruptEnable);

	/* Enable at the NVIC */
	EnableIRQ(UART_IRQ);
	/* Set interrupt priority */
	NVIC_SetPriority(UART_IRQ, 2);

	/* Enable the UART transmitter and receiver */
	UART_EnableTx(UART_BASEADDR, true);
	UART_EnableRx(UART_BASEADDR, true);
}


