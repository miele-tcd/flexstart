/*
 * Copyright 2017-2018 NXP
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file    peripherals.h
 * @brief   Peripherals initialization header file.
 */
 
/* This is a template for board specific configuration created by MCUXpresso IDE Project Wizard.*/

#ifndef _PERIPHERALS_H_
#define _PERIPHERALS_H_

#if defined(__cplusplus)
extern "C" {
#endif /* __cplusplus */


/*******************************************************************************
 * Definitions
 * Pins: compare with pin_mux.c / pin_mux.h)
 * Configurations: compare with chapter 5 in Tutorial
 ******************************************************************************/

/* The FTM (Flex-Timer) instance/channel used for board
 * Generate PWM signal */
#define FTM_BASEADDR FTM0 /* FTM Base address / name */
#define FTM_CHANNEL_PAIR1 kFTM_Chnl_1	/* channel pair 1: FTM0-CH2 and FTM0-CH3 (pin 43 and 44: PWM H1 und L1) */
#define FTM_CHANNEL_PAIR2 kFTM_Chnl_2 /* channel pair 2: FTM0-CH4 and FTM0-CH5 (pin 45 and 46: PWM H2 und L2) */
#define FTM_CHANNEL_FLAG kFTM_FaultFlag /* Fault flag */
/* Get source clock for FTM driver */
#define FTM_SOURCE_CLOCK CLOCK_GetFreq(kCLOCK_CoreSysClk) /* Source Clock: System Clock */

/* The PDB instance/channel used for board
 * Trigger ADC conversions
 * Synchronization between FTM and ADC */
#define PDB_BASE PDB0 /* PDB Base address / name */
#define PDB_PRETRIGGER_CHANNEL0_MASK 1U
#define PDB_PRETRIGGER_CHANNEL1_MASK 2U
#define PDB_TRIGGER_CHANNEL0 kPDB_ADCTriggerChannel0
#define PDB_TRIGGER_CHANNEL1 kPDB_ADCTriggerChannel1

/* The ADC instance/channel used for board
 * Measurement of Current and Voltage */
#define ADC_BASE0 ADC0 /* ADC Base address / name */
#define ADC_BASE1 ADC1 /* ADC Base address / name */
#define ADC0_USER_CHANNEL1 1U 	/* PTE16, ADC0_SE1 (pin 3: IsU) */
#define	ADC0_USER_CHANNEL2 2U	/* PTD1, ADC0_SE2 (pin 42: VsU) */
#define ADC1_USER_CHANNEL2 2U	/* PTB3, ADC1_SE2 (pin 30: VsV) */
#define ADC1_USER_CHANNEL8 8U	/* PTB0, ADC1_SE8 (pin 27: UDC1) */
#define ADC_CHANNEL_GROUP0 0U
#define ADC_CHANNEL_GROUP1 1U

#define ADC0_IRQ_HANDLER ADC0_IRQHandler /* Interrupt Handler */
#define ADC1_IRQ_HANDLER ADC1_IRQHandler /* Interrupt Handler */

/* The Comparator instance/channel used for board
 * Detect over current and current zero crossing */
#define CMP0_BASE CMP0 /* CMP Base address / name */
#define CMP_USER_CHANNEL_OVC 5U /* PTE29, CMP0_IN5 (pin 13) over current detect */
#define CMP_DAC_CHANNEL_OVC 7U

#define CMP1_BASE CMP1 /* CMP Base address / name */
#define CMP_USER_CHANNEL_ZC 0U /* PTC2, CMP1_IN0 (pin 35) planned for current zero crossing detect - not used! zero crossing detected in software of generated code */
#define CMP_DAC_CHANNEL_ZC 7U

#define FREEMASTER
//#define MATLAB //inverser avec chayma a revoir

#ifdef MATLAB
//#ifdef FREEMASTER
/* The UART instance/channel used for board
 * communication between board and PC */
#define UART_BASEADDR UART0 /* UART Base address / name */
#define UART_SOURCE_CLOCK (CLOCK_GetFreq(kCLOCK_CoreSysClk)) /* Source Clock: System Clock */
#define UART_BAUD_RATE 19200 /* BAUD Rate: communication speed */

#define UART_HANDLER UART0_IRQHandler /* Interrupt Handler */
#define UART_IRQ UART0_IRQn

#endif

#ifdef FREEMASTER
//#ifdef MATLAB
/* The UART instance/channel used for board
 * communication between board and PC */
#define UART_BASEADDR UART1 /* UART Base address / name */
#define UART_SOURCE_CLOCK (CLOCK_GetFreq(kCLOCK_BusClk)) /* Source Clock: System Clock */
#define UART_BAUD_RATE 9600 //19200 /* BAUD Rate: communication speed */

#define UART_HANDLER UART1_IRQHandler /* Interrupt Handler */
#define UART_IRQ UART1_IRQn

#endif


/*******************************************************************************
 * Prototypes
 ******************************************************************************/

void BOARD_InitBootPeripherals(void);
void InitPWM (void);
void InitPDB(void);
void InitADC(void);
void systick_kv10z_init(void);
void InitCMP (void) ;
void InitUart(void);

#if defined(__cplusplus)
}
#endif /* __cplusplus */

#endif /* _PERIPHERALS_H_ */


