/**
 * @file Tests.h
 * @brief contrains interface for chosen test to be executed on interrupt
 * @date 26.01.2022
 * @author Mansour
 * @copyright Miele&Cie KG 2020(c)
 */

#ifndef TESTS_H_
#define TESTS_H_

#include "Types.h"

typedef enum {
	Angle_sweep, Alternate, Separately, Alternate_step_2, Heating,
} Test_method;

typedef struct ANgle_sweep_struct {
	int16_t speed;
	int16_t angle;
} ANgle_sweep_struct;

ANgle_sweep_struct Angle_sweep_s16(int64_t flexstart_reset_interval, uint8_t start_test);

int16_t Alternate_s16(int64_t flexstart_reset_interval, uint8_t start_test);


int16_t Separately_s16(int64_t flexstart_reset_interval, uint8_t start_test);

int16_t Alternate_step_2_s16(int64_t flexstart_reset_interval, uint8_t start_test, uint8_t step);

int16_t Heating_s16(int64_t system_time, int64_t flexstart_reset_interval, uint8_t start_test);

#endif /* TESTS_H_ */
