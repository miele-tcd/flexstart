/**
 * @file  Types.h
 * @brief contains typedefs to use physical units instead of integers
 * @date 04.05.2021
 * @author dewernt
 */

#ifndef TYPES_H_
#define TYPES_H_

#include <stdint.h>

typedef int32_t uA;
typedef int32_t mV;
typedef int64_t ms;
typedef int32_t Hz;

#endif /* TYPES_H_ */
