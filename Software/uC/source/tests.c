/**
 * @file inverter.c
 * @brief contrains interface for generated inverter code to be executed on interrupt
 * @date 11.08.2020
 * @author dewernt
 * @copyright Miele&Cie KG 2020(c)
 */

#include "stdint.h"
#include "Tests.h"
#include "Types.h"



volatile static int16_t MOTOR_MBDFLEXSTART_count1_s16=0, MOTOR_MBDFLEXSTART_count2_s16=0, MOTOR_MBDFLEXSTART_count3_s16=0;
volatile static int16_t nb_test=0,  MOTOR_MBDFLEXSTART_old_speed_s16,MOTOR_MBDFLEXSTART_speed_s16, start_angle_neg=-32000;
ANgle_sweep_struct sweep={0, 0};
// Angle sweep automatically,

ANgle_sweep_struct Angle_sweep_s16(int64_t flexstart_reset_interval, uint8_t start_test){
    if (start_test==0){start_angle_neg=-32000;
    MOTOR_MBDFLEXSTART_count2_s16=0;}
		if(MOTOR_MBDFLEXSTART_count1_s16 < 300 && start_test!=0){
			sweep.speed=0;
			MOTOR_MBDFLEXSTART_count1_s16++;
		}else if(MOTOR_MBDFLEXSTART_count1_s16 >= 300 && MOTOR_MBDFLEXSTART_count1_s16 < (flexstart_reset_interval) && start_test!=0){
			MOTOR_MBDFLEXSTART_count1_s16++;
			sweep.speed=-750;
			if(MOTOR_MBDFLEXSTART_count1_s16==flexstart_reset_interval-1){
				start_angle_neg=start_angle_neg+1000;
				sweep.angle=start_angle_neg;
				MOTOR_MBDFLEXSTART_count2_s16++;
			}
		}else {MOTOR_MBDFLEXSTART_count1_s16=0;}
		if(MOTOR_MBDFLEXSTART_count2_s16==65){
			MOTOR_MBDFLEXSTART_count1_s16=0;
		  sweep.speed=0;
		  start_angle_neg=14500;
		  sweep.angle=start_angle_neg;
		}
		return sweep;
}

//Change direction automatically + heating tests

int16_t Alternate_s16(int64_t flexstart_reset_interval, uint8_t start_test){

 if (start_test==0){nb_test=0;}
 if (nb_test<40){
   if(MOTOR_MBDFLEXSTART_count1_s16 < 300 && start_test!=0){
				MOTOR_MBDFLEXSTART_speed_s16=0;
				MOTOR_MBDFLEXSTART_count1_s16++;
   }else if(MOTOR_MBDFLEXSTART_count1_s16 >= 300 && MOTOR_MBDFLEXSTART_count1_s16 < (flexstart_reset_interval) && start_test!=0){
				if (MOTOR_MBDFLEXSTART_count1_s16==flexstart_reset_interval-1){nb_test++;}
  	    MOTOR_MBDFLEXSTART_count1_s16++;
				MOTOR_MBDFLEXSTART_speed_s16=-750;
   }else if(MOTOR_MBDFLEXSTART_count1_s16 >= flexstart_reset_interval && MOTOR_MBDFLEXSTART_count1_s16 < (flexstart_reset_interval+300)&& start_test!=0){
				MOTOR_MBDFLEXSTART_count1_s16++;
				MOTOR_MBDFLEXSTART_speed_s16=0;
   }else if (MOTOR_MBDFLEXSTART_count1_s16>=(flexstart_reset_interval+300) && MOTOR_MBDFLEXSTART_count1_s16<2*flexstart_reset_interval && start_test!=0){
  	 if (MOTOR_MBDFLEXSTART_count1_s16==2*flexstart_reset_interval-1){nb_test++;}
  	    MOTOR_MBDFLEXSTART_count1_s16++;
				MOTOR_MBDFLEXSTART_speed_s16=750;
   }else {
				MOTOR_MBDFLEXSTART_count1_s16=0;
				MOTOR_MBDFLEXSTART_speed_s16=0;}

   if (nb_test==40
  		 ){MOTOR_MBDFLEXSTART_speed_s16=0;}

	//MOTOR_MBDFLEXSTART_old_speed_s16=MOTOR_MBDFLEXSTART_speed_s16;
   }
	return MOTOR_MBDFLEXSTART_speed_s16;
}

int16_t Separately_s16(int64_t flexstart_reset_interval, uint8_t start_test){
	if (start_test==0){MOTOR_MBDFLEXSTART_count2_s16= 0;
	nb_test=0;}
	if (nb_test<=40){
	   if(MOTOR_MBDFLEXSTART_count1_s16 < 300 && start_test!=0){
					MOTOR_MBDFLEXSTART_speed_s16=0;
					MOTOR_MBDFLEXSTART_count1_s16++;
	   }else if(MOTOR_MBDFLEXSTART_count1_s16 >= 300 && MOTOR_MBDFLEXSTART_count1_s16 < (flexstart_reset_interval) && start_test!=0){
					MOTOR_MBDFLEXSTART_count1_s16++;
					if (MOTOR_MBDFLEXSTART_count1_s16==flexstart_reset_interval ){MOTOR_MBDFLEXSTART_count2_s16++;
					nb_test=MOTOR_MBDFLEXSTART_count2_s16;}
					if (MOTOR_MBDFLEXSTART_count2_s16<=20){MOTOR_MBDFLEXSTART_speed_s16=-750;
					}else if (MOTOR_MBDFLEXSTART_count2_s16<=40){MOTOR_MBDFLEXSTART_speed_s16=750;}
					else {MOTOR_MBDFLEXSTART_speed_s16=0;}
     }else {
          MOTOR_MBDFLEXSTART_count1_s16=0;
          MOTOR_MBDFLEXSTART_speed_s16=0;}
	}
	return MOTOR_MBDFLEXSTART_speed_s16;
}

int16_t Alternate_step_2_s16(int64_t flexstart_reset_interval, uint8_t start_test, uint8_t step){
   if (step<2){step=2;}
   if (start_test==0){MOTOR_MBDFLEXSTART_count2_s16= 0;
   MOTOR_MBDFLEXSTART_count3_s16 = 0;
   nb_test=0;}
   if (nb_test<=40){
      if(MOTOR_MBDFLEXSTART_count1_s16 < 300 && start_test!=0){
					MOTOR_MBDFLEXSTART_speed_s16=0;
					MOTOR_MBDFLEXSTART_count1_s16++;
	    }else if(MOTOR_MBDFLEXSTART_count1_s16 >= 300 && MOTOR_MBDFLEXSTART_count1_s16 < (flexstart_reset_interval) && start_test!=0){
					MOTOR_MBDFLEXSTART_count1_s16++;
					if (MOTOR_MBDFLEXSTART_count2_s16==step){
						MOTOR_MBDFLEXSTART_speed_s16=750;
						if (MOTOR_MBDFLEXSTART_count1_s16==flexstart_reset_interval ){MOTOR_MBDFLEXSTART_count3_s16++;nb_test++;}
						if (MOTOR_MBDFLEXSTART_count3_s16==step){MOTOR_MBDFLEXSTART_count2_s16=0;MOTOR_MBDFLEXSTART_count3_s16=0;}
					}else	if (MOTOR_MBDFLEXSTART_count1_s16==flexstart_reset_interval && MOTOR_MBDFLEXSTART_count3_s16==0){
						MOTOR_MBDFLEXSTART_count2_s16++;
						nb_test++;
					}else{MOTOR_MBDFLEXSTART_speed_s16=-750;}
	    }else {
          MOTOR_MBDFLEXSTART_count1_s16=0;
          MOTOR_MBDFLEXSTART_speed_s16=0;}
   }else { MOTOR_MBDFLEXSTART_speed_s16=0;}
	return MOTOR_MBDFLEXSTART_speed_s16;
}
int16_t Heating_s16(int64_t system_time, int64_t flexstart_reset_interval, uint8_t start_test){
	if (start_test==0){nb_test=0;
	MOTOR_MBDFLEXSTART_count1_s16=0;}
	if (system_time>30000){ //Run motor 5 mn for heating test
		 if (nb_test<40){
		   if(MOTOR_MBDFLEXSTART_count1_s16 < 300 && start_test!=0){
						MOTOR_MBDFLEXSTART_speed_s16=0;
						MOTOR_MBDFLEXSTART_count1_s16++;
		   }else if(MOTOR_MBDFLEXSTART_count1_s16 >= 300 && MOTOR_MBDFLEXSTART_count1_s16 < (flexstart_reset_interval) && start_test!=0){
						if (MOTOR_MBDFLEXSTART_count1_s16==flexstart_reset_interval-1){nb_test++;}
		  	    MOTOR_MBDFLEXSTART_count1_s16++;
						MOTOR_MBDFLEXSTART_speed_s16=-750;
		   }else if(MOTOR_MBDFLEXSTART_count1_s16 >= flexstart_reset_interval && MOTOR_MBDFLEXSTART_count1_s16 < (flexstart_reset_interval+300)&& start_test!=0){
						MOTOR_MBDFLEXSTART_count1_s16++;
						MOTOR_MBDFLEXSTART_speed_s16=0;
		   }else if (MOTOR_MBDFLEXSTART_count1_s16>=(flexstart_reset_interval+300) && MOTOR_MBDFLEXSTART_count1_s16<2*flexstart_reset_interval && start_test!=0){
		  	 if (MOTOR_MBDFLEXSTART_count1_s16==2*flexstart_reset_interval-1){nb_test++;}
		  	    MOTOR_MBDFLEXSTART_count1_s16++;
						MOTOR_MBDFLEXSTART_speed_s16=750;
		   }else {
						MOTOR_MBDFLEXSTART_count1_s16=0;
						MOTOR_MBDFLEXSTART_speed_s16=0;}

		   if (nb_test==40
		  		 ){MOTOR_MBDFLEXSTART_speed_s16=0;}

		//	MOTOR_MBDFLEXSTART_old_speed_s16=MOTOR_MBDFLEXSTART_speed_s16;
		 }

	}else if (start_test!=0){MOTOR_MBDFLEXSTART_speed_s16=750;
	 nb_test=0;}

	return MOTOR_MBDFLEXSTART_speed_s16;
}

