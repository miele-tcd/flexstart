/**
 * @file moving_average.h
 * @brief moving average implementation without statics --> reusable
 * @date 28.04.2021
 * @author dewernt
 */

#ifndef MOVING_AVERAGE_H_
#define MOVING_AVERAGE_H_

#include <stdint.h>

/**
 * @return moving average of inputs
 * @param state pointer to state var. must be a variable that is only touched by moving average function
 * @param input input value
 * @param strength strength of filter (min 1, max 15)
 * */
int32_t moving_average(int64_t* state, int32_t input, int8_t strength);

#endif /* MOVING_AVERAGE_H_ */
