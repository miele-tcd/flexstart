/*
 * Copyright 2016-2018 NXP Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file    main.c
 * @brief   program entry
 *
 * This program is the hardware abstraction layer for the mpp inverter.
 * Procedure is as follows:
 *
 * 1. init hardware (clocks, ports, timers)
 * 2. init freemaster
 * 3. indefinite loop with sleep mode. is entered any time all interrupts are done
 *
 * the program is mainly interrupt driven. Enabled Interrupts are:
 * - SysTick in-cpu-clock timed on 1ms period for system interrupt. serves as a system timer
 * - ADC1 is triggered at the end of all conversions after a pwm event. it is launched in 8kHz speed.
 *   Here, inverter data is fetched and processed. inverter is programmed for the next phase.
 *
 * - UART1 is the freemaster uart event. it is necessary for freemaster communication
 */

#include "pin_mux.h"
#include "clock_config.h"
#include "peripherals.h"
#include "inverter.h"
#include "freemaster.h"
#include "flexstart.h"
#include "tests.h"
#include "Types.h"
#include "Motor_1pHController.h"
/*******************************************************************************
 * Definitions
 ******************************************************************************/
/*
 * @brief   Application entry point.
 */

// system time in milliseconds
static ms system_time = 0; //ms
// system time reset value. system time is set to zero after this time
volatile static ms MOTOR_MBDFLEXSTART_reset_interval_s64 = 6000; // ms
volatile static uint8_t MOTOR_MBDFLEXSTART_start_test_u8=0, MOTOR_MBDFLEXSTART_step_test_u8=2;
// direction indicator for FlexStart
volatile static Direction direction = CW;
volatile static Test_method test_method =Alternate;
#define TESTS_MOTOR_MBDFLEXSTART
//#define MANUAL_TESTS
//------------------------------------------- Inputs Interrupt 1k -----------------------------------------------
#define MOTOR_MBDFLEXSTART_MOTOR_ON										1
#define MOTOR_MBDFLEXSTART_SPEED_COMMAND						  0 //750
#define MOTOR_MBDFLEXSTART_CURRENT_COMMAND				20000
#define MOTOR_MBDFLEXSTART_VOLTAGE_REF              800
#define MOTOR_MBDFLEXSTART_RAMP_COMMAND								0
#define MOTOR_MBDFLEXSTART_BOOST_COMMAND_POS			 2300
#define MOTOR_MBDFLEXSTART_BOOST_COMMAND_NEG			 2300 //720
#define MOTOR_MBDFLEXSTART_BEMF_COMMAND								0
#define MOTOR_MBDFLEXSTART_SPEED_COMMAND_VF_POS			750
#define MOTOR_MBDFLEXSTART_SPEED_COMMAND_VF_NEG		 -750
//-----------------------------------------------End-------------------------------------------------------------

//------------------------------------------- Inputs Interrupt 8k -----------------------------------------------
#define MOTOR_MBDFLEXSTART_FAULT_CLEAR								0
#define MOTOR_MBDFLEXSTART_DIRECTION									0
#define MOTOR_MBDFLEXSTART_FAULT1									    1
#define MOTOR_MBDFLEXSTART_FAULT2										  0
#define MOTOR_MBDFLEXSTART_ALIGN_DELAY							300
#define MOTOR_MBDFLEXSTART_START_CTR								  0
#define MOTOR_MBDFLEXSTART_START_BEMF_MEAS					  1
#define MOTOR_MBDFLEXSTART_START_ANGLE_POS			 -11000//0
#define MOTOR_MBDFLEXSTART_START_ANGLE_NEG			  14500
#define MOTOR_MBDFLEXSTART_IS_KP									  328
#define MOTOR_MBDFLEXSTART_IS_KI										 41
#define MOTOR_MBDFLEXSTART_BEMF_KP										0
#define MOTOR_MBDFLEXSTART_BEMF_KI									  0
#define MOTOR_MBDFLEXSTART_SPEED_KP								    0
#define MOTOR_MBDFLEXSTART_SPEED_KI								    0

//-----------------------------------------------End-------------------------------------------------------------

//------------------------------------------- Inputs Interrupt 1k -----------------------------------------------
uint16_t MOTOR_MBDFLEXSTART_motor_on_u16        		 = MOTOR_MBDFLEXSTART_MOTOR_ON;
int16_t  MOTOR_MBDFLEXSTART_speed_command_s16   		 = MOTOR_MBDFLEXSTART_SPEED_COMMAND;
int16_t  MOTOR_MBDFLEXSTART_current_command_s16      = MOTOR_MBDFLEXSTART_CURRENT_COMMAND;
int16_t  MOTOR_MBDFLEXSTART_ramp_command_s16         = MOTOR_MBDFLEXSTART_RAMP_COMMAND;
int16_t  MOTOR_MBDFLEXSTART_boost_command_pos_s16    = MOTOR_MBDFLEXSTART_BOOST_COMMAND_POS;
int16_t  MOTOR_MBDFLEXSTART_boost_command_neg_s16    = MOTOR_MBDFLEXSTART_BOOST_COMMAND_NEG;
int16_t  MOTOR_MBDFLEXSTART_speed_command_vf_pos_s16 = MOTOR_MBDFLEXSTART_SPEED_COMMAND_VF_POS;
int16_t  MOTOR_MBDFLEXSTART_speed_command_vf_neg_s16 = MOTOR_MBDFLEXSTART_SPEED_COMMAND_VF_NEG;

//-----------------------------------------------End-------------------------------------------------------------

//------------------------------------------- Inputs Interrupt 8k -----------------------------------------------
uint16_t MOTOR_MBDFLEXSTART_fault_clear_u16  			= MOTOR_MBDFLEXSTART_FAULT_CLEAR;
uint8_t MOTOR_MBDFLEXSTART_Direction_u8         = MOTOR_MBDFLEXSTART_DIRECTION;
uint16_t MOTOR_MBDFLEXSTART_fault1_u16  					= MOTOR_MBDFLEXSTART_FAULT1;
uint16_t MOTOR_MBDFLEXSTART_fault2_u16  					= MOTOR_MBDFLEXSTART_FAULT2;
uint16_t MOTOR_MBDFLEXSTART_align_delay_u16   	  = MOTOR_MBDFLEXSTART_ALIGN_DELAY;
uint16_t MOTOR_MBDFLEXSTART_start_ctr_u16 				= MOTOR_MBDFLEXSTART_START_CTR;
int16_t  MOTOR_MBDFLEXSTART_start_angle_pos_s16   = MOTOR_MBDFLEXSTART_START_ANGLE_POS;
int16_t  MOTOR_MBDFLEXSTART_start_angle_neg_s16   = MOTOR_MBDFLEXSTART_START_ANGLE_NEG;

//-----------------------------------------------End-------------------------------------------------------------

//------------------------------------------- Outputs Interrupt 1k -----------------------------------------------
uint16_t MOTOR_MBDFLEXSTART_controller_mode_u16;
int16_t  MOTOR_MBDFLEXSTART_speed_reference_s16;
int16_t  MOTOR_MBDFLEXSTART_voltage_reference_s16;
int16_t  MOTOR_MBDFLEXSTART_current_reference_s16;
//int16_t  MOTOR_MBDFLEXSTART_bemf_reference_s16;
//-----------------------------------------------End-------------------------------------------------------------

//------------------------------------------- Outputs Interrupt 8k -----------------------------------------------
uint16_t MOTOR_MBDFLEXSTART_controller_State_u16;
int16_t  MOTOR_MBDFLEXSTART_Vs_s_s16;
uint16_t MOTOR_MBDFLEXSTART_PWM_CMD1_u16;
uint16_t MOTOR_MBDFLEXSTART_PWM_CMD2_u16;
uint16_t MOTOR_MBDFLEXSTART_PWM_OFF_u16;
int16_t  MOTOR_MBDFLEXSTART_voltage_ref_s16 = MOTOR_MBDFLEXSTART_VOLTAGE_REF;
int16_t  MOTOR_MBDFLEXSTART_InvVoltageOut_s16;
int16_t  MOTOR_MBDFLEXSTART_old_InvVoltageOut_s16;
//-----------------------------------------------End-------------------------------------------------------------


int main(void) {

	/* Init board hardware. */


	BOARD_InitBootPins(); // ./board/pin_mux.c: pin init. and conf.
	BOARD_InitBootClocks(); // ./board/clock_config.c: clock init. and conf.
	BOARD_InitBootPeripherals(); // ./board/peripherals.c: user init. and conf.
	Motor_1pHController_initialize();

#ifdef FREEMASTER
	FMSTR_Init(); // Freemaster UART
#endif

	while (1) {
		__WFI(); // enter sleep mode if nothing is to be done
	}

	return 0;
}

//-----------------------------------Commented by Mansour 02/12/2021--------------------------------------------------

/*Systick Interrupt 1k
void SysTick_Handler(void) {
	system_time++; // ms
}

ADC1 Interrupt 8k
void ADC1_IRQ_HANDLER(void) {
	// get adc data
	Adc results = INV_process_adc_measurements_tst();

	// compute new flex start output
	Result r = FLEX_interrupt_handler(system_time, direction, results.I_dc);

	INV_output_active_v(r.active); // set output clear or false
	INV_set_output_voltage_v(r.voltage); // set output voltage

	// make sure startup process is repeated
	static uint8_t reset_flag = 0; // reset flag to avoid multiple executions of reset
	if (!(system_time % MOTOR_MBDFLEXSTART_reset_interval_s64)) {
		if(!reset_flag){
			FLEX_reset(system_time); // reset flex start control
			direction = !direction; // invert direction
			reset_flag = 1; // make sure this is only executed once (this interrupt runs 8times faster than systick...)
		}
	}else{
		reset_flag = 0; // give way for next reset
	}
}*/

//-----------------------------------------------End---------------------------------------------------------------


//-----------------------------------Added by Mansour 02/12/2021---------------------------------------------------
/*Systick Interrupt 1k*/
void SysTick_Handler(void) {
	 // ms
	// get adc data
	Adc results = INV_process_adc_measurements_tst();
#ifdef MANUAL_TESTS
	if (MOTOR_MBDFLEXSTART_start_test_u8==0){
	MOTOR_MBDFLEXSTART_Direction_u8      = direction;
	MOTOR_MBDFLEXSTART_speed_command_s16 = 0;
	}else{
		MOTOR_MBDFLEXSTART_Direction_u8      = direction;
	  MOTOR_MBDFLEXSTART_speed_command_s16 = (MOTOR_MBDFLEXSTART_Direction_u8 == CW ? 750 : -750);}
#endif
// Switch case to select test to be done, this section can be skipped by comment out line 74, uncomment line 75 and control will be from freemaster
#ifdef TESTS_MOTOR_MBDFLEXSTART
switch (test_method)
{
case Angle_sweep:
	if (MOTOR_MBDFLEXSTART_start_test_u8==0 && MOTOR_MBDFLEXSTART_start_angle_neg_s16>-32000 && MOTOR_MBDFLEXSTART_start_angle_neg_s16<32000){
		MOTOR_MBDFLEXSTART_speed_command_s16=0;
		MOTOR_MBDFLEXSTART_start_angle_neg_s16=14500;
		Angle_sweep_s16(MOTOR_MBDFLEXSTART_reset_interval_s64, MOTOR_MBDFLEXSTART_start_test_u8);
	}else{
		ANgle_sweep_struct A_SWP=Angle_sweep_s16(MOTOR_MBDFLEXSTART_reset_interval_s64, MOTOR_MBDFLEXSTART_start_test_u8);
		MOTOR_MBDFLEXSTART_start_angle_neg_s16 = A_SWP.angle;
		MOTOR_MBDFLEXSTART_speed_command_s16 = A_SWP.speed;
		if (MOTOR_MBDFLEXSTART_speed_command_s16>0){MOTOR_MBDFLEXSTART_Direction_u8 = 1;}
		else {MOTOR_MBDFLEXSTART_Direction_u8 = 0;}
	}
	break;
case Alternate:
	if(MOTOR_MBDFLEXSTART_start_test_u8==0){MOTOR_MBDFLEXSTART_start_angle_neg_s16=14500;}
	MOTOR_MBDFLEXSTART_speed_command_s16 = Alternate_s16(MOTOR_MBDFLEXSTART_reset_interval_s64, MOTOR_MBDFLEXSTART_start_test_u8);
	if (MOTOR_MBDFLEXSTART_speed_command_s16>0){MOTOR_MBDFLEXSTART_Direction_u8 = 1;}
	else {MOTOR_MBDFLEXSTART_Direction_u8 = 0;}
	break;
case Separately:
	if(MOTOR_MBDFLEXSTART_start_test_u8==0){MOTOR_MBDFLEXSTART_start_angle_neg_s16=14500;}
	MOTOR_MBDFLEXSTART_speed_command_s16 = Separately_s16(MOTOR_MBDFLEXSTART_reset_interval_s64, MOTOR_MBDFLEXSTART_start_test_u8);
	if (MOTOR_MBDFLEXSTART_speed_command_s16>0){MOTOR_MBDFLEXSTART_Direction_u8 = 1;}
	else {MOTOR_MBDFLEXSTART_Direction_u8 = 0;}
	break;
case Alternate_step_2:
	if(MOTOR_MBDFLEXSTART_start_test_u8==0){MOTOR_MBDFLEXSTART_start_angle_neg_s16=14500;}
	MOTOR_MBDFLEXSTART_speed_command_s16 = Alternate_step_2_s16(MOTOR_MBDFLEXSTART_reset_interval_s64, MOTOR_MBDFLEXSTART_start_test_u8, MOTOR_MBDFLEXSTART_step_test_u8);
	if (MOTOR_MBDFLEXSTART_speed_command_s16>0){MOTOR_MBDFLEXSTART_Direction_u8 = 1;}
	else {MOTOR_MBDFLEXSTART_Direction_u8 = 0;}
	break;
case Heating:
	if (MOTOR_MBDFLEXSTART_start_test_u8!=0){
		  system_time++;
	    MOTOR_MBDFLEXSTART_speed_command_s16 = Heating_s16(system_time, MOTOR_MBDFLEXSTART_reset_interval_s64, MOTOR_MBDFLEXSTART_start_test_u8);
			if (MOTOR_MBDFLEXSTART_speed_command_s16>0){MOTOR_MBDFLEXSTART_Direction_u8 = 1;}
			else {MOTOR_MBDFLEXSTART_Direction_u8 = 0;}
	}else{system_time=0;
	    MOTOR_MBDFLEXSTART_speed_command_s16 = Heating_s16(system_time, MOTOR_MBDFLEXSTART_reset_interval_s64, MOTOR_MBDFLEXSTART_start_test_u8);
	    MOTOR_MBDFLEXSTART_start_angle_neg_s16=14500;}
	break;

default:
	//MOTOR_MBDFLEXSTART_start_test_u8=0;
	MOTOR_MBDFLEXSTART_speed_command_s16=0;
}
#endif

	//
	//------------------------------------------- Inputs Interrupt 1k -----------------------------------------------
	Motor_1pHController_U.motor_on 							= MOTOR_MBDFLEXSTART_motor_on_u16;
	Motor_1pHController_U.speed_command					= MOTOR_MBDFLEXSTART_speed_command_s16;
	Motor_1pHController_U.current_command				= MOTOR_MBDFLEXSTART_current_command_s16;
	Motor_1pHController_U.ramp_command					= MOTOR_MBDFLEXSTART_ramp_command_s16;
	Motor_1pHController_U.boost_command_pos			= MOTOR_MBDFLEXSTART_boost_command_pos_s16;
	Motor_1pHController_U.boost_command_neg			= MOTOR_MBDFLEXSTART_boost_command_neg_s16;
	Motor_1pHController_U.vdc_adc								= (int16_t)(results.V_dc);
	Motor_1pHController_U.speed_command_vf_pos	= MOTOR_MBDFLEXSTART_speed_command_vf_pos_s16;
	Motor_1pHController_U.speed_command_vf_neg	= MOTOR_MBDFLEXSTART_speed_command_vf_neg_s16;
	//-----------------------------------------------End-------------------------------------------------------------

	Motor_1pHController_step1();

	//------------------------------------------- Outputs Interrupt 1k -----------------------------------------------

  MOTOR_MBDFLEXSTART_speed_reference_s16 			= Motor_1pHController_B.speed_reference;


	//-----------------------------------------------End-------------------------------------------------------------
}

/*ADC1 Interrupt 8k */
void ADC1_IRQ_HANDLER(void) {

	// get adc data
	Adc results = INV_process_adc_measurements_tst();

	//------------------------------------------- Inputs Interrupt 8k -----------------------------------------------
	Motor_1pHController_U.fault_clear				= MOTOR_MBDFLEXSTART_fault_clear_u16;
	Motor_1pHController_U.Direction					= MOTOR_MBDFLEXSTART_Direction_u8;
	Motor_1pHController_U.fault1						= MOTOR_MBDFLEXSTART_fault1_u16;
	Motor_1pHController_U.fault2						= MOTOR_MBDFLEXSTART_fault2_u16;
	Motor_1pHController_U.align_delay				= MOTOR_MBDFLEXSTART_align_delay_u16;
	Motor_1pHController_U.start_ctr					= MOTOR_MBDFLEXSTART_start_ctr_u16;
	Motor_1pHController_U.start_angle_pos		= MOTOR_MBDFLEXSTART_start_angle_pos_s16;
	Motor_1pHController_U.start_angle_neg		= MOTOR_MBDFLEXSTART_start_angle_neg_s16;

	//-----------------------------------------------End-------------------------------------------------------------

	Motor_1pHController_step0();

	//------------------------------------------- Outputs Interrupt 8k -----------------------------------------------
	MOTOR_MBDFLEXSTART_Vs_s_s16						    = Motor_1pHController_Y.Vs_s;
	MOTOR_MBDFLEXSTART_PWM_OFF_u16						= Motor_1pHController_Y.PWM_OFF;

	//-----------------------------------------------End-------------------------------------------------------------

	INV_output_active_v(!MOTOR_MBDFLEXSTART_PWM_OFF_u16); // set output clear or false
	INV_set_output_voltage_v(MOTOR_MBDFLEXSTART_Vs_s_s16);
	//Record();


}
//-----------------------------------------------End---------------------------------------------------------------

/* Uart Interrupt Handler */
//void UART0_IRQHandler(void) {
#ifdef FREEMASTER
void UART_HANDLER(void){
	FMSTR_Isr();
}

#endif
