/**
 * @file flexstart.h
 * @brief flexstart controls
 * @date 22.04.2021
 * @author dewernt
 */

#ifndef FLEXSTART_H_
#define FLEXSTART_H_

#include "Types.h"

typedef enum {
	CW, CCW
} Direction;

typedef struct Result {
	uint8_t active;
	mV voltage;
} Result;

/**
 * @brief interrupt handler for flex start. should be issued at least every 1ms
 * @param system_time system time in milliseconds
 * @param dir direction in which motor should start
 * @param current actual current running through motor in uA */
Result FLEX_interrupt_handler(ms system_time, Direction dir, uA current);

/**
 * @brief resets flex start system. motor will redo startup process
 * @param system_time current system time in milliseconds */
void FLEX_reset(ms system_time);

#endif /* FLEXSTART_H_ */
