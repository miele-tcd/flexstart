/**
 * @file sinus.h
 * @brief module for sinus and cosinus computation without floats
 * @date 22.04.2021
 * @author dewernt
 */

#ifndef SINUS_H_
#define SINUS_H_

#include "stdint.h"

typedef struct {
	int16_t sin;
	int16_t cos;
}SinCos;


/**
 * @brief calculates sinus and cosinus values for certain angle
 * @param angle angle input in rad/2/pi*65536
 * @returns struct of sinus and cosinus values, normalized to int16 (1-->32767, -1 --> -32767) */
SinCos sin_cos(uint16_t angle);

#endif /* SINUS_H_ */
