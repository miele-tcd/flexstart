/**
 * @file inverter.c
 * @brief contrains interface for generated inverter code to be executed on interrupt
 * @date 11.08.2020
 * @author dewernt
 * @copyright Miele&Cie KG 2020(c)
 */

#include "stdint.h"
#include "peripherals.h"
#include "fsl_adc16.h"
#include "fsl_cmp.h"
#include "fsl_ftm.h"
#include "inverter.h"
#include "Types.h"

static Adc INV_adc_data_st = { 0, 0, 0, 0 };
//static uint8_t percent;
 static uint16_t INV_ftm_mod_reg_value=4500;
 static uint16_t H4_duty[2];
mV INV_get_V_dc(void) {
	return INV_adc_data_st.V_dc;
}
mV INV_get_V_s1(void) {
	return INV_adc_data_st.V_s1;
}
mV INV_get_V_s2(void) {
	return INV_adc_data_st.V_s2;
}
uA INV_get_I_dc(void) {
	return INV_adc_data_st.I_dc;
}

void INV_set_output_voltage_v(mV voltage) {
    // pwm output duty vars
   // uint16_t H4_duty[2];

    // check voltage polarity
    if (voltage > 0) {
        if (INV_adc_data_st.V_dc < voltage) {
            voltage = INV_adc_data_st.V_dc;
        }
        H4_duty[0] = INV_ftm_mod_reg_value - (INV_ftm_mod_reg_value * voltage / INV_adc_data_st.V_dc);
        H4_duty[1] = INV_ftm_mod_reg_value;
    }
    else {
        // limit input to bus voltage (maximum output)
        if (INV_adc_data_st.V_dc <- voltage) {
            voltage = INV_adc_data_st.V_dc;
        }
        H4_duty[1] = INV_ftm_mod_reg_value + (INV_ftm_mod_reg_value * voltage / INV_adc_data_st.V_dc);
        H4_duty[0] = INV_ftm_mod_reg_value;
    }

    // update hardware pwm with calculation
    FTM_BASEADDR->CONTROLS[kFTM_Chnl_2].CnV = H4_duty[0];
    FTM_BASEADDR->CONTROLS[kFTM_Chnl_4].CnV = H4_duty[1];

    //Load Enable bit: all values are updated at the same instance
    FTM_BASEADDR->PWMLOAD = FTM_PWMLOAD_LDOK_MASK;
}

/*void INV_set_output_voltage_v(uint16_t TON1, uint16_t TON2) {

	//set duty in percent
	FTM_BASEADDR->CONTROLS[kFTM_Chnl_2].CnV = 4500 - TON1;
	FTM_BASEADDR->CONTROLS[kFTM_Chnl_4].CnV = 4500 - TON2;

	//Load Enable bit: all values are updated at the same instance
	FTM_BASEADDR->PWMLOAD = FTM_PWMLOAD_LDOK_MASK;
}*/
void INV_output_active_v(uint8_t active) {
	if (active) {
		//Activate PWM output.
		FTM_SetOutputMask(FTM_BASEADDR, kFTM_Chnl_2, false);
		FTM_SetOutputMask(FTM_BASEADDR, kFTM_Chnl_3, false);
		FTM_SetOutputMask(FTM_BASEADDR, kFTM_Chnl_4, false);
		FTM_SetOutputMask(FTM_BASEADDR, kFTM_Chnl_5, false);
	}
	else {
		//Outmask PWM output.
		FTM_SetOutputMask(FTM_BASEADDR, kFTM_Chnl_2, true);
		FTM_SetOutputMask(FTM_BASEADDR, kFTM_Chnl_3, true);
		FTM_SetOutputMask(FTM_BASEADDR, kFTM_Chnl_4, true);
		FTM_SetOutputMask(FTM_BASEADDR, kFTM_Chnl_5, true);
	}
}

Adc INV_process_adc_measurements_tst(void) {
	// enumeration to identify values in adc result array
	enum {
		V_s1, V_s2, I_s, V_dc
	};

	int16_t adc_result_as16[4];
	// loop over all adc outputs and store value in result array
	for (uint8_t i = 0; i < 4; i++) {
		// all values are transformed from 12 to 16 bit values
		adc_result_as16[i] = ADC16_GetChannelConversionValue(i % 2 ? ADC_BASE1 : ADC_BASE0, i / 2);
	}

	// current offset = half digits on a 12dig adc --> 1 << 11
	static const int16_t INV_I_s_offset_s16 = 1 << 11;

	// save V_dc in static variable for freemaster display
	//INV_adc_data_st.V_dc = (adc_result_as16[V_dc] * 401714L) >> 12; // v_adc * 3,3V / 4096dig * 998,2kOhm / 8,2kOhm * 1000 mV/V = v_adc * 98.07 = v_adc * 401714 / 4096
	//INV_adc_data_st.V_s1 = (adc_result_as16[V_s1] * 377300L) >> 12; // v_adc * 3,3V / 4096dig * 205.8kOhm / 1.8kOhm * 1000 mV/V = v_adc * 92.11 = v_adc * 377300 / 4096
	//INV_adc_data_st.V_s2 = (adc_result_as16[V_s2] * 377300L) >> 12; // v_adc * 3,3V / 4096dig * 205.8kOhm / 1.8kOhm * 1000 mV/V = v_adc * 92.11 = v_adc * 377300 / 4096
  //INV_adc_data_st.I_dc = ((adc_result_as16[I_s] - INV_I_s_offset_s16) * 264000L) >> 15; // v_adc * 3,3V / 4096dig / (0.5*100Ohm) * 1000000uA/A = v_adc * 8.06 = v_adc * 264000 / 32768

	//------------- Current Factor corrected by Mansour --------------------------------
  INV_adc_data_st.V_dc = (adc_result_as16[V_dc] * 1004) >> 10;                        // v_adc * 3,3V / 4096dig * 990kOhm / 8,2kOhm * 10 /V = v_adc * 0.9807 = v_adc * 1004 / 1024
  INV_adc_data_st.V_s1 = (adc_result_as16[V_s1] * 935) >> 10;                         // v_adc * 3,3V / 4096dig * 204kOhm / 1.8kOhm * 10 /V = v_adc * 0.9131 = v_adc * 935 / 1024
  INV_adc_data_st.V_s2 = (adc_result_as16[V_s2] * 935) >> 10;                         // v_adc * 3,3V / 4096dig * 204kOhm / 1.8kOhm * 10 /V = v_adc * 0.9131 = v_adc * 935 / 1024
	INV_adc_data_st.I_dc = ((adc_result_as16[I_s] - INV_I_s_offset_s16) * 16504) >> 10; // v_adc * 3,3V / 4096dig / (0.5*100Ohm) * 1000000uA/A = v_adc * 16.11 = v_adc * 16504 / 1024
	//------------------------End ------------------------------------------------------

	return INV_adc_data_st;
}

