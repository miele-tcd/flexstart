/**
 * @file inverter.h
 * @brief contrains interface for generated inverter code to be executed on interrupt
 * @date 11.08.2020
 * @author dewernt
 * @copyright Miele&Cie KG 2020(c)
 */

#ifndef INVERTER_H_
#define INVERTER_H_

#include "Types.h"

typedef struct Adc {
	uA I_dc;
	mV V_s1;
	mV V_s2;
	mV V_dc;
}Adc;


mV INV_get_V_dc(void);
mV INV_get_V_s1(void);
mV INV_get_V_s2(void);
uA INV_get_I_dc(void);

/**
 * @brief event handler for pwm output
 * @return struct with all adc values*/
Adc INV_process_adc_measurements_tst(void);

/**
 * @brief updates the duty output of inverter. *
 * @param voltage output voltage in mV */
void INV_set_output_voltage_v(mV voltage);

//void INV_set_output_voltage_v(uint16_t TON1, uint16_t TON2);

/**
 * @brief if active > 0 then activate pwm output on hardware, else not */
void INV_output_active_v(uint8_t active);

#endif /* INVERTER_H_ */
