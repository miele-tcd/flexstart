/**
 * @file test_pin.c
 * @brief test pin peripheral access for algorithm time measurement
 * @date 11.08.2020
 * @author dewernt
 * @copyright Miele&Cie KG 2020(c)
 */

#include "fsl_gpio.h"
#include "MKV10Z1287.h"
#include "test_pin.h"

void TSTPIN_init(void){

}

void TSTPIN_set(void){
	GPIO_PortSet(GPIOD, 1U << 6U);
}

void TSTPIN_clear(void){
	GPIO_PortClear(GPIOD, 1U << 6U);
}
