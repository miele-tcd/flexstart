/*
 * moving_average.c
 *
 *  Created on: 28.04.2021
 *      Author: dewernt
 */

#include "moving_average.h"

int32_t moving_average(int64_t* state, int32_t input, int8_t strength){
	*state += input;
	*state -= *state >> strength;
	return *state >> strength;
}
