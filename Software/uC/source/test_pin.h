/**
 * @file test_pin.h
 * @brief test pin peripheral access for algorithm time measurement
 * @date 11.08.2020
 * @author dewernt
 * @copyright Miele&Cie KG 2020(c)
 */


#ifndef TESTPIN_H_
#define TESTPIN_H_

/**
 * @brief initializes peripheral */
void TSTPIN_init(void);

/**
 * @brief sets output of pin */
void TSTPIN_set();
/** clears test pin */
void TSTPIN_clear();

#endif /*TESTPIN_H_*/
