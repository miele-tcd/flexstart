/*
 * flexstart.c
 *
 *  Created on: 22.04.2021
 *      Author: dewernt
 */

#include "flexstart.h"
#include "sinus.h"
#include "inverter.h"
#include "moving_average.h"

volatile static mV FLEX_sine_amplitude_min = 50000; // mV non-controlled output voltage amplitude
volatile static mV FLEX_sine_amplitude = 200000; // mV result output voltage
volatile static mV FLEX_sine_amp_control = 200000; //mV voltage amplitude added on minimum according to normal motor current
volatile static mV FLEX_sine_amp_factor = 32768; // amplification factor for controlled voltage part
volatile static mV FLEX_alignment_voltage = -50000; // mV dc voltage for motor alignment
volatile static ms FLEX_t_break1 = 200; //ms
volatile static ms FLEX_t_align = 300; //ms
volatile static ms FLEX_t_break2 = 200; //ms
volatile static Hz FLEX_frequency = 50; // Hz
volatile static ms FLEX_t_start = 0; // start time of flex start in milliseconds

// reset flex start system
void FLEX_reset(ms system_time){
	FLEX_t_start = system_time;
}

// filter current signal from adc
static uA current_filter(uA current){
	static int64_t state = 0;
	static const uint8_t scale = 14;
	return moving_average(&state, current, scale);
}

Result FLEX_interrupt_handler(ms system_time, Direction dir, uA current){
	// calc flex start time
	ms runtime = system_time - FLEX_t_start;
	// state machine
	Result res = {0,0}; // default output: no voltage, inactive
	if(runtime < FLEX_t_break1){
		// disable output. do nothing
	}else if(runtime >= FLEX_t_break1 && runtime < (FLEX_t_break1 + FLEX_t_align)){
		// alignment with postive voltage
		res.voltage = FLEX_alignment_voltage;
		res.active = 1;
	}else if(runtime >= (FLEX_t_break1 + FLEX_t_align) && runtime < (FLEX_t_break1 + FLEX_t_align + FLEX_t_break2)){
		// pause again. return default;
	}else{
		// adapt output voltage to current
		// filter current
		current = current_filter(current);
		// calc normal no load current for this amplitude. I_normal/uA = 0.1182 * V_amp/mV - 12880
		uA current_normal = (((int64_t)FLEX_sine_amplitude * 3873) >> 15) - 12880;
		mV voltage_controlled = ((int64_t)FLEX_sine_amp_control * current / current_normal * FLEX_sine_amp_factor) >> 14;
		FLEX_sine_amplitude = FLEX_sine_amplitude_min + voltage_controlled;

		// sinus output
		const int32_t angular_speed = FLEX_frequency << 16; // 50Hz
		SinCos x = sin_cos((int64_t)angular_speed * (runtime - (FLEX_t_break1 + FLEX_t_align + FLEX_t_break2)) / 1000);
		res.voltage = ((int64_t)FLEX_sine_amplitude * (dir == CW ? x.sin : x.cos)) >> 15;
		res.active = 1;
	}
	return res;
}
