% initialization for Library_Example_PMSM_1Ph.slx
% 
% load PMSM and Load data from <simparam.mat>, with the contents:
% - PMSM. <tunable parameter in ref. model>
%    - p                    : Pole pairs
%    - psiF20               : PM-Flux (Sum all Poles)/ Vs
%    - Rstr                 : Resistance (Sum all Poles) / Ohm
%    - Ls                   : Inductance with Saturation (Sum all Poles) / H
%    - Lookup.is_hat        : x-axis Lookup, akt. current / A
%    - Lookup.alpha_el_rad  : y-axis Lookup, akt. Rotorposition / rad
%    - Mrel                 : Reluctance Torque Amp. / Nm
%    - teta0                : Rotor natural angle / rad
%    - Jrotor_kgm2          : Rotor inertia (from CAD) / kg m2
%    - IronLoss.Mv          : Lookup Iron Loss (Torque Losses) / Nm
%    - IronLoss.n           : Lookup axis for Iron Losses / rpm
%    - IronLoss.is_hat      : Lookup axis for Iron Losses / A (Amplitude)
%    - ExtraLoss.Mv         : Lookup Friction Loss (Torque Losses) / Nm
%    - ExtraLoss.n          : Lookup axis for Friction Losses / rpm
% - Load. <tunable parameter outside of ref. model>
%    - M_W2_29mm                : Load W2 impeller 29 mm / Nm
%    - M_W2_33mm                : Load W2 impeller  33 mm / Nm
%    - n                        : x-axis Lookup, Rot. Speed / rpm
%
% Autor: Aryanti Putri (EU)
%
% Version: 18.12.2020
% MATLAB-Version: R2019a
%
clear all
close all

run Elektronik_TEC152.m % Elektronik TEC
run init_flexstart.m % flexstart parameter

%% Input
simparam = 'SimParam_MppXX_3000wdg_0.224mm_20C_Lib.mat';
N1 = 3000; d1 = 0.224; % design 1: var 230 Vrms
N2 = 3200; d2 = 0.198; % var 190 Vrms

tsim = 2.0; % max. Simulation time / s
Tsample = 1e-4; % Sample Time
fel_rms = 10; % Frequency to create rms values

% alignment
t_align = 0.3; % time to perform alignment / s ( have to be greater than electric and mechanical time constant)

% open loop
cramp = 0; % a constant to create voltage Ramp for open loop / V (amplitude), Uramp = 2*pi*fel*cramp (cramp could be time dependent)
speed_vf=2000;  % [rpm] Speed command during Vf phase 

% control
bemf_soll = 0; % BEMF value (zero --> current and bemf almost in phase)

% Speed
n_soll = [0  1.5  2.0  2.5  3.0  3.5  4.0  4.5  5.0  5.5  6.0; ...
        2000 2250 2500 2750 3000 3250 3500 3750 4000 4250 4500]; % time / s; speed / rpm

% Temperaturen
Tcu = 20; % � C

%% Parameter for Motor / Elektronic
I_align = 0.27; % Current Amplitude to perform alignment / A
Uboost = 150; % Boost Command / V (amplitude), boost value of the voltage to perform open loop, Uref = Uboost + Uramp, var 100 Vrms
start_angle = 0; % voltage start angle

%% Machine Parameter
load(simparam)
% Auswahl Motor, Parameter manuell �ndern: 
% -I_align, 
% -Uboost,
% -inverter.BusVoltage

PMSM.cT_wind = 3.93e-3; % Temperatur koeffizient Wicklung
PMSM.cT_PM = -0.0020; % Temperatur koeffizient Ferrit
PMSM.alpha0_rotor_rad = deg2rad(7.3); %  Rotor Start position

% Skalierung nach Windungszahl
PMSM.Rstr = PMSM.Rstr*(N2/N1)*(d1/d2)^2*(1+PMSM.cT_wind*(Tcu-20)); % Resistance (Sum all poles) / Ohm, 
PMSM.psiF20= PMSM.psiF20*(N2/N1);  % Permanent magnet flux (Sum all poles) Amplitude / Vs
PMSM.Ls = mean(mean(PMSM.Ls *((N2/N1)^2))); % inductance vs rotor angle & current
PMSM.Lookup.is_hat = PMSM.Lookup.is_hat*(N1/N2); % current amplitude / A
PMSM.IronLoss.is_hat = PMSM.IronLoss.is_hat*(N1/N2);

PMSM.psiF20= PMSM.psiF20*0.976; % Schnittanpassung f�r Konstruktion, 17.08.2020

%% Load
Load.Mload = Load.M_W2_33mm;

%% Umrichter + Controller
Inverter.SlowInterrupt = 1/HALCPU_IRQ_SYSTICK_FREQUENCY_HZ; % 1 kHz interrupt, dt in s
Inverter.fPWM=HALCPU_PWM3PHASE_FREQ; % Switching frequency 
Inverter.TPWM=1/Inverter.fPWM; % 8 kHz interrupt, dt in s

% Motor and Electronic Parameter
Inverter.HalfBridge.Vdc = 270;  % DC Bus voltage / V (190*sqrt(2))
Inverter.Tdead=1e-6;         % Dead time / s
% ------------------------------Inverter-----------------------------------

% PWM
PWM_MAX_DUTYCYCLE = uint16(HALCPU_PWM3PHASE_TPWM_COUNT);

Inverter.HalfBridge.Switch.Ron=2e-3;        % MOSFET Resistance / Ohm
Inverter.HalfBridge.Switch.Lon = 0;       % MOSFET Diode Inductance / H
Inverter.HalfBridge.Switch.Rd = 0.01;       % MOSFET Diode Resistance / Ohm
Inverter.HalfBridge.Switch.Vf = 0.9;       % MOSFET Diode Forward Voltage / V
Inverter.HalfBridge.Switch.Rs = 1e5;       % MOSFET Snubber Resistance / Ohm
Inverter.HalfBridge.Switch.Cs = inf;       % MOSFET Snubber Capacitor / Ohm

Inverter.TzeroCrossing = 5e-3;  % start detect zero crossing after this time / s, calculated from max. speed. Here: 5000rpm / 60 * p = 83.3 Hz --> 12 ms. Sinufunc. has 2 ZeroCrossing --> max. 6 ms)
Inverter.TdelayMeas = 500e-6; % delay before measuring bemf / s, depends on filter time constant, depends on the hardware, determined empirically
Inverter.Rshunt = 2; % Ohm


% --------------------------------Filter----------------------------------
% Compare Filter with the per Unit Values!
% Filter for Vs1 und Vs2 Measurement - hardware dependent
Filter.R1=204e3; % Ohm
Filter.R2=1.8e3; % Ohm
Filter.C=22e-9; % Farad
% Filter for Vdc Measurement - hardware dependent
Filter.Rdc1=990e3; % Ohm
Filter.Rdc2=8.2e3; % Ohm
Filter.Cdc=10e-9; % Farad
% Filter for Ishunt Measurement - hardware dependent
Filter.Rshunt1=1e3; % Ohm
Filter.Rshunt2=1e3; % Ohm
Filter.Cshunt=0; % Farad

zerocrossing = round(Inverter.TzeroCrossing/Inverter.TPWM); % detect current zero crossing, normed to interrupt 8kHz
delay=round(Inverter.TdelayMeas/Inverter.TPWM); % delay for voltage measurement, normed to interrupt 8kHz

% ------------------------------Alignment----------------------------------
alignment.current=I_align; % Current reference
alignment.VoltageLimit=Inverter.HalfBridge.Vdc/sqrt(3);   % Voltage limit
alignment.time=t_align*1e3; % time to perform alignment / ms ( greater than L/R = 9.45 ms)
alignment.delay = alignment.time-(Inverter.SlowInterrupt/1e-3); % alignment time (in Stateflow in ms!)

% --------------------------------Open Loop--------------------------------
openloop.SpeedMax=5000;          % Maximum speed of the motor / rpm (only in open loop)
openloop.SpeedVFLimit=1500;      % Speed limit of the open loop --> control mode
openloop.pi=pi;                  % PI

%----------------------------------Closed Loop-----------------------------
Controller.MovingWindow.Delay = 8;
closedloop.EnableSynchronisationTime = 0.4; % [s] Enable Closed Loop operation after VF has started
closedloop.EnableSynchronisation = ceil(closedloop.EnableSynchronisationTime/((60/2)/abs(speed_vf))); % [-] Amount of zero crossing to allow closed loop operation
closedloop.EnableSynchronisationMaximumVoltageDeviation = fix(Inverter.HalfBridge.Vdc/Controller.MovingWindow.Delay); % [V] Maximum voltage deviation of Bemf measurement in comparison to mean value to allow closed loop operation
Controller.Filter.PT1.Tn =0.05;

%Output bemf Miele
closedloop.BemfKp=0.1*(N1/N2);    
closedloop.BemfKi=60*20e-3*(N1/N2); % 20 ms, Frequenz bei 50 Hz, ca. Mittelpunkt des Arbeitbereichs

BemfKp=closedloop.BemfKp; % Stateflow
BemfKi=closedloop.BemfKi; % Stateflow
SpeedLimitMin = 1350; % rpm, limit of speed (motor operating area)
SpeedLimitMax = 5000; % rpm, limit of speed (motor operating area)

%Output Voltage Miele
closedloop.SpeedKp_M=0.000471*(N2/N1);   
closedloop.SpeedKi_M=40*Inverter.TPWM*(N2/N1);
SpeedKp_M=closedloop.SpeedKp_M; % Stateflow
SpeedKi_M=closedloop.SpeedKi_M; % Stateflow
VoltageLimitMin = 65; % V, limit of input voltage (motor operating area), 190 Vrms

%Output Current Miele
closedloop.IsKp=100*(N2/N1)^2;          
closedloop.IsKi=10000*Inverter.TPWM*(N2/N1)^2; 
IsKp=closedloop.IsKp; % Stateflow
IsKi=closedloop.IsKi; % Stateflow

%% ------------------------ Per Unit--------------------------
% Depends on the component on the Inverter Board!
Meas.VoltageReg=3.3; % V

% SWITCH
%%%%%%%%%%%%%%%%%%%%%%%%%%% 230 Vrms Anfang %%%%%%%%%%%%%%%%%%%%%%%
%Current phase measurment : ADC0-SE1 = Gcurrent*Vsh + Goffset*VoltageReg
Meas.R17=1e3; % Ohm
Meas.R19=1e3; % Ohm
Meas.Gcurrent=Meas.R17/(Meas.R17+Meas.R19);
Meas.Goffset=Meas.R19/(Meas.R17+Meas.R19);
%Max current : Imax * Rshunt * Gcurrent + offset = VoltageReg
Meas.Rshunt=Inverter.Rshunt; % Ohm
Meas.offset=Meas.Goffset*Meas.VoltageReg;
Meas.Imax=(Meas.VoltageReg-Meas.offset)/(Meas.Rshunt*Meas.Gcurrent);

%U phase voltage measurment : ADC0-SE2 = Guvoltage * U-phase-U
Meas.R16=1.8e3; % Ohm
Meas.R13=68e3; % Ohm
Meas.R14=68e3; % Ohm
Meas.R15=68e3; % Ohm
Meas.Guvoltage=Meas.R16/(Meas.R16+Meas.R13+Meas.R14+Meas.R15);
%Max U voltage : Umax * Guvoltage = VoltageReg
Meas.Umax=Meas.VoltageReg/Meas.Guvoltage;

%V phase oltage measurment : ADC1-SE2 = Gvvoltage * U-phase-V
Meas.R26=1.8e3; % Ohm
Meas.R23=68e3; % Ohm
Meas.R24=68e3; % Ohm
Meas.R25=68e3; % Ohm
Meas.Gvvoltage=Meas.R26/(Meas.R26+Meas.R23+Meas.R24+Meas.R25);
%Max V voltage : Vmax * Gvvoltage = VoltageReg
Meas.Vmax=Meas.VoltageReg/Meas.Gvvoltage;

%DC phase Voltage measurment : ADC1-SE8 = Gdcvoltage * UDC1
Meas.R27=8.2e3; % Ohm
Meas.R20=330e3; % Ohm
Meas.R21=330e3; % Ohm
Meas.R22=330e3; % Ohm
Meas.Gdcvoltage=Meas.R27/(Meas.R27+Meas.R20+Meas.R21+Meas.R22);
%Max DC voltage : VDCmax * Gdcvoltage = VoltageReg
Meas.VDCmax=Meas.VoltageReg/Meas.Gdcvoltage;

% Base quantities for normalization
perunit.Ib=Meas.Imax; % Current
perunit.Ub=max(max(Meas.Umax,Meas.Vmax),Meas.VDCmax); % Voltage, U, V and DC
perunit.Wb=openloop.SpeedMax; % Max. Speed
perunit.Wbe=perunit.Wb*((2*pi)/60)*PMSM.p; % Max. electrical frequency
perunit.tetab=openloop.pi;  % Max. angle

% Open Loop variables
perunit_SpeedVFLimit=(openloop.SpeedVFLimit/perunit.Wb);

% closed loop variables
% Scaling of PI-controller constants depends on the equations
% BEMF
perunit_BemfKp=BemfKp*(perunit.Ub/perunit.Wb);
perunit_BemfKi=BemfKi*(perunit.Ub/perunit.Wb);
perunit_SpeedLimitMin = SpeedLimitMin*(1/perunit.Wb);
perunit_SpeedLimitMax = SpeedLimitMax*(1/perunit.Wb);

% Speed
perunit_SpeedKp_M=SpeedKp_M*(perunit.Wb/perunit.Ub);
perunit_SpeedKi_M=SpeedKi_M*(perunit.Wb/perunit.Ub);
perunit_VoltageLimitMin = VoltageLimitMin*(1/perunit.Ub);

% Current
perunit_IsKp=IsKp*(perunit.Ib/perunit.Ub);
perunit_IsKi=IsKi*(perunit.Ib/perunit.Ub);
%%%%%%%%%%%%%%%%%%%%%%%%%%% 230 Vrms Ende %%%%%%%%%%%%%%%%%%%%%%%


%% ------------------------Closed Loop | Fixed Point-----------------------
% Controller Board: signed integer 16 Bit, -2^15 ... 2^15-1 (-32768 ... 32767)
scale=2^15; 
scale_uint16 = 2^16;

% ADC 12 Bit: unsigned integer 12 Bit, 0 ... (2^12)-1 (0 ... 4095)
scale_adc_uint = 2^12;
scale_adc_int = 2^11;
scale_adc_offset = scale_adc_uint-scale_adc_int;

% Lookup Table Cosinus 
x_int16 = int16(-scale:256:scale-1);
x_double = -pi:2*pi/length(x_int16):pi-2*pi/length(x_int16);
cos_x_int16 = int16(cos(x_double)*scale);

Controller.CosFcn.x = x_int16;
Controller.CosFcn.y = cos_x_int16;

% Open Loop variables
fixedPoint_SpeedVFLimit=int16(perunit_SpeedVFLimit*scale);

% closed loop variables
% BEMF
fixedPoint_BemfKp_int16=int16(perunit_BemfKp*scale); % perunit_BemfKp = 0.0065
fixedPoint_BemfKi_int16=int16(perunit_BemfKi*scale); % perunit_BemfKi = 0.0779
fixedPoint_SpeedLimitMin=int16(perunit_SpeedLimitMin*scale);
fixedPoint_SpeedLimitMax=int16(perunit_SpeedLimitMax*scale);

% Speed
fixedPoint_SpeedKp_M_int16=int16(perunit_SpeedKp_M*scale); % perunit_SpeedKp_M = 0.0154
fixedPoint_SpeedKi_M_int16=int16(perunit_SpeedKi_M*scale); % perunit_SpeedKi_M = 0.2887
fixedPoint_VoltageLimitMin = int16(perunit_VoltageLimitMin*scale);

% Current
fixedPoint_IsKp_int16=int16(perunit_IsKp*scale); % perunit_IsKp = 0.4235
fixedPoint_IsKi_int16=int16(perunit_IsKi*scale); % perunit_IsKi = 0.0053

% Scaling
scale_speed = scale/perunit.Wb;
scale_fel = scale/perunit.Wbe;
scale_current = scale/perunit.Ib;
scale_voltage = scale/perunit.Ub;
scale_theta = scale/perunit.tetab;
    
%% Scaling Controller
Controller.Scale.ADC2Fixed.Vphase = scale/Meas.VoltageReg;
Controller.Scale.ADC2Fixed.Vdc = scale/Meas.VoltageReg;
Controller.Scale.ADC2Fixed.Iphase = 2*scale/Meas.VoltageReg;

Controller.Scale.Product = scale;
Controller.Scale.Float2Fixed.alpha = scale_theta;
Controller.Scale.Float2Fixed.fel = scale_fel;

Controller.MovingWindow.MaxDeviation = int16(closedloop.EnableSynchronisationMaximumVoltageDeviation*scale_voltage);
