function CodeDefine() { 
this.def = new Array();
this.def["rt_OneStep"] = {file: "ert_main_c.html",line:35,type:"fcn"};
this.def["main"] = {file: "ert_main_c.html",line:72,type:"fcn"};
this.def["rtDW"] = {file: "FlexStart_c.html",line:79,type:"var"};
this.def["rtU"] = {file: "FlexStart_c.html",line:82,type:"var"};
this.def["rtY"] = {file: "FlexStart_c.html",line:85,type:"var"};
this.def["rtM_"] = {file: "FlexStart_c.html",line:88,type:"var"};
this.def["rtM"] = {file: "FlexStart_c.html",line:89,type:"var"};
this.def["FlexStart.c:look1_is32lu32n31Du32_binlcse"] = {file: "FlexStart_c.html",line:100,type:"fcn"};
this.def["FlexStart.c:mul_wide_s32"] = {file: "FlexStart_c.html",line:169,type:"fcn"};
this.def["FlexStart.c:mul_s32_sat"] = {file: "FlexStart_c.html",line:215,type:"fcn"};
this.def["FlexStart.c:mul_wide_u32"] = {file: "FlexStart_c.html",line:233,type:"fcn"};
this.def["FlexStart.c:mul_u32_loSR"] = {file: "FlexStart_c.html",line:267,type:"fcn"};
this.def["FlexStart.c:div_nzp_repeat_u32"] = {file: "FlexStart_c.html",line:275,type:"fcn"};
this.def["FlexStart_step"] = {file: "FlexStart_c.html",line:297,type:"fcn"};
this.def["FlexStart_initialize"] = {file: "FlexStart_c.html",line:455,type:"fcn"};
this.def["RT_MODEL"] = {file: "FlexStart_h.html",line:35,type:"type"};
this.def["DW"] = {file: "FlexStart_h.html",line:49,type:"type"};
this.def["ConstP"] = {file: "FlexStart_h.html",line:62,type:"type"};
this.def["ExtU"] = {file: "FlexStart_h.html",line:72,type:"type"};
this.def["ExtY"] = {file: "FlexStart_h.html",line:77,type:"type"};
this.def["rtConstP"] = {file: "FlexStart_data_c.html",line:21,type:"var"};
this.def["int8_T"] = {file: "rtwtypes_h.html",line:49,type:"type"};
this.def["uint8_T"] = {file: "rtwtypes_h.html",line:50,type:"type"};
this.def["int16_T"] = {file: "rtwtypes_h.html",line:51,type:"type"};
this.def["uint16_T"] = {file: "rtwtypes_h.html",line:52,type:"type"};
this.def["int32_T"] = {file: "rtwtypes_h.html",line:53,type:"type"};
this.def["uint32_T"] = {file: "rtwtypes_h.html",line:54,type:"type"};
this.def["real32_T"] = {file: "rtwtypes_h.html",line:55,type:"type"};
this.def["real64_T"] = {file: "rtwtypes_h.html",line:56,type:"type"};
this.def["real_T"] = {file: "rtwtypes_h.html",line:62,type:"type"};
this.def["time_T"] = {file: "rtwtypes_h.html",line:63,type:"type"};
this.def["boolean_T"] = {file: "rtwtypes_h.html",line:64,type:"type"};
this.def["int_T"] = {file: "rtwtypes_h.html",line:65,type:"type"};
this.def["uint_T"] = {file: "rtwtypes_h.html",line:66,type:"type"};
this.def["ulong_T"] = {file: "rtwtypes_h.html",line:67,type:"type"};
this.def["char_T"] = {file: "rtwtypes_h.html",line:68,type:"type"};
this.def["uchar_T"] = {file: "rtwtypes_h.html",line:69,type:"type"};
this.def["byte_T"] = {file: "rtwtypes_h.html",line:70,type:"type"};
this.def["pointer_T"] = {file: "rtwtypes_h.html",line:88,type:"type"};
}
CodeDefine.instance = new CodeDefine();
var testHarnessInfo = {OwnerFileName: "", HarnessOwner: "", HarnessName: "", IsTestHarness: "0"};
var relPathToBuildDir = "../ert_main.c";
var fileSep = "\\";
var isPC = true;
function Html2SrcLink() {
	this.html2SrcPath = new Array;
	this.html2Root = new Array;
	this.html2SrcPath["ert_main_c.html"] = "../ert_main.c";
	this.html2Root["ert_main_c.html"] = "ert_main_c.html";
	this.html2SrcPath["FlexStart_c.html"] = "../FlexStart.c";
	this.html2Root["FlexStart_c.html"] = "FlexStart_c.html";
	this.html2SrcPath["FlexStart_h.html"] = "../FlexStart.h";
	this.html2Root["FlexStart_h.html"] = "FlexStart_h.html";
	this.html2SrcPath["FlexStart_data_c.html"] = "../FlexStart_data.c";
	this.html2Root["FlexStart_data_c.html"] = "FlexStart_data_c.html";
	this.html2SrcPath["rtwtypes_h.html"] = "../rtwtypes.h";
	this.html2Root["rtwtypes_h.html"] = "rtwtypes_h.html";
	this.getLink2Src = function (htmlFileName) {
		 if (this.html2SrcPath[htmlFileName])
			 return this.html2SrcPath[htmlFileName];
		 else
			 return null;
	}
	this.getLinkFromRoot = function (htmlFileName) {
		 if (this.html2Root[htmlFileName])
			 return this.html2Root[htmlFileName];
		 else
			 return null;
	}
}
Html2SrcLink.instance = new Html2SrcLink();
var fileList = [
"ert_main_c.html","FlexStart_c.html","FlexStart_h.html","FlexStart_data_c.html","rtwtypes_h.html"];
