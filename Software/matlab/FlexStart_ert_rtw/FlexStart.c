/*
 * File: FlexStart.c
 *
 * Code generated for Simulink model 'FlexStart'.
 *
 * Model version                  : 1.89
 * Simulink Coder version         : 9.1 (R2019a) 23-Nov-2018
 * C/C++ source code generated on : Tue Jun  8 11:09:15 2021
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: NXP->Cortex-M0/M0+
 * Code generation objectives:
 *    1. RAM efficiency
 *    2. Execution efficiency
 * Validation result: Not run
 */

#include "FlexStart.h"

/* Named constants for Chart: '<Root>/FlexStart' */
#define IN_Alignment                   ((uint8_T)1U)
#define IN_MotorOn                     ((uint8_T)2U)
#define IN_NO_ACTIVE_CHILD             ((uint8_T)0U)
#define IN_Stop                        ((uint8_T)3U)
#define IN_alignment_pulse             ((uint8_T)1U)
#define IN_done                        ((uint8_T)2U)
#define IN_pause_1                     ((uint8_T)3U)
#define IN_pause_2                     ((uint8_T)4U)
#define Stop                           ((uint8_T)0U)
#ifndef UCHAR_MAX
#include <limits.h>
#endif

#if ( UCHAR_MAX != (0xFFU) ) || ( SCHAR_MAX != (0x7F) )
#error Code was generated for compiler with different sized uchar/char. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

#if ( USHRT_MAX != (0xFFFFU) ) || ( SHRT_MAX != (0x7FFF) )
#error Code was generated for compiler with different sized ushort/short. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

#if ( UINT_MAX != (0xFFFFFFFFU) ) || ( INT_MAX != (0x7FFFFFFF) )
#error Code was generated for compiler with different sized uint/int. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

#if ( ULONG_MAX != (0xFFFFFFFFU) ) || ( LONG_MAX != (0x7FFFFFFF) )
#error Code was generated for compiler with different sized ulong/long. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

/* Block signals and states (default storage) */
DW rtDW;

/* External inputs (root inport signals with default storage) */
ExtU rtU;

/* External outputs (root outports fed by signals with default storage) */
ExtY rtY;

/* Real-time model */
RT_MODEL rtM_;
RT_MODEL *const rtM = &rtM_;
static int32_T look1_is32lu32n31Du32_binlcse(int32_T u0, const int32_T bp0[],
  const int32_T table[], uint32_T maxIndex);
static void mul_wide_s32(int32_T in0, int32_T in1, uint32_T *ptrOutBitsHi,
  uint32_T *ptrOutBitsLo);
static int32_T mul_s32_sat(int32_T a, int32_T b);
static void mul_wide_u32(uint32_T in0, uint32_T in1, uint32_T *ptrOutBitsHi,
  uint32_T *ptrOutBitsLo);
static uint32_T mul_u32_loSR(uint32_T a, uint32_T b, uint32_T aShift);
static uint32_T div_nzp_repeat_u32(uint32_T numerator, uint32_T denominator,
  uint32_T nRepeatSub);
static int32_T look1_is32lu32n31Du32_binlcse(int32_T u0, const int32_T bp0[],
  const int32_T table[], uint32_T maxIndex)
{
  int32_T y;
  uint32_T frac;
  int32_T yR_0d0;
  uint32_T iRght;
  uint32_T iLeft;

  /* Column-major Lookup 1-D
     Search method: 'binary'
     Use previous index: 'off'
     Interpolation method: 'Linear point-slope'
     Extrapolation method: 'Clip'
     Use last breakpoint for index at or above upper limit: 'off'
     Remove protection against out-of-range input in generated code: 'off'
     Rounding mode: 'simplest'
   */
  /* Prelookup - Index and Fraction
     Index Search method: 'binary'
     Extrapolation method: 'Clip'
     Use previous index: 'off'
     Use last breakpoint for index at or above upper limit: 'off'
     Remove protection against out-of-range input in generated code: 'off'
     Rounding mode: 'simplest'
   */
  if (u0 <= bp0[0U]) {
    iLeft = 0U;
    frac = 0U;
  } else if (u0 < bp0[maxIndex]) {
    /* Binary Search */
    frac = maxIndex >> 1U;
    iLeft = 0U;
    iRght = maxIndex;
    while (iRght - iLeft > 1U) {
      if (u0 < bp0[frac]) {
        iRght = frac;
      } else {
        iLeft = frac;
      }

      frac = (iRght + iLeft) >> 1U;
    }

    frac = div_nzp_repeat_u32((uint32_T)u0 - bp0[iLeft], (uint32_T)bp0[iLeft +
      1U] - bp0[iLeft], 31U);
  } else {
    iLeft = maxIndex - 1U;
    frac = 2147483648U;
  }

  /* Column-major Interpolation 1-D
     Interpolation method: 'Linear point-slope'
     Use last breakpoint for index at or above upper limit: 'off'
     Rounding mode: 'simplest'
     Overflow mode: 'wrapping'
   */
  yR_0d0 = table[iLeft + 1U];
  if (yR_0d0 >= table[iLeft]) {
    y = (int32_T)mul_u32_loSR(frac, (uint32_T)yR_0d0 - table[iLeft], 31U) +
      table[iLeft];
  } else {
    y = table[iLeft] - (int32_T)mul_u32_loSR(frac, (uint32_T)table[iLeft] -
      yR_0d0, 31U);
  }

  return y;
}

static void mul_wide_s32(int32_T in0, int32_T in1, uint32_T *ptrOutBitsHi,
  uint32_T *ptrOutBitsLo)
{
  uint32_T absIn0;
  uint32_T absIn1;
  uint32_T in0Lo;
  uint32_T in0Hi;
  uint32_T in1Hi;
  uint32_T productHiLo;
  uint32_T productLoHi;
  absIn0 = in0 < 0 ? ~(uint32_T)in0 + 1U : (uint32_T)in0;
  absIn1 = in1 < 0 ? ~(uint32_T)in1 + 1U : (uint32_T)in1;
  in0Hi = absIn0 >> 16U;
  in0Lo = absIn0 & 65535U;
  in1Hi = absIn1 >> 16U;
  absIn0 = absIn1 & 65535U;
  productHiLo = in0Hi * absIn0;
  productLoHi = in0Lo * in1Hi;
  absIn0 *= in0Lo;
  absIn1 = 0U;
  in0Lo = (productLoHi << /*MW:OvBitwiseOk*/ 16U) + /*MW:OvCarryOk*/ absIn0;
  if (in0Lo < absIn0) {
    absIn1 = 1U;
  }

  absIn0 = in0Lo;
  in0Lo += /*MW:OvCarryOk*/ productHiLo << /*MW:OvBitwiseOk*/ 16U;
  if (in0Lo < absIn0) {
    absIn1++;
  }

  absIn0 = (((productLoHi >> 16U) + (productHiLo >> 16U)) + in0Hi * in1Hi) +
    absIn1;
  if ((in0 != 0) && ((in1 != 0) && ((in0 > 0) != (in1 > 0)))) {
    absIn0 = ~absIn0;
    in0Lo = ~in0Lo;
    in0Lo++;
    if (in0Lo == 0U) {
      absIn0++;
    }
  }

  *ptrOutBitsHi = absIn0;
  *ptrOutBitsLo = in0Lo;
}

static int32_T mul_s32_sat(int32_T a, int32_T b)
{
  int32_T result;
  uint32_T u32_chi;
  uint32_T u32_clo;
  mul_wide_s32(a, b, &u32_chi, &u32_clo);
  if (((int32_T)u32_chi > 0) || ((u32_chi == 0U) && (u32_clo >= 2147483648U))) {
    result = MAX_int32_T;
  } else if (((int32_T)u32_chi < -1) || (((int32_T)u32_chi == -1) && (u32_clo <
               2147483648U))) {
    result = MIN_int32_T;
  } else {
    result = (int32_T)u32_clo;
  }

  return result;
}

static void mul_wide_u32(uint32_T in0, uint32_T in1, uint32_T *ptrOutBitsHi,
  uint32_T *ptrOutBitsLo)
{
  uint32_T outBitsLo;
  uint32_T in0Lo;
  uint32_T in0Hi;
  uint32_T in1Lo;
  uint32_T in1Hi;
  uint32_T productHiLo;
  uint32_T productLoHi;
  in0Hi = in0 >> 16U;
  in0Lo = in0 & 65535U;
  in1Hi = in1 >> 16U;
  in1Lo = in1 & 65535U;
  productHiLo = in0Hi * in1Lo;
  productLoHi = in0Lo * in1Hi;
  in0Lo *= in1Lo;
  in1Lo = 0U;
  outBitsLo = (productLoHi << /*MW:OvBitwiseOk*/ 16U) + /*MW:OvCarryOk*/ in0Lo;
  if (outBitsLo < in0Lo) {
    in1Lo = 1U;
  }

  in0Lo = outBitsLo;
  outBitsLo += /*MW:OvCarryOk*/ productHiLo << /*MW:OvBitwiseOk*/ 16U;
  if (outBitsLo < in0Lo) {
    in1Lo++;
  }

  *ptrOutBitsHi = (((productLoHi >> 16U) + (productHiLo >> 16U)) + in0Hi * in1Hi)
    + in1Lo;
  *ptrOutBitsLo = outBitsLo;
}

static uint32_T mul_u32_loSR(uint32_T a, uint32_T b, uint32_T aShift)
{
  uint32_T result;
  uint32_T u32_chi;
  mul_wide_u32(a, b, &u32_chi, &result);
  return u32_chi << /*MW:OvBitwiseOk*/ (32U - aShift) | result >> aShift;
}

static uint32_T div_nzp_repeat_u32(uint32_T numerator, uint32_T denominator,
  uint32_T nRepeatSub)
{
  uint32_T quotient;
  uint32_T iRepeatSub;
  boolean_T numeratorExtraBit;
  quotient = numerator / denominator;
  numerator %= denominator;
  for (iRepeatSub = 0U; iRepeatSub < nRepeatSub; iRepeatSub++) {
    numeratorExtraBit = (numerator >= 2147483648U);
    numerator <<= 1U;
    quotient <<= 1U;
    if (numeratorExtraBit || (numerator >= denominator)) {
      quotient++;
      numerator -= denominator;
    }
  }

  return quotient;
}

/* Model step function */
void FlexStart_step(void)
{
  boolean_T Reset_prev;
  int32_T q1;
  int32_T tmp;

  /* Chart: '<Root>/FlexStart' incorporates:
   *  Inport: '<Root>/I_dc'
   *  Inport: '<Root>/MotorCmd'
   *  Inport: '<Root>/Reset'
   *  Inport: '<Root>/V_amp'
   *  Inport: '<Root>/f_v_amp'
   */
  if (rtDW.temporalCounter_i1 < 4095U) {
    rtDW.temporalCounter_i1++;
  }

  Reset_prev = rtDW.bitsForTID0.Reset_start;
  rtDW.bitsForTID0.Reset_start = rtU.Reset;
  if (rtDW.bitsForTID0.is_active_c2_FlexStart == 0U) {
    rtDW.bitsForTID0.is_active_c2_FlexStart = 1;
    rtDW.bitsForTID0.is_c2_FlexStart = IN_Alignment;
    rtDW.bitsForTID0.is_Alignment = IN_pause_1;
    rtDW.temporalCounter_i1 = 0U;

    /* Outport: '<Root>/V_out' */
    rtY.V_out = 0;
  } else {
    switch (rtDW.bitsForTID0.is_c2_FlexStart) {
     case IN_Alignment:
      if (rtDW.bitsForTID0.is_aligned) {
        rtDW.bitsForTID0.is_Alignment = IN_NO_ACTIVE_CHILD;
        rtDW.bitsForTID0.is_c2_FlexStart = IN_Stop;

        /* Outport: '<Root>/V_out' */
        rtY.V_out = 0;
      } else {
        switch (rtDW.bitsForTID0.is_Alignment) {
         case IN_alignment_pulse:
          if (rtDW.temporalCounter_i1 >= 3000U) {
            rtDW.bitsForTID0.is_Alignment = IN_pause_2;
            rtDW.temporalCounter_i1 = 0U;

            /* Outport: '<Root>/V_out' */
            rtY.V_out = 0;
          }
          break;

         case IN_done:
          break;

         case IN_pause_1:
          if (rtDW.temporalCounter_i1 >= 3000U) {
            rtDW.bitsForTID0.is_Alignment = IN_alignment_pulse;
            rtDW.temporalCounter_i1 = 0U;

            /* Outport: '<Root>/V_out' */
            rtY.V_out = -50;
          }
          break;

         default:
          /* case IN_pause_2: */
          if (rtDW.temporalCounter_i1 >= 3000U) {
            rtDW.bitsForTID0.is_Alignment = IN_done;
            rtDW.bitsForTID0.is_aligned = true;
          }
          break;
        }
      }
      break;

     case IN_MotorOn:
      if (rtU.MotorCmd == Stop) {
        rtDW.bitsForTID0.is_c2_FlexStart = IN_Alignment;
        rtDW.bitsForTID0.is_Alignment = IN_pause_1;
        rtDW.temporalCounter_i1 = 0U;

        /* Outport: '<Root>/V_out' */
        rtY.V_out = 0;
      } else if (Reset_prev != rtDW.bitsForTID0.Reset_start) {
        rtDW.bitsForTID0.is_aligned = false;
        rtDW.bitsForTID0.is_c2_FlexStart = IN_Stop;

        /* Outport: '<Root>/V_out' */
        rtY.V_out = 0;
      } else {
        /*  add time step to time */
        /*  calc voltage amplitude */
        /*  compute output voltage */
        q1 = mul_s32_sat(rtU.I_dc, rtU.f_v_amp) / 65536;

        /* Outputs for Function Call SubSystem: '<S1>/CalcSinVoltageOutput' */
        /* Switch: '<S2>/Switch' incorporates:
         *  Constant: '<S2>/Constant1'
         *  Constant: '<S2>/Constant2'
         *  Inport: '<Root>/I_dc'
         *  Inport: '<Root>/f_v_amp'
         */
        if (rtU.MotorCmd > 1) {
          tmp = 0;
        } else {
          tmp = 16384;
        }

        /* End of Switch: '<S2>/Switch' */
        /* End of Outputs for SubSystem: '<S1>/CalcSinVoltageOutput' */
        if ((rtU.V_amp < 0) && (q1 < MIN_int32_T - rtU.V_amp)) {
          q1 = MIN_int32_T;
        } else if ((rtU.V_amp > 0) && (q1 > MAX_int32_T - rtU.V_amp)) {
          q1 = MAX_int32_T;
        } else {
          q1 += rtU.V_amp;
        }

        /* Outputs for Function Call SubSystem: '<S1>/CalcSinVoltageOutput' */
        /* Outport: '<Root>/V_out' incorporates:
         *  Inport: '<Root>/V_amp'
         *  Inport: '<Root>/speed_Hz'
         *  Lookup_n-D: '<S2>/1-D Lookup Table'
         *  Product: '<S2>/Product'
         *  Product: '<S2>/Product1'
         *  Sum: '<S2>/Subtract'
         */
        rtY.V_out = look1_is32lu32n31Du32_binlcse((int16_T)((int16_T)((int16_T)
          (rtDW.t * rtU.speed_Hz) << 15) - tmp), rtConstP.uDLookupTable_bp01Data,
          rtConstP.uDLookupTable_tableData, 255U) * q1;

        /* End of Outputs for SubSystem: '<S1>/CalcSinVoltageOutput' */
      }
      break;

     default:
      /* case IN_Stop: */
      Reset_prev = !rtU.Reset;
      if ((!rtDW.bitsForTID0.is_aligned) && Reset_prev) {
        rtDW.bitsForTID0.is_aligned = false;
        rtDW.bitsForTID0.is_c2_FlexStart = IN_Alignment;
        rtDW.bitsForTID0.is_Alignment = IN_pause_1;
        rtDW.temporalCounter_i1 = 0U;

        /* Outport: '<Root>/V_out' */
        rtY.V_out = 0;
      } else {
        if ((rtU.MotorCmd != Stop) && Reset_prev) {
          rtDW.bitsForTID0.is_aligned = false;
          rtDW.bitsForTID0.is_c2_FlexStart = IN_MotorOn;
          rtDW.t = 0;
        }
      }
      break;
    }
  }

  /* End of Chart: '<Root>/FlexStart' */
}

/* Model initialize function */
void FlexStart_initialize(void)
{
  /* (no initialization code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
