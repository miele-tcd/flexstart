/*
 * File: FlexStart.h
 *
 * Code generated for Simulink model 'FlexStart'.
 *
 * Model version                  : 1.89
 * Simulink Coder version         : 9.1 (R2019a) 23-Nov-2018
 * C/C++ source code generated on : Tue Jun  8 11:09:15 2021
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: NXP->Cortex-M0/M0+
 * Code generation objectives:
 *    1. RAM efficiency
 *    2. Execution efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_FlexStart_h_
#define RTW_HEADER_FlexStart_h_
#ifndef FlexStart_COMMON_INCLUDES_
# define FlexStart_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* FlexStart_COMMON_INCLUDES_ */

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

/* Forward declaration for rtModel */
typedef struct tag_RTM RT_MODEL;

/* Block signals and states (default storage) for system '<Root>' */
typedef struct {
  int32_T t;                           /* '<Root>/FlexStart' */
  struct {
    uint_T is_Alignment:3;             /* '<Root>/FlexStart' */
    uint_T is_c2_FlexStart:2;          /* '<Root>/FlexStart' */
    uint_T is_active_c2_FlexStart:1;   /* '<Root>/FlexStart' */
    uint_T Reset_start:1;              /* '<Root>/FlexStart' */
    uint_T is_aligned:1;               /* '<Root>/FlexStart' */
  } bitsForTID0;

  uint16_T temporalCounter_i1;         /* '<Root>/FlexStart' */
} DW;

/* Constant parameters (default storage) */
typedef struct {
  /* Computed Parameter: uDLookupTable_tableData
   * Referenced by: '<S2>/1-D Lookup Table'
   */
  int32_T uDLookupTable_tableData[256];

  /* Computed Parameter: uDLookupTable_bp01Data
   * Referenced by: '<S2>/1-D Lookup Table'
   */
  int32_T uDLookupTable_bp01Data[256];
} ConstP;

/* External inputs (root inport signals with default storage) */
typedef struct {
  uint8_T MotorCmd;                    /* '<Root>/MotorCmd' */
  int32_T speed_Hz;                    /* '<Root>/speed_Hz' */
  int32_T V_amp;                       /* '<Root>/V_amp' */
  int16_T f_v_amp;                     /* '<Root>/f_v_amp' */
  int32_T I_dc;                        /* '<Root>/I_dc' */
  boolean_T Reset;                     /* '<Root>/Reset' */
} ExtU;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  int32_T V_out;                       /* '<Root>/V_out' */
} ExtY;

/* Real-time Model Data Structure */
struct tag_RTM {
  const char_T * volatile errorStatus;
};

/* Block signals and states (default storage) */
extern DW rtDW;

/* External inputs (root inport signals with default storage) */
extern ExtU rtU;

/* External outputs (root outports fed by signals with default storage) */
extern ExtY rtY;

/* Constant parameters (default storage) */
extern const ConstP rtConstP;

/* Model entry point functions */
extern void FlexStart_initialize(void);
extern void FlexStart_step(void);

/* Real-time Model object */
extern RT_MODEL *const rtM;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Note that this particular code originates from a subsystem build,
 * and has its own system numbers different from the parent model.
 * Refer to the system hierarchy for this subsystem below, and use the
 * MATLAB hilite_system command to trace the generated code back
 * to the parent model.  For example,
 *
 * hilite_system('Library_Example_PMSM_1Ph/FlexStart')    - opens subsystem Library_Example_PMSM_1Ph/FlexStart
 * hilite_system('Library_Example_PMSM_1Ph/FlexStart/Kp') - opens and selects block Kp
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'Library_Example_PMSM_1Ph'
 * '<S1>'   : 'Library_Example_PMSM_1Ph/FlexStart'
 * '<S2>'   : 'Library_Example_PMSM_1Ph/FlexStart/CalcSinVoltageOutput'
 */
#endif                                 /* RTW_HEADER_FlexStart_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
