% init vars for flexstart stateflow object

FS.Stop = 0;
FS.CW = 1;
FS.CCW = 2;

frequency_interrupt_Hz = 8000;
FS.dt_us = int32(1e6 / frequency_interrupt_Hz);

sintable_size = 256;
FS.sintable_s16 = int16(32768 * sin(linspace(0, 2 * pi, sintable_size)));
FS.sintable_index_u8 = 0 : 1 : (sintable_size - 1);

FS.angle_scale_denominator_u16 = 65536;
FS.angle_scale_u16 = 65535 * FS.angle_scale_denominator_u16 / 1e6;