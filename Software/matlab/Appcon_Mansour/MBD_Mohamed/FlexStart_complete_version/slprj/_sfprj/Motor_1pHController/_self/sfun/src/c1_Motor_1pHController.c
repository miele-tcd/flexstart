/* Include files */

#include "Motor_1pHController_sfun.h"
#include "c1_Motor_1pHController.h"
#include "mwmathutil.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "Motor_1pHController_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(S);
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);
static const mxArray* sf_opaque_get_hover_data_for_msg(void *chartInstance,
  int32_T msgSSID);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)
#define c1_IN_NO_ACTIVE_CHILD          ((uint8_T)0U)
#define c1_IN_Init_Mode                ((uint8_T)1U)
#define c1_IN_Motor_On                 ((uint8_T)2U)
#define c1_IN_Stop_Mode                ((uint8_T)3U)
#define c1_IN_Alignment_Done           ((uint8_T)1U)
#define c1_IN_Alignment_Mode           ((uint8_T)2U)
#define c1_IN_ControlMode              ((uint8_T)3U)
#define c1_IN_Error_Mode               ((uint8_T)4U)
#define c1_IN_VF_Mode                  ((uint8_T)5U)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c1_sv0[10] = { "Init", "InitDone", "Stop", "StopDone",
  "Alignment", "AlignmentDone", "VF", "VFDone", "Fault", "FaultClear" };

static const int32_T c1_iv0[10] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

/* Function Declarations */
static void initialize_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void initialize_params_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void enable_c1_Motor_1pHController(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance);
static void disable_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void c1_update_debugger_state_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void ext_mode_exec_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static const mxArray *get_sim_state_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void set_sim_state_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, const mxArray *c1_st);
static void c1_set_sim_state_side_effects_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void finalize_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void sf_gateway_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void mdl_start_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void c1_chartstep_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void initSimStructsc1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void c1_enter_atomic_Stop_Mode(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance);
static void c1_Motor_On(SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void c1_enter_atomic_Error_Mode(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance);
static void c1_Alignment_Done(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance);
static void c1_VF_Mode(SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void c1_exit_atomic_VF_Mode(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance);
static void c1_enter_atomic_ControlMode(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance);
static void c1_ControlMode(SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c1_machineNumber, uint32_T
  c1_chartNumber, uint32_T c1_instanceNumber);
static const mxArray *c1_sfAfterOrElapsed(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance);
static const mxArray *c1_b_sfAfterOrElapsed
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void c1_Miele(SFc1_Motor_1pHControllerInstanceStruct *chartInstance,
                     int32_T c1_c_Power, int16_T c1_d_Speed, int32_T
                     c1_e_Counter, boolean_T *c1_c_Slurp_Detected, int32_T
                     *c1_e_Counter_output);
static void c1_Appcon(SFc1_Motor_1pHControllerInstanceStruct *chartInstance,
                      int16_T c1_b_Bemf_Sample, int32_T c1_e_Counter, int16_T
                      c1_b_Activate, int16_T c1_b_Activation_Counter, int16_T
                      c1_d_Speed, int16_T c1_b_Speed_Filtered, boolean_T
                      *c1_c_Slurp_Detected, int32_T *c1_e_Counter_output,
                      int16_T *c1_b_Activation_Counter_output);
static void c1_Speed_Observation(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, int16_T c1_d_Speed, int16_T c1_b_Speed_reference, int32_T
  c1_e_Counter, boolean_T *c1_c_Speed_control_fault, int32_T
  *c1_e_Counter_output);
static c1_ControlFlag c1_VFDone_Flag(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, int16_T c1_e_speed_reference, boolean_T c1_c_start_ctr);
static void c1_CheckforSynchronisation(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, int16_T c1_c_bemf, int32_T c1_b_Time_State, boolean_T
  *c1_b_Synchronisation_enabled, int32_T *c1_b_Time_state_output);
static void c1_Bemf_Observation(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, int16_T c1_c_bemf, int16_T c1_c_bemf_reference, int32_T
  c1_e_Counter, boolean_T *c1_c_Bemf_control_fault, int32_T *c1_e_Counter_output);
static boolean_T c1_b_fault(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, boolean_T c1_c_fault1, boolean_T c1_c_fault2);
static c1_ControlFlag c1_Protections(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, boolean_T c1_c_fault_clear);
static void c1_state_variable_init(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, int16_T *c1_c_OL_voltage_state_is, int16_T
  *c1_c_OL_voltage_state_bemf, int16_T *c1_c_OL_speed_state, int16_T
  *c1_c_OLvoltage_sat, int16_T *c1_c_OLspeed_sat);
static int32_T c1_PowerCalculation(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, int16_T c1_b_Current, int16_T c1_b_InverterVoltage, real_T
  c1_b_Reset);
static void c1_SpeedRateLimit(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, int16_T c1_b_Reference, int32_T c1_b_State_variable_in,
  int16_T *c1_b_ReferenceOut, int32_T *c1_b_State_variable_out);
static int16_T c1_SpeedToAngle(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, int16_T c1_e_speed_reference, int16_T c1_b_angle_reference);
static void c1_PI_Controller(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, int16_T c1_b_soll, int16_T c1_b_ist, int16_T c1_b_kp, int16_T
  c1_b_ki, int16_T c1_b_I_state_in, int16_T c1_b_limit_low, int16_T
  c1_b_limit_high, int16_T *c1_b_output, int16_T *c1_b_I_state_out);
static void c1_modulation(SFc1_Motor_1pHControllerInstanceStruct *chartInstance,
  int16_T c1_d_voltage_reference, int16_T c1_b_rotor_angle, int16_T
  c1_c_voltage_dc, uint16_T *c1_d_PWM_CMD1, uint16_T *c1_d_PWM_CMD2, int16_T
  *c1_b_InverterOutputVoltage);
static int16_T c1_Duty_Cycle_To_Voltage(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, uint16_T c1_d_PWM_CMD1, uint16_T c1_d_PWM_CMD2, int16_T
  c1_c_voltage_dc);
static void c1_OpenClosedHBridge(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance);
static void c1_PosToNeg_control(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance);
static void c1_NegToPos_control(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance);
static const mxArray *c1_sf_marshallOut(void *chartInstanceVoid, void *c1_inData);
static int32_T c1_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_b_sfEvent, const char_T *c1_identifier);
static int32_T c1_b_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_b_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static uint8_T c1_c_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_b_tp_Stop_Mode, const char_T *c1_identifier);
static uint8_T c1_d_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_c_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static int16_T c1_e_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_b_Vs_s, const char_T *c1_identifier);
static int16_T c1_f_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_d_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static c1_ControlFlag c1_g_emlrt_marshallIn
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, const mxArray
   *c1_b_controller_flag, const char_T *c1_identifier);
static c1_ControlFlag c1_h_emlrt_marshallIn
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, const mxArray *c1_u,
   const emlrtMsgIdentifier *c1_parentId);
static void c1_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_e_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static c1_struct_F7N0deEp72x1GuUaWKNfKD c1_i_emlrt_marshallIn
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, const mxArray *c1_u,
   const emlrtMsgIdentifier *c1_parentId);
static real_T c1_j_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_f_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static boolean_T c1_k_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_d_fault, const char_T *c1_identifier);
static boolean_T c1_l_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_g_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static const mxArray *c1_h_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static uint16_T c1_m_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_c_PWM_OFF, const char_T *c1_identifier);
static uint16_T c1_n_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_i_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static void c1_o_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId,
  c1_struct_AnKM5BqWxAKQWowfA30Y4B *c1_y);
static void c1_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_j_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static void c1_i_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static uint32_T c1_p_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_b_temporalCounter_i1, const char_T
  *c1_identifier);
static uint32_T c1_q_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static const mxArray *c1_r_emlrt_marshallIn
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, const mxArray
   *c1_b_setSimStateSideEffectsInfo, const char_T *c1_identifier);
static const mxArray *c1_s_emlrt_marshallIn
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, const mxArray *c1_u,
   const emlrtMsgIdentifier *c1_parentId);
static void c1_slStringInitializeDynamicBuffers
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static const mxArray *sf_get_hover_data_for_msg
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, int32_T c1_ssid);
static void c1_init_sf_message_store_memory
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static int32_T c1__s32_add__(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, int32_T c1_b, int32_T c1_c, int32_T c1_EMLOvCount_src_loc,
  uint32_T c1_ssid_src_loc, int32_T c1_offset_src_loc, int32_T c1_length_src_loc);
static void init_test_point_addr_map(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance);
static void **get_test_point_address_map(SFc1_Motor_1pHControllerInstanceStruct *
  chartInstance);
static rtwCAPI_ModelMappingInfo *get_test_point_mapping_info
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void init_dsm_address_info(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  setLegacyDebuggerFlag(chartInstance->S, true);
  setDebuggerFlag(chartInstance->S, true);
  c1_slStringInitializeDynamicBuffers(chartInstance);
  if (sf_is_first_init_cond(chartInstance->S)) {
    initSimStructsc1_Motor_1pHController(chartInstance);
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  sim_mode_is_external(chartInstance->S);
  chartInstance->c1_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c1_doSetSimStateSideEffects = 0U;
  chartInstance->c1_setSimStateSideEffectsInfo = NULL;
  chartInstance->c1_tp_Init_Mode = 0U;
  chartInstance->c1_is_Motor_On = c1_IN_NO_ACTIVE_CHILD;
  chartInstance->c1_tp_Motor_On = 0U;
  chartInstance->c1_tp_Alignment_Done = 0U;
  chartInstance->c1_temporalCounter_i1 = 0U;
  chartInstance->c1_tp_Alignment_Mode = 0U;
  chartInstance->c1_temporalCounter_i1 = 0U;
  chartInstance->c1_tp_ControlMode = 0U;
  chartInstance->c1_tp_Error_Mode = 0U;
  chartInstance->c1_tp_VF_Mode = 0U;
  chartInstance->c1_tp_Stop_Mode = 0U;
  chartInstance->c1_is_active_c1_Motor_1pHController = 0U;
  chartInstance->c1_is_c1_Motor_1pHController = c1_IN_NO_ACTIVE_CHILD;
  chartInstance->c1_fault = false;
  chartInstance->c1_old_is = 0;
  chartInstance->c1_FirstCurrentZeroCrossingFlagPositiveToNegative = 1U;
  chartInstance->c1_FirstCurrentZeroCrossingFlagNegativeToPositive = 0U;
  chartInstance->c1_CurrentZeroCrossingFlagPositiveToNegativeCounter = 0U;
  chartInstance->c1_CurrentZeroCrossingFlagNegativeToPositiveCounter = 0U;
  chartInstance->c1_MaskFlag = 0U;
  chartInstance->c1_OL_speed_state = 0;
  chartInstance->c1_OLspeed_sat = 0;
  chartInstance->c1_OLvoltage_sat = 0;
  chartInstance->c1_OL_voltage_state_is = 0;
  chartInstance->c1_OL_voltage_state_bemf = 0;
  chartInstance->c1_PT1_StateVariable = 0;
  chartInstance->c1_SynchronisationTime_state = 0;
  chartInstance->c1_Vf_Done_SynchronisationEnable = false;
  chartInstance->c1_rotor_position_reference = 0;
  chartInstance->c1_SpeedReference_PT1 = 0;
  chartInstance->c1_BackEmfObersvationCounterValue = 0;
  chartInstance->c1_SlurpDetectionCounterAppcon = 0;
  chartInstance->c1_SlurpDetectionCounterMiele = 0;
  chartInstance->c1_SlurpDetection_Appcon_ActivationCounter = 0;
  chartInstance->c1_SpeedObersvationCounterValue = 0;
  chartInstance->c1_is_Rshunt = 0;
  chartInstance->c1_bemf_test = 0;
  if (sf_get_output_port_reusable(chartInstance->S, 1) == 0) {
    *chartInstance->c1_Vs_s = 0;
  }

  if (sf_get_output_port_reusable(chartInstance->S, 2) == 0) {
    *chartInstance->c1_controller_flag = ControlFlag_Init;
  }

  if (sf_get_output_port_reusable(chartInstance->S, 3) == 0) {
    *chartInstance->c1_PWM_CMD1 = 0;
  }

  if (sf_get_output_port_reusable(chartInstance->S, 4) == 0) {
    *chartInstance->c1_PWM_CMD2 = 0;
  }

  if (sf_get_output_port_reusable(chartInstance->S, 5) == 0) {
    *chartInstance->c1_PWM_OFF = 0U;
  }

  if (sf_get_output_port_reusable(chartInstance->S, 6) == 0) {
    *chartInstance->c1_bemf_zcr = 0;
  }

  if (sf_get_output_port_reusable(chartInstance->S, 7) == 0) {
    *chartInstance->c1_SlurpDetectedAppcon = false;
  }

  if (sf_get_output_port_reusable(chartInstance->S, 8) == 0) {
    *chartInstance->c1_ApproxPower = 0;
  }

  if (sf_get_output_port_reusable(chartInstance->S, 9) == 0) {
    *chartInstance->c1_Bemf_control_fault = false;
  }

  if (sf_get_output_port_reusable(chartInstance->S, 10) == 0) {
    *chartInstance->c1_Speed_control_fault = false;
  }

  if (sf_get_output_port_reusable(chartInstance->S, 11) == 0) {
    *chartInstance->c1_speed_pll = 0;
  }

  if (sf_get_output_port_reusable(chartInstance->S, 12) == 0) {
    *chartInstance->c1_voltage_ref = 0;
  }

  if (sf_get_output_port_reusable(chartInstance->S, 13) == 0) {
    *chartInstance->c1_InvVoltageOut = 0;
  }

  if (sf_get_output_port_reusable(chartInstance->S, 14) == 0) {
    *chartInstance->c1_SlurpDetectedMiele = false;
  }

  if (sf_get_output_port_reusable(chartInstance->S, 15) == 0) {
    *chartInstance->c1_old_InvVoltageOut = 0;
  }

  if (sf_get_output_port_reusable(chartInstance->S, 16) == 0) {
    *chartInstance->c1_testpoint = 0;
  }

  if (sf_get_output_port_reusable(chartInstance->S, 17) == 0) {
    *chartInstance->c1_testpoint2 = 0U;
  }

  if (sf_get_output_port_reusable(chartInstance->S, 18) == 0) {
    *chartInstance->c1_testpoint3 = 0U;
  }

  chartInstance->c1_presentTicks = 0U;
  chartInstance->c1_elapsedTicks = 0U;
  chartInstance->c1_previousTicks = 0U;
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_fault, 20U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 20U, (real_T)
                    chartInstance->c1_fault);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_old_is, 22U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 22U, (real_T)
                    chartInstance->c1_old_is);
  _SFD_DATA_RANGE_CHECK((real_T)
                        chartInstance->c1_FirstCurrentZeroCrossingFlagPositiveToNegative,
                        4U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 4U, (real_T)
                    chartInstance->c1_FirstCurrentZeroCrossingFlagPositiveToNegative);
  _SFD_DATA_RANGE_CHECK((real_T)
                        chartInstance->c1_FirstCurrentZeroCrossingFlagNegativeToPositive,
                        3U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 3U, (real_T)
                    chartInstance->c1_FirstCurrentZeroCrossingFlagNegativeToPositive);
  _SFD_DATA_RANGE_CHECK((real_T)
                        chartInstance->c1_CurrentZeroCrossingFlagPositiveToNegativeCounter,
                        2U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 2U, (real_T)
                    chartInstance->c1_CurrentZeroCrossingFlagPositiveToNegativeCounter);
  _SFD_DATA_RANGE_CHECK((real_T)
                        chartInstance->c1_CurrentZeroCrossingFlagNegativeToPositiveCounter,
                        1U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 1U, (real_T)
                    chartInstance->c1_CurrentZeroCrossingFlagNegativeToPositiveCounter);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_MaskFlag, 5U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 5U, (real_T)
                    chartInstance->c1_MaskFlag);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OL_speed_state, 6U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 6U, (real_T)
                    chartInstance->c1_OL_speed_state);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OLspeed_sat, 9U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 9U, (real_T)
                    chartInstance->c1_OLspeed_sat);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OLvoltage_sat, 10U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 10U, (real_T)
                    chartInstance->c1_OLvoltage_sat);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OL_voltage_state_is, 8U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 8U, (real_T)
                    chartInstance->c1_OL_voltage_state_is);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OL_voltage_state_bemf, 7U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 7U, (real_T)
                    chartInstance->c1_OL_voltage_state_bemf);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_PT1_StateVariable, 11U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 11U, (real_T)
                    chartInstance->c1_PT1_StateVariable);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_SynchronisationTime_state, 17U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 17U, (real_T)
                    chartInstance->c1_SynchronisationTime_state);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_Vf_Done_SynchronisationEnable,
                        18U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 18U, (real_T)
                    chartInstance->c1_Vf_Done_SynchronisationEnable);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_rotor_position_reference, 23U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 23U, (real_T)
                    chartInstance->c1_rotor_position_reference);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_SpeedReference_PT1, 16U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 16U, (real_T)
                    chartInstance->c1_SpeedReference_PT1);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_BackEmfObersvationCounterValue,
                        0U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 0U, (real_T)
                    chartInstance->c1_BackEmfObersvationCounterValue);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_SlurpDetectionCounterAppcon,
                        12U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 12U, (real_T)
                    chartInstance->c1_SlurpDetectionCounterAppcon);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_SlurpDetectionCounterMiele,
                        13U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 13U, (real_T)
                    chartInstance->c1_SlurpDetectionCounterMiele);
  _SFD_DATA_RANGE_CHECK((real_T)
                        chartInstance->c1_SlurpDetection_Appcon_ActivationCounter,
                        14U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 14U, (real_T)
                    chartInstance->c1_SlurpDetection_Appcon_ActivationCounter);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_SpeedObersvationCounterValue,
                        15U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 15U, (real_T)
                    chartInstance->c1_SpeedObersvationCounterValue);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_is_Rshunt, 21U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 21U, (real_T)
                    chartInstance->c1_is_Rshunt);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_bemf_test, 19U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 19U, (real_T)
                    chartInstance->c1_bemf_test);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_Vs_s, 50U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 50U, (real_T)
                    *chartInstance->c1_Vs_s);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD1, 52U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 52U, (real_T)
                    *chartInstance->c1_PWM_CMD1);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD2, 53U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 53U, (real_T)
                    *chartInstance->c1_PWM_CMD2);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_OFF, 54U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 54U, (real_T)
                    *chartInstance->c1_PWM_OFF);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_bemf_zcr, 55U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 55U, (real_T)
                    *chartInstance->c1_bemf_zcr);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_SlurpDetectedAppcon, 56U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 56U, (real_T)
                    *chartInstance->c1_SlurpDetectedAppcon);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_ApproxPower, 57U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 57U, (real_T)
                    *chartInstance->c1_ApproxPower);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_Bemf_control_fault, 58U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 58U, (real_T)
                    *chartInstance->c1_Bemf_control_fault);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_Speed_control_fault, 59U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 59U, (real_T)
                    *chartInstance->c1_Speed_control_fault);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_speed_pll, 60U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 60U, (real_T)
                    *chartInstance->c1_speed_pll);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_voltage_ref, 61U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 61U, (real_T)
                    *chartInstance->c1_voltage_ref);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_InvVoltageOut, 62U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 62U, (real_T)
                    *chartInstance->c1_InvVoltageOut);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_SlurpDetectedMiele, 63U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 63U, (real_T)
                    *chartInstance->c1_SlurpDetectedMiele);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_old_InvVoltageOut, 64U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 64U, (real_T)
                    *chartInstance->c1_old_InvVoltageOut);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_testpoint, 65U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 65U, (real_T)
                    *chartInstance->c1_testpoint);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_testpoint2, 66U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 66U, (real_T)
                    *chartInstance->c1_testpoint2);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_testpoint3, 67U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 67U, (real_T)
                    *chartInstance->c1_testpoint3);
}

static void initialize_params_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  const mxArray *c1_m0 = NULL;
  static const char * c1_fieldNames[3] = { "SpeedMax", "SpeedVFLimit", "pi" };

  const mxArray *c1_mxField;
  c1_struct_F7N0deEp72x1GuUaWKNfKD c1_r0;
  const mxArray *c1_m1 = NULL;
  static const char * c1_b_fieldNames[9] = { "EnableSynchronisationTime",
    "EnableSynchronisation", "EnableSynchronisationMaximumVoltageDeviation",
    "BemfKp", "BemfKi", "SpeedKp_M", "SpeedKi_M", "IsKp", "IsKi" };

  const mxArray *c1_b_mxField;
  c1_struct_AnKM5BqWxAKQWowfA30Y4B c1_r1;
  real_T c1_d0;
  real_T c1_d1;
  int16_T c1_i0;
  real_T c1_d2;
  real_T c1_d3;
  int16_T c1_i1;
  real_T c1_d4;
  real_T c1_d5;
  int16_T c1_i2;
  real_T c1_d6;
  real_T c1_d7;
  int16_T c1_i3;
  real_T c1_d8;
  real_T c1_d9;
  int16_T c1_i4;
  real_T c1_d10;
  real_T c1_d11;
  int16_T c1_i5;
  real_T c1_d12;
  real_T c1_d13;
  uint16_T c1_u0;
  c1_m0 = sf_mex_get_sfun_param(chartInstance->S, 6U, 1U);
  sf_mex_check_bus_parameter(c1_m0, 0, NULL, 3, c1_fieldNames, "openloop",
    "struct_F7N0deEp72x1GuUaWKNfKD");
  c1_mxField = sf_mex_getfield(c1_m0, "SpeedMax", "openloop", 0);
  sf_mex_import_named("openloop", sf_mex_dup(c1_mxField), &c1_r0.SpeedMax, 1, 0,
                      0U, 0, 0U, 0);
  c1_mxField = sf_mex_getfield(c1_m0, "SpeedVFLimit", "openloop", 0);
  sf_mex_import_named("openloop", sf_mex_dup(c1_mxField), &c1_r0.SpeedVFLimit, 1,
                      0, 0U, 0, 0U, 0);
  c1_mxField = sf_mex_getfield(c1_m0, "pi", "openloop", 0);
  sf_mex_import_named("openloop", sf_mex_dup(c1_mxField), &c1_r0.pi, 1, 0, 0U, 0,
                      0U, 0);
  sf_mex_destroy(&c1_m0);
  chartInstance->c1_openloop = c1_r0;
  c1_m1 = sf_mex_get_sfun_param(chartInstance->S, 1U, 1U);
  sf_mex_check_bus_parameter(c1_m1, 0, NULL, 9, c1_b_fieldNames, "closedloop",
    "struct_AnKM5BqWxAKQWowfA30Y4B");
  c1_b_mxField = sf_mex_getfield(c1_m1, "EnableSynchronisationTime",
    "closedloop", 0);
  sf_mex_import_named("closedloop", sf_mex_dup(c1_b_mxField),
                      &c1_r1.EnableSynchronisationTime, 1, 0, 0U, 0, 0U, 0);
  c1_b_mxField = sf_mex_getfield(c1_m1, "EnableSynchronisation", "closedloop", 0);
  sf_mex_import_named("closedloop", sf_mex_dup(c1_b_mxField),
                      &c1_r1.EnableSynchronisation, 1, 0, 0U, 0, 0U, 0);
  c1_b_mxField = sf_mex_getfield(c1_m1,
    "EnableSynchronisationMaximumVoltageDeviation", "closedloop", 0);
  sf_mex_import_named("closedloop", sf_mex_dup(c1_b_mxField),
                      &c1_r1.EnableSynchronisationMaximumVoltageDeviation, 1, 0,
                      0U, 0, 0U, 0);
  c1_b_mxField = sf_mex_getfield(c1_m1, "BemfKp", "closedloop", 0);
  sf_mex_import_named("closedloop", sf_mex_dup(c1_b_mxField), &c1_r1.BemfKp, 1,
                      0, 0U, 0, 0U, 0);
  c1_b_mxField = sf_mex_getfield(c1_m1, "BemfKi", "closedloop", 0);
  sf_mex_import_named("closedloop", sf_mex_dup(c1_b_mxField), &c1_r1.BemfKi, 1,
                      0, 0U, 0, 0U, 0);
  c1_b_mxField = sf_mex_getfield(c1_m1, "SpeedKp_M", "closedloop", 0);
  sf_mex_import_named("closedloop", sf_mex_dup(c1_b_mxField), &c1_r1.SpeedKp_M,
                      1, 0, 0U, 0, 0U, 0);
  c1_b_mxField = sf_mex_getfield(c1_m1, "SpeedKi_M", "closedloop", 0);
  sf_mex_import_named("closedloop", sf_mex_dup(c1_b_mxField), &c1_r1.SpeedKi_M,
                      1, 0, 0U, 0, 0U, 0);
  c1_b_mxField = sf_mex_getfield(c1_m1, "IsKp", "closedloop", 0);
  sf_mex_import_named("closedloop", sf_mex_dup(c1_b_mxField), &c1_r1.IsKp, 1, 0,
                      0U, 0, 0U, 0);
  c1_b_mxField = sf_mex_getfield(c1_m1, "IsKi", "closedloop", 0);
  sf_mex_import_named("closedloop", sf_mex_dup(c1_b_mxField), &c1_r1.IsKi, 1, 0,
                      0U, 0, 0U, 0);
  sf_mex_destroy(&c1_m1);
  chartInstance->c1_closedloop = c1_r1;
  sf_mex_import_named("delay", sf_mex_get_sfun_param(chartInstance->S, 2U, 0U),
                      &c1_d0, 0, 0, 0U, 0, 0U, 0);
  c1_d1 = c1_d0;
  if (c1_d1 < 32768.0) {
    if (c1_d1 >= -32768.0) {
      c1_i0 = (int16_T)c1_d1;
    } else {
      c1_i0 = MIN_int16_T;
    }
  } else if (c1_d1 >= 32768.0) {
    c1_i0 = MAX_int16_T;
  } else {
    c1_i0 = 0;
  }

  chartInstance->c1_delay = c1_i0;
  sf_mex_import_named("zerocrossing", sf_mex_get_sfun_param(chartInstance->S, 8U,
    0U), &c1_d2, 0, 0, 0U, 0, 0U, 0);
  c1_d3 = c1_d2;
  if (c1_d3 < 32768.0) {
    if (c1_d3 >= -32768.0) {
      c1_i1 = (int16_T)c1_d3;
    } else {
      c1_i1 = MIN_int16_T;
    }
  } else if (c1_d3 >= 32768.0) {
    c1_i1 = MAX_int16_T;
  } else {
    c1_i1 = 0;
  }

  chartInstance->c1_zerocrossing = c1_i1;
  sf_mex_import_named("scale", sf_mex_get_sfun_param(chartInstance->S, 7U, 0U),
                      &c1_d4, 0, 0, 0U, 0, 0U, 0);
  c1_d5 = c1_d4;
  if (c1_d5 < 32768.0) {
    if (c1_d5 >= -32768.0) {
      c1_i2 = (int16_T)c1_d5;
    } else {
      c1_i2 = MIN_int16_T;
    }
  } else if (c1_d5 >= 32768.0) {
    c1_i2 = MAX_int16_T;
  } else {
    c1_i2 = 0;
  }

  chartInstance->c1_scale = c1_i2;
  sf_mex_import_named("fixedPoint_SpeedLimitMax", sf_mex_get_sfun_param
                      (chartInstance->S, 3U, 0U), &c1_d6, 0, 0, 0U, 0, 0U, 0);
  c1_d7 = c1_d6;
  if (c1_d7 < 32768.0) {
    if (c1_d7 >= -32768.0) {
      c1_i3 = (int16_T)c1_d7;
    } else {
      c1_i3 = MIN_int16_T;
    }
  } else if (c1_d7 >= 32768.0) {
    c1_i3 = MAX_int16_T;
  } else {
    c1_i3 = 0;
  }

  chartInstance->c1_fixedPoint_SpeedLimitMax = c1_i3;
  sf_mex_import_named("fixedPoint_SpeedLimitMin", sf_mex_get_sfun_param
                      (chartInstance->S, 4U, 0U), &c1_d8, 0, 0, 0U, 0, 0U, 0);
  c1_d9 = c1_d8;
  if (c1_d9 < 32768.0) {
    if (c1_d9 >= -32768.0) {
      c1_i4 = (int16_T)c1_d9;
    } else {
      c1_i4 = MIN_int16_T;
    }
  } else if (c1_d9 >= 32768.0) {
    c1_i4 = MAX_int16_T;
  } else {
    c1_i4 = 0;
  }

  chartInstance->c1_fixedPoint_SpeedLimitMin = c1_i4;
  sf_mex_import_named("fixedPoint_VoltageLimitMin", sf_mex_get_sfun_param
                      (chartInstance->S, 5U, 0U), &c1_d10, 0, 0, 0U, 0, 0U, 0);
  c1_d11 = c1_d10;
  if (c1_d11 < 32768.0) {
    if (c1_d11 >= -32768.0) {
      c1_i5 = (int16_T)c1_d11;
    } else {
      c1_i5 = MIN_int16_T;
    }
  } else if (c1_d11 >= 32768.0) {
    c1_i5 = MAX_int16_T;
  } else {
    c1_i5 = 0;
  }

  chartInstance->c1_fixedPoint_VoltageLimitMin = c1_i5;
  sf_mex_import_named("PWM_MAX_DUTYCYCLE", sf_mex_get_sfun_param
                      (chartInstance->S, 0U, 0U), &c1_d12, 0, 0, 0U, 0, 0U, 0);
  c1_d13 = c1_d12;
  if (c1_d13 < 65536.0) {
    if (c1_d13 >= 0.0) {
      c1_u0 = (uint16_T)c1_d13;
    } else {
      c1_u0 = 0U;
    }
  } else if (c1_d13 >= 65536.0) {
    c1_u0 = MAX_uint16_T;
  } else {
    c1_u0 = 0U;
  }

  chartInstance->c1_PWM_MAX_DUTYCYCLE = c1_u0;
}

static void enable_c1_Motor_1pHController(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
  sf_call_output_fcn_enable(chartInstance->S, 0, "VFStart", 0);
  sf_call_output_fcn_enable(chartInstance->S, 1, "Miele", 0);
  sf_call_output_fcn_enable(chartInstance->S, 2, "Appcon", 0);
  sf_call_output_fcn_enable(chartInstance->S, 3, "Speed_Observation", 0);
  sf_call_output_fcn_enable(chartInstance->S, 4, "VFDone_Flag", 0);
  sf_call_output_fcn_enable(chartInstance->S, 5, "CheckforSynchronisation", 0);
  sf_call_output_fcn_enable(chartInstance->S, 6, "Bemf_Observation", 0);
  sf_call_output_fcn_enable(chartInstance->S, 7, "fault", 0);
  sf_call_output_fcn_enable(chartInstance->S, 8, "Protections", 0);
  sf_call_output_fcn_enable(chartInstance->S, 9, "state_variable_init", 0);
  sf_call_output_fcn_enable(chartInstance->S, 10, "PowerCalculation", 0);
  sf_call_output_fcn_enable(chartInstance->S, 11, "SpeedRateLimit", 0);
  sf_call_output_fcn_enable(chartInstance->S, 12, "Current_Measurement", 0);
  sf_call_output_fcn_enable(chartInstance->S, 13, "SpeedToAngle", 0);
  sf_call_output_fcn_enable(chartInstance->S, 14, "PI_Controller", 0);
  sf_call_output_fcn_enable(chartInstance->S, 15, "modulation", 0);
  sf_call_output_fcn_enable(chartInstance->S, 16, "Duty_Cycle_To_Voltage", 0);
  chartInstance->c1_presentTicks = (uint32_T)muDoubleScalarFloor((_sfTime_ -
    sf_get_start_time(chartInstance->S)) / 0.000125 + 0.5);
  chartInstance->c1_previousTicks = chartInstance->c1_presentTicks;
}

static void disable_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c1_presentTicks = (uint32_T)muDoubleScalarFloor((_sfTime_ -
    sf_get_start_time(chartInstance->S)) / 0.000125 + 0.5);
  chartInstance->c1_elapsedTicks = chartInstance->c1_presentTicks -
    chartInstance->c1_previousTicks;
  chartInstance->c1_previousTicks = chartInstance->c1_presentTicks;
  if (chartInstance->c1_temporalCounter_i1 + chartInstance->c1_elapsedTicks <=
      524287U) {
    chartInstance->c1_temporalCounter_i1 += chartInstance->c1_elapsedTicks;
  } else {
    chartInstance->c1_temporalCounter_i1 = 524287U;
  }

  sf_call_output_fcn_disable(chartInstance->S, 0, "VFStart", 0);
  sf_call_output_fcn_disable(chartInstance->S, 1, "Miele", 0);
  sf_call_output_fcn_disable(chartInstance->S, 2, "Appcon", 0);
  sf_call_output_fcn_disable(chartInstance->S, 3, "Speed_Observation", 0);
  sf_call_output_fcn_disable(chartInstance->S, 4, "VFDone_Flag", 0);
  sf_call_output_fcn_disable(chartInstance->S, 5, "CheckforSynchronisation", 0);
  sf_call_output_fcn_disable(chartInstance->S, 6, "Bemf_Observation", 0);
  sf_call_output_fcn_disable(chartInstance->S, 7, "fault", 0);
  sf_call_output_fcn_disable(chartInstance->S, 8, "Protections", 0);
  sf_call_output_fcn_disable(chartInstance->S, 9, "state_variable_init", 0);
  sf_call_output_fcn_disable(chartInstance->S, 10, "PowerCalculation", 0);
  sf_call_output_fcn_disable(chartInstance->S, 11, "SpeedRateLimit", 0);
  sf_call_output_fcn_disable(chartInstance->S, 12, "Current_Measurement", 0);
  sf_call_output_fcn_disable(chartInstance->S, 13, "SpeedToAngle", 0);
  sf_call_output_fcn_disable(chartInstance->S, 14, "PI_Controller", 0);
  sf_call_output_fcn_disable(chartInstance->S, 15, "modulation", 0);
  sf_call_output_fcn_disable(chartInstance->S, 16, "Duty_Cycle_To_Voltage", 0);
}

static void c1_update_debugger_state_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  uint32_T c1_prevAniVal;
  c1_prevAniVal = _SFD_GET_ANIMATION();
  _SFD_SET_ANIMATION(0U);
  _SFD_SET_HONOR_BREAKPOINTS(0U);
  if (chartInstance->c1_is_active_c1_Motor_1pHController == 1U) {
    _SFD_CC_CALL(CHART_ACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_c1_Motor_1pHController == c1_IN_Stop_Mode) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 24U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 24U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_c1_Motor_1pHController == c1_IN_Init_Mode) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_c1_Motor_1pHController == c1_IN_Motor_On) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 5U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_Motor_On == c1_IN_Alignment_Mode) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 7U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 7U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_Motor_On == c1_IN_Error_Mode) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 9U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 9U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_Motor_On == c1_IN_Alignment_Done) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 6U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_Motor_On == c1_IN_VF_Mode) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 10U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 10U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_Motor_On == c1_IN_ControlMode) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 8U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 8U, chartInstance->c1_sfEvent);
  }

  _SFD_SET_ANIMATION(c1_prevAniVal);
  _SFD_SET_HONOR_BREAKPOINTS(1U);
  _SFD_ANIMATE();
}

static void ext_mode_exec_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  const mxArray *c1_st;
  const mxArray *c1_y = NULL;
  int32_T c1_hoistedGlobal;
  const mxArray *c1_b_y = NULL;
  boolean_T c1_b_hoistedGlobal;
  const mxArray *c1_c_y = NULL;
  int16_T c1_c_hoistedGlobal;
  const mxArray *c1_d_y = NULL;
  int32_T c1_d_hoistedGlobal;
  const mxArray *c1_e_y = NULL;
  int32_T c1_e_hoistedGlobal;
  const mxArray *c1_f_y = NULL;
  uint16_T c1_f_hoistedGlobal;
  const mxArray *c1_g_y = NULL;
  boolean_T c1_g_hoistedGlobal;
  const mxArray *c1_h_y = NULL;
  boolean_T c1_h_hoistedGlobal;
  const mxArray *c1_i_y = NULL;
  boolean_T c1_i_hoistedGlobal;
  const mxArray *c1_j_y = NULL;
  int16_T c1_j_hoistedGlobal;
  const mxArray *c1_k_y = NULL;
  int16_T c1_k_hoistedGlobal;
  const mxArray *c1_l_y = NULL;
  c1_ControlFlag c1_l_hoistedGlobal;
  const mxArray *c1_m_y = NULL;
  int32_T c1_u;
  const mxArray *c1_n_y = NULL;
  const mxArray *c1_m2 = NULL;
  int16_T c1_m_hoistedGlobal;
  const mxArray *c1_o_y = NULL;
  int16_T c1_n_hoistedGlobal;
  const mxArray *c1_p_y = NULL;
  int16_T c1_o_hoistedGlobal;
  const mxArray *c1_q_y = NULL;
  uint16_T c1_p_hoistedGlobal;
  const mxArray *c1_r_y = NULL;
  uint16_T c1_q_hoistedGlobal;
  const mxArray *c1_s_y = NULL;
  int16_T c1_r_hoistedGlobal;
  const mxArray *c1_t_y = NULL;
  int32_T c1_s_hoistedGlobal;
  const mxArray *c1_u_y = NULL;
  uint16_T c1_t_hoistedGlobal;
  const mxArray *c1_v_y = NULL;
  uint16_T c1_u_hoistedGlobal;
  const mxArray *c1_w_y = NULL;
  uint16_T c1_v_hoistedGlobal;
  const mxArray *c1_x_y = NULL;
  uint16_T c1_w_hoistedGlobal;
  const mxArray *c1_y_y = NULL;
  uint16_T c1_x_hoistedGlobal;
  const mxArray *c1_ab_y = NULL;
  int16_T c1_y_hoistedGlobal;
  const mxArray *c1_bb_y = NULL;
  int16_T c1_ab_hoistedGlobal;
  const mxArray *c1_cb_y = NULL;
  int16_T c1_bb_hoistedGlobal;
  const mxArray *c1_db_y = NULL;
  int16_T c1_cb_hoistedGlobal;
  const mxArray *c1_eb_y = NULL;
  int16_T c1_db_hoistedGlobal;
  const mxArray *c1_fb_y = NULL;
  int32_T c1_eb_hoistedGlobal;
  const mxArray *c1_gb_y = NULL;
  int32_T c1_fb_hoistedGlobal;
  const mxArray *c1_hb_y = NULL;
  int32_T c1_gb_hoistedGlobal;
  const mxArray *c1_ib_y = NULL;
  int16_T c1_hb_hoistedGlobal;
  const mxArray *c1_jb_y = NULL;
  int32_T c1_ib_hoistedGlobal;
  const mxArray *c1_kb_y = NULL;
  int16_T c1_jb_hoistedGlobal;
  const mxArray *c1_lb_y = NULL;
  int32_T c1_kb_hoistedGlobal;
  const mxArray *c1_mb_y = NULL;
  boolean_T c1_lb_hoistedGlobal;
  const mxArray *c1_nb_y = NULL;
  int16_T c1_mb_hoistedGlobal;
  const mxArray *c1_ob_y = NULL;
  boolean_T c1_nb_hoistedGlobal;
  const mxArray *c1_pb_y = NULL;
  int16_T c1_ob_hoistedGlobal;
  const mxArray *c1_qb_y = NULL;
  int16_T c1_pb_hoistedGlobal;
  const mxArray *c1_rb_y = NULL;
  int16_T c1_qb_hoistedGlobal;
  const mxArray *c1_sb_y = NULL;
  uint8_T c1_rb_hoistedGlobal;
  const mxArray *c1_tb_y = NULL;
  uint8_T c1_sb_hoistedGlobal;
  const mxArray *c1_ub_y = NULL;
  uint8_T c1_tb_hoistedGlobal;
  const mxArray *c1_vb_y = NULL;
  uint32_T c1_ub_hoistedGlobal;
  const mxArray *c1_wb_y = NULL;
  uint32_T c1_vb_hoistedGlobal;
  const mxArray *c1_xb_y = NULL;
  c1_st = NULL;
  c1_st = NULL;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_createcellmatrix(47, 1), false);
  c1_hoistedGlobal = *chartInstance->c1_ApproxPower;
  c1_b_y = NULL;
  sf_mex_assign(&c1_b_y, sf_mex_create("y", &c1_hoistedGlobal, 6, 0U, 0U, 0U, 0),
                false);
  sf_mex_setcell(c1_y, 0, c1_b_y);
  c1_b_hoistedGlobal = *chartInstance->c1_Bemf_control_fault;
  c1_c_y = NULL;
  sf_mex_assign(&c1_c_y, sf_mex_create("y", &c1_b_hoistedGlobal, 11, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 1, c1_c_y);
  c1_c_hoistedGlobal = *chartInstance->c1_InvVoltageOut;
  c1_d_y = NULL;
  sf_mex_assign(&c1_d_y, sf_mex_create("y", &c1_c_hoistedGlobal, 4, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 2, c1_d_y);
  c1_d_hoistedGlobal = *chartInstance->c1_PWM_CMD1;
  c1_e_y = NULL;
  sf_mex_assign(&c1_e_y, sf_mex_create("y", &c1_d_hoistedGlobal, 6, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 3, c1_e_y);
  c1_e_hoistedGlobal = *chartInstance->c1_PWM_CMD2;
  c1_f_y = NULL;
  sf_mex_assign(&c1_f_y, sf_mex_create("y", &c1_e_hoistedGlobal, 6, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 4, c1_f_y);
  c1_f_hoistedGlobal = *chartInstance->c1_PWM_OFF;
  c1_g_y = NULL;
  sf_mex_assign(&c1_g_y, sf_mex_create("y", &c1_f_hoistedGlobal, 5, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 5, c1_g_y);
  c1_g_hoistedGlobal = *chartInstance->c1_SlurpDetectedAppcon;
  c1_h_y = NULL;
  sf_mex_assign(&c1_h_y, sf_mex_create("y", &c1_g_hoistedGlobal, 11, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 6, c1_h_y);
  c1_h_hoistedGlobal = *chartInstance->c1_SlurpDetectedMiele;
  c1_i_y = NULL;
  sf_mex_assign(&c1_i_y, sf_mex_create("y", &c1_h_hoistedGlobal, 11, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 7, c1_i_y);
  c1_i_hoistedGlobal = *chartInstance->c1_Speed_control_fault;
  c1_j_y = NULL;
  sf_mex_assign(&c1_j_y, sf_mex_create("y", &c1_i_hoistedGlobal, 11, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 8, c1_j_y);
  c1_j_hoistedGlobal = *chartInstance->c1_Vs_s;
  c1_k_y = NULL;
  sf_mex_assign(&c1_k_y, sf_mex_create("y", &c1_j_hoistedGlobal, 4, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 9, c1_k_y);
  c1_k_hoistedGlobal = *chartInstance->c1_bemf_zcr;
  c1_l_y = NULL;
  sf_mex_assign(&c1_l_y, sf_mex_create("y", &c1_k_hoistedGlobal, 4, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 10, c1_l_y);
  c1_l_hoistedGlobal = *chartInstance->c1_controller_flag;
  c1_m_y = NULL;
  sf_mex_check_enum("ControlFlag", 10, c1_sv0, c1_iv0);
  c1_u = (int32_T)c1_l_hoistedGlobal;
  c1_n_y = NULL;
  sf_mex_assign(&c1_n_y, sf_mex_create("y", &c1_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_m2, c1_n_y, false);
  sf_mex_assign(&c1_m_y, sf_mex_create_enum("ControlFlag", c1_m2), false);
  sf_mex_destroy(&c1_m2);
  sf_mex_setcell(c1_y, 11, c1_m_y);
  c1_m_hoistedGlobal = *chartInstance->c1_old_InvVoltageOut;
  c1_o_y = NULL;
  sf_mex_assign(&c1_o_y, sf_mex_create("y", &c1_m_hoistedGlobal, 4, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 12, c1_o_y);
  c1_n_hoistedGlobal = *chartInstance->c1_speed_pll;
  c1_p_y = NULL;
  sf_mex_assign(&c1_p_y, sf_mex_create("y", &c1_n_hoistedGlobal, 4, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 13, c1_p_y);
  c1_o_hoistedGlobal = *chartInstance->c1_testpoint;
  c1_q_y = NULL;
  sf_mex_assign(&c1_q_y, sf_mex_create("y", &c1_o_hoistedGlobal, 4, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 14, c1_q_y);
  c1_p_hoistedGlobal = *chartInstance->c1_testpoint2;
  c1_r_y = NULL;
  sf_mex_assign(&c1_r_y, sf_mex_create("y", &c1_p_hoistedGlobal, 5, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 15, c1_r_y);
  c1_q_hoistedGlobal = *chartInstance->c1_testpoint3;
  c1_s_y = NULL;
  sf_mex_assign(&c1_s_y, sf_mex_create("y", &c1_q_hoistedGlobal, 5, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 16, c1_s_y);
  c1_r_hoistedGlobal = *chartInstance->c1_voltage_ref;
  c1_t_y = NULL;
  sf_mex_assign(&c1_t_y, sf_mex_create("y", &c1_r_hoistedGlobal, 4, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 17, c1_t_y);
  c1_s_hoistedGlobal = chartInstance->c1_BackEmfObersvationCounterValue;
  c1_u_y = NULL;
  sf_mex_assign(&c1_u_y, sf_mex_create("y", &c1_s_hoistedGlobal, 6, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 18, c1_u_y);
  c1_t_hoistedGlobal =
    chartInstance->c1_CurrentZeroCrossingFlagNegativeToPositiveCounter;
  c1_v_y = NULL;
  sf_mex_assign(&c1_v_y, sf_mex_create("y", &c1_t_hoistedGlobal, 5, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 19, c1_v_y);
  c1_u_hoistedGlobal =
    chartInstance->c1_CurrentZeroCrossingFlagPositiveToNegativeCounter;
  c1_w_y = NULL;
  sf_mex_assign(&c1_w_y, sf_mex_create("y", &c1_u_hoistedGlobal, 5, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 20, c1_w_y);
  c1_v_hoistedGlobal =
    chartInstance->c1_FirstCurrentZeroCrossingFlagNegativeToPositive;
  c1_x_y = NULL;
  sf_mex_assign(&c1_x_y, sf_mex_create("y", &c1_v_hoistedGlobal, 5, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 21, c1_x_y);
  c1_w_hoistedGlobal =
    chartInstance->c1_FirstCurrentZeroCrossingFlagPositiveToNegative;
  c1_y_y = NULL;
  sf_mex_assign(&c1_y_y, sf_mex_create("y", &c1_w_hoistedGlobal, 5, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 22, c1_y_y);
  c1_x_hoistedGlobal = chartInstance->c1_MaskFlag;
  c1_ab_y = NULL;
  sf_mex_assign(&c1_ab_y, sf_mex_create("y", &c1_x_hoistedGlobal, 5, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 23, c1_ab_y);
  c1_y_hoistedGlobal = chartInstance->c1_OL_speed_state;
  c1_bb_y = NULL;
  sf_mex_assign(&c1_bb_y, sf_mex_create("y", &c1_y_hoistedGlobal, 4, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 24, c1_bb_y);
  c1_ab_hoistedGlobal = chartInstance->c1_OL_voltage_state_bemf;
  c1_cb_y = NULL;
  sf_mex_assign(&c1_cb_y, sf_mex_create("y", &c1_ab_hoistedGlobal, 4, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 25, c1_cb_y);
  c1_bb_hoistedGlobal = chartInstance->c1_OL_voltage_state_is;
  c1_db_y = NULL;
  sf_mex_assign(&c1_db_y, sf_mex_create("y", &c1_bb_hoistedGlobal, 4, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 26, c1_db_y);
  c1_cb_hoistedGlobal = chartInstance->c1_OLspeed_sat;
  c1_eb_y = NULL;
  sf_mex_assign(&c1_eb_y, sf_mex_create("y", &c1_cb_hoistedGlobal, 4, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 27, c1_eb_y);
  c1_db_hoistedGlobal = chartInstance->c1_OLvoltage_sat;
  c1_fb_y = NULL;
  sf_mex_assign(&c1_fb_y, sf_mex_create("y", &c1_db_hoistedGlobal, 4, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 28, c1_fb_y);
  c1_eb_hoistedGlobal = chartInstance->c1_PT1_StateVariable;
  c1_gb_y = NULL;
  sf_mex_assign(&c1_gb_y, sf_mex_create("y", &c1_eb_hoistedGlobal, 6, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 29, c1_gb_y);
  c1_fb_hoistedGlobal = chartInstance->c1_SlurpDetectionCounterAppcon;
  c1_hb_y = NULL;
  sf_mex_assign(&c1_hb_y, sf_mex_create("y", &c1_fb_hoistedGlobal, 6, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 30, c1_hb_y);
  c1_gb_hoistedGlobal = chartInstance->c1_SlurpDetectionCounterMiele;
  c1_ib_y = NULL;
  sf_mex_assign(&c1_ib_y, sf_mex_create("y", &c1_gb_hoistedGlobal, 6, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 31, c1_ib_y);
  c1_hb_hoistedGlobal =
    chartInstance->c1_SlurpDetection_Appcon_ActivationCounter;
  c1_jb_y = NULL;
  sf_mex_assign(&c1_jb_y, sf_mex_create("y", &c1_hb_hoistedGlobal, 4, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 32, c1_jb_y);
  c1_ib_hoistedGlobal = chartInstance->c1_SpeedObersvationCounterValue;
  c1_kb_y = NULL;
  sf_mex_assign(&c1_kb_y, sf_mex_create("y", &c1_ib_hoistedGlobal, 6, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 33, c1_kb_y);
  c1_jb_hoistedGlobal = chartInstance->c1_SpeedReference_PT1;
  c1_lb_y = NULL;
  sf_mex_assign(&c1_lb_y, sf_mex_create("y", &c1_jb_hoistedGlobal, 4, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 34, c1_lb_y);
  c1_kb_hoistedGlobal = chartInstance->c1_SynchronisationTime_state;
  c1_mb_y = NULL;
  sf_mex_assign(&c1_mb_y, sf_mex_create("y", &c1_kb_hoistedGlobal, 6, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 35, c1_mb_y);
  c1_lb_hoistedGlobal = chartInstance->c1_Vf_Done_SynchronisationEnable;
  c1_nb_y = NULL;
  sf_mex_assign(&c1_nb_y, sf_mex_create("y", &c1_lb_hoistedGlobal, 11, 0U, 0U,
    0U, 0), false);
  sf_mex_setcell(c1_y, 36, c1_nb_y);
  c1_mb_hoistedGlobal = chartInstance->c1_bemf_test;
  c1_ob_y = NULL;
  sf_mex_assign(&c1_ob_y, sf_mex_create("y", &c1_mb_hoistedGlobal, 4, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 37, c1_ob_y);
  c1_nb_hoistedGlobal = chartInstance->c1_fault;
  c1_pb_y = NULL;
  sf_mex_assign(&c1_pb_y, sf_mex_create("y", &c1_nb_hoistedGlobal, 11, 0U, 0U,
    0U, 0), false);
  sf_mex_setcell(c1_y, 38, c1_pb_y);
  c1_ob_hoistedGlobal = chartInstance->c1_is_Rshunt;
  c1_qb_y = NULL;
  sf_mex_assign(&c1_qb_y, sf_mex_create("y", &c1_ob_hoistedGlobal, 4, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 39, c1_qb_y);
  c1_pb_hoistedGlobal = chartInstance->c1_old_is;
  c1_rb_y = NULL;
  sf_mex_assign(&c1_rb_y, sf_mex_create("y", &c1_pb_hoistedGlobal, 4, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 40, c1_rb_y);
  c1_qb_hoistedGlobal = chartInstance->c1_rotor_position_reference;
  c1_sb_y = NULL;
  sf_mex_assign(&c1_sb_y, sf_mex_create("y", &c1_qb_hoistedGlobal, 4, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 41, c1_sb_y);
  c1_rb_hoistedGlobal = chartInstance->c1_is_active_c1_Motor_1pHController;
  c1_tb_y = NULL;
  sf_mex_assign(&c1_tb_y, sf_mex_create("y", &c1_rb_hoistedGlobal, 3, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 42, c1_tb_y);
  c1_sb_hoistedGlobal = chartInstance->c1_is_c1_Motor_1pHController;
  c1_ub_y = NULL;
  sf_mex_assign(&c1_ub_y, sf_mex_create("y", &c1_sb_hoistedGlobal, 3, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 43, c1_ub_y);
  c1_tb_hoistedGlobal = chartInstance->c1_is_Motor_On;
  c1_vb_y = NULL;
  sf_mex_assign(&c1_vb_y, sf_mex_create("y", &c1_tb_hoistedGlobal, 3, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 44, c1_vb_y);
  c1_ub_hoistedGlobal = chartInstance->c1_temporalCounter_i1;
  c1_wb_y = NULL;
  sf_mex_assign(&c1_wb_y, sf_mex_create("y", &c1_ub_hoistedGlobal, 7, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 45, c1_wb_y);
  c1_vb_hoistedGlobal = chartInstance->c1_previousTicks;
  c1_xb_y = NULL;
  sf_mex_assign(&c1_xb_y, sf_mex_create("y", &c1_vb_hoistedGlobal, 7, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 46, c1_xb_y);
  sf_mex_assign(&c1_st, c1_y, false);
  return c1_st;
}

static void set_sim_state_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, const mxArray *c1_st)
{
  const mxArray *c1_u;
  c1_u = sf_mex_dup(c1_st);
  *chartInstance->c1_ApproxPower = c1_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c1_u, 0)), "ApproxPower");
  *chartInstance->c1_Bemf_control_fault = c1_k_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 1)), "Bemf_control_fault");
  *chartInstance->c1_InvVoltageOut = c1_e_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 2)), "InvVoltageOut");
  *chartInstance->c1_PWM_CMD1 = c1_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c1_u, 3)), "PWM_CMD1");
  *chartInstance->c1_PWM_CMD2 = c1_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c1_u, 4)), "PWM_CMD2");
  *chartInstance->c1_PWM_OFF = c1_m_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c1_u, 5)), "PWM_OFF");
  *chartInstance->c1_SlurpDetectedAppcon = c1_k_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 6)), "SlurpDetectedAppcon");
  *chartInstance->c1_SlurpDetectedMiele = c1_k_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 7)), "SlurpDetectedMiele");
  *chartInstance->c1_Speed_control_fault = c1_k_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 8)), "Speed_control_fault");
  *chartInstance->c1_Vs_s = c1_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c1_u, 9)), "Vs_s");
  *chartInstance->c1_bemf_zcr = c1_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c1_u, 10)), "bemf_zcr");
  *chartInstance->c1_controller_flag = c1_g_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 11)), "controller_flag");
  *chartInstance->c1_old_InvVoltageOut = c1_e_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 12)), "old_InvVoltageOut");
  *chartInstance->c1_speed_pll = c1_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c1_u, 13)), "speed_pll");
  *chartInstance->c1_testpoint = c1_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c1_u, 14)), "testpoint");
  *chartInstance->c1_testpoint2 = c1_m_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 15)), "testpoint2");
  *chartInstance->c1_testpoint3 = c1_m_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 16)), "testpoint3");
  *chartInstance->c1_voltage_ref = c1_e_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 17)), "voltage_ref");
  chartInstance->c1_BackEmfObersvationCounterValue = c1_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c1_u, 18)),
     "BackEmfObersvationCounterValue");
  chartInstance->c1_CurrentZeroCrossingFlagNegativeToPositiveCounter =
    c1_m_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c1_u, 19)),
    "CurrentZeroCrossingFlagNegativeToPositiveCounter");
  chartInstance->c1_CurrentZeroCrossingFlagPositiveToNegativeCounter =
    c1_m_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c1_u, 20)),
    "CurrentZeroCrossingFlagPositiveToNegativeCounter");
  chartInstance->c1_FirstCurrentZeroCrossingFlagNegativeToPositive =
    c1_m_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c1_u, 21)),
    "FirstCurrentZeroCrossingFlagNegativeToPositive");
  chartInstance->c1_FirstCurrentZeroCrossingFlagPositiveToNegative =
    c1_m_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c1_u, 22)),
    "FirstCurrentZeroCrossingFlagPositiveToNegative");
  chartInstance->c1_MaskFlag = c1_m_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c1_u, 23)), "MaskFlag");
  chartInstance->c1_OL_speed_state = c1_e_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 24)), "OL_speed_state");
  chartInstance->c1_OL_voltage_state_bemf = c1_e_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 25)), "OL_voltage_state_bemf");
  chartInstance->c1_OL_voltage_state_is = c1_e_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 26)), "OL_voltage_state_is");
  chartInstance->c1_OLspeed_sat = c1_e_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 27)), "OLspeed_sat");
  chartInstance->c1_OLvoltage_sat = c1_e_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 28)), "OLvoltage_sat");
  chartInstance->c1_PT1_StateVariable = c1_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 29)), "PT1_StateVariable");
  chartInstance->c1_SlurpDetectionCounterAppcon = c1_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c1_u, 30)),
     "SlurpDetectionCounterAppcon");
  chartInstance->c1_SlurpDetectionCounterMiele = c1_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c1_u, 31)),
     "SlurpDetectionCounterMiele");
  chartInstance->c1_SlurpDetection_Appcon_ActivationCounter =
    c1_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c1_u, 32)),
    "SlurpDetection_Appcon_ActivationCounter");
  chartInstance->c1_SpeedObersvationCounterValue = c1_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c1_u, 33)),
     "SpeedObersvationCounterValue");
  chartInstance->c1_SpeedReference_PT1 = c1_e_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 34)), "SpeedReference_PT1");
  chartInstance->c1_SynchronisationTime_state = c1_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c1_u, 35)),
     "SynchronisationTime_state");
  chartInstance->c1_Vf_Done_SynchronisationEnable = c1_k_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c1_u, 36)),
     "Vf_Done_SynchronisationEnable");
  chartInstance->c1_bemf_test = c1_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c1_u, 37)), "bemf_test");
  chartInstance->c1_fault = c1_k_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c1_u, 38)), "fault");
  chartInstance->c1_is_Rshunt = c1_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c1_u, 39)), "is_Rshunt");
  chartInstance->c1_old_is = c1_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c1_u, 40)), "old_is");
  chartInstance->c1_rotor_position_reference = c1_e_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c1_u, 41)),
     "rotor_position_reference");
  chartInstance->c1_is_active_c1_Motor_1pHController = c1_c_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c1_u, 42)),
     "is_active_c1_Motor_1pHController");
  chartInstance->c1_is_c1_Motor_1pHController = c1_c_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c1_u, 43)),
     "is_c1_Motor_1pHController");
  chartInstance->c1_is_Motor_On = c1_c_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 44)), "is_Motor_On");
  chartInstance->c1_temporalCounter_i1 = c1_p_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 45)), "temporalCounter_i1");
  chartInstance->c1_previousTicks = c1_p_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 46)), "previousTicks");
  sf_mex_assign(&chartInstance->c1_setSimStateSideEffectsInfo,
                c1_r_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c1_u, 47)), "setSimStateSideEffectsInfo"), true);
  sf_mex_destroy(&c1_u);
  chartInstance->c1_doSetSimStateSideEffects = 1U;
  c1_update_debugger_state_c1_Motor_1pHController(chartInstance);
  sf_mex_destroy(&c1_st);
}

static void c1_set_sim_state_side_effects_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  if (chartInstance->c1_doSetSimStateSideEffects != 0) {
    if (chartInstance->c1_is_c1_Motor_1pHController == c1_IN_Init_Mode) {
      chartInstance->c1_tp_Init_Mode = 1U;
    } else {
      chartInstance->c1_tp_Init_Mode = 0U;
    }

    if (chartInstance->c1_is_c1_Motor_1pHController == c1_IN_Motor_On) {
      chartInstance->c1_tp_Motor_On = 1U;
    } else {
      chartInstance->c1_tp_Motor_On = 0U;
    }

    if (chartInstance->c1_is_Motor_On == c1_IN_Alignment_Done) {
      chartInstance->c1_tp_Alignment_Done = 1U;
      if (sf_mex_sub(chartInstance->c1_setSimStateSideEffectsInfo,
                     "setSimStateSideEffectsInfo", 1, 4) == 0.0) {
        chartInstance->c1_temporalCounter_i1 = 0U;
      }
    } else {
      chartInstance->c1_tp_Alignment_Done = 0U;
    }

    if (chartInstance->c1_is_Motor_On == c1_IN_Alignment_Mode) {
      chartInstance->c1_tp_Alignment_Mode = 1U;
      if (sf_mex_sub(chartInstance->c1_setSimStateSideEffectsInfo,
                     "setSimStateSideEffectsInfo", 1, 5) == 0.0) {
        chartInstance->c1_temporalCounter_i1 = 0U;
      }
    } else {
      chartInstance->c1_tp_Alignment_Mode = 0U;
    }

    if (chartInstance->c1_is_Motor_On == c1_IN_ControlMode) {
      chartInstance->c1_tp_ControlMode = 1U;
    } else {
      chartInstance->c1_tp_ControlMode = 0U;
    }

    if (chartInstance->c1_is_Motor_On == c1_IN_Error_Mode) {
      chartInstance->c1_tp_Error_Mode = 1U;
    } else {
      chartInstance->c1_tp_Error_Mode = 0U;
    }

    if (chartInstance->c1_is_Motor_On == c1_IN_VF_Mode) {
      chartInstance->c1_tp_VF_Mode = 1U;
    } else {
      chartInstance->c1_tp_VF_Mode = 0U;
    }

    if (chartInstance->c1_is_c1_Motor_1pHController == c1_IN_Stop_Mode) {
      chartInstance->c1_tp_Stop_Mode = 1U;
    } else {
      chartInstance->c1_tp_Stop_Mode = 0U;
    }

    chartInstance->c1_doSetSimStateSideEffects = 0U;
  }
}

static void finalize_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  sfListenerLightTerminate(chartInstance->c1_RuntimeVar);
  sf_mex_destroy(&chartInstance->c1_setSimStateSideEffectsInfo);
  covrtDeleteStateflowInstanceData(chartInstance->c1_covrtInstance);
}

static void sf_gateway_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  c1_set_sim_state_side_effects_c1_Motor_1pHController(chartInstance);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c1_presentTicks = (uint32_T)muDoubleScalarFloor((_sfTime_ -
    sf_get_start_time(chartInstance->S)) / 0.000125 + 0.5);
  chartInstance->c1_elapsedTicks = chartInstance->c1_presentTicks -
    chartInstance->c1_previousTicks;
  chartInstance->c1_previousTicks = chartInstance->c1_presentTicks;
  if (chartInstance->c1_temporalCounter_i1 + chartInstance->c1_elapsedTicks <=
      524287U) {
    chartInstance->c1_temporalCounter_i1 += chartInstance->c1_elapsedTicks;
  } else {
    chartInstance->c1_temporalCounter_i1 = 524287U;
  }

  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 0, chartInstance->c1_sfEvent);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_fixedPoint_SpeedKi_M_int16,
                        49U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 49U, (real_T)
                    *chartInstance->c1_fixedPoint_SpeedKi_M_int16);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_fixedPoint_SpeedKp_M_int16,
                        48U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 48U, (real_T)
                    *chartInstance->c1_fixedPoint_SpeedKp_M_int16);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_fixedPoint_BemfKi_int16, 47U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 47U, (real_T)
                    *chartInstance->c1_fixedPoint_BemfKi_int16);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_fixedPoint_BemfKp_int16, 46U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 46U, (real_T)
                    *chartInstance->c1_fixedPoint_BemfKp_int16);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_fixedPoint_IsKi_int16, 45U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 45U, (real_T)
                    *chartInstance->c1_fixedPoint_IsKi_int16);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_fixedPoint_IsKp_int16, 44U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 44U, (real_T)
                    *chartInstance->c1_fixedPoint_IsKp_int16);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_start_angle_neg, 43U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 43U, (real_T)
                    *chartInstance->c1_start_angle_neg);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_start_angle_pos, 42U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 42U, (real_T)
                    *chartInstance->c1_start_angle_pos);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_bemf_vf, 41U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 41U, (real_T)
                    *chartInstance->c1_bemf_vf);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_vdc, 40U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 40U, (real_T)
                    *chartInstance->c1_vdc);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_Vs2, 39U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 39U, (real_T)
                    *chartInstance->c1_Vs2);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_Vs1, 38U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 38U, (real_T)
                    *chartInstance->c1_Vs1);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_start_ctr, 37U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 37U, (real_T)
                    *chartInstance->c1_start_ctr);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_align_delay, 36U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 36U, (real_T)
                    *chartInstance->c1_align_delay);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_fault2, 35U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 35U, (real_T)
                    *chartInstance->c1_fault2);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_fault1, 34U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 34U, (real_T)
                    *chartInstance->c1_fault1);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_iRshunt2_adc, 33U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 33U, (real_T)
                    *chartInstance->c1_iRshunt2_adc);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_iRshunt1_adc, 32U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 32U, (real_T)
                    *chartInstance->c1_iRshunt1_adc);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_is_adc, 31U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 31U, (real_T)
                    *chartInstance->c1_is_adc);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_bemf_reference, 30U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 30U, (real_T)
                    *chartInstance->c1_bemf_reference);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_current_reference, 29U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 29U, (real_T)
                    *chartInstance->c1_current_reference);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_voltage_reference, 28U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 28U, (real_T)
                    *chartInstance->c1_voltage_reference);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_speed_reference, 27U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 27U, (real_T)
                    *chartInstance->c1_speed_reference);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_Direction, 25U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 25U, (real_T)
                    *chartInstance->c1_Direction);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_fault_clear, 24U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 24U, (real_T)
                    *chartInstance->c1_fault_clear);
  chartInstance->c1_sfEvent = CALL_EVENT;
  c1_chartstep_c1_Motor_1pHController(chartInstance);
  _SFD_SYMBOL_SCOPE_POP();
}

static void mdl_start_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  static const uint32_T c1_decisionTxtStartIdx = 0U;
  static const uint32_T c1_decisionTxtEndIdx = 0U;
  static const uint32_T c1_b_decisionTxtStartIdx = 0U;
  static const uint32_T c1_b_decisionTxtEndIdx = 0U;
  static const uint32_T c1_c_decisionTxtStartIdx = 0U;
  static const uint32_T c1_c_decisionTxtEndIdx = 0U;
  static const uint32_T c1_d_decisionTxtStartIdx = 0U;
  static const uint32_T c1_d_decisionTxtEndIdx = 0U;
  static const uint32_T c1_saturationTxtStartIdx[2] = { 409U, 418U };

  static const uint32_T c1_saturationTxtEndIdx[2] = { 417U, 426U };

  static const uint32_T c1_e_decisionTxtStartIdx = 0U;
  static const uint32_T c1_e_decisionTxtEndIdx = 0U;
  static const uint32_T c1_b_saturationTxtStartIdx[1] = { 153U };

  static const uint32_T c1_b_saturationTxtEndIdx[1] = { 173U };

  static const uint32_T c1_f_decisionTxtStartIdx = 0U;
  static const uint32_T c1_f_decisionTxtEndIdx = 0U;
  static const uint32_T c1_g_decisionTxtStartIdx = 0U;
  static const uint32_T c1_g_decisionTxtEndIdx = 0U;
  static const uint32_T c1_c_saturationTxtStartIdx[5] = { 1349U, 1358U, 617U,
    756U, 865U };

  static const uint32_T c1_c_saturationTxtEndIdx[5] = { 1357U, 1366U, 637U, 776U,
    885U };

  static const uint32_T c1_h_decisionTxtStartIdx = 0U;
  static const uint32_T c1_h_decisionTxtEndIdx = 0U;
  static const uint32_T c1_i_decisionTxtStartIdx = 0U;
  static const uint32_T c1_i_decisionTxtEndIdx = 0U;
  static const uint32_T c1_j_decisionTxtStartIdx = 0U;
  static const uint32_T c1_j_decisionTxtEndIdx = 0U;
  static const uint32_T c1_k_decisionTxtStartIdx = 0U;
  static const uint32_T c1_k_decisionTxtEndIdx = 0U;
  static const uint32_T c1_l_decisionTxtStartIdx = 0U;
  static const uint32_T c1_l_decisionTxtEndIdx = 0U;
  static const uint32_T c1_m_decisionTxtStartIdx = 0U;
  static const uint32_T c1_m_decisionTxtEndIdx = 0U;
  static const uint32_T c1_n_decisionTxtStartIdx = 0U;
  static const uint32_T c1_n_decisionTxtEndIdx = 0U;
  static const uint32_T c1_o_decisionTxtStartIdx = 0U;
  static const uint32_T c1_o_decisionTxtEndIdx = 0U;
  static const uint32_T c1_p_decisionTxtStartIdx = 0U;
  static const uint32_T c1_p_decisionTxtEndIdx = 0U;
  static const uint32_T c1_q_decisionTxtStartIdx = 0U;
  static const uint32_T c1_q_decisionTxtEndIdx = 0U;
  static const uint32_T c1_r_decisionTxtStartIdx = 0U;
  static const uint32_T c1_r_decisionTxtEndIdx = 0U;
  static const uint32_T c1_s_decisionTxtStartIdx = 0U;
  static const uint32_T c1_s_decisionTxtEndIdx = 0U;
  static const uint32_T c1_t_decisionTxtStartIdx = 0U;
  static const uint32_T c1_t_decisionTxtEndIdx = 0U;
  static const uint32_T c1_u_decisionTxtStartIdx = 0U;
  static const uint32_T c1_u_decisionTxtEndIdx = 0U;
  static const uint32_T c1_v_decisionTxtStartIdx = 0U;
  static const uint32_T c1_v_decisionTxtEndIdx = 0U;
  static const uint32_T c1_w_decisionTxtStartIdx = 0U;
  static const uint32_T c1_w_decisionTxtEndIdx = 0U;
  static const uint32_T c1_x_decisionTxtStartIdx = 0U;
  static const uint32_T c1_x_decisionTxtEndIdx = 0U;
  static const uint32_T c1_y_decisionTxtStartIdx = 0U;
  static const uint32_T c1_y_decisionTxtEndIdx = 0U;
  static const uint32_T c1_ab_decisionTxtStartIdx = 0U;
  static const uint32_T c1_ab_decisionTxtEndIdx = 0U;
  static const uint32_T c1_bb_decisionTxtStartIdx = 0U;
  static const uint32_T c1_bb_decisionTxtEndIdx = 0U;
  static const uint32_T c1_cb_decisionTxtStartIdx = 0U;
  static const uint32_T c1_cb_decisionTxtEndIdx = 0U;
  static const uint32_T c1_db_decisionTxtStartIdx = 0U;
  static const uint32_T c1_db_decisionTxtEndIdx = 0U;
  static const uint32_T c1_eb_decisionTxtStartIdx = 0U;
  static const uint32_T c1_eb_decisionTxtEndIdx = 0U;
  static const uint32_T c1_fb_decisionTxtStartIdx = 0U;
  static const uint32_T c1_fb_decisionTxtEndIdx = 0U;
  static const uint32_T c1_gb_decisionTxtStartIdx = 0U;
  static const uint32_T c1_gb_decisionTxtEndIdx = 0U;
  static const uint32_T c1_transitionTxtStartIdx[1] = { 1U };

  static const uint32_T c1_transitionTxtEndIdx[1] = { 26U };

  static const int32_T c1_postfixPredicateTree[1] = { 0 };

  static const uint32_T c1_b_transitionTxtStartIdx[1] = { 1U };

  static const uint32_T c1_b_transitionTxtEndIdx[1] = { 31U };

  static const int32_T c1_b_postfixPredicateTree[1] = { 0 };

  static const uint32_T c1_c_transitionTxtStartIdx[1] = { 1U };

  static const uint32_T c1_c_transitionTxtEndIdx[1] = { 26U };

  static const int32_T c1_c_postfixPredicateTree[1] = { 0 };

  static const uint32_T c1_d_transitionTxtStartIdx[1] = { 1U };

  static const uint32_T c1_d_transitionTxtEndIdx[1] = { 56U };

  static const int32_T c1_d_postfixPredicateTree[1] = { 0 };

  static const int32_T c1_relopTxtStartIdx[1] = { 1 };

  static const int32_T c1_relopTxtEndIdx[1] = { 56 };

  static const int32_T c1_relationalopEps[1] = { 0 };

  static const int32_T c1_relationalopType[1] = { 1 };

  static const uint32_T c1_e_transitionTxtStartIdx[1] = { 1U };

  static const uint32_T c1_e_transitionTxtEndIdx[1] = { 24U };

  static const int32_T c1_e_postfixPredicateTree[1] = { 0 };

  static const uint32_T c1_d_saturationTxtStartIdx[1] = { 1U };

  static const uint32_T c1_d_saturationTxtEndIdx[1] = { 6U };

  static const uint32_T c1_f_transitionTxtStartIdx[1] = { 1U };

  static const uint32_T c1_f_transitionTxtEndIdx[1] = { 45U };

  static const int32_T c1_f_postfixPredicateTree[1] = { 0 };

  static const int32_T c1_b_relopTxtStartIdx[1] = { 1 };

  static const int32_T c1_b_relopTxtEndIdx[1] = { 45 };

  static const int32_T c1_b_relationalopEps[1] = { 0 };

  static const int32_T c1_b_relationalopType[1] = { 1 };

  static const uint32_T c1_g_transitionTxtStartIdx[1] = { 1U };

  static const uint32_T c1_g_transitionTxtEndIdx[1] = { 55U };

  static const int32_T c1_g_postfixPredicateTree[1] = { 0 };

  static const int32_T c1_c_relopTxtStartIdx[1] = { 1 };

  static const int32_T c1_c_relopTxtEndIdx[1] = { 55 };

  static const int32_T c1_c_relationalopEps[1] = { 0 };

  static const int32_T c1_c_relationalopType[1] = { 1 };

  static const uint32_T c1_h_transitionTxtStartIdx[1] = { 1U };

  static const uint32_T c1_h_transitionTxtEndIdx[1] = { 55U };

  static const int32_T c1_h_postfixPredicateTree[1] = { 0 };

  static const int32_T c1_d_relopTxtStartIdx[1] = { 1 };

  static const int32_T c1_d_relopTxtEndIdx[1] = { 55 };

  static const int32_T c1_d_relationalopEps[1] = { 0 };

  static const int32_T c1_d_relationalopType[1] = { 1 };

  static const uint32_T c1_i_transitionTxtStartIdx[1] = { 1U };

  static const uint32_T c1_i_transitionTxtEndIdx[1] = { 29U };

  static const int32_T c1_i_postfixPredicateTree[1] = { 0 };

  static const uint32_T c1_j_transitionTxtStartIdx[1] = { 3U };

  static const uint32_T c1_j_transitionTxtEndIdx[1] = { 52U };

  static const int32_T c1_j_postfixPredicateTree[1] = { 0 };

  static const int32_T c1_e_relopTxtStartIdx[1] = { 3 };

  static const int32_T c1_e_relopTxtEndIdx[1] = { 52 };

  static const int32_T c1_e_relationalopEps[1] = { 0 };

  static const int32_T c1_e_relationalopType[1] = { 0 };

  static const uint32_T c1_k_transitionTxtStartIdx[1] = { 1U };

  static const uint32_T c1_k_transitionTxtEndIdx[1] = { 56U };

  static const int32_T c1_k_postfixPredicateTree[1] = { 0 };

  static const int32_T c1_f_relopTxtStartIdx[1] = { 1 };

  static const int32_T c1_f_relopTxtEndIdx[1] = { 56 };

  static const int32_T c1_f_relationalopEps[1] = { 0 };

  static const int32_T c1_f_relationalopType[1] = { 0 };

  static const uint32_T c1_l_transitionTxtStartIdx[1] = { 1U };

  static const uint32_T c1_l_transitionTxtEndIdx[1] = { 63U };

  static const int32_T c1_l_postfixPredicateTree[1] = { 0 };

  static const int32_T c1_g_relopTxtStartIdx[1] = { 1 };

  static const int32_T c1_g_relopTxtEndIdx[1] = { 63 };

  static const int32_T c1_g_relationalopEps[1] = { 0 };

  static const int32_T c1_g_relationalopType[1] = { 0 };

  static const uint32_T c1_m_transitionTxtStartIdx[4] = { 3U, 20U, 35U, 90U };

  static const uint32_T c1_m_transitionTxtEndIdx[4] = { 14U, 30U, 84U, 100U };

  static const int32_T c1_m_postfixPredicateTree[7] = { 0, 1, -3, 2, -3, 3, -3 };

  static const int32_T c1_h_relopTxtStartIdx[4] = { 3, 20, 35, 90 };

  static const int32_T c1_h_relopTxtEndIdx[4] = { 14, 30, 84, 100 };

  static const int32_T c1_h_relationalopEps[4] = { 0, 0, 0, 0 };

  static const int32_T c1_h_relationalopType[4] = { 5, 2, 0, 0 };

  static const uint32_T c1_n_transitionTxtStartIdx[4] = { 3U, 20U, 36U, 91U };

  static const uint32_T c1_n_transitionTxtEndIdx[4] = { 14U, 30U, 85U, 101U };

  static const int32_T c1_n_postfixPredicateTree[7] = { 0, 1, -3, 2, -3, 3, -3 };

  static const int32_T c1_i_relopTxtStartIdx[4] = { 3, 20, 36, 91 };

  static const int32_T c1_i_relopTxtEndIdx[4] = { 14, 30, 85, 101 };

  static const int32_T c1_i_relationalopEps[4] = { 0, 0, 0, 0 };

  static const int32_T c1_i_relationalopType[4] = { 3, 4, 0, 0 };

  static const uint32_T c1_o_transitionTxtStartIdx[1] = { 3U };

  static const uint32_T c1_o_transitionTxtEndIdx[1] = { 52U };

  static const int32_T c1_o_postfixPredicateTree[1] = { 0 };

  static const int32_T c1_j_relopTxtStartIdx[1] = { 3 };

  static const int32_T c1_j_relopTxtEndIdx[1] = { 52 };

  static const int32_T c1_j_relationalopEps[1] = { 0 };

  static const int32_T c1_j_relationalopType[1] = { 0 };

  static const uint32_T c1_p_transitionTxtStartIdx[1] = { 1U };

  static const uint32_T c1_p_transitionTxtEndIdx[1] = { 56U };

  static const int32_T c1_p_postfixPredicateTree[1] = { 0 };

  static const int32_T c1_k_relopTxtStartIdx[1] = { 1 };

  static const int32_T c1_k_relopTxtEndIdx[1] = { 56 };

  static const int32_T c1_k_relationalopEps[1] = { 0 };

  static const int32_T c1_k_relationalopType[1] = { 0 };

  static const uint32_T c1_q_transitionTxtStartIdx[1] = { 1U };

  static const uint32_T c1_q_transitionTxtEndIdx[1] = { 63U };

  static const int32_T c1_q_postfixPredicateTree[1] = { 0 };

  static const int32_T c1_l_relopTxtStartIdx[1] = { 1 };

  static const int32_T c1_l_relopTxtEndIdx[1] = { 63 };

  static const int32_T c1_l_relationalopEps[1] = { 0 };

  static const int32_T c1_l_relationalopType[1] = { 0 };

  static const uint32_T c1_r_transitionTxtStartIdx[1] = { 3U };

  static const uint32_T c1_r_transitionTxtEndIdx[1] = { 14U };

  static const int32_T c1_r_postfixPredicateTree[1] = { 0 };

  static const int32_T c1_m_relopTxtStartIdx[1] = { 3 };

  static const int32_T c1_m_relopTxtEndIdx[1] = { 14 };

  static const int32_T c1_m_relationalopEps[1] = { 0 };

  static const int32_T c1_m_relationalopType[1] = { 0 };

  static const uint32_T c1_s_transitionTxtStartIdx[2] = { 3U, 58U };

  static const uint32_T c1_s_transitionTxtEndIdx[2] = { 52U, 68U };

  static const int32_T c1_s_postfixPredicateTree[3] = { 0, 1, -3 };

  static const int32_T c1_n_relopTxtStartIdx[2] = { 3, 58 };

  static const int32_T c1_n_relopTxtEndIdx[2] = { 52, 68 };

  static const int32_T c1_n_relationalopEps[2] = { 0, 0 };

  static const int32_T c1_n_relationalopType[2] = { 0, 0 };

  static const uint32_T c1_e_saturationTxtStartIdx[1] = { 1U };

  static const uint32_T c1_e_saturationTxtEndIdx[1] = { 51U };

  static const uint32_T c1_t_transitionTxtStartIdx[1] = { 1U };

  static const uint32_T c1_t_transitionTxtEndIdx[1] = { 56U };

  static const int32_T c1_t_postfixPredicateTree[1] = { 0 };

  static const int32_T c1_o_relopTxtStartIdx[1] = { 1 };

  static const int32_T c1_o_relopTxtEndIdx[1] = { 56 };

  static const int32_T c1_o_relationalopEps[1] = { 0 };

  static const int32_T c1_o_relationalopType[1] = { 0 };

  static const uint32_T c1_f_saturationTxtStartIdx[1] = { 13U };

  static const uint32_T c1_f_saturationTxtEndIdx[1] = { 14U };

  static const uint32_T c1_u_transitionTxtStartIdx[1] = { 1U };

  static const uint32_T c1_u_transitionTxtEndIdx[1] = { 63U };

  static const int32_T c1_u_postfixPredicateTree[1] = { 0 };

  static const int32_T c1_p_relopTxtStartIdx[1] = { 1 };

  static const int32_T c1_p_relopTxtEndIdx[1] = { 63 };

  static const int32_T c1_p_relationalopEps[1] = { 0 };

  static const int32_T c1_p_relationalopType[1] = { 0 };

  static const uint32_T c1_v_transitionTxtStartIdx[2] = { 3U, 58U };

  static const uint32_T c1_v_transitionTxtEndIdx[2] = { 52U, 68U };

  static const int32_T c1_v_postfixPredicateTree[3] = { 0, 1, -3 };

  static const int32_T c1_q_relopTxtStartIdx[2] = { 3, 58 };

  static const int32_T c1_q_relopTxtEndIdx[2] = { 52, 68 };

  static const int32_T c1_q_relationalopEps[2] = { 0, 0 };

  static const int32_T c1_q_relationalopType[2] = { 0, 0 };

  static const uint32_T c1_g_saturationTxtStartIdx[1] = { 1U };

  static const uint32_T c1_g_saturationTxtEndIdx[1] = { 51U };

  static const uint32_T c1_w_transitionTxtStartIdx[1] = { 1U };

  static const uint32_T c1_w_transitionTxtEndIdx[1] = { 56U };

  static const int32_T c1_w_postfixPredicateTree[1] = { 0 };

  static const int32_T c1_r_relopTxtStartIdx[1] = { 1 };

  static const int32_T c1_r_relopTxtEndIdx[1] = { 56 };

  static const int32_T c1_r_relationalopEps[1] = { 0 };

  static const int32_T c1_r_relationalopType[1] = { 0 };

  static const uint32_T c1_h_saturationTxtStartIdx[1] = { 13U };

  static const uint32_T c1_h_saturationTxtEndIdx[1] = { 14U };

  static const uint32_T c1_x_transitionTxtStartIdx[1] = { 1U };

  static const uint32_T c1_x_transitionTxtEndIdx[1] = { 63U };

  static const int32_T c1_x_postfixPredicateTree[1] = { 0 };

  static const int32_T c1_s_relopTxtStartIdx[1] = { 1 };

  static const int32_T c1_s_relopTxtEndIdx[1] = { 63 };

  static const int32_T c1_s_relationalopEps[1] = { 0 };

  static const int32_T c1_s_relationalopType[1] = { 0 };

  chartInstance->c1_RuntimeVar = sfListenerCacheSimStruct(chartInstance->S);
  sfListenerRegisterHover(chartInstance->c1_RuntimeVar, (void *)
    sf_c1_Motor_1pHController_getDebuggerHoverDataFor);
  c1_init_sf_message_store_memory(chartInstance);
  sim_mode_is_external(chartInstance->S);
  covrtCreateStateflowInstanceData(chartInstance->c1_covrtInstance, 34U, 0U, 80U,
    357U);
  covrtChartInitFcn(chartInstance->c1_covrtInstance, 3U, true, false, false);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 4U, 0U, false, false, false,
                    0U, &c1_decisionTxtStartIdx, &c1_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 5U, 5U, true, true, false,
                    0U, &c1_b_decisionTxtStartIdx, &c1_b_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 6U, 0U, false, false, false,
                    0U, &c1_c_decisionTxtStartIdx, &c1_c_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 7U, 0U, false, false, false,
                    0U, &c1_d_decisionTxtStartIdx, &c1_d_decisionTxtEndIdx);
  covrtSaturationInitFcn(chartInstance->c1_covrtInstance, 4U, 7U, 2U,
    c1_saturationTxtStartIdx, c1_saturationTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 8U, 0U, false, false, false,
                    0U, &c1_e_decisionTxtStartIdx, &c1_e_decisionTxtEndIdx);
  covrtSaturationInitFcn(chartInstance->c1_covrtInstance, 4U, 8U, 1U,
    c1_b_saturationTxtStartIdx, c1_b_saturationTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 9U, 0U, false, false, false,
                    0U, &c1_f_decisionTxtStartIdx, &c1_f_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 10U, 0U, false, false,
                    false, 0U, &c1_g_decisionTxtStartIdx,
                    &c1_g_decisionTxtEndIdx);
  covrtSaturationInitFcn(chartInstance->c1_covrtInstance, 4U, 10U, 5U,
    c1_c_saturationTxtStartIdx, c1_c_saturationTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 24U, 0U, false, false,
                    false, 0U, &c1_h_decisionTxtStartIdx,
                    &c1_h_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 0U, 0U, false, false, false,
                    0U, &c1_i_decisionTxtStartIdx, &c1_i_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 1U, 0U, false, false, false,
                    0U, &c1_j_decisionTxtStartIdx, &c1_j_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 2U, 0U, false, false, false,
                    0U, &c1_k_decisionTxtStartIdx, &c1_k_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 3U, 0U, false, false, false,
                    0U, &c1_l_decisionTxtStartIdx, &c1_l_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 11U, 0U, false, false,
                    false, 0U, &c1_m_decisionTxtStartIdx,
                    &c1_m_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 12U, 0U, false, false,
                    false, 0U, &c1_n_decisionTxtStartIdx,
                    &c1_n_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 13U, 0U, false, false,
                    false, 0U, &c1_o_decisionTxtStartIdx,
                    &c1_o_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 14U, 0U, false, false,
                    false, 0U, &c1_p_decisionTxtStartIdx,
                    &c1_p_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 15U, 0U, false, false,
                    false, 0U, &c1_q_decisionTxtStartIdx,
                    &c1_q_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 16U, 0U, false, false,
                    false, 0U, &c1_r_decisionTxtStartIdx,
                    &c1_r_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 17U, 0U, false, false,
                    false, 0U, &c1_s_decisionTxtStartIdx,
                    &c1_s_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 19U, 0U, false, false,
                    false, 0U, &c1_t_decisionTxtStartIdx,
                    &c1_t_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 20U, 0U, false, false,
                    false, 0U, &c1_u_decisionTxtStartIdx,
                    &c1_u_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 21U, 0U, false, false,
                    false, 0U, &c1_v_decisionTxtStartIdx,
                    &c1_v_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 22U, 0U, false, false,
                    false, 0U, &c1_w_decisionTxtStartIdx,
                    &c1_w_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 23U, 0U, false, false,
                    false, 0U, &c1_x_decisionTxtStartIdx,
                    &c1_x_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 25U, 0U, false, false,
                    false, 0U, &c1_y_decisionTxtStartIdx,
                    &c1_y_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 26U, 0U, false, false,
                    false, 0U, &c1_ab_decisionTxtStartIdx,
                    &c1_ab_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 27U, 0U, false, false,
                    false, 0U, &c1_bb_decisionTxtStartIdx,
                    &c1_bb_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 28U, 0U, false, false,
                    false, 0U, &c1_cb_decisionTxtStartIdx,
                    &c1_cb_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 30U, 0U, false, false,
                    false, 0U, &c1_db_decisionTxtStartIdx,
                    &c1_db_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 31U, 0U, false, false,
                    false, 0U, &c1_eb_decisionTxtStartIdx,
                    &c1_eb_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 32U, 0U, false, false,
                    false, 0U, &c1_fb_decisionTxtStartIdx,
                    &c1_fb_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 33U, 0U, false, false,
                    false, 0U, &c1_gb_decisionTxtStartIdx,
                    &c1_gb_decisionTxtEndIdx);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 5U, 0, NULL, NULL, 0U, NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 4U, 1,
                    c1_transitionTxtStartIdx, c1_transitionTxtEndIdx, 1U,
                    c1_postfixPredicateTree);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 1U, 1,
                    c1_b_transitionTxtStartIdx, c1_b_transitionTxtEndIdx, 1U,
                    c1_b_postfixPredicateTree);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 0U, 1,
                    c1_c_transitionTxtStartIdx, c1_c_transitionTxtEndIdx, 1U,
                    c1_c_postfixPredicateTree);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 2U, 1,
                    c1_d_transitionTxtStartIdx, c1_d_transitionTxtEndIdx, 1U,
                    c1_d_postfixPredicateTree);
  covrtRelationalopInitFcn(chartInstance->c1_covrtInstance, 2U, 1,
    c1_relopTxtStartIdx, c1_relopTxtEndIdx, c1_relationalopEps,
    c1_relationalopType);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 79U, 1,
                    c1_e_transitionTxtStartIdx, c1_e_transitionTxtEndIdx, 1U,
                    c1_e_postfixPredicateTree);
  covrtSaturationInitFcn(chartInstance->c1_covrtInstance, 5U, 79U, 1U,
    c1_d_saturationTxtStartIdx, c1_d_saturationTxtEndIdx);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 78U, 1,
                    c1_f_transitionTxtStartIdx, c1_f_transitionTxtEndIdx, 1U,
                    c1_f_postfixPredicateTree);
  covrtRelationalopInitFcn(chartInstance->c1_covrtInstance, 78U, 1,
    c1_b_relopTxtStartIdx, c1_b_relopTxtEndIdx, c1_b_relationalopEps,
    c1_b_relationalopType);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 3U, 1,
                    c1_g_transitionTxtStartIdx, c1_g_transitionTxtEndIdx, 1U,
                    c1_g_postfixPredicateTree);
  covrtRelationalopInitFcn(chartInstance->c1_covrtInstance, 3U, 1,
    c1_c_relopTxtStartIdx, c1_c_relopTxtEndIdx, c1_c_relationalopEps,
    c1_c_relationalopType);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 6U, 1,
                    c1_h_transitionTxtStartIdx, c1_h_transitionTxtEndIdx, 1U,
                    c1_h_postfixPredicateTree);
  covrtRelationalopInitFcn(chartInstance->c1_covrtInstance, 6U, 1,
    c1_d_relopTxtStartIdx, c1_d_relopTxtEndIdx, c1_d_relationalopEps,
    c1_d_relationalopType);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 7U, 1,
                    c1_i_transitionTxtStartIdx, c1_i_transitionTxtEndIdx, 1U,
                    c1_i_postfixPredicateTree);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 52U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 58U, 1,
                    c1_j_transitionTxtStartIdx, c1_j_transitionTxtEndIdx, 1U,
                    c1_j_postfixPredicateTree);
  covrtRelationalopInitFcn(chartInstance->c1_covrtInstance, 58U, 1,
    c1_e_relopTxtStartIdx, c1_e_relopTxtEndIdx, c1_e_relationalopEps,
    c1_e_relationalopType);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 59U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 57U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 60U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 64U, 1,
                    c1_k_transitionTxtStartIdx, c1_k_transitionTxtEndIdx, 1U,
                    c1_k_postfixPredicateTree);
  covrtRelationalopInitFcn(chartInstance->c1_covrtInstance, 64U, 1,
    c1_f_relopTxtStartIdx, c1_f_relopTxtEndIdx, c1_f_relationalopEps,
    c1_f_relationalopType);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 53U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 61U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 55U, 1,
                    c1_l_transitionTxtStartIdx, c1_l_transitionTxtEndIdx, 1U,
                    c1_l_postfixPredicateTree);
  covrtRelationalopInitFcn(chartInstance->c1_covrtInstance, 55U, 1,
    c1_g_relopTxtStartIdx, c1_g_relopTxtEndIdx, c1_g_relationalopEps,
    c1_g_relationalopType);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 54U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 56U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 62U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 63U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 12U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 11U, 4,
                    c1_m_transitionTxtStartIdx, c1_m_transitionTxtEndIdx, 7U,
                    c1_m_postfixPredicateTree);
  covrtRelationalopInitFcn(chartInstance->c1_covrtInstance, 11U, 4,
    c1_h_relopTxtStartIdx, c1_h_relopTxtEndIdx, c1_h_relationalopEps,
    c1_h_relationalopType);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 8U, 0, NULL, NULL, 0U, NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 10U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 9U, 0, NULL, NULL, 0U, NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 13U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 14U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 19U, 4,
                    c1_n_transitionTxtStartIdx, c1_n_transitionTxtEndIdx, 7U,
                    c1_n_postfixPredicateTree);
  covrtRelationalopInitFcn(chartInstance->c1_covrtInstance, 19U, 4,
    c1_i_relopTxtStartIdx, c1_i_relopTxtEndIdx, c1_i_relationalopEps,
    c1_i_relationalopType);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 15U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 18U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 17U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 16U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 67U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 69U, 1,
                    c1_o_transitionTxtStartIdx, c1_o_transitionTxtEndIdx, 1U,
                    c1_o_postfixPredicateTree);
  covrtRelationalopInitFcn(chartInstance->c1_covrtInstance, 69U, 1,
    c1_j_relopTxtStartIdx, c1_j_relopTxtEndIdx, c1_j_relationalopEps,
    c1_j_relationalopType);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 70U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 71U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 68U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 72U, 1,
                    c1_p_transitionTxtStartIdx, c1_p_transitionTxtEndIdx, 1U,
                    c1_p_postfixPredicateTree);
  covrtRelationalopInitFcn(chartInstance->c1_covrtInstance, 72U, 1,
    c1_k_relopTxtStartIdx, c1_k_relopTxtEndIdx, c1_k_relationalopEps,
    c1_k_relationalopType);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 77U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 65U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 66U, 1,
                    c1_q_transitionTxtStartIdx, c1_q_transitionTxtEndIdx, 1U,
                    c1_q_postfixPredicateTree);
  covrtRelationalopInitFcn(chartInstance->c1_covrtInstance, 66U, 1,
    c1_l_relopTxtStartIdx, c1_l_relopTxtEndIdx, c1_l_relationalopEps,
    c1_l_relationalopType);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 76U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 73U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 75U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 74U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 46U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 49U, 1,
                    c1_r_transitionTxtStartIdx, c1_r_transitionTxtEndIdx, 1U,
                    c1_r_postfixPredicateTree);
  covrtRelationalopInitFcn(chartInstance->c1_covrtInstance, 49U, 1,
    c1_m_relopTxtStartIdx, c1_m_relopTxtEndIdx, c1_m_relationalopEps,
    c1_m_relationalopType);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 47U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 50U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 48U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 51U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 20U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 21U, 2,
                    c1_s_transitionTxtStartIdx, c1_s_transitionTxtEndIdx, 3U,
                    c1_s_postfixPredicateTree);
  covrtRelationalopInitFcn(chartInstance->c1_covrtInstance, 21U, 2,
    c1_n_relopTxtStartIdx, c1_n_relopTxtEndIdx, c1_n_relationalopEps,
    c1_n_relationalopType);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 22U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 23U, 0, NULL, NULL, 0U,
                    NULL);
  covrtSaturationInitFcn(chartInstance->c1_covrtInstance, 5U, 23U, 1U,
    c1_e_saturationTxtStartIdx, c1_e_saturationTxtEndIdx);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 31U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 24U, 1,
                    c1_t_transitionTxtStartIdx, c1_t_transitionTxtEndIdx, 1U,
                    c1_t_postfixPredicateTree);
  covrtRelationalopInitFcn(chartInstance->c1_covrtInstance, 24U, 1,
    c1_o_relopTxtStartIdx, c1_o_relopTxtEndIdx, c1_o_relationalopEps,
    c1_o_relationalopType);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 26U, 0, NULL, NULL, 0U,
                    NULL);
  covrtSaturationInitFcn(chartInstance->c1_covrtInstance, 5U, 26U, 1U,
    c1_f_saturationTxtStartIdx, c1_f_saturationTxtEndIdx);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 29U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 25U, 1,
                    c1_u_transitionTxtStartIdx, c1_u_transitionTxtEndIdx, 1U,
                    c1_u_postfixPredicateTree);
  covrtRelationalopInitFcn(chartInstance->c1_covrtInstance, 25U, 1,
    c1_p_relopTxtStartIdx, c1_p_relopTxtEndIdx, c1_p_relationalopEps,
    c1_p_relationalopType);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 30U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 32U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 28U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 27U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 33U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 34U, 2,
                    c1_v_transitionTxtStartIdx, c1_v_transitionTxtEndIdx, 3U,
                    c1_v_postfixPredicateTree);
  covrtRelationalopInitFcn(chartInstance->c1_covrtInstance, 34U, 2,
    c1_q_relopTxtStartIdx, c1_q_relopTxtEndIdx, c1_q_relationalopEps,
    c1_q_relationalopType);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 35U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 39U, 0, NULL, NULL, 0U,
                    NULL);
  covrtSaturationInitFcn(chartInstance->c1_covrtInstance, 5U, 39U, 1U,
    c1_g_saturationTxtStartIdx, c1_g_saturationTxtEndIdx);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 44U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 36U, 1,
                    c1_w_transitionTxtStartIdx, c1_w_transitionTxtEndIdx, 1U,
                    c1_w_postfixPredicateTree);
  covrtRelationalopInitFcn(chartInstance->c1_covrtInstance, 36U, 1,
    c1_r_relopTxtStartIdx, c1_r_relopTxtEndIdx, c1_r_relationalopEps,
    c1_r_relationalopType);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 38U, 0, NULL, NULL, 0U,
                    NULL);
  covrtSaturationInitFcn(chartInstance->c1_covrtInstance, 5U, 38U, 1U,
    c1_h_saturationTxtStartIdx, c1_h_saturationTxtEndIdx);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 42U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 37U, 1,
                    c1_x_transitionTxtStartIdx, c1_x_transitionTxtEndIdx, 1U,
                    c1_x_postfixPredicateTree);
  covrtRelationalopInitFcn(chartInstance->c1_covrtInstance, 37U, 1,
    c1_s_relopTxtStartIdx, c1_s_relopTxtEndIdx, c1_s_relationalopEps,
    c1_s_relationalopType);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 45U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 43U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 41U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 40U, 0, NULL, NULL, 0U,
                    NULL);
}

static void c1_chartstep_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  boolean_T c1_out;
  boolean_T c1_b_out;
  uint16_T c1_d_PWM_CMD1;
  uint16_T c1_d_PWM_CMD2;
  int16_T c1_b_InverterOutputVoltage;
  int32_T c1_i6;
  boolean_T c1_covSaturation;
  int32_T c1_i7;
  boolean_T c1_b_covSaturation;
  int16_T c1_c_OL_voltage_state_is;
  int16_T c1_c_OL_voltage_state_bemf;
  int16_T c1_c_OL_speed_state;
  int16_T c1_c_OLvoltage_sat;
  int16_T c1_c_OLspeed_sat;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
  if (chartInstance->c1_is_active_c1_Motor_1pHController == 0U) {
    _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
    chartInstance->c1_is_active_c1_Motor_1pHController = 1U;
    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 5U, chartInstance->c1_sfEvent);
    chartInstance->c1_is_c1_Motor_1pHController = c1_IN_Init_Mode;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
    chartInstance->c1_tp_Init_Mode = 1U;
    *chartInstance->c1_controller_flag = ControlFlag_Init;
    chartInstance->c1_rotor_position_reference = 0;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_rotor_position_reference,
                          23U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 23U, (real_T)
                      chartInstance->c1_rotor_position_reference);
    *chartInstance->c1_voltage_ref = 0;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_voltage_ref, 61U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 61U, (real_T)
                      *chartInstance->c1_voltage_ref);
    *chartInstance->c1_speed_pll = 0;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_speed_pll, 60U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 60U, (real_T)
                      *chartInstance->c1_speed_pll);
    chartInstance->c1_fault = false;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_fault, 20U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 20U, (real_T)
                      chartInstance->c1_fault);
    *chartInstance->c1_SlurpDetectedAppcon = false;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_SlurpDetectedAppcon, 56U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 56U, (real_T)
                      *chartInstance->c1_SlurpDetectedAppcon);
    *chartInstance->c1_Speed_control_fault = false;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_Speed_control_fault, 59U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 59U, (real_T)
                      *chartInstance->c1_Speed_control_fault);
    *chartInstance->c1_Bemf_control_fault = false;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_Bemf_control_fault, 58U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 58U, (real_T)
                      *chartInstance->c1_Bemf_control_fault);
    *chartInstance->c1_PWM_CMD1 = (uint16_T)((uint32_T)
      chartInstance->c1_PWM_MAX_DUTYCYCLE >> 1);
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD1, 52U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 52U, (real_T)
                      *chartInstance->c1_PWM_CMD1);
    *chartInstance->c1_PWM_CMD2 = (uint16_T)((uint32_T)
      chartInstance->c1_PWM_MAX_DUTYCYCLE >> 1);
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD2, 53U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 53U, (real_T)
                      *chartInstance->c1_PWM_CMD2);
    *chartInstance->c1_Vs_s = 0;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_Vs_s, 50U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 50U, (real_T)
                      *chartInstance->c1_Vs_s);
    *chartInstance->c1_PWM_OFF = 1U;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_OFF, 54U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 54U, (real_T)
                      *chartInstance->c1_PWM_OFF);
    c1_state_variable_init(chartInstance, &c1_c_OL_voltage_state_is,
      &c1_c_OL_voltage_state_bemf, &c1_c_OL_speed_state, &c1_c_OLvoltage_sat,
      &c1_c_OLspeed_sat);
    chartInstance->c1_OL_voltage_state_is = c1_c_OL_voltage_state_is;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OL_voltage_state_is, 8U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 8U, (real_T)
                      chartInstance->c1_OL_voltage_state_is);
    chartInstance->c1_OL_voltage_state_bemf = c1_c_OL_voltage_state_bemf;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OL_voltage_state_bemf, 7U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 7U, (real_T)
                      chartInstance->c1_OL_voltage_state_bemf);
    chartInstance->c1_OL_speed_state = c1_c_OL_speed_state;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OL_speed_state, 6U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 6U, (real_T)
                      chartInstance->c1_OL_speed_state);
    chartInstance->c1_OLvoltage_sat = c1_c_OLvoltage_sat;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OLvoltage_sat, 10U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 10U, (real_T)
                      chartInstance->c1_OLvoltage_sat);
    chartInstance->c1_OLspeed_sat = c1_c_OLspeed_sat;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OLspeed_sat, 9U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 9U, (real_T)
                      chartInstance->c1_OLspeed_sat);
    *chartInstance->c1_controller_flag = ControlFlag_InitDone;
  } else {
    switch (chartInstance->c1_is_c1_Motor_1pHController) {
     case c1_IN_Init_Mode:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 1U, 0, 0,
                        c1_IN_Init_Mode);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 4U,
                   chartInstance->c1_sfEvent);
      c1_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance, 5U,
        4U, 0U, covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 4U, 0, (*
        chartInstance->c1_controller_mode == ControlModeState_StopMode) != 0U)
        != 0U);
      if (c1_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
        chartInstance->c1_tp_Init_Mode = 0U;
        chartInstance->c1_is_c1_Motor_1pHController = c1_IN_NO_ACTIVE_CHILD;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
        chartInstance->c1_is_c1_Motor_1pHController = c1_IN_Stop_Mode;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 24U, chartInstance->c1_sfEvent);
        chartInstance->c1_tp_Stop_Mode = 1U;
        c1_enter_atomic_Stop_Mode(chartInstance);
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 4U,
                     chartInstance->c1_sfEvent);
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 4U, chartInstance->c1_sfEvent);
      break;

     case c1_IN_Motor_On:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 1U, 0, 0,
                        c1_IN_Motor_On);
      c1_Motor_On(chartInstance);
      break;

     case c1_IN_Stop_Mode:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 1U, 0, 0,
                        c1_IN_Stop_Mode);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 1U,
                   chartInstance->c1_sfEvent);
      c1_b_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance, 5U,
        1U, 0U, covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 1U, 0, (*
        chartInstance->c1_controller_mode == ControlModeState_AlignmentMode) !=
        0U) != 0U);
      if (c1_b_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
        chartInstance->c1_tp_Stop_Mode = 0U;
        chartInstance->c1_is_c1_Motor_1pHController = c1_IN_NO_ACTIVE_CHILD;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 24U, chartInstance->c1_sfEvent);
        chartInstance->c1_is_c1_Motor_1pHController = c1_IN_Motor_On;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 5U, chartInstance->c1_sfEvent);
        chartInstance->c1_tp_Motor_On = 1U;
        chartInstance->c1_is_Motor_On = c1_IN_Alignment_Mode;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 7U, chartInstance->c1_sfEvent);
        chartInstance->c1_temporalCounter_i1 = 0U;
        chartInstance->c1_tp_Alignment_Mode = 1U;
        *chartInstance->c1_controller_flag = ControlFlag_Alignment;
        *chartInstance->c1_PWM_OFF = 0U;
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_OFF, 54U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 54U, (real_T)
                          *chartInstance->c1_PWM_OFF);
        c1_modulation(chartInstance, 800, 0, *chartInstance->c1_vdc,
                      &c1_d_PWM_CMD1, &c1_d_PWM_CMD2,
                      &c1_b_InverterOutputVoltage);
        *chartInstance->c1_PWM_CMD1 = c1_d_PWM_CMD1;
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD1, 52U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 52U, (real_T)
                          *chartInstance->c1_PWM_CMD1);
        *chartInstance->c1_PWM_CMD2 = c1_d_PWM_CMD2;
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD2, 53U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 53U, (real_T)
                          *chartInstance->c1_PWM_CMD2);
        *chartInstance->c1_InvVoltageOut = c1_b_InverterOutputVoltage;
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_InvVoltageOut, 62U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 62U, (real_T)
                          *chartInstance->c1_InvVoltageOut);
        c1_i6 = *chartInstance->c1_PWM_CMD1;
        c1_covSaturation = false;
        if (c1_i6 < 0) {
          c1_covSaturation = true;
          c1_i6 = 0;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 240U, 409U, 8U);
        } else {
          if (c1_i6 > 65535) {
            c1_i6 = 65535;
            _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 240U, 409U, 8U);
          }

          covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 4, 7, 0, 0,
            c1_covSaturation);
        }

        c1_i7 = *chartInstance->c1_PWM_CMD2;
        c1_b_covSaturation = false;
        if (c1_i7 < 0) {
          c1_b_covSaturation = true;
          c1_i7 = 0;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 240U, 418U, 8U);
        } else {
          if (c1_i7 > 65535) {
            c1_i7 = 65535;
            _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 240U, 418U, 8U);
          }

          covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 4, 7, 1, 0,
            c1_b_covSaturation);
        }

        *chartInstance->c1_Vs_s = c1_Duty_Cycle_To_Voltage(chartInstance,
          (uint16_T)c1_i6, (uint16_T)c1_i7, *chartInstance->c1_vdc);
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_Vs_s, 50U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 50U, (real_T)
                          *chartInstance->c1_Vs_s);
        *chartInstance->c1_ApproxPower = c1_PowerCalculation(chartInstance,
          *chartInstance->c1_is_adc, *chartInstance->c1_InvVoltageOut, 0.0);
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_ApproxPower, 57U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 57U, (real_T)
                          *chartInstance->c1_ApproxPower);
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 24U,
                     chartInstance->c1_sfEvent);
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 24U, chartInstance->c1_sfEvent);
      break;

     default:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 1U, 0, 0, 0);

      /* Unreachable state, for coverage only */
      chartInstance->c1_is_c1_Motor_1pHController = c1_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
      break;
    }
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
}

static void initSimStructsc1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c1_enter_atomic_Stop_Mode(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance)
{
  *chartInstance->c1_controller_flag = ControlFlag_Stop;
  *chartInstance->c1_PWM_CMD1 = (uint16_T)((uint32_T)
    chartInstance->c1_PWM_MAX_DUTYCYCLE >> 1);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD1, 52U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 52U, (real_T)
                    *chartInstance->c1_PWM_CMD1);
  *chartInstance->c1_PWM_CMD2 = (uint16_T)((uint32_T)
    chartInstance->c1_PWM_MAX_DUTYCYCLE >> 1);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD2, 53U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 53U, (real_T)
                    *chartInstance->c1_PWM_CMD2);
  *chartInstance->c1_Vs_s = 0;
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_Vs_s, 50U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 50U, (real_T)
                    *chartInstance->c1_Vs_s);
  *chartInstance->c1_PWM_OFF = 1U;
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_OFF, 54U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 54U, (real_T)
                    *chartInstance->c1_PWM_OFF);
  chartInstance->c1_rotor_position_reference = 0;
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_rotor_position_reference, 23U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 23U, (real_T)
                    chartInstance->c1_rotor_position_reference);
  *chartInstance->c1_speed_pll = 0;
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_speed_pll, 60U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 60U, (real_T)
                    *chartInstance->c1_speed_pll);
  chartInstance->c1_PT1_StateVariable = 0;
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_PT1_StateVariable, 11U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 11U, (real_T)
                    chartInstance->c1_PT1_StateVariable);
  chartInstance->c1_SpeedReference_PT1 = 0;
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_SpeedReference_PT1, 16U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 16U, (real_T)
                    chartInstance->c1_SpeedReference_PT1);
  *chartInstance->c1_SlurpDetectedAppcon = false;
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_SlurpDetectedAppcon, 56U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 56U, (real_T)
                    *chartInstance->c1_SlurpDetectedAppcon);
  *chartInstance->c1_Speed_control_fault = false;
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_Speed_control_fault, 59U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 59U, (real_T)
                    *chartInstance->c1_Speed_control_fault);
  *chartInstance->c1_Bemf_control_fault = false;
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_Bemf_control_fault, 58U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 58U, (real_T)
                    *chartInstance->c1_Bemf_control_fault);
}

static void c1_Motor_On(SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  boolean_T c1_out;
  int16_T c1_c_OL_voltage_state_is;
  int16_T c1_c_OL_voltage_state_bemf;
  int16_T c1_c_OL_speed_state;
  int16_T c1_c_OLvoltage_sat;
  int16_T c1_c_OLspeed_sat;
  int32_T c1_i8;
  boolean_T c1_covSaturation;
  boolean_T c1_b_out;
  int32_T c1_i9;
  boolean_T c1_c_out;
  uint16_T c1_d_PWM_CMD1;
  uint16_T c1_d_PWM_CMD2;
  int16_T c1_b_InverterOutputVoltage;
  boolean_T c1_b_Synchronisation_enabled;
  int32_T c1_b_Time_state_output;
  int32_T c1_i10;
  boolean_T c1_b_covSaturation;
  int32_T c1_i11;
  boolean_T c1_c_covSaturation;
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 0U, chartInstance->c1_sfEvent);
  c1_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance, 5U, 0U,
    0U, covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 0U, 0,
    (*chartInstance->c1_controller_mode == ControlModeState_StopMode) != 0U) !=
    0U);
  if (c1_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
    c1_state_variable_init(chartInstance, &c1_c_OL_voltage_state_is,
      &c1_c_OL_voltage_state_bemf, &c1_c_OL_speed_state, &c1_c_OLvoltage_sat,
      &c1_c_OLspeed_sat);
    chartInstance->c1_OL_voltage_state_is = c1_c_OL_voltage_state_is;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OL_voltage_state_is, 8U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 8U, (real_T)
                      chartInstance->c1_OL_voltage_state_is);
    chartInstance->c1_OL_voltage_state_bemf = c1_c_OL_voltage_state_bemf;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OL_voltage_state_bemf, 7U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 7U, (real_T)
                      chartInstance->c1_OL_voltage_state_bemf);
    chartInstance->c1_OL_speed_state = c1_c_OL_speed_state;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OL_speed_state, 6U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 6U, (real_T)
                      chartInstance->c1_OL_speed_state);
    chartInstance->c1_OLvoltage_sat = c1_c_OLvoltage_sat;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OLvoltage_sat, 10U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 10U, (real_T)
                      chartInstance->c1_OLvoltage_sat);
    chartInstance->c1_OLspeed_sat = c1_c_OLspeed_sat;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OLspeed_sat, 9U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 9U, (real_T)
                      chartInstance->c1_OLspeed_sat);
    switch (chartInstance->c1_is_Motor_On) {
     case c1_IN_Alignment_Done:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 4U, 5, 1,
                        c1_IN_Alignment_Done);
      chartInstance->c1_tp_Alignment_Done = 0U;
      chartInstance->c1_is_Motor_On = c1_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U, chartInstance->c1_sfEvent);
      break;

     case c1_IN_Alignment_Mode:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 4U, 5, 1,
                        c1_IN_Alignment_Mode);
      chartInstance->c1_tp_Alignment_Mode = 0U;
      chartInstance->c1_is_Motor_On = c1_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 7U, chartInstance->c1_sfEvent);
      break;

     case c1_IN_ControlMode:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 4U, 5, 1,
                        c1_IN_ControlMode);
      chartInstance->c1_tp_ControlMode = 0U;
      chartInstance->c1_is_Motor_On = c1_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 8U, chartInstance->c1_sfEvent);
      break;

     case c1_IN_Error_Mode:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 4U, 5, 1,
                        c1_IN_Error_Mode);
      chartInstance->c1_tp_Error_Mode = 0U;
      chartInstance->c1_is_Motor_On = c1_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 9U, chartInstance->c1_sfEvent);
      break;

     case c1_IN_VF_Mode:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 4U, 5, 1, c1_IN_VF_Mode);
      chartInstance->c1_tp_VF_Mode = 0U;
      c1_exit_atomic_VF_Mode(chartInstance);
      chartInstance->c1_is_Motor_On = c1_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 10U, chartInstance->c1_sfEvent);
      break;

     default:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 4U, 5, 1, 0);
      chartInstance->c1_is_Motor_On = c1_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U, chartInstance->c1_sfEvent);
      break;
    }

    chartInstance->c1_tp_Motor_On = 0U;
    chartInstance->c1_is_c1_Motor_1pHController = c1_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, chartInstance->c1_sfEvent);
    chartInstance->c1_is_c1_Motor_1pHController = c1_IN_Stop_Mode;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 24U, chartInstance->c1_sfEvent);
    chartInstance->c1_tp_Stop_Mode = 1U;
    c1_enter_atomic_Stop_Mode(chartInstance);
  } else {
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 5U, chartInstance->c1_sfEvent);
    switch (chartInstance->c1_is_Motor_On) {
     case c1_IN_Alignment_Done:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 4U, 5, 0,
                        c1_IN_Alignment_Done);
      c1_Alignment_Done(chartInstance);
      break;

     case c1_IN_Alignment_Mode:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 4U, 5, 0,
                        c1_IN_Alignment_Mode);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 79U,
                   chartInstance->c1_sfEvent);
      c1_i8 = *chartInstance->c1_align_delay << 3;
      c1_covSaturation = false;
      if (c1_i8 < 0) {
        c1_covSaturation = true;
        c1_i8 = 0;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 2043U, 1U, 5U);
      }

      covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 5, 79, 0, 0,
        c1_covSaturation);
      c1_b_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance, 5U,
        79U, 0U, covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 79U, 0,
        (chartInstance->c1_temporalCounter_i1 >= (uint32_T)c1_i8) != 0U) != 0U);
      sf_temporal_value_range_check_min(chartInstance->S, 2043U, (real_T)
        *chartInstance->c1_align_delay, 0.0);
      if (c1_b_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 79U, chartInstance->c1_sfEvent);
        chartInstance->c1_tp_Alignment_Mode = 0U;
        chartInstance->c1_is_Motor_On = c1_IN_NO_ACTIVE_CHILD;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 7U, chartInstance->c1_sfEvent);
        chartInstance->c1_is_Motor_On = c1_IN_Alignment_Done;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 6U, chartInstance->c1_sfEvent);
        chartInstance->c1_temporalCounter_i1 = 0U;
        chartInstance->c1_tp_Alignment_Done = 1U;
        *chartInstance->c1_controller_flag = ControlFlag_AlignmentDone;
        c1_CheckforSynchronisation(chartInstance, 0, 0,
          &c1_b_Synchronisation_enabled, &c1_b_Time_state_output);
        chartInstance->c1_Vf_Done_SynchronisationEnable =
          c1_b_Synchronisation_enabled;
        _SFD_DATA_RANGE_CHECK((real_T)
                              chartInstance->c1_Vf_Done_SynchronisationEnable,
                              18U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 18U, (real_T)
                          chartInstance->c1_Vf_Done_SynchronisationEnable);
        chartInstance->c1_SynchronisationTime_state = c1_b_Time_state_output;
        _SFD_DATA_RANGE_CHECK((real_T)
                              chartInstance->c1_SynchronisationTime_state, 17U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 17U, (real_T)
                          chartInstance->c1_SynchronisationTime_state);
        *chartInstance->c1_Vs_s = 0;
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_Vs_s, 50U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 50U, (real_T)
                          *chartInstance->c1_Vs_s);
        *chartInstance->c1_PWM_CMD1 = (uint16_T)((uint32_T)
          chartInstance->c1_PWM_MAX_DUTYCYCLE >> 1);
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD1, 52U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 52U, (real_T)
                          *chartInstance->c1_PWM_CMD1);
        *chartInstance->c1_PWM_CMD2 = (uint16_T)((uint32_T)
          chartInstance->c1_PWM_MAX_DUTYCYCLE >> 1);
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD2, 53U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 53U, (real_T)
                          *chartInstance->c1_PWM_CMD2);
        *chartInstance->c1_PWM_OFF = 1U;
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_OFF, 54U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 54U, (real_T)
                          *chartInstance->c1_PWM_OFF);
      } else {
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 2U,
                     chartInstance->c1_sfEvent);
        c1_i9 = (*chartInstance->c1_controller_mode ==
                 ControlModeState_ErrorMode) | (int32_T)c1_b_fault(chartInstance,
          *chartInstance->c1_fault1, *chartInstance->c1_fault2);
        c1_c_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance,
          5U, 2U, 0U, covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 2U,
          0, (boolean_T)covrtRelationalopUpdateFcn
          (chartInstance->c1_covrtInstance, 5U, 2U, 0U, (real_T)c1_i9, 0.0, 0,
           1U, c1_i9 != 0) != 0U) != 0U);
        if (c1_c_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 2U, chartInstance->c1_sfEvent);
          chartInstance->c1_tp_Alignment_Mode = 0U;
          chartInstance->c1_is_Motor_On = c1_IN_NO_ACTIVE_CHILD;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 7U, chartInstance->c1_sfEvent);
          chartInstance->c1_is_Motor_On = c1_IN_Error_Mode;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 9U, chartInstance->c1_sfEvent);
          chartInstance->c1_tp_Error_Mode = 1U;
          c1_enter_atomic_Error_Mode(chartInstance);
        } else {
          _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 7U,
                       chartInstance->c1_sfEvent);
          c1_modulation(chartInstance, 800, 0, *chartInstance->c1_vdc,
                        &c1_d_PWM_CMD1, &c1_d_PWM_CMD2,
                        &c1_b_InverterOutputVoltage);
          *chartInstance->c1_PWM_CMD1 = c1_d_PWM_CMD1;
          _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD1, 52U);
          covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 52U, (real_T)
                            *chartInstance->c1_PWM_CMD1);
          *chartInstance->c1_PWM_CMD2 = c1_d_PWM_CMD2;
          _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD2, 53U);
          covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 53U, (real_T)
                            *chartInstance->c1_PWM_CMD2);
          *chartInstance->c1_InvVoltageOut = c1_b_InverterOutputVoltage;
          _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_InvVoltageOut, 62U);
          covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 62U, (real_T)
                            *chartInstance->c1_InvVoltageOut);
          c1_i10 = *chartInstance->c1_PWM_CMD1;
          c1_b_covSaturation = false;
          if (c1_i10 < 0) {
            c1_b_covSaturation = true;
            c1_i10 = 0;
            _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 240U, 409U, 8U);
          } else {
            if (c1_i10 > 65535) {
              c1_i10 = 65535;
              _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 240U, 409U, 8U);
            }

            covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 4, 7, 0, 0,
              c1_b_covSaturation);
          }

          c1_i11 = *chartInstance->c1_PWM_CMD2;
          c1_c_covSaturation = false;
          if (c1_i11 < 0) {
            c1_c_covSaturation = true;
            c1_i11 = 0;
            _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 240U, 418U, 8U);
          } else {
            if (c1_i11 > 65535) {
              c1_i11 = 65535;
              _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 240U, 418U, 8U);
            }

            covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 4, 7, 1, 0,
              c1_c_covSaturation);
          }

          *chartInstance->c1_Vs_s = c1_Duty_Cycle_To_Voltage(chartInstance,
            (uint16_T)c1_i10, (uint16_T)c1_i11, *chartInstance->c1_vdc);
          _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_Vs_s, 50U);
          covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 50U, (real_T)
                            *chartInstance->c1_Vs_s);
          *chartInstance->c1_ApproxPower = c1_PowerCalculation(chartInstance,
            *chartInstance->c1_is_adc, *chartInstance->c1_InvVoltageOut, 0.0);
          _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_ApproxPower, 57U);
          covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 57U, (real_T)
                            *chartInstance->c1_ApproxPower);
        }
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 7U, chartInstance->c1_sfEvent);
      break;

     case c1_IN_ControlMode:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 4U, 5, 0,
                        c1_IN_ControlMode);
      c1_ControlMode(chartInstance);
      break;

     case c1_IN_Error_Mode:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 4U, 5, 0,
                        c1_IN_Error_Mode);
      _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 9U,
                   chartInstance->c1_sfEvent);
      *chartInstance->c1_controller_flag = c1_Protections(chartInstance,
        *chartInstance->c1_fault_clear);
      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 9U, chartInstance->c1_sfEvent);
      break;

     case c1_IN_VF_Mode:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 4U, 5, 0, c1_IN_VF_Mode);
      c1_VF_Mode(chartInstance);
      break;

     default:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 4U, 5, 0, 0);

      /* Unreachable state, for coverage only */
      chartInstance->c1_is_Motor_On = c1_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U, chartInstance->c1_sfEvent);
      break;
    }
  }

  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 5U, chartInstance->c1_sfEvent);
}

static void c1_enter_atomic_Error_Mode(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance)
{
  *chartInstance->c1_PWM_CMD1 = (uint16_T)((uint32_T)
    chartInstance->c1_PWM_MAX_DUTYCYCLE >> 1);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD1, 52U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 52U, (real_T)
                    *chartInstance->c1_PWM_CMD1);
  *chartInstance->c1_PWM_CMD2 = (uint16_T)((uint32_T)
    chartInstance->c1_PWM_MAX_DUTYCYCLE >> 1);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD2, 53U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 53U, (real_T)
                    *chartInstance->c1_PWM_CMD2);
  *chartInstance->c1_Vs_s = 0;
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_Vs_s, 50U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 50U, (real_T)
                    *chartInstance->c1_Vs_s);
  *chartInstance->c1_PWM_OFF = 1U;
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_OFF, 54U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 54U, (real_T)
                    *chartInstance->c1_PWM_OFF);
  *chartInstance->c1_controller_flag = c1_Protections(chartInstance,
    *chartInstance->c1_fault_clear);
}

static void c1_Alignment_Done(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance)
{
  boolean_T c1_out;
  boolean_T c1_b_Synchronisation_enabled;
  int32_T c1_b_Time_state_output;
  boolean_T c1_hoistedGlobal;
  int16_T c1_b_hoistedGlobal;
  int16_T c1_c_hoistedGlobal;
  int16_T c1_e_speed_reference;
  int16_T c1_b_rotor_start_positive;
  int16_T c1_b_rotor_start_negative;
  int16_T c1_d_rotor_position_reference;
  int16_T c1_i12;
  int16_T c1_i13;
  int32_T c1_i14;
  boolean_T c1_covSaturation;
  int16_T c1_i15;
  int16_T c1_i16;
  int32_T c1_i17;
  boolean_T c1_b_covSaturation;
  int16_T c1_i18;
  int16_T c1_i19;
  int32_T c1_i20;
  boolean_T c1_c_covSaturation;
  int16_T c1_b_ReferenceOut;
  int32_T c1_b_State_variable_out;
  int32_T c1_i21;
  boolean_T c1_d_covSaturation;
  int32_T c1_i22;
  boolean_T c1_e_covSaturation;
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 78U, chartInstance->c1_sfEvent);
  c1_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance, 5U, 78U,
    0U, covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 78U, 0,
    (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance, 5U,
    78U, 0U, (real_T)((*chartInstance->c1_controller_mode ==
                       ControlModeState_VFMode) &
                      (chartInstance->c1_temporalCounter_i1 >= 20000U)), 0.0, 0,
    1U, ((*chartInstance->c1_controller_mode == ControlModeState_VFMode) &
         (chartInstance->c1_temporalCounter_i1 >= 20000U)) != 0) != 0U) != 0U);
  sf_temporal_value_range_check_min(chartInstance->S, 1270U, 2500.0, 0.0);
  if (c1_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 78U, chartInstance->c1_sfEvent);
    chartInstance->c1_tp_Alignment_Done = 0U;
    chartInstance->c1_is_Motor_On = c1_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U, chartInstance->c1_sfEvent);
    chartInstance->c1_is_Motor_On = c1_IN_VF_Mode;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 10U, chartInstance->c1_sfEvent);
    chartInstance->c1_tp_VF_Mode = 1U;
    c1_hoistedGlobal = *chartInstance->c1_Direction;
    c1_b_hoistedGlobal = *chartInstance->c1_start_angle_pos;
    c1_c_hoistedGlobal = *chartInstance->c1_start_angle_neg;
    c1_e_speed_reference = (int16_T)c1_hoistedGlobal;
    c1_b_rotor_start_positive = c1_b_hoistedGlobal;
    c1_b_rotor_start_negative = c1_c_hoistedGlobal;
    _SFD_SET_DATA_VALUE_PTR(118U, &c1_d_rotor_position_reference);
    _SFD_SET_DATA_VALUE_PTR(107U, &c1_b_rotor_start_negative);
    _SFD_SET_DATA_VALUE_PTR(95U, &c1_b_rotor_start_positive);
    _SFD_SET_DATA_VALUE_PTR(77U, &c1_e_speed_reference);
    _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 26U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH(4U, 0U);
    _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("speed_reference", &c1_e_speed_reference,
      c1_c_sf_marshallOut, c1_c_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("rotor_start_positive",
      &c1_b_rotor_start_positive, c1_c_sf_marshallOut, c1_c_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("rotor_start_negative",
      &c1_b_rotor_start_negative, c1_c_sf_marshallOut, c1_c_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("rotor_position_reference",
      &c1_d_rotor_position_reference, c1_c_sf_marshallOut, c1_c_sf_marshallIn);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 26U, chartInstance->c1_sfEvent);
    *chartInstance->c1_b_speed_reference = c1_e_speed_reference;
    *chartInstance->c1_rotor_start_positive = c1_b_rotor_start_positive;
    *chartInstance->c1_rotor_start_negative = c1_b_rotor_start_negative;
    sf_call_output_fcn_call(chartInstance->S, 0, "VFStart", 0);
    c1_d_rotor_position_reference =
      *chartInstance->c1_b_rotor_position_reference;
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 26U, chartInstance->c1_sfEvent);
    _SFD_UNSET_DATA_VALUE_PTR(118U);
    _SFD_UNSET_DATA_VALUE_PTR(107U);
    _SFD_UNSET_DATA_VALUE_PTR(95U);
    _SFD_UNSET_DATA_VALUE_PTR(77U);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 26U, chartInstance->c1_sfEvent);
    chartInstance->c1_rotor_position_reference = c1_d_rotor_position_reference;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_rotor_position_reference,
                          23U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 23U, (real_T)
                      chartInstance->c1_rotor_position_reference);
    chartInstance->c1_MaskFlag = 0U;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_MaskFlag, 5U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 5U, (real_T)
                      chartInstance->c1_MaskFlag);
    chartInstance->c1_FirstCurrentZeroCrossingFlagPositiveToNegative = 0U;
    _SFD_DATA_RANGE_CHECK((real_T)
                          chartInstance->c1_FirstCurrentZeroCrossingFlagPositiveToNegative,
                          4U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 4U, (real_T)
                      chartInstance->c1_FirstCurrentZeroCrossingFlagPositiveToNegative);
    chartInstance->c1_FirstCurrentZeroCrossingFlagNegativeToPositive = 0U;
    _SFD_DATA_RANGE_CHECK((real_T)
                          chartInstance->c1_FirstCurrentZeroCrossingFlagNegativeToPositive,
                          3U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 3U, (real_T)
                      chartInstance->c1_FirstCurrentZeroCrossingFlagNegativeToPositive);
    chartInstance->c1_CurrentZeroCrossingFlagPositiveToNegativeCounter = 0U;
    _SFD_DATA_RANGE_CHECK((real_T)
                          chartInstance->c1_CurrentZeroCrossingFlagPositiveToNegativeCounter,
                          2U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 2U, (real_T)
                      chartInstance->c1_CurrentZeroCrossingFlagPositiveToNegativeCounter);
    chartInstance->c1_CurrentZeroCrossingFlagNegativeToPositiveCounter = 0U;
    _SFD_DATA_RANGE_CHECK((real_T)
                          chartInstance->c1_CurrentZeroCrossingFlagNegativeToPositiveCounter,
                          1U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 1U, (real_T)
                      chartInstance->c1_CurrentZeroCrossingFlagNegativeToPositiveCounter);
    c1_i12 = *chartInstance->c1_speed_reference;
    if (c1_i12 < 0) {
      c1_i14 = -c1_i12;
      c1_covSaturation = false;
      if (c1_i14 > 32767) {
        c1_covSaturation = true;
        c1_i14 = 32767;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 617U, 20U);
      } else {
        if (c1_i14 < -32768) {
          c1_covSaturation = true;
          c1_i14 = -32768;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 617U, 20U);
        }
      }

      covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 4, 10, 2, 0,
        c1_covSaturation);
      c1_i13 = (int16_T)c1_i14;
    } else {
      c1_i13 = c1_i12;
    }

    *chartInstance->c1_controller_flag = c1_VFDone_Flag(chartInstance, c1_i13,
      *chartInstance->c1_start_ctr);
    c1_i15 = *chartInstance->c1_speed_reference;
    if (c1_i15 < 0) {
      c1_i17 = -c1_i15;
      c1_b_covSaturation = false;
      if (c1_i17 > 32767) {
        c1_b_covSaturation = true;
        c1_i17 = 32767;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 756U, 20U);
      } else {
        if (c1_i17 < -32768) {
          c1_b_covSaturation = true;
          c1_i17 = -32768;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 756U, 20U);
        }
      }

      covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 4, 10, 3, 0,
        c1_b_covSaturation);
      c1_i16 = (int16_T)c1_i17;
    } else {
      c1_i16 = c1_i15;
    }

    chartInstance->c1_rotor_position_reference = c1_SpeedToAngle(chartInstance,
      c1_i16, chartInstance->c1_rotor_position_reference);
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_rotor_position_reference,
                          23U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 23U, (real_T)
                      chartInstance->c1_rotor_position_reference);
    c1_i18 = *chartInstance->c1_speed_reference;
    if (c1_i18 < 0) {
      c1_i20 = -c1_i18;
      c1_c_covSaturation = false;
      if (c1_i20 > 32767) {
        c1_c_covSaturation = true;
        c1_i20 = 32767;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 865U, 20U);
      } else {
        if (c1_i20 < -32768) {
          c1_c_covSaturation = true;
          c1_i20 = -32768;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 865U, 20U);
        }
      }

      covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 4, 10, 4, 0,
        c1_c_covSaturation);
      c1_i19 = (int16_T)c1_i20;
    } else {
      c1_i19 = c1_i18;
    }

    c1_SpeedRateLimit(chartInstance, 0, c1_i19 * chartInstance->c1_scale,
                      &c1_b_ReferenceOut, &c1_b_State_variable_out);
    chartInstance->c1_SpeedReference_PT1 = c1_b_ReferenceOut;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_SpeedReference_PT1, 16U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 16U, (real_T)
                      chartInstance->c1_SpeedReference_PT1);
    chartInstance->c1_PT1_StateVariable = c1_b_State_variable_out;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_PT1_StateVariable, 11U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 11U, (real_T)
                      chartInstance->c1_PT1_StateVariable);
    *chartInstance->c1_ApproxPower = c1_PowerCalculation(chartInstance,
      *chartInstance->c1_is_adc, *chartInstance->c1_InvVoltageOut, 0.0);
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_ApproxPower, 57U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 57U, (real_T)
                      *chartInstance->c1_ApproxPower);
    *chartInstance->c1_voltage_ref = *chartInstance->c1_voltage_reference;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_voltage_ref, 61U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 61U, (real_T)
                      *chartInstance->c1_voltage_ref);
    *chartInstance->c1_speed_pll = chartInstance->c1_SpeedReference_PT1;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_speed_pll, 60U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 60U, (real_T)
                      *chartInstance->c1_speed_pll);
    c1_OpenClosedHBridge(chartInstance);
    c1_i21 = *chartInstance->c1_PWM_CMD1;
    c1_d_covSaturation = false;
    if (c1_i21 < 0) {
      c1_d_covSaturation = true;
      c1_i21 = 0;
      _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 1349U, 8U);
    } else {
      if (c1_i21 > 65535) {
        c1_i21 = 65535;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 1349U, 8U);
      }

      covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 4, 10, 0, 0,
        c1_d_covSaturation);
    }

    c1_i22 = *chartInstance->c1_PWM_CMD2;
    c1_e_covSaturation = false;
    if (c1_i22 < 0) {
      c1_e_covSaturation = true;
      c1_i22 = 0;
      _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 1358U, 8U);
    } else {
      if (c1_i22 > 65535) {
        c1_i22 = 65535;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 1358U, 8U);
      }

      covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 4, 10, 1, 0,
        c1_e_covSaturation);
    }

    *chartInstance->c1_Vs_s = c1_Duty_Cycle_To_Voltage(chartInstance, (uint16_T)
      c1_i21, (uint16_T)c1_i22, *chartInstance->c1_vdc);
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_Vs_s, 50U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 50U, (real_T)
                      *chartInstance->c1_Vs_s);
    *chartInstance->c1_old_InvVoltageOut = *chartInstance->c1_InvVoltageOut;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_old_InvVoltageOut, 64U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 64U, (real_T)
                      *chartInstance->c1_old_InvVoltageOut);
    chartInstance->c1_old_is = *chartInstance->c1_is_adc;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_old_is, 22U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 22U, (real_T)
                      chartInstance->c1_old_is);
    *chartInstance->c1_testpoint = *chartInstance->c1_start_angle_neg;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_testpoint, 65U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 65U, (real_T)
                      *chartInstance->c1_testpoint);
    *chartInstance->c1_testpoint2 = (uint16_T)*chartInstance->c1_Direction;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_testpoint2, 66U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 66U, (real_T)
                      *chartInstance->c1_testpoint2);
  } else {
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 6U, chartInstance->c1_sfEvent);
    *chartInstance->c1_controller_flag = ControlFlag_AlignmentDone;
    c1_CheckforSynchronisation(chartInstance, 0, 0,
      &c1_b_Synchronisation_enabled, &c1_b_Time_state_output);
    chartInstance->c1_Vf_Done_SynchronisationEnable =
      c1_b_Synchronisation_enabled;
    _SFD_DATA_RANGE_CHECK((real_T)
                          chartInstance->c1_Vf_Done_SynchronisationEnable, 18U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 18U, (real_T)
                      chartInstance->c1_Vf_Done_SynchronisationEnable);
    chartInstance->c1_SynchronisationTime_state = c1_b_Time_state_output;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_SynchronisationTime_state,
                          17U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 17U, (real_T)
                      chartInstance->c1_SynchronisationTime_state);
    *chartInstance->c1_Vs_s = 0;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_Vs_s, 50U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 50U, (real_T)
                      *chartInstance->c1_Vs_s);
    *chartInstance->c1_PWM_CMD1 = (uint16_T)((uint32_T)
      chartInstance->c1_PWM_MAX_DUTYCYCLE >> 1);
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD1, 52U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 52U, (real_T)
                      *chartInstance->c1_PWM_CMD1);
    *chartInstance->c1_PWM_CMD2 = (uint16_T)((uint32_T)
      chartInstance->c1_PWM_MAX_DUTYCYCLE >> 1);
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD2, 53U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 53U, (real_T)
                      *chartInstance->c1_PWM_CMD2);
    *chartInstance->c1_PWM_OFF = 1U;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_OFF, 54U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 54U, (real_T)
                      *chartInstance->c1_PWM_OFF);
  }

  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 6U, chartInstance->c1_sfEvent);
}

static void c1_VF_Mode(SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  int32_T c1_i23;
  boolean_T c1_out;
  boolean_T c1_b_out;
  int16_T c1_i24;
  int16_T c1_i25;
  int32_T c1_i26;
  boolean_T c1_covSaturation;
  int16_T c1_i27;
  int16_T c1_i28;
  int32_T c1_i29;
  boolean_T c1_b_covSaturation;
  int16_T c1_i30;
  int16_T c1_i31;
  int32_T c1_i32;
  boolean_T c1_c_covSaturation;
  int16_T c1_b_ReferenceOut;
  int32_T c1_b_State_variable_out;
  int32_T c1_i33;
  boolean_T c1_d_covSaturation;
  int32_T c1_i34;
  boolean_T c1_e_covSaturation;
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 3U, chartInstance->c1_sfEvent);
  c1_i23 = (*chartInstance->c1_controller_mode == ControlModeState_ErrorMode) |
    (int32_T)c1_b_fault(chartInstance, *chartInstance->c1_fault1,
                        *chartInstance->c1_fault2);
  c1_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance, 5U, 3U,
    0U, covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 3U, 0,
    (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance, 5U,
    3U, 0U, (real_T)c1_i23, 0.0, 0, 1U, c1_i23 != 0) != 0U) != 0U);
  if (c1_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 3U, chartInstance->c1_sfEvent);
    chartInstance->c1_tp_VF_Mode = 0U;
    c1_exit_atomic_VF_Mode(chartInstance);
    chartInstance->c1_is_Motor_On = c1_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 10U, chartInstance->c1_sfEvent);
    chartInstance->c1_is_Motor_On = c1_IN_Error_Mode;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 9U, chartInstance->c1_sfEvent);
    chartInstance->c1_tp_Error_Mode = 1U;
    c1_enter_atomic_Error_Mode(chartInstance);
  } else {
    _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 7U, chartInstance->c1_sfEvent);
    c1_b_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance, 5U,
      7U, 0U, covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 7U, 0,
      (*chartInstance->c1_controller_mode == ControlModeState_ControlMode) != 0U)
      != 0U);
    if (c1_b_out) {
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 7U, chartInstance->c1_sfEvent);
      chartInstance->c1_tp_VF_Mode = 0U;
      c1_exit_atomic_VF_Mode(chartInstance);
      chartInstance->c1_is_Motor_On = c1_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 10U, chartInstance->c1_sfEvent);
      chartInstance->c1_is_Motor_On = c1_IN_ControlMode;
      _SFD_CS_CALL(STATE_ACTIVE_TAG, 8U, chartInstance->c1_sfEvent);
      chartInstance->c1_tp_ControlMode = 1U;
      c1_enter_atomic_ControlMode(chartInstance);
    } else {
      _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 10U,
                   chartInstance->c1_sfEvent);
      c1_i24 = *chartInstance->c1_speed_reference;
      if (c1_i24 < 0) {
        c1_i26 = -c1_i24;
        c1_covSaturation = false;
        if (c1_i26 > 32767) {
          c1_covSaturation = true;
          c1_i26 = 32767;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 617U, 20U);
        } else {
          if (c1_i26 < -32768) {
            c1_covSaturation = true;
            c1_i26 = -32768;
            _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 617U, 20U);
          }
        }

        covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 4, 10, 2, 0,
          c1_covSaturation);
        c1_i25 = (int16_T)c1_i26;
      } else {
        c1_i25 = c1_i24;
      }

      *chartInstance->c1_controller_flag = c1_VFDone_Flag(chartInstance, c1_i25,
        *chartInstance->c1_start_ctr);
      c1_i27 = *chartInstance->c1_speed_reference;
      if (c1_i27 < 0) {
        c1_i29 = -c1_i27;
        c1_b_covSaturation = false;
        if (c1_i29 > 32767) {
          c1_b_covSaturation = true;
          c1_i29 = 32767;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 756U, 20U);
        } else {
          if (c1_i29 < -32768) {
            c1_b_covSaturation = true;
            c1_i29 = -32768;
            _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 756U, 20U);
          }
        }

        covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 4, 10, 3, 0,
          c1_b_covSaturation);
        c1_i28 = (int16_T)c1_i29;
      } else {
        c1_i28 = c1_i27;
      }

      chartInstance->c1_rotor_position_reference = c1_SpeedToAngle(chartInstance,
        c1_i28, chartInstance->c1_rotor_position_reference);
      _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_rotor_position_reference,
                            23U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 23U, (real_T)
                        chartInstance->c1_rotor_position_reference);
      c1_i30 = *chartInstance->c1_speed_reference;
      if (c1_i30 < 0) {
        c1_i32 = -c1_i30;
        c1_c_covSaturation = false;
        if (c1_i32 > 32767) {
          c1_c_covSaturation = true;
          c1_i32 = 32767;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 865U, 20U);
        } else {
          if (c1_i32 < -32768) {
            c1_c_covSaturation = true;
            c1_i32 = -32768;
            _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 865U, 20U);
          }
        }

        covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 4, 10, 4, 0,
          c1_c_covSaturation);
        c1_i31 = (int16_T)c1_i32;
      } else {
        c1_i31 = c1_i30;
      }

      c1_SpeedRateLimit(chartInstance, 0, c1_i31 * chartInstance->c1_scale,
                        &c1_b_ReferenceOut, &c1_b_State_variable_out);
      chartInstance->c1_SpeedReference_PT1 = c1_b_ReferenceOut;
      _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_SpeedReference_PT1, 16U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 16U, (real_T)
                        chartInstance->c1_SpeedReference_PT1);
      chartInstance->c1_PT1_StateVariable = c1_b_State_variable_out;
      _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_PT1_StateVariable, 11U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 11U, (real_T)
                        chartInstance->c1_PT1_StateVariable);
      *chartInstance->c1_ApproxPower = c1_PowerCalculation(chartInstance,
        *chartInstance->c1_is_adc, *chartInstance->c1_InvVoltageOut, 0.0);
      _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_ApproxPower, 57U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 57U, (real_T)
                        *chartInstance->c1_ApproxPower);
      *chartInstance->c1_voltage_ref = *chartInstance->c1_voltage_reference;
      _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_voltage_ref, 61U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 61U, (real_T)
                        *chartInstance->c1_voltage_ref);
      *chartInstance->c1_speed_pll = chartInstance->c1_SpeedReference_PT1;
      _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_speed_pll, 60U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 60U, (real_T)
                        *chartInstance->c1_speed_pll);
      c1_OpenClosedHBridge(chartInstance);
      c1_i33 = *chartInstance->c1_PWM_CMD1;
      c1_d_covSaturation = false;
      if (c1_i33 < 0) {
        c1_d_covSaturation = true;
        c1_i33 = 0;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 1349U, 8U);
      } else {
        if (c1_i33 > 65535) {
          c1_i33 = 65535;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 1349U, 8U);
        }

        covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 4, 10, 0, 0,
          c1_d_covSaturation);
      }

      c1_i34 = *chartInstance->c1_PWM_CMD2;
      c1_e_covSaturation = false;
      if (c1_i34 < 0) {
        c1_e_covSaturation = true;
        c1_i34 = 0;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 1358U, 8U);
      } else {
        if (c1_i34 > 65535) {
          c1_i34 = 65535;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 1358U, 8U);
        }

        covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 4, 10, 1, 0,
          c1_e_covSaturation);
      }

      *chartInstance->c1_Vs_s = c1_Duty_Cycle_To_Voltage(chartInstance,
        (uint16_T)c1_i33, (uint16_T)c1_i34, *chartInstance->c1_vdc);
      _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_Vs_s, 50U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 50U, (real_T)
                        *chartInstance->c1_Vs_s);
      *chartInstance->c1_old_InvVoltageOut = *chartInstance->c1_InvVoltageOut;
      _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_old_InvVoltageOut, 64U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 64U, (real_T)
                        *chartInstance->c1_old_InvVoltageOut);
      chartInstance->c1_old_is = *chartInstance->c1_is_adc;
      _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_old_is, 22U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 22U, (real_T)
                        chartInstance->c1_old_is);
      *chartInstance->c1_testpoint = *chartInstance->c1_start_angle_neg;
      _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_testpoint, 65U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 65U, (real_T)
                        *chartInstance->c1_testpoint);
      *chartInstance->c1_testpoint2 = (uint16_T)*chartInstance->c1_Direction;
      _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_testpoint2, 66U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 66U, (real_T)
                        *chartInstance->c1_testpoint2);
    }
  }

  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 10U, chartInstance->c1_sfEvent);
}

static void c1_exit_atomic_VF_Mode(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance)
{
  boolean_T c1_c_Bemf_control_fault;
  int32_T c1_e_Counter_output;
  boolean_T c1_c_Speed_control_fault;
  int32_T c1_f_Counter_output;
  boolean_T c1_c_Slurp_Detected;
  int32_T c1_g_Counter_output;
  boolean_T c1_d_Slurp_Detected;
  int32_T c1_h_Counter_output;
  int16_T c1_b_Activation_Counter_output;
  _SFD_CS_CALL(STATE_ENTER_EXIT_FUNCTION_TAG, 10U, chartInstance->c1_sfEvent);
  chartInstance->c1_OLspeed_sat = chartInstance->c1_SpeedReference_PT1;
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OLspeed_sat, 9U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 9U, (real_T)
                    chartInstance->c1_OLspeed_sat);
  chartInstance->c1_OLvoltage_sat = *chartInstance->c1_voltage_reference;
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OLvoltage_sat, 10U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 10U, (real_T)
                    chartInstance->c1_OLvoltage_sat);
  chartInstance->c1_OL_speed_state = chartInstance->c1_SpeedReference_PT1;
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OL_speed_state, 6U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 6U, (real_T)
                    chartInstance->c1_OL_speed_state);
  chartInstance->c1_OL_voltage_state_bemf = *chartInstance->c1_voltage_reference;
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OL_voltage_state_bemf, 7U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 7U, (real_T)
                    chartInstance->c1_OL_voltage_state_bemf);
  c1_Bemf_Observation(chartInstance, 0, 0, 0, &c1_c_Bemf_control_fault,
                      &c1_e_Counter_output);
  *chartInstance->c1_Bemf_control_fault = c1_c_Bemf_control_fault;
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_Bemf_control_fault, 58U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 58U, (real_T)
                    *chartInstance->c1_Bemf_control_fault);
  chartInstance->c1_BackEmfObersvationCounterValue = c1_e_Counter_output;
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_BackEmfObersvationCounterValue,
                        0U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 0U, (real_T)
                    chartInstance->c1_BackEmfObersvationCounterValue);
  c1_Speed_Observation(chartInstance, 0, 0, 0, &c1_c_Speed_control_fault,
                       &c1_f_Counter_output);
  *chartInstance->c1_Speed_control_fault = c1_c_Speed_control_fault;
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_Speed_control_fault, 59U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 59U, (real_T)
                    *chartInstance->c1_Speed_control_fault);
  chartInstance->c1_SpeedObersvationCounterValue = c1_f_Counter_output;
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_SpeedObersvationCounterValue,
                        15U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 15U, (real_T)
                    chartInstance->c1_SpeedObersvationCounterValue);
  c1_Miele(chartInstance, 0, 0, 0, &c1_c_Slurp_Detected, &c1_g_Counter_output);
  *chartInstance->c1_SlurpDetectedMiele = c1_c_Slurp_Detected;
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_SlurpDetectedMiele, 63U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 63U, (real_T)
                    *chartInstance->c1_SlurpDetectedMiele);
  chartInstance->c1_SlurpDetectionCounterMiele = c1_g_Counter_output;
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_SlurpDetectionCounterMiele,
                        13U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 13U, (real_T)
                    chartInstance->c1_SlurpDetectionCounterMiele);
  c1_Appcon(chartInstance, 0, 0, 0, 0, 0, 0, &c1_d_Slurp_Detected,
            &c1_h_Counter_output, &c1_b_Activation_Counter_output);
  *chartInstance->c1_SlurpDetectedAppcon = c1_d_Slurp_Detected;
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_SlurpDetectedAppcon, 56U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 56U, (real_T)
                    *chartInstance->c1_SlurpDetectedAppcon);
  chartInstance->c1_SlurpDetectionCounterAppcon = c1_h_Counter_output;
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_SlurpDetectionCounterAppcon,
                        12U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 12U, (real_T)
                    chartInstance->c1_SlurpDetectionCounterAppcon);
  chartInstance->c1_SlurpDetection_Appcon_ActivationCounter =
    c1_b_Activation_Counter_output;
  _SFD_DATA_RANGE_CHECK((real_T)
                        chartInstance->c1_SlurpDetection_Appcon_ActivationCounter,
                        14U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 14U, (real_T)
                    chartInstance->c1_SlurpDetection_Appcon_ActivationCounter);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 10U, chartInstance->c1_sfEvent);
}

static void c1_enter_atomic_ControlMode(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance)
{
  int16_T c1_i35;
  int16_T c1_i36;
  int32_T c1_i37;
  boolean_T c1_covSaturation;
  int16_T c1_b_ReferenceOut;
  int32_T c1_b_State_variable_out;
  boolean_T c1_c_Speed_control_fault;
  int32_T c1_e_Counter_output;
  boolean_T c1_c_Slurp_Detected;
  int32_T c1_f_Counter_output;
  boolean_T c1_temp;
  boolean_T c1_b_temp;
  boolean_T c1_c_temp;
  boolean_T c1_out;
  boolean_T c1_d_temp;
  boolean_T c1_e_temp;
  boolean_T c1_f_temp;
  boolean_T c1_b_out;
  chartInstance->c1_rotor_position_reference = c1_SpeedToAngle(chartInstance,
    *chartInstance->c1_speed_pll, chartInstance->c1_rotor_position_reference);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_rotor_position_reference, 23U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 23U, (real_T)
                    chartInstance->c1_rotor_position_reference);
  c1_i35 = *chartInstance->c1_speed_reference;
  if (c1_i35 < 0) {
    c1_i37 = -c1_i35;
    c1_covSaturation = false;
    if (c1_i37 > 32767) {
      c1_covSaturation = true;
      c1_i37 = 32767;
      _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 768U, 153U, 20U);
    } else {
      if (c1_i37 < -32768) {
        c1_covSaturation = true;
        c1_i37 = -32768;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 768U, 153U, 20U);
      }
    }

    covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 4, 8, 0, 0,
      c1_covSaturation);
    c1_i36 = (int16_T)c1_i37;
  } else {
    c1_i36 = c1_i35;
  }

  c1_SpeedRateLimit(chartInstance, c1_i36, chartInstance->c1_PT1_StateVariable,
                    &c1_b_ReferenceOut, &c1_b_State_variable_out);
  chartInstance->c1_SpeedReference_PT1 = c1_b_ReferenceOut;
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_SpeedReference_PT1, 16U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 16U, (real_T)
                    chartInstance->c1_SpeedReference_PT1);
  chartInstance->c1_PT1_StateVariable = c1_b_State_variable_out;
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_PT1_StateVariable, 11U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 11U, (real_T)
                    chartInstance->c1_PT1_StateVariable);
  *chartInstance->c1_ApproxPower = c1_PowerCalculation(chartInstance,
    *chartInstance->c1_is_adc, *chartInstance->c1_InvVoltageOut, 0.0);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_ApproxPower, 57U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 57U, (real_T)
                    *chartInstance->c1_ApproxPower);
  c1_Speed_Observation(chartInstance, *chartInstance->c1_speed_pll,
                       chartInstance->c1_SpeedReference_PT1,
                       chartInstance->c1_SpeedObersvationCounterValue,
                       &c1_c_Speed_control_fault, &c1_e_Counter_output);
  *chartInstance->c1_Speed_control_fault = c1_c_Speed_control_fault;
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_Speed_control_fault, 59U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 59U, (real_T)
                    *chartInstance->c1_Speed_control_fault);
  chartInstance->c1_SpeedObersvationCounterValue = c1_e_Counter_output;
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_SpeedObersvationCounterValue,
                        15U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 15U, (real_T)
                    chartInstance->c1_SpeedObersvationCounterValue);
  c1_Miele(chartInstance, *chartInstance->c1_ApproxPower,
           *chartInstance->c1_speed_pll,
           chartInstance->c1_SlurpDetectionCounterMiele, &c1_c_Slurp_Detected,
           &c1_f_Counter_output);
  *chartInstance->c1_SlurpDetectedMiele = c1_c_Slurp_Detected;
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_SlurpDetectedMiele, 63U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 63U, (real_T)
                    *chartInstance->c1_SlurpDetectedMiele);
  chartInstance->c1_SlurpDetectionCounterMiele = c1_f_Counter_output;
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_SlurpDetectionCounterMiele,
                        13U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 13U, (real_T)
                    chartInstance->c1_SlurpDetectionCounterMiele);
  *chartInstance->c1_testpoint = chartInstance->c1_is_Rshunt;
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_testpoint, 65U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 65U, (real_T)
                    *chartInstance->c1_testpoint);
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 27U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 27U, chartInstance->c1_sfEvent);
  _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 14U, chartInstance->c1_sfEvent);
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 19U, chartInstance->c1_sfEvent);
  c1_temp = covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 19U, 0,
    (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance, 5U,
    19U, 0U, (real_T)chartInstance->c1_old_is, 0.0, 0, 3U,
    chartInstance->c1_old_is <= 0) != 0U);
  if (c1_temp) {
    c1_temp = covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 19U, 1,
      (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance, 5U,
      19U, 1U, (real_T)*chartInstance->c1_is_adc, 0.0, 0, 4U,
      *chartInstance->c1_is_adc > 0) != 0U);
  }

  c1_b_temp = c1_temp;
  if (c1_b_temp) {
    c1_b_temp = covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 19U, 2,
      (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance, 5U,
      19U, 2U, (real_T)
      chartInstance->c1_FirstCurrentZeroCrossingFlagNegativeToPositive, 0.0, 0,
      0U, chartInstance->c1_FirstCurrentZeroCrossingFlagNegativeToPositive == 0)
      != 0U);
  }

  c1_c_temp = c1_b_temp;
  if (c1_c_temp) {
    c1_c_temp = covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 19U, 3,
      (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance, 5U,
      19U, 3U, (real_T)*chartInstance->c1_bemf_vf, 1.0, 0, 0U, (int32_T)
      *chartInstance->c1_bemf_vf) != 0U);
  }

  c1_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance, 5U, 19U,
    0U, c1_c_temp != 0U);
  if (c1_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 19U, chartInstance->c1_sfEvent);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 18U, chartInstance->c1_sfEvent);
    chartInstance->c1_FirstCurrentZeroCrossingFlagNegativeToPositive = 1U;
    _SFD_DATA_RANGE_CHECK((real_T)
                          chartInstance->c1_FirstCurrentZeroCrossingFlagNegativeToPositive,
                          3U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 3U, (real_T)
                      chartInstance->c1_FirstCurrentZeroCrossingFlagNegativeToPositive);
    chartInstance->c1_CurrentZeroCrossingFlagNegativeToPositiveCounter = 0U;
    _SFD_DATA_RANGE_CHECK((real_T)
                          chartInstance->c1_CurrentZeroCrossingFlagNegativeToPositiveCounter,
                          1U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 1U, (real_T)
                      chartInstance->c1_CurrentZeroCrossingFlagNegativeToPositiveCounter);
    chartInstance->c1_MaskFlag = 1U;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_MaskFlag, 5U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 5U, (real_T)
                      chartInstance->c1_MaskFlag);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 17U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 15U, chartInstance->c1_sfEvent);
  }

  _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 16U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 27U, chartInstance->c1_sfEvent);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 27U, chartInstance->c1_sfEvent);
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 28U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 28U, chartInstance->c1_sfEvent);
  _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 12U, chartInstance->c1_sfEvent);
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 11U, chartInstance->c1_sfEvent);
  c1_d_temp = covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 11U, 0,
    (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance, 5U,
    11U, 0U, (real_T)chartInstance->c1_old_is, 0.0, 0, 5U,
    chartInstance->c1_old_is >= 0) != 0U);
  if (c1_d_temp) {
    c1_d_temp = covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 11U, 1,
      (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance, 5U,
      11U, 1U, (real_T)*chartInstance->c1_is_adc, 0.0, 0, 2U,
      *chartInstance->c1_is_adc < 0) != 0U);
  }

  c1_e_temp = c1_d_temp;
  if (c1_e_temp) {
    c1_e_temp = covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 11U, 2,
      (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance, 5U,
      11U, 2U, (real_T)
      chartInstance->c1_FirstCurrentZeroCrossingFlagPositiveToNegative, 0.0, 0,
      0U, chartInstance->c1_FirstCurrentZeroCrossingFlagPositiveToNegative == 0)
      != 0U);
  }

  c1_f_temp = c1_e_temp;
  if (c1_f_temp) {
    c1_f_temp = covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 11U, 3,
      (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance, 5U,
      11U, 3U, (real_T)*chartInstance->c1_bemf_vf, 1.0, 0, 0U, (int32_T)
      *chartInstance->c1_bemf_vf) != 0U);
  }

  c1_b_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance, 5U,
    11U, 0U, c1_f_temp != 0U);
  if (c1_b_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 11U, chartInstance->c1_sfEvent);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 10U, chartInstance->c1_sfEvent);
    chartInstance->c1_FirstCurrentZeroCrossingFlagPositiveToNegative = 1U;
    _SFD_DATA_RANGE_CHECK((real_T)
                          chartInstance->c1_FirstCurrentZeroCrossingFlagPositiveToNegative,
                          4U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 4U, (real_T)
                      chartInstance->c1_FirstCurrentZeroCrossingFlagPositiveToNegative);
    chartInstance->c1_CurrentZeroCrossingFlagPositiveToNegativeCounter = 0U;
    _SFD_DATA_RANGE_CHECK((real_T)
                          chartInstance->c1_CurrentZeroCrossingFlagPositiveToNegativeCounter,
                          2U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 2U, (real_T)
                      chartInstance->c1_CurrentZeroCrossingFlagPositiveToNegativeCounter);
    chartInstance->c1_MaskFlag = 1U;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_MaskFlag, 5U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 5U, (real_T)
                      chartInstance->c1_MaskFlag);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 9U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 8U, chartInstance->c1_sfEvent);
  }

  _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 13U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 28U, chartInstance->c1_sfEvent);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 28U, chartInstance->c1_sfEvent);
  c1_NegToPos_control(chartInstance);
  c1_PosToNeg_control(chartInstance);
  *chartInstance->c1_old_InvVoltageOut = *chartInstance->c1_InvVoltageOut;
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_old_InvVoltageOut, 64U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 64U, (real_T)
                    *chartInstance->c1_old_InvVoltageOut);
  chartInstance->c1_old_is = *chartInstance->c1_is_adc;
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_old_is, 22U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 22U, (real_T)
                    chartInstance->c1_old_is);
}

static void c1_ControlMode(SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  int32_T c1_i38;
  boolean_T c1_out;
  int16_T c1_i39;
  int16_T c1_i40;
  int32_T c1_i41;
  boolean_T c1_covSaturation;
  int16_T c1_b_ReferenceOut;
  int32_T c1_b_State_variable_out;
  boolean_T c1_c_Speed_control_fault;
  int32_T c1_e_Counter_output;
  boolean_T c1_c_Slurp_Detected;
  int32_T c1_f_Counter_output;
  boolean_T c1_temp;
  boolean_T c1_b_temp;
  boolean_T c1_c_temp;
  boolean_T c1_b_out;
  boolean_T c1_d_temp;
  boolean_T c1_e_temp;
  boolean_T c1_f_temp;
  boolean_T c1_c_out;
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 6U, chartInstance->c1_sfEvent);
  c1_i38 = (*chartInstance->c1_controller_mode == ControlModeState_ErrorMode) |
    (int32_T)c1_b_fault(chartInstance, *chartInstance->c1_fault1,
                        *chartInstance->c1_fault2);
  c1_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance, 5U, 6U,
    0U, covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 6U, 0,
    (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance, 5U,
    6U, 0U, (real_T)c1_i38, 0.0, 0, 1U, c1_i38 != 0) != 0U) != 0U);
  if (c1_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 6U, chartInstance->c1_sfEvent);
    chartInstance->c1_tp_ControlMode = 0U;
    chartInstance->c1_is_Motor_On = c1_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 8U, chartInstance->c1_sfEvent);
    chartInstance->c1_is_Motor_On = c1_IN_Error_Mode;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 9U, chartInstance->c1_sfEvent);
    chartInstance->c1_tp_Error_Mode = 1U;
    c1_enter_atomic_Error_Mode(chartInstance);
  } else {
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 8U, chartInstance->c1_sfEvent);
    chartInstance->c1_rotor_position_reference = c1_SpeedToAngle(chartInstance, *
      chartInstance->c1_speed_pll, chartInstance->c1_rotor_position_reference);
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_rotor_position_reference,
                          23U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 23U, (real_T)
                      chartInstance->c1_rotor_position_reference);
    c1_i39 = *chartInstance->c1_speed_reference;
    if (c1_i39 < 0) {
      c1_i41 = -c1_i39;
      c1_covSaturation = false;
      if (c1_i41 > 32767) {
        c1_covSaturation = true;
        c1_i41 = 32767;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 768U, 153U, 20U);
      } else {
        if (c1_i41 < -32768) {
          c1_covSaturation = true;
          c1_i41 = -32768;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 768U, 153U, 20U);
        }
      }

      covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 4, 8, 0, 0,
        c1_covSaturation);
      c1_i40 = (int16_T)c1_i41;
    } else {
      c1_i40 = c1_i39;
    }

    c1_SpeedRateLimit(chartInstance, c1_i40, chartInstance->c1_PT1_StateVariable,
                      &c1_b_ReferenceOut, &c1_b_State_variable_out);
    chartInstance->c1_SpeedReference_PT1 = c1_b_ReferenceOut;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_SpeedReference_PT1, 16U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 16U, (real_T)
                      chartInstance->c1_SpeedReference_PT1);
    chartInstance->c1_PT1_StateVariable = c1_b_State_variable_out;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_PT1_StateVariable, 11U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 11U, (real_T)
                      chartInstance->c1_PT1_StateVariable);
    *chartInstance->c1_ApproxPower = c1_PowerCalculation(chartInstance,
      *chartInstance->c1_is_adc, *chartInstance->c1_InvVoltageOut, 0.0);
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_ApproxPower, 57U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 57U, (real_T)
                      *chartInstance->c1_ApproxPower);
    c1_Speed_Observation(chartInstance, *chartInstance->c1_speed_pll,
                         chartInstance->c1_SpeedReference_PT1,
                         chartInstance->c1_SpeedObersvationCounterValue,
                         &c1_c_Speed_control_fault, &c1_e_Counter_output);
    *chartInstance->c1_Speed_control_fault = c1_c_Speed_control_fault;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_Speed_control_fault, 59U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 59U, (real_T)
                      *chartInstance->c1_Speed_control_fault);
    chartInstance->c1_SpeedObersvationCounterValue = c1_e_Counter_output;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_SpeedObersvationCounterValue,
                          15U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 15U, (real_T)
                      chartInstance->c1_SpeedObersvationCounterValue);
    c1_Miele(chartInstance, *chartInstance->c1_ApproxPower,
             *chartInstance->c1_speed_pll,
             chartInstance->c1_SlurpDetectionCounterMiele, &c1_c_Slurp_Detected,
             &c1_f_Counter_output);
    *chartInstance->c1_SlurpDetectedMiele = c1_c_Slurp_Detected;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_SlurpDetectedMiele, 63U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 63U, (real_T)
                      *chartInstance->c1_SlurpDetectedMiele);
    chartInstance->c1_SlurpDetectionCounterMiele = c1_f_Counter_output;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_SlurpDetectionCounterMiele,
                          13U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 13U, (real_T)
                      chartInstance->c1_SlurpDetectionCounterMiele);
    *chartInstance->c1_testpoint = chartInstance->c1_is_Rshunt;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_testpoint, 65U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 65U, (real_T)
                      *chartInstance->c1_testpoint);
    _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 27U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 27U, chartInstance->c1_sfEvent);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 14U, chartInstance->c1_sfEvent);
    _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 19U,
                 chartInstance->c1_sfEvent);
    c1_temp = covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 19U, 0,
      (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance, 5U,
      19U, 0U, (real_T)chartInstance->c1_old_is, 0.0, 0, 3U,
      chartInstance->c1_old_is <= 0) != 0U);
    if (c1_temp) {
      c1_temp = covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 19U, 1,
        (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance,
        5U, 19U, 1U, (real_T)*chartInstance->c1_is_adc, 0.0, 0, 4U,
        *chartInstance->c1_is_adc > 0) != 0U);
    }

    c1_b_temp = c1_temp;
    if (c1_b_temp) {
      c1_b_temp = covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 19U, 2,
        (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance,
        5U, 19U, 2U, (real_T)
        chartInstance->c1_FirstCurrentZeroCrossingFlagNegativeToPositive, 0.0, 0,
        0U, chartInstance->c1_FirstCurrentZeroCrossingFlagNegativeToPositive ==
        0) != 0U);
    }

    c1_c_temp = c1_b_temp;
    if (c1_c_temp) {
      c1_c_temp = covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 19U, 3,
        (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance,
        5U, 19U, 3U, (real_T)*chartInstance->c1_bemf_vf, 1.0, 0, 0U, (int32_T)
        *chartInstance->c1_bemf_vf) != 0U);
    }

    c1_b_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance, 5U,
      19U, 0U, c1_c_temp != 0U);
    if (c1_b_out) {
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 19U, chartInstance->c1_sfEvent);
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 18U, chartInstance->c1_sfEvent);
      chartInstance->c1_FirstCurrentZeroCrossingFlagNegativeToPositive = 1U;
      _SFD_DATA_RANGE_CHECK((real_T)
                            chartInstance->c1_FirstCurrentZeroCrossingFlagNegativeToPositive,
                            3U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 3U, (real_T)
                        chartInstance->c1_FirstCurrentZeroCrossingFlagNegativeToPositive);
      chartInstance->c1_CurrentZeroCrossingFlagNegativeToPositiveCounter = 0U;
      _SFD_DATA_RANGE_CHECK((real_T)
                            chartInstance->c1_CurrentZeroCrossingFlagNegativeToPositiveCounter,
                            1U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 1U, (real_T)
                        chartInstance->c1_CurrentZeroCrossingFlagNegativeToPositiveCounter);
      chartInstance->c1_MaskFlag = 1U;
      _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_MaskFlag, 5U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 5U, (real_T)
                        chartInstance->c1_MaskFlag);
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 17U, chartInstance->c1_sfEvent);
    } else {
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 15U, chartInstance->c1_sfEvent);
    }

    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 16U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 27U, chartInstance->c1_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 27U, chartInstance->c1_sfEvent);
    _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 28U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 28U, chartInstance->c1_sfEvent);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 12U, chartInstance->c1_sfEvent);
    _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 11U,
                 chartInstance->c1_sfEvent);
    c1_d_temp = covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 11U, 0,
      (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance, 5U,
      11U, 0U, (real_T)chartInstance->c1_old_is, 0.0, 0, 5U,
      chartInstance->c1_old_is >= 0) != 0U);
    if (c1_d_temp) {
      c1_d_temp = covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 11U, 1,
        (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance,
        5U, 11U, 1U, (real_T)*chartInstance->c1_is_adc, 0.0, 0, 2U,
        *chartInstance->c1_is_adc < 0) != 0U);
    }

    c1_e_temp = c1_d_temp;
    if (c1_e_temp) {
      c1_e_temp = covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 11U, 2,
        (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance,
        5U, 11U, 2U, (real_T)
        chartInstance->c1_FirstCurrentZeroCrossingFlagPositiveToNegative, 0.0, 0,
        0U, chartInstance->c1_FirstCurrentZeroCrossingFlagPositiveToNegative ==
        0) != 0U);
    }

    c1_f_temp = c1_e_temp;
    if (c1_f_temp) {
      c1_f_temp = covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 11U, 3,
        (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance,
        5U, 11U, 3U, (real_T)*chartInstance->c1_bemf_vf, 1.0, 0, 0U, (int32_T)
        *chartInstance->c1_bemf_vf) != 0U);
    }

    c1_c_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance, 5U,
      11U, 0U, c1_f_temp != 0U);
    if (c1_c_out) {
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 11U, chartInstance->c1_sfEvent);
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 10U, chartInstance->c1_sfEvent);
      chartInstance->c1_FirstCurrentZeroCrossingFlagPositiveToNegative = 1U;
      _SFD_DATA_RANGE_CHECK((real_T)
                            chartInstance->c1_FirstCurrentZeroCrossingFlagPositiveToNegative,
                            4U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 4U, (real_T)
                        chartInstance->c1_FirstCurrentZeroCrossingFlagPositiveToNegative);
      chartInstance->c1_CurrentZeroCrossingFlagPositiveToNegativeCounter = 0U;
      _SFD_DATA_RANGE_CHECK((real_T)
                            chartInstance->c1_CurrentZeroCrossingFlagPositiveToNegativeCounter,
                            2U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 2U, (real_T)
                        chartInstance->c1_CurrentZeroCrossingFlagPositiveToNegativeCounter);
      chartInstance->c1_MaskFlag = 1U;
      _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_MaskFlag, 5U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 5U, (real_T)
                        chartInstance->c1_MaskFlag);
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 9U, chartInstance->c1_sfEvent);
    } else {
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 8U, chartInstance->c1_sfEvent);
    }

    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 13U, chartInstance->c1_sfEvent);
    _SFD_SYMBOL_SCOPE_POP();
    _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 28U, chartInstance->c1_sfEvent);
    _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 28U, chartInstance->c1_sfEvent);
    c1_NegToPos_control(chartInstance);
    c1_PosToNeg_control(chartInstance);
    *chartInstance->c1_old_InvVoltageOut = *chartInstance->c1_InvVoltageOut;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_old_InvVoltageOut, 64U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 64U, (real_T)
                      *chartInstance->c1_old_InvVoltageOut);
    chartInstance->c1_old_is = *chartInstance->c1_is_adc;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_old_is, 22U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 22U, (real_T)
                      chartInstance->c1_old_is);
  }

  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 8U, chartInstance->c1_sfEvent);
}

static void init_script_number_translation(uint32_T c1_machineNumber, uint32_T
  c1_chartNumber, uint32_T c1_instanceNumber)
{
  (void)(c1_machineNumber);
  (void)(c1_chartNumber);
  (void)(c1_instanceNumber);
}

const mxArray *sf_c1_Motor_1pHController_get_eml_resolved_functions_info(void)
{
  const mxArray *c1_nameCaptureInfo = NULL;
  c1_nameCaptureInfo = NULL;
  sf_mex_assign(&c1_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c1_nameCaptureInfo;
}

static const mxArray *c1_sfAfterOrElapsed(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance)
{
  const mxArray *c1_b;
  real_T c1_d14;
  real_T c1_d15;
  c1_b = NULL;
  c1_b = NULL;
  if (chartInstance->c1_is_Motor_On == c1_IN_Alignment_Mode) {
    c1_d15 = 0.000125 * (real_T)chartInstance->c1_temporalCounter_i1;
    sf_mex_assign(&c1_b, sf_mex_create("unnamed temp", &c1_d15, 0, 0U, 0U, 0U, 0),
                  false);
  } else {
    c1_d14 = -1.0;
    sf_mex_assign(&c1_b, sf_mex_create("unnamed temp", &c1_d14, 0, 0U, 0U, 0U, 0),
                  false);
  }

  return c1_b;
}

static const mxArray *c1_b_sfAfterOrElapsed
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  const mxArray *c1_b;
  real_T c1_d16;
  real_T c1_d17;
  c1_b = NULL;
  c1_b = NULL;
  if (chartInstance->c1_is_Motor_On == c1_IN_Alignment_Done) {
    c1_d17 = 0.000125 * (real_T)chartInstance->c1_temporalCounter_i1;
    sf_mex_assign(&c1_b, sf_mex_create("unnamed temp", &c1_d17, 0, 0U, 0U, 0U, 0),
                  false);
  } else {
    c1_d16 = -1.0;
    sf_mex_assign(&c1_b, sf_mex_create("unnamed temp", &c1_d16, 0, 0U, 0U, 0U, 0),
                  false);
  }

  return c1_b;
}

static void c1_Miele(SFc1_Motor_1pHControllerInstanceStruct *chartInstance,
                     int32_T c1_c_Power, int16_T c1_d_Speed, int32_T
                     c1_e_Counter, boolean_T *c1_c_Slurp_Detected, int32_T
                     *c1_e_Counter_output)
{
  _SFD_SET_DATA_VALUE_PTR(141U, c1_e_Counter_output);
  _SFD_SET_DATA_VALUE_PTR(120U, c1_c_Slurp_Detected);
  _SFD_SET_DATA_VALUE_PTR(102U, &c1_e_Counter);
  _SFD_SET_DATA_VALUE_PTR(94U, &c1_d_Speed);
  _SFD_SET_DATA_VALUE_PTR(81U, &c1_c_Power);
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 20U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(5U, 0U);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Power", &c1_c_Power, c1_sf_marshallOut,
    c1_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Speed", &c1_d_Speed, c1_c_sf_marshallOut,
    c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Counter", &c1_e_Counter, c1_sf_marshallOut,
    c1_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Slurp_Detected", c1_c_Slurp_Detected,
    c1_f_sf_marshallOut, c1_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Counter_output", c1_e_Counter_output,
    c1_sf_marshallOut, c1_sf_marshallIn);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 20U, chartInstance->c1_sfEvent);
  *chartInstance->c1_Power = c1_c_Power;
  *chartInstance->c1_Speed = c1_d_Speed;
  *chartInstance->c1_Counter = c1_e_Counter;
  sf_call_output_fcn_call(chartInstance->S, 1, "Miele", 0);
  *c1_c_Slurp_Detected = *chartInstance->c1_Slurp_Detected;
  *c1_e_Counter_output = *chartInstance->c1_Counter_output;
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 20U, chartInstance->c1_sfEvent);
  _SFD_UNSET_DATA_VALUE_PTR(141U);
  _SFD_UNSET_DATA_VALUE_PTR(120U);
  _SFD_UNSET_DATA_VALUE_PTR(102U);
  _SFD_UNSET_DATA_VALUE_PTR(94U);
  _SFD_UNSET_DATA_VALUE_PTR(81U);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 20U, chartInstance->c1_sfEvent);
}

static void c1_Appcon(SFc1_Motor_1pHControllerInstanceStruct *chartInstance,
                      int16_T c1_b_Bemf_Sample, int32_T c1_e_Counter, int16_T
                      c1_b_Activate, int16_T c1_b_Activation_Counter, int16_T
                      c1_d_Speed, int16_T c1_b_Speed_Filtered, boolean_T
                      *c1_c_Slurp_Detected, int32_T *c1_e_Counter_output,
                      int16_T *c1_b_Activation_Counter_output)
{
  _SFD_SET_DATA_VALUE_PTR(144U, c1_b_Activation_Counter_output);
  _SFD_SET_DATA_VALUE_PTR(136U, c1_e_Counter_output);
  _SFD_SET_DATA_VALUE_PTR(128U, c1_c_Slurp_Detected);
  _SFD_SET_DATA_VALUE_PTR(115U, &c1_b_Speed_Filtered);
  _SFD_SET_DATA_VALUE_PTR(113U, &c1_d_Speed);
  _SFD_SET_DATA_VALUE_PTR(109U, &c1_b_Activation_Counter);
  _SFD_SET_DATA_VALUE_PTR(104U, &c1_b_Activate);
  _SFD_SET_DATA_VALUE_PTR(89U, &c1_e_Counter);
  _SFD_SET_DATA_VALUE_PTR(71U, &c1_b_Bemf_Sample);
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 19U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(9U, 0U);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Bemf_Sample", &c1_b_Bemf_Sample,
    c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Counter", &c1_e_Counter, c1_sf_marshallOut,
    c1_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Activate", &c1_b_Activate,
    c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Activation_Counter",
    &c1_b_Activation_Counter, c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Speed", &c1_d_Speed, c1_c_sf_marshallOut,
    c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Speed_Filtered", &c1_b_Speed_Filtered,
    c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Slurp_Detected", c1_c_Slurp_Detected,
    c1_f_sf_marshallOut, c1_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Counter_output", c1_e_Counter_output,
    c1_sf_marshallOut, c1_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Activation_Counter_output",
    c1_b_Activation_Counter_output, c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 19U, chartInstance->c1_sfEvent);
  *chartInstance->c1_Bemf_Sample = c1_b_Bemf_Sample;
  *chartInstance->c1_b_Counter = c1_e_Counter;
  *chartInstance->c1_Activate = c1_b_Activate;
  *chartInstance->c1_Activation_Counter = c1_b_Activation_Counter;
  *chartInstance->c1_b_Speed = c1_d_Speed;
  *chartInstance->c1_Speed_Filtered = c1_b_Speed_Filtered;
  sf_call_output_fcn_call(chartInstance->S, 2, "Appcon", 0);
  *c1_c_Slurp_Detected = *chartInstance->c1_b_Slurp_Detected;
  *c1_e_Counter_output = *chartInstance->c1_b_Counter_output;
  *c1_b_Activation_Counter_output = *chartInstance->c1_Activation_Counter_output;
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 19U, chartInstance->c1_sfEvent);
  _SFD_UNSET_DATA_VALUE_PTR(144U);
  _SFD_UNSET_DATA_VALUE_PTR(136U);
  _SFD_UNSET_DATA_VALUE_PTR(128U);
  _SFD_UNSET_DATA_VALUE_PTR(115U);
  _SFD_UNSET_DATA_VALUE_PTR(113U);
  _SFD_UNSET_DATA_VALUE_PTR(109U);
  _SFD_UNSET_DATA_VALUE_PTR(104U);
  _SFD_UNSET_DATA_VALUE_PTR(89U);
  _SFD_UNSET_DATA_VALUE_PTR(71U);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 19U, chartInstance->c1_sfEvent);
}

static void c1_Speed_Observation(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, int16_T c1_d_Speed, int16_T c1_b_Speed_reference, int32_T
  c1_e_Counter, boolean_T *c1_c_Speed_control_fault, int32_T
  *c1_e_Counter_output)
{
  _SFD_SET_DATA_VALUE_PTR(135U, c1_e_Counter_output);
  _SFD_SET_DATA_VALUE_PTR(125U, c1_c_Speed_control_fault);
  _SFD_SET_DATA_VALUE_PTR(105U, &c1_e_Counter);
  _SFD_SET_DATA_VALUE_PTR(96U, &c1_b_Speed_reference);
  _SFD_SET_DATA_VALUE_PTR(76U, &c1_d_Speed);
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 23U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(5U, 0U);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Speed", &c1_d_Speed, c1_c_sf_marshallOut,
    c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Speed_reference", &c1_b_Speed_reference,
    c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Counter", &c1_e_Counter, c1_sf_marshallOut,
    c1_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Speed_control_fault",
    c1_c_Speed_control_fault, c1_f_sf_marshallOut, c1_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Counter_output", c1_e_Counter_output,
    c1_sf_marshallOut, c1_sf_marshallIn);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 23U, chartInstance->c1_sfEvent);
  *chartInstance->c1_c_Speed = c1_d_Speed;
  *chartInstance->c1_Speed_reference = c1_b_Speed_reference;
  *chartInstance->c1_c_Counter = c1_e_Counter;
  sf_call_output_fcn_call(chartInstance->S, 3, "Speed_Observation", 0);
  *c1_c_Speed_control_fault = *chartInstance->c1_b_Speed_control_fault;
  *c1_e_Counter_output = *chartInstance->c1_c_Counter_output;
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 23U, chartInstance->c1_sfEvent);
  _SFD_UNSET_DATA_VALUE_PTR(135U);
  _SFD_UNSET_DATA_VALUE_PTR(125U);
  _SFD_UNSET_DATA_VALUE_PTR(105U);
  _SFD_UNSET_DATA_VALUE_PTR(96U);
  _SFD_UNSET_DATA_VALUE_PTR(76U);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 23U, chartInstance->c1_sfEvent);
}

static c1_ControlFlag c1_VFDone_Flag(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, int16_T c1_e_speed_reference, boolean_T c1_c_start_ctr)
{
  c1_ControlFlag c1_c_control_flag;
  _SFD_SET_DATA_VALUE_PTR(123U, &c1_c_control_flag);
  _SFD_SET_DATA_VALUE_PTR(98U, &c1_c_start_ctr);
  _SFD_SET_DATA_VALUE_PTR(68U, &c1_e_speed_reference);
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 25U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(3U, 0U);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("speed_reference", &c1_e_speed_reference,
    c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("start_ctr", &c1_c_start_ctr,
    c1_f_sf_marshallOut, c1_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("control_flag", &c1_c_control_flag,
    c1_d_sf_marshallOut, c1_d_sf_marshallIn);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 25U, chartInstance->c1_sfEvent);
  *chartInstance->c1_c_speed_reference = c1_e_speed_reference;
  *chartInstance->c1_b_start_ctr = c1_c_start_ctr;
  sf_call_output_fcn_call(chartInstance->S, 4, "VFDone_Flag", 0);
  c1_c_control_flag = *chartInstance->c1_control_flag;
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 25U, chartInstance->c1_sfEvent);
  _SFD_UNSET_DATA_VALUE_PTR(123U);
  _SFD_UNSET_DATA_VALUE_PTR(98U);
  _SFD_UNSET_DATA_VALUE_PTR(68U);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 25U, chartInstance->c1_sfEvent);
  return c1_c_control_flag;
}

static void c1_CheckforSynchronisation(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, int16_T c1_c_bemf, int32_T c1_b_Time_State, boolean_T
  *c1_b_Synchronisation_enabled, int32_T *c1_b_Time_state_output)
{
  _SFD_SET_DATA_VALUE_PTR(142U, c1_b_Time_state_output);
  _SFD_SET_DATA_VALUE_PTR(121U, c1_b_Synchronisation_enabled);
  _SFD_SET_DATA_VALUE_PTR(88U, &c1_b_Time_State);
  _SFD_SET_DATA_VALUE_PTR(74U, &c1_c_bemf);
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(4U, 0U);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("bemf", &c1_c_bemf, c1_c_sf_marshallOut,
    c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Time_State", &c1_b_Time_State,
    c1_sf_marshallOut, c1_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Synchronisation_enabled",
    c1_b_Synchronisation_enabled, c1_f_sf_marshallOut, c1_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Time_state_output", c1_b_Time_state_output,
    c1_sf_marshallOut, c1_sf_marshallIn);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c1_sfEvent);
  *chartInstance->c1_bemf = c1_c_bemf;
  *chartInstance->c1_Time_State = c1_b_Time_State;
  sf_call_output_fcn_call(chartInstance->S, 5, "CheckforSynchronisation", 0);
  *c1_b_Synchronisation_enabled = *chartInstance->c1_Synchronisation_enabled;
  *c1_b_Time_state_output = *chartInstance->c1_Time_state_output;
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
  _SFD_UNSET_DATA_VALUE_PTR(142U);
  _SFD_UNSET_DATA_VALUE_PTR(121U);
  _SFD_UNSET_DATA_VALUE_PTR(88U);
  _SFD_UNSET_DATA_VALUE_PTR(74U);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c1_sfEvent);
}

static void c1_Bemf_Observation(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, int16_T c1_c_bemf, int16_T c1_c_bemf_reference, int32_T
  c1_e_Counter, boolean_T *c1_c_Bemf_control_fault, int32_T *c1_e_Counter_output)
{
  _SFD_SET_DATA_VALUE_PTR(138U, c1_e_Counter_output);
  _SFD_SET_DATA_VALUE_PTR(129U, c1_c_Bemf_control_fault);
  _SFD_SET_DATA_VALUE_PTR(101U, &c1_e_Counter);
  _SFD_SET_DATA_VALUE_PTR(92U, &c1_c_bemf_reference);
  _SFD_SET_DATA_VALUE_PTR(72U, &c1_c_bemf);
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(5U, 0U);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("bemf", &c1_c_bemf, c1_c_sf_marshallOut,
    c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("bemf_reference", &c1_c_bemf_reference,
    c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Counter", &c1_e_Counter, c1_sf_marshallOut,
    c1_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Bemf_control_fault", c1_c_Bemf_control_fault,
    c1_f_sf_marshallOut, c1_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Counter_output", c1_e_Counter_output,
    c1_sf_marshallOut, c1_sf_marshallIn);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
  *chartInstance->c1_b_bemf = c1_c_bemf;
  *chartInstance->c1_b_bemf_reference = c1_c_bemf_reference;
  *chartInstance->c1_d_Counter = c1_e_Counter;
  sf_call_output_fcn_call(chartInstance->S, 6, "Bemf_Observation", 0);
  *c1_c_Bemf_control_fault = *chartInstance->c1_b_Bemf_control_fault;
  *c1_e_Counter_output = *chartInstance->c1_d_Counter_output;
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
  _SFD_UNSET_DATA_VALUE_PTR(138U);
  _SFD_UNSET_DATA_VALUE_PTR(129U);
  _SFD_UNSET_DATA_VALUE_PTR(101U);
  _SFD_UNSET_DATA_VALUE_PTR(92U);
  _SFD_UNSET_DATA_VALUE_PTR(72U);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
}

static boolean_T c1_b_fault(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, boolean_T c1_c_fault1, boolean_T c1_c_fault2)
{
  boolean_T c1_d_fault;
  _SFD_SET_DATA_VALUE_PTR(132U, &c1_d_fault);
  _SFD_SET_DATA_VALUE_PTR(91U, &c1_c_fault2);
  _SFD_SET_DATA_VALUE_PTR(73U, &c1_c_fault1);
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 31U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(3U, 0U);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("fault1", &c1_c_fault1, c1_f_sf_marshallOut,
    c1_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("fault2", &c1_c_fault2, c1_f_sf_marshallOut,
    c1_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("fault", &c1_d_fault, c1_f_sf_marshallOut,
    c1_f_sf_marshallIn);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 31U, chartInstance->c1_sfEvent);
  *chartInstance->c1_b_fault1 = c1_c_fault1;
  *chartInstance->c1_b_fault2 = c1_c_fault2;
  sf_call_output_fcn_call(chartInstance->S, 7, "fault", 0);
  c1_d_fault = *chartInstance->c1_c_fault;
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 31U, chartInstance->c1_sfEvent);
  _SFD_UNSET_DATA_VALUE_PTR(132U);
  _SFD_UNSET_DATA_VALUE_PTR(91U);
  _SFD_UNSET_DATA_VALUE_PTR(73U);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 31U, chartInstance->c1_sfEvent);
  return c1_d_fault;
}

static c1_ControlFlag c1_Protections(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, boolean_T c1_c_fault_clear)
{
  c1_ControlFlag c1_c_control_flag;
  _SFD_SET_DATA_VALUE_PTR(126U, &c1_c_control_flag);
  _SFD_SET_DATA_VALUE_PTR(82U, &c1_c_fault_clear);
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 30U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(2U, 0U);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("fault_clear", &c1_c_fault_clear,
    c1_f_sf_marshallOut, c1_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("control_flag", &c1_c_control_flag,
    c1_d_sf_marshallOut, c1_d_sf_marshallIn);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 30U, chartInstance->c1_sfEvent);
  *chartInstance->c1_b_fault_clear = c1_c_fault_clear;
  sf_call_output_fcn_call(chartInstance->S, 8, "Protections", 0);
  c1_c_control_flag = *chartInstance->c1_b_control_flag;
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 30U, chartInstance->c1_sfEvent);
  _SFD_UNSET_DATA_VALUE_PTR(126U);
  _SFD_UNSET_DATA_VALUE_PTR(82U);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 30U, chartInstance->c1_sfEvent);
  return c1_c_control_flag;
}

static void c1_state_variable_init(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, int16_T *c1_c_OL_voltage_state_is, int16_T
  *c1_c_OL_voltage_state_bemf, int16_T *c1_c_OL_speed_state, int16_T
  *c1_c_OLvoltage_sat, int16_T *c1_c_OLspeed_sat)
{
  _SFD_SET_DATA_VALUE_PTR(147U, c1_c_OLspeed_sat);
  _SFD_SET_DATA_VALUE_PTR(146U, c1_c_OLvoltage_sat);
  _SFD_SET_DATA_VALUE_PTR(143U, c1_c_OL_speed_state);
  _SFD_SET_DATA_VALUE_PTR(140U, c1_c_OL_voltage_state_bemf);
  _SFD_SET_DATA_VALUE_PTR(130U, c1_c_OL_voltage_state_is);
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 33U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(5U, 0U);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("OL_voltage_state_is",
    c1_c_OL_voltage_state_is, c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("OL_voltage_state_bemf",
    c1_c_OL_voltage_state_bemf, c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("OL_speed_state", c1_c_OL_speed_state,
    c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("OLvoltage_sat", c1_c_OLvoltage_sat,
    c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("OLspeed_sat", c1_c_OLspeed_sat,
    c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 33U, chartInstance->c1_sfEvent);
  sf_call_output_fcn_call(chartInstance->S, 9, "state_variable_init", 0);
  *c1_c_OL_voltage_state_is = *chartInstance->c1_b_OL_voltage_state_is;
  *c1_c_OL_voltage_state_bemf = *chartInstance->c1_b_OL_voltage_state_bemf;
  *c1_c_OL_speed_state = *chartInstance->c1_b_OL_speed_state;
  *c1_c_OLvoltage_sat = *chartInstance->c1_b_OLvoltage_sat;
  *c1_c_OLspeed_sat = *chartInstance->c1_b_OLspeed_sat;
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 33U, chartInstance->c1_sfEvent);
  _SFD_UNSET_DATA_VALUE_PTR(147U);
  _SFD_UNSET_DATA_VALUE_PTR(146U);
  _SFD_UNSET_DATA_VALUE_PTR(143U);
  _SFD_UNSET_DATA_VALUE_PTR(140U);
  _SFD_UNSET_DATA_VALUE_PTR(130U);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 33U, chartInstance->c1_sfEvent);
}

static int32_T c1_PowerCalculation(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, int16_T c1_b_Current, int16_T c1_b_InverterVoltage, real_T
  c1_b_Reset)
{
  int32_T c1_c_Power;
  _SFD_SET_DATA_VALUE_PTR(131U, &c1_c_Power);
  _SFD_SET_DATA_VALUE_PTR(100U, &c1_b_Reset);
  _SFD_SET_DATA_VALUE_PTR(85U, &c1_b_InverterVoltage);
  _SFD_SET_DATA_VALUE_PTR(83U, &c1_b_Current);
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 17U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(4U, 0U);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Current", &c1_b_Current, c1_c_sf_marshallOut,
    c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("InverterVoltage", &c1_b_InverterVoltage,
    c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Reset", &c1_b_Reset, c1_j_sf_marshallOut,
    c1_i_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Power", &c1_c_Power, c1_sf_marshallOut,
    c1_sf_marshallIn);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 17U, chartInstance->c1_sfEvent);
  *chartInstance->c1_Current = c1_b_Current;
  *chartInstance->c1_InverterVoltage = c1_b_InverterVoltage;
  *chartInstance->c1_Reset = c1_b_Reset;
  sf_call_output_fcn_call(chartInstance->S, 10, "PowerCalculation", 0);
  c1_c_Power = *chartInstance->c1_b_Power;
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 17U, chartInstance->c1_sfEvent);
  _SFD_UNSET_DATA_VALUE_PTR(131U);
  _SFD_UNSET_DATA_VALUE_PTR(100U);
  _SFD_UNSET_DATA_VALUE_PTR(85U);
  _SFD_UNSET_DATA_VALUE_PTR(83U);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 17U, chartInstance->c1_sfEvent);
  return c1_c_Power;
}

static void c1_SpeedRateLimit(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, int16_T c1_b_Reference, int32_T c1_b_State_variable_in,
  int16_T *c1_b_ReferenceOut, int32_T *c1_b_State_variable_out)
{
  _SFD_SET_DATA_VALUE_PTR(137U, c1_b_State_variable_out);
  _SFD_SET_DATA_VALUE_PTR(117U, c1_b_ReferenceOut);
  _SFD_SET_DATA_VALUE_PTR(93U, &c1_b_State_variable_in);
  _SFD_SET_DATA_VALUE_PTR(69U, &c1_b_Reference);
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 21U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(4U, 0U);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Reference", &c1_b_Reference,
    c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("State_variable_in", &c1_b_State_variable_in,
    c1_sf_marshallOut, c1_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("ReferenceOut", c1_b_ReferenceOut,
    c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("State_variable_out", c1_b_State_variable_out,
    c1_sf_marshallOut, c1_sf_marshallIn);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 21U, chartInstance->c1_sfEvent);
  *chartInstance->c1_Reference = c1_b_Reference;
  *chartInstance->c1_State_variable_in = c1_b_State_variable_in;
  sf_call_output_fcn_call(chartInstance->S, 11, "SpeedRateLimit", 0);
  *c1_b_ReferenceOut = *chartInstance->c1_ReferenceOut;
  *c1_b_State_variable_out = *chartInstance->c1_State_variable_out;
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 21U, chartInstance->c1_sfEvent);
  _SFD_UNSET_DATA_VALUE_PTR(137U);
  _SFD_UNSET_DATA_VALUE_PTR(117U);
  _SFD_UNSET_DATA_VALUE_PTR(93U);
  _SFD_UNSET_DATA_VALUE_PTR(69U);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 21U, chartInstance->c1_sfEvent);
}

static int16_T c1_SpeedToAngle(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, int16_T c1_e_speed_reference, int16_T c1_b_angle_reference)
{
  int16_T c1_d_rotor_position_reference;
  _SFD_SET_DATA_VALUE_PTR(124U, &c1_d_rotor_position_reference);
  _SFD_SET_DATA_VALUE_PTR(90U, &c1_b_angle_reference);
  _SFD_SET_DATA_VALUE_PTR(80U, &c1_e_speed_reference);
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 22U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(3U, 0U);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("speed_reference", &c1_e_speed_reference,
    c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("angle_reference", &c1_b_angle_reference,
    c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("rotor_position_reference",
    &c1_d_rotor_position_reference, c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 22U, chartInstance->c1_sfEvent);
  *chartInstance->c1_d_speed_reference = c1_e_speed_reference;
  *chartInstance->c1_angle_reference = c1_b_angle_reference;
  sf_call_output_fcn_call(chartInstance->S, 13, "SpeedToAngle", 0);
  c1_d_rotor_position_reference = *chartInstance->c1_c_rotor_position_reference;
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 22U, chartInstance->c1_sfEvent);
  _SFD_UNSET_DATA_VALUE_PTR(124U);
  _SFD_UNSET_DATA_VALUE_PTR(90U);
  _SFD_UNSET_DATA_VALUE_PTR(80U);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 22U, chartInstance->c1_sfEvent);
  return c1_d_rotor_position_reference;
}

static void c1_PI_Controller(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, int16_T c1_b_soll, int16_T c1_b_ist, int16_T c1_b_kp, int16_T
  c1_b_ki, int16_T c1_b_I_state_in, int16_T c1_b_limit_low, int16_T
  c1_b_limit_high, int16_T *c1_b_output, int16_T *c1_b_I_state_out)
{
  _SFD_SET_DATA_VALUE_PTR(139U, c1_b_I_state_out);
  _SFD_SET_DATA_VALUE_PTR(122U, c1_b_output);
  _SFD_SET_DATA_VALUE_PTR(116U, &c1_b_limit_high);
  _SFD_SET_DATA_VALUE_PTR(114U, &c1_b_limit_low);
  _SFD_SET_DATA_VALUE_PTR(112U, &c1_b_I_state_in);
  _SFD_SET_DATA_VALUE_PTR(110U, &c1_b_ki);
  _SFD_SET_DATA_VALUE_PTR(106U, &c1_b_kp);
  _SFD_SET_DATA_VALUE_PTR(97U, &c1_b_ist);
  _SFD_SET_DATA_VALUE_PTR(79U, &c1_b_soll);
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 14U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(9U, 0U);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("soll", &c1_b_soll, c1_c_sf_marshallOut,
    c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("ist", &c1_b_ist, c1_c_sf_marshallOut,
    c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("kp", &c1_b_kp, c1_c_sf_marshallOut,
    c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("ki", &c1_b_ki, c1_c_sf_marshallOut,
    c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("I_state_in", &c1_b_I_state_in,
    c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("limit_low", &c1_b_limit_low,
    c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("limit_high", &c1_b_limit_high,
    c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("output", c1_b_output, c1_c_sf_marshallOut,
    c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("I_state_out", c1_b_I_state_out,
    c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 14U, chartInstance->c1_sfEvent);
  *chartInstance->c1_soll = c1_b_soll;
  *chartInstance->c1_ist = c1_b_ist;
  *chartInstance->c1_kp = c1_b_kp;
  *chartInstance->c1_ki = c1_b_ki;
  *chartInstance->c1_I_state_in = c1_b_I_state_in;
  *chartInstance->c1_limit_low = c1_b_limit_low;
  *chartInstance->c1_limit_high = c1_b_limit_high;
  sf_call_output_fcn_call(chartInstance->S, 14, "PI_Controller", 0);
  *c1_b_output = *chartInstance->c1_output;
  *c1_b_I_state_out = *chartInstance->c1_I_state_out;
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 14U, chartInstance->c1_sfEvent);
  _SFD_UNSET_DATA_VALUE_PTR(139U);
  _SFD_UNSET_DATA_VALUE_PTR(122U);
  _SFD_UNSET_DATA_VALUE_PTR(116U);
  _SFD_UNSET_DATA_VALUE_PTR(114U);
  _SFD_UNSET_DATA_VALUE_PTR(112U);
  _SFD_UNSET_DATA_VALUE_PTR(110U);
  _SFD_UNSET_DATA_VALUE_PTR(106U);
  _SFD_UNSET_DATA_VALUE_PTR(97U);
  _SFD_UNSET_DATA_VALUE_PTR(79U);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 14U, chartInstance->c1_sfEvent);
}

static void c1_modulation(SFc1_Motor_1pHControllerInstanceStruct *chartInstance,
  int16_T c1_d_voltage_reference, int16_T c1_b_rotor_angle, int16_T
  c1_c_voltage_dc, uint16_T *c1_d_PWM_CMD1, uint16_T *c1_d_PWM_CMD2, int16_T
  *c1_b_InverterOutputVoltage)
{
  _SFD_SET_DATA_VALUE_PTR(145U, c1_b_InverterOutputVoltage);
  _SFD_SET_DATA_VALUE_PTR(134U, c1_d_PWM_CMD2);
  _SFD_SET_DATA_VALUE_PTR(133U, c1_d_PWM_CMD1);
  _SFD_SET_DATA_VALUE_PTR(103U, &c1_c_voltage_dc);
  _SFD_SET_DATA_VALUE_PTR(86U, &c1_b_rotor_angle);
  _SFD_SET_DATA_VALUE_PTR(78U, &c1_d_voltage_reference);
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 32U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(6U, 0U);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("voltage_reference", &c1_d_voltage_reference,
    c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("rotor_angle", &c1_b_rotor_angle,
    c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("voltage_dc", &c1_c_voltage_dc,
    c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("PWM_CMD1", c1_d_PWM_CMD1,
    c1_h_sf_marshallOut, c1_g_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("PWM_CMD2", c1_d_PWM_CMD2,
    c1_h_sf_marshallOut, c1_g_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("InverterOutputVoltage",
    c1_b_InverterOutputVoltage, c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 32U, chartInstance->c1_sfEvent);
  *chartInstance->c1_c_voltage_reference = c1_d_voltage_reference;
  *chartInstance->c1_rotor_angle = c1_b_rotor_angle;
  *chartInstance->c1_voltage_dc = c1_c_voltage_dc;
  sf_call_output_fcn_call(chartInstance->S, 15, "modulation", 0);
  *c1_d_PWM_CMD1 = *chartInstance->c1_b_PWM_CMD1;
  *c1_d_PWM_CMD2 = *chartInstance->c1_b_PWM_CMD2;
  *c1_b_InverterOutputVoltage = *chartInstance->c1_InverterOutputVoltage;
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 32U, chartInstance->c1_sfEvent);
  _SFD_UNSET_DATA_VALUE_PTR(145U);
  _SFD_UNSET_DATA_VALUE_PTR(134U);
  _SFD_UNSET_DATA_VALUE_PTR(133U);
  _SFD_UNSET_DATA_VALUE_PTR(103U);
  _SFD_UNSET_DATA_VALUE_PTR(86U);
  _SFD_UNSET_DATA_VALUE_PTR(78U);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 32U, chartInstance->c1_sfEvent);
}

static int16_T c1_Duty_Cycle_To_Voltage(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, uint16_T c1_d_PWM_CMD1, uint16_T c1_d_PWM_CMD2, int16_T
  c1_c_voltage_dc)
{
  int16_T c1_b_Vs;
  _SFD_SET_DATA_VALUE_PTR(127U, &c1_b_Vs);
  _SFD_SET_DATA_VALUE_PTR(99U, &c1_c_voltage_dc);
  _SFD_SET_DATA_VALUE_PTR(87U, &c1_d_PWM_CMD2);
  _SFD_SET_DATA_VALUE_PTR(70U, &c1_d_PWM_CMD1);
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 3U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(4U, 0U);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("PWM_CMD1", &c1_d_PWM_CMD1,
    c1_h_sf_marshallOut, c1_g_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("PWM_CMD2", &c1_d_PWM_CMD2,
    c1_h_sf_marshallOut, c1_g_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("voltage_dc", &c1_c_voltage_dc,
    c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("Vs", &c1_b_Vs, c1_c_sf_marshallOut,
    c1_c_sf_marshallIn);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 3U, chartInstance->c1_sfEvent);
  *chartInstance->c1_c_PWM_CMD1 = c1_d_PWM_CMD1;
  *chartInstance->c1_c_PWM_CMD2 = c1_d_PWM_CMD2;
  *chartInstance->c1_b_voltage_dc = c1_c_voltage_dc;
  sf_call_output_fcn_call(chartInstance->S, 16, "Duty_Cycle_To_Voltage", 0);
  c1_b_Vs = *chartInstance->c1_Vs;
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 3U, chartInstance->c1_sfEvent);
  _SFD_UNSET_DATA_VALUE_PTR(127U);
  _SFD_UNSET_DATA_VALUE_PTR(99U);
  _SFD_UNSET_DATA_VALUE_PTR(87U);
  _SFD_UNSET_DATA_VALUE_PTR(70U);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U, chartInstance->c1_sfEvent);
  return c1_b_Vs;
}

static void c1_OpenClosedHBridge(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance)
{
  boolean_T c1_out;
  uint16_T c1_d_PWM_CMD1;
  uint16_T c1_d_PWM_CMD2;
  int16_T c1_b_InverterOutputVoltage;
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 13U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 13U, chartInstance->c1_sfEvent);
  _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 46U, chartInstance->c1_sfEvent);
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 49U, chartInstance->c1_sfEvent);
  c1_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance, 5U, 49U,
    0U, covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 49U, 0,
    (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance, 5U,
    49U, 0U, (real_T)chartInstance->c1_MaskFlag, 1.0, 0, 0U,
    chartInstance->c1_MaskFlag == 1) != 0U) != 0U);
  if (c1_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 49U, chartInstance->c1_sfEvent);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 50U, chartInstance->c1_sfEvent);
    *chartInstance->c1_PWM_OFF = 1U;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_OFF, 54U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 54U, (real_T)
                      *chartInstance->c1_PWM_OFF);
    *chartInstance->c1_PWM_CMD1 = (uint16_T)((uint32_T)
      chartInstance->c1_PWM_MAX_DUTYCYCLE >> 1);
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD1, 52U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 52U, (real_T)
                      *chartInstance->c1_PWM_CMD1);
    *chartInstance->c1_PWM_CMD2 = (uint16_T)((uint32_T)
      chartInstance->c1_PWM_MAX_DUTYCYCLE >> 1);
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD2, 53U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 53U, (real_T)
                      *chartInstance->c1_PWM_CMD2);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 48U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 47U, chartInstance->c1_sfEvent);
    *chartInstance->c1_PWM_OFF = 0U;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_OFF, 54U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 54U, (real_T)
                      *chartInstance->c1_PWM_OFF);
    c1_modulation(chartInstance, *chartInstance->c1_voltage_ref,
                  chartInstance->c1_rotor_position_reference,
                  *chartInstance->c1_vdc, &c1_d_PWM_CMD1, &c1_d_PWM_CMD2,
                  &c1_b_InverterOutputVoltage);
    *chartInstance->c1_PWM_CMD1 = c1_d_PWM_CMD1;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD1, 52U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 52U, (real_T)
                      *chartInstance->c1_PWM_CMD1);
    *chartInstance->c1_PWM_CMD2 = c1_d_PWM_CMD2;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD2, 53U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 53U, (real_T)
                      *chartInstance->c1_PWM_CMD2);
    *chartInstance->c1_InvVoltageOut = c1_b_InverterOutputVoltage;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_InvVoltageOut, 62U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 62U, (real_T)
                      *chartInstance->c1_InvVoltageOut);
  }

  _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 51U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 13U, chartInstance->c1_sfEvent);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 13U, chartInstance->c1_sfEvent);
}

static void c1_PosToNeg_control(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance)
{
  boolean_T c1_temp;
  boolean_T c1_out;
  int32_T c1_i42;
  boolean_T c1_covSaturation;
  boolean_T c1_b_out;
  boolean_T c1_c_out;
  int32_T c1_i43;
  boolean_T c1_b_covSaturation;
  int16_T c1_b_output;
  int16_T c1_b_I_state_out;
  int16_T c1_c_output;
  int16_T c1_c_I_state_out;
  boolean_T c1_c_Bemf_control_fault;
  int32_T c1_e_Counter_output;
  boolean_T c1_c_Slurp_Detected;
  int32_T c1_f_Counter_output;
  int16_T c1_b_Activation_Counter_output;
  boolean_T guard1 = false;
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 15U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 15U, chartInstance->c1_sfEvent);
  _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 20U, chartInstance->c1_sfEvent);
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 21U, chartInstance->c1_sfEvent);
  c1_temp = covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 21U, 0,
    (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance, 5U,
    21U, 0U, (real_T)
    chartInstance->c1_FirstCurrentZeroCrossingFlagPositiveToNegative, 1.0, 0, 0U,
    chartInstance->c1_FirstCurrentZeroCrossingFlagPositiveToNegative == 1) != 0U);
  if (c1_temp) {
    c1_temp = covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 21U, 1,
      (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance, 5U,
      21U, 1U, (real_T)*chartInstance->c1_bemf_vf, 1.0, 0, 0U, (int32_T)
      *chartInstance->c1_bemf_vf) != 0U);
  }

  c1_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance, 5U, 21U,
    0U, c1_temp != 0U);
  guard1 = false;
  if (c1_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 21U, chartInstance->c1_sfEvent);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 23U, chartInstance->c1_sfEvent);
    c1_i42 = c1__s32_add__(chartInstance, (int32_T)
      chartInstance->c1_CurrentZeroCrossingFlagPositiveToNegativeCounter, 1, 0,
      964U, 1, 50);
    c1_covSaturation = false;
    if (c1_i42 < 0) {
      c1_covSaturation = true;
      c1_i42 = 0;
      _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 964U, 1U, 50U);
    } else {
      if (c1_i42 > 65535) {
        c1_i42 = 65535;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 964U, 1U, 50U);
      }

      covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 5, 23, 0, 0,
        c1_covSaturation);
    }

    chartInstance->c1_CurrentZeroCrossingFlagPositiveToNegativeCounter =
      (uint16_T)c1_i42;
    _SFD_DATA_RANGE_CHECK((real_T)
                          chartInstance->c1_CurrentZeroCrossingFlagPositiveToNegativeCounter,
                          2U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 2U, (real_T)
                      chartInstance->c1_CurrentZeroCrossingFlagPositiveToNegativeCounter);
    _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 24U,
                 chartInstance->c1_sfEvent);
    c1_b_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance, 5U,
      24U, 0U, covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 24U, 0,
      (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance, 5U,
      24U, 0U, (real_T)
      chartInstance->c1_CurrentZeroCrossingFlagPositiveToNegativeCounter,
      (real_T)chartInstance->c1_delay, 0, 0U,
      chartInstance->c1_CurrentZeroCrossingFlagPositiveToNegativeCounter ==
      chartInstance->c1_delay) != 0U) != 0U);
    if (c1_b_out) {
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 24U, chartInstance->c1_sfEvent);
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 26U, chartInstance->c1_sfEvent);
      c1_i43 = *chartInstance->c1_Vs1 - *chartInstance->c1_Vs2;
      c1_b_covSaturation = false;
      if (c1_i43 > 32767) {
        c1_b_covSaturation = true;
        c1_i43 = 32767;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 967U, 13U, 1U);
      } else {
        if (c1_i43 < -32768) {
          c1_b_covSaturation = true;
          c1_i43 = -32768;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 967U, 13U, 1U);
        }
      }

      covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 5, 26, 0, 0,
        c1_b_covSaturation);
      *chartInstance->c1_bemf_zcr = (int16_T)c1_i43;
      _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_bemf_zcr, 55U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 55U, (real_T)
                        *chartInstance->c1_bemf_zcr);
      c1_PI_Controller(chartInstance, *chartInstance->c1_bemf_reference,
                       *chartInstance->c1_bemf_zcr,
                       *chartInstance->c1_fixedPoint_BemfKp_int16,
                       *chartInstance->c1_fixedPoint_BemfKi_int16,
                       chartInstance->c1_OL_speed_state,
                       chartInstance->c1_fixedPoint_SpeedLimitMin,
                       chartInstance->c1_fixedPoint_SpeedLimitMax, &c1_b_output,
                       &c1_b_I_state_out);
      *chartInstance->c1_speed_pll = c1_b_output;
      _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_speed_pll, 60U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 60U, (real_T)
                        *chartInstance->c1_speed_pll);
      chartInstance->c1_OL_speed_state = c1_b_I_state_out;
      _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OL_speed_state, 6U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 6U, (real_T)
                        chartInstance->c1_OL_speed_state);
      c1_PI_Controller(chartInstance, chartInstance->c1_SpeedReference_PT1,
                       *chartInstance->c1_speed_pll,
                       *chartInstance->c1_fixedPoint_SpeedKp_M_int16,
                       *chartInstance->c1_fixedPoint_SpeedKi_M_int16,
                       chartInstance->c1_OL_voltage_state_bemf,
                       chartInstance->c1_fixedPoint_VoltageLimitMin,
                       *chartInstance->c1_vdc, &c1_c_output, &c1_c_I_state_out);
      *chartInstance->c1_voltage_ref = c1_c_output;
      _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_voltage_ref, 61U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 61U, (real_T)
                        *chartInstance->c1_voltage_ref);
      chartInstance->c1_OL_voltage_state_bemf = c1_c_I_state_out;
      _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OL_voltage_state_bemf, 7U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 7U, (real_T)
                        chartInstance->c1_OL_voltage_state_bemf);
      c1_Bemf_Observation(chartInstance, *chartInstance->c1_bemf_zcr,
                          *chartInstance->c1_bemf_reference,
                          chartInstance->c1_BackEmfObersvationCounterValue,
                          &c1_c_Bemf_control_fault, &c1_e_Counter_output);
      *chartInstance->c1_Bemf_control_fault = c1_c_Bemf_control_fault;
      _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_Bemf_control_fault, 58U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 58U, (real_T)
                        *chartInstance->c1_Bemf_control_fault);
      chartInstance->c1_BackEmfObersvationCounterValue = c1_e_Counter_output;
      _SFD_DATA_RANGE_CHECK((real_T)
                            chartInstance->c1_BackEmfObersvationCounterValue, 0U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 0U, (real_T)
                        chartInstance->c1_BackEmfObersvationCounterValue);
      c1_Appcon(chartInstance, *chartInstance->c1_bemf_zcr,
                chartInstance->c1_SlurpDetectionCounterAppcon, 1,
                chartInstance->c1_SlurpDetection_Appcon_ActivationCounter,
                *chartInstance->c1_speed_reference,
                chartInstance->c1_SpeedReference_PT1, &c1_c_Slurp_Detected,
                &c1_f_Counter_output, &c1_b_Activation_Counter_output);
      *chartInstance->c1_SlurpDetectedAppcon = c1_c_Slurp_Detected;
      _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_SlurpDetectedAppcon, 56U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 56U, (real_T)
                        *chartInstance->c1_SlurpDetectedAppcon);
      chartInstance->c1_SlurpDetectionCounterAppcon = c1_f_Counter_output;
      _SFD_DATA_RANGE_CHECK((real_T)
                            chartInstance->c1_SlurpDetectionCounterAppcon, 12U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 12U, (real_T)
                        chartInstance->c1_SlurpDetectionCounterAppcon);
      chartInstance->c1_SlurpDetection_Appcon_ActivationCounter =
        c1_b_Activation_Counter_output;
      _SFD_DATA_RANGE_CHECK((real_T)
                            chartInstance->c1_SlurpDetection_Appcon_ActivationCounter,
                            14U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 14U, (real_T)
                        chartInstance->c1_SlurpDetection_Appcon_ActivationCounter);
      chartInstance->c1_MaskFlag = 0U;
      _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_MaskFlag, 5U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 5U, (real_T)
                        chartInstance->c1_MaskFlag);
      guard1 = true;
    } else {
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 25U,
                   chartInstance->c1_sfEvent);
      c1_c_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance, 5U,
        25U, 0U, covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 25U, 0,
        (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance,
        5U, 25U, 0U, (real_T)
        chartInstance->c1_CurrentZeroCrossingFlagPositiveToNegativeCounter,
        (real_T)chartInstance->c1_zerocrossing, 0, 0U,
        chartInstance->c1_CurrentZeroCrossingFlagPositiveToNegativeCounter ==
        chartInstance->c1_zerocrossing) != 0U) != 0U);
      if (c1_c_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 25U, chartInstance->c1_sfEvent);
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 30U, chartInstance->c1_sfEvent);
        chartInstance->c1_FirstCurrentZeroCrossingFlagNegativeToPositive = 0U;
        _SFD_DATA_RANGE_CHECK((real_T)
                              chartInstance->c1_FirstCurrentZeroCrossingFlagNegativeToPositive,
                              3U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 3U, (real_T)
                          chartInstance->c1_FirstCurrentZeroCrossingFlagNegativeToPositive);
        guard1 = true;
      } else {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 31U, chartInstance->c1_sfEvent);
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 29U, chartInstance->c1_sfEvent);
      }
    }
  } else {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 22U, chartInstance->c1_sfEvent);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 29U, chartInstance->c1_sfEvent);
  }

  if (guard1) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 32U, chartInstance->c1_sfEvent);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 28U, chartInstance->c1_sfEvent);
  }

  _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 27U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 15U, chartInstance->c1_sfEvent);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 15U, chartInstance->c1_sfEvent);
}

static void c1_NegToPos_control(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance)
{
  boolean_T c1_temp;
  boolean_T c1_out;
  int32_T c1_i44;
  boolean_T c1_covSaturation;
  boolean_T c1_b_out;
  boolean_T c1_c_out;
  int32_T c1_i45;
  boolean_T c1_b_covSaturation;
  int16_T c1_b_output;
  int16_T c1_b_I_state_out;
  int16_T c1_c_output;
  int16_T c1_c_I_state_out;
  boolean_T c1_c_Bemf_control_fault;
  int32_T c1_e_Counter_output;
  boolean_T c1_c_Slurp_Detected;
  int32_T c1_f_Counter_output;
  int16_T c1_b_Activation_Counter_output;
  boolean_T guard1 = false;
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 11U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 11U, chartInstance->c1_sfEvent);
  _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 33U, chartInstance->c1_sfEvent);
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 34U, chartInstance->c1_sfEvent);
  c1_temp = covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 34U, 0,
    (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance, 5U,
    34U, 0U, (real_T)
    chartInstance->c1_FirstCurrentZeroCrossingFlagNegativeToPositive, 1.0, 0, 0U,
    chartInstance->c1_FirstCurrentZeroCrossingFlagNegativeToPositive == 1) != 0U);
  if (c1_temp) {
    c1_temp = covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 34U, 1,
      (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance, 5U,
      34U, 1U, (real_T)*chartInstance->c1_bemf_vf, 1.0, 0, 0U, (int32_T)
      *chartInstance->c1_bemf_vf) != 0U);
  }

  c1_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance, 5U, 34U,
    0U, c1_temp != 0U);
  guard1 = false;
  if (c1_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 34U, chartInstance->c1_sfEvent);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 39U, chartInstance->c1_sfEvent);
    c1_i44 = c1__s32_add__(chartInstance, (int32_T)
      chartInstance->c1_CurrentZeroCrossingFlagNegativeToPositiveCounter, 1, 0,
      1044U, 1, 50);
    c1_covSaturation = false;
    if (c1_i44 < 0) {
      c1_covSaturation = true;
      c1_i44 = 0;
      _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1044U, 1U, 50U);
    } else {
      if (c1_i44 > 65535) {
        c1_i44 = 65535;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1044U, 1U, 50U);
      }

      covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 5, 39, 0, 0,
        c1_covSaturation);
    }

    chartInstance->c1_CurrentZeroCrossingFlagNegativeToPositiveCounter =
      (uint16_T)c1_i44;
    _SFD_DATA_RANGE_CHECK((real_T)
                          chartInstance->c1_CurrentZeroCrossingFlagNegativeToPositiveCounter,
                          1U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 1U, (real_T)
                      chartInstance->c1_CurrentZeroCrossingFlagNegativeToPositiveCounter);
    _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 36U,
                 chartInstance->c1_sfEvent);
    c1_b_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance, 5U,
      36U, 0U, covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 36U, 0,
      (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance, 5U,
      36U, 0U, (real_T)
      chartInstance->c1_CurrentZeroCrossingFlagNegativeToPositiveCounter,
      (real_T)chartInstance->c1_delay, 0, 0U,
      chartInstance->c1_CurrentZeroCrossingFlagNegativeToPositiveCounter ==
      chartInstance->c1_delay) != 0U) != 0U);
    if (c1_b_out) {
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 36U, chartInstance->c1_sfEvent);
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 38U, chartInstance->c1_sfEvent);
      c1_i45 = *chartInstance->c1_Vs2 - *chartInstance->c1_Vs1;
      c1_b_covSaturation = false;
      if (c1_i45 > 32767) {
        c1_b_covSaturation = true;
        c1_i45 = 32767;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1043U, 13U, 1U);
      } else {
        if (c1_i45 < -32768) {
          c1_b_covSaturation = true;
          c1_i45 = -32768;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1043U, 13U, 1U);
        }
      }

      covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 5, 38, 0, 0,
        c1_b_covSaturation);
      *chartInstance->c1_bemf_zcr = (int16_T)c1_i45;
      _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_bemf_zcr, 55U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 55U, (real_T)
                        *chartInstance->c1_bemf_zcr);
      c1_PI_Controller(chartInstance, *chartInstance->c1_bemf_reference,
                       *chartInstance->c1_bemf_zcr,
                       *chartInstance->c1_fixedPoint_BemfKp_int16,
                       *chartInstance->c1_fixedPoint_BemfKi_int16,
                       chartInstance->c1_OL_speed_state,
                       chartInstance->c1_fixedPoint_SpeedLimitMin,
                       chartInstance->c1_fixedPoint_SpeedLimitMax, &c1_b_output,
                       &c1_b_I_state_out);
      *chartInstance->c1_speed_pll = c1_b_output;
      _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_speed_pll, 60U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 60U, (real_T)
                        *chartInstance->c1_speed_pll);
      chartInstance->c1_OL_speed_state = c1_b_I_state_out;
      _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OL_speed_state, 6U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 6U, (real_T)
                        chartInstance->c1_OL_speed_state);
      c1_PI_Controller(chartInstance, chartInstance->c1_SpeedReference_PT1,
                       *chartInstance->c1_speed_pll,
                       *chartInstance->c1_fixedPoint_SpeedKp_M_int16,
                       *chartInstance->c1_fixedPoint_SpeedKi_M_int16,
                       chartInstance->c1_OL_voltage_state_bemf,
                       chartInstance->c1_fixedPoint_VoltageLimitMin,
                       *chartInstance->c1_vdc, &c1_c_output, &c1_c_I_state_out);
      *chartInstance->c1_voltage_ref = c1_c_output;
      _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_voltage_ref, 61U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 61U, (real_T)
                        *chartInstance->c1_voltage_ref);
      chartInstance->c1_OL_voltage_state_bemf = c1_c_I_state_out;
      _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OL_voltage_state_bemf, 7U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 7U, (real_T)
                        chartInstance->c1_OL_voltage_state_bemf);
      c1_Bemf_Observation(chartInstance, *chartInstance->c1_bemf_zcr,
                          *chartInstance->c1_bemf_reference,
                          chartInstance->c1_BackEmfObersvationCounterValue,
                          &c1_c_Bemf_control_fault, &c1_e_Counter_output);
      *chartInstance->c1_Bemf_control_fault = c1_c_Bemf_control_fault;
      _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_Bemf_control_fault, 58U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 58U, (real_T)
                        *chartInstance->c1_Bemf_control_fault);
      chartInstance->c1_BackEmfObersvationCounterValue = c1_e_Counter_output;
      _SFD_DATA_RANGE_CHECK((real_T)
                            chartInstance->c1_BackEmfObersvationCounterValue, 0U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 0U, (real_T)
                        chartInstance->c1_BackEmfObersvationCounterValue);
      c1_Appcon(chartInstance, *chartInstance->c1_bemf_zcr,
                chartInstance->c1_SlurpDetectionCounterAppcon, 1,
                chartInstance->c1_SlurpDetection_Appcon_ActivationCounter,
                *chartInstance->c1_speed_reference,
                chartInstance->c1_SpeedReference_PT1, &c1_c_Slurp_Detected,
                &c1_f_Counter_output, &c1_b_Activation_Counter_output);
      *chartInstance->c1_SlurpDetectedAppcon = c1_c_Slurp_Detected;
      _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_SlurpDetectedAppcon, 56U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 56U, (real_T)
                        *chartInstance->c1_SlurpDetectedAppcon);
      chartInstance->c1_SlurpDetectionCounterAppcon = c1_f_Counter_output;
      _SFD_DATA_RANGE_CHECK((real_T)
                            chartInstance->c1_SlurpDetectionCounterAppcon, 12U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 12U, (real_T)
                        chartInstance->c1_SlurpDetectionCounterAppcon);
      chartInstance->c1_SlurpDetection_Appcon_ActivationCounter =
        c1_b_Activation_Counter_output;
      _SFD_DATA_RANGE_CHECK((real_T)
                            chartInstance->c1_SlurpDetection_Appcon_ActivationCounter,
                            14U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 14U, (real_T)
                        chartInstance->c1_SlurpDetection_Appcon_ActivationCounter);
      chartInstance->c1_MaskFlag = 0U;
      _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_MaskFlag, 5U);
      covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 5U, (real_T)
                        chartInstance->c1_MaskFlag);
      guard1 = true;
    } else {
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 37U,
                   chartInstance->c1_sfEvent);
      c1_c_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance, 5U,
        37U, 0U, covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 37U, 0,
        (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance,
        5U, 37U, 0U, (real_T)
        chartInstance->c1_CurrentZeroCrossingFlagNegativeToPositiveCounter,
        (real_T)chartInstance->c1_zerocrossing, 0, 0U,
        chartInstance->c1_CurrentZeroCrossingFlagNegativeToPositiveCounter ==
        chartInstance->c1_zerocrossing) != 0U) != 0U);
      if (c1_c_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 37U, chartInstance->c1_sfEvent);
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 45U, chartInstance->c1_sfEvent);
        chartInstance->c1_FirstCurrentZeroCrossingFlagPositiveToNegative = 0U;
        _SFD_DATA_RANGE_CHECK((real_T)
                              chartInstance->c1_FirstCurrentZeroCrossingFlagPositiveToNegative,
                              4U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 4U, (real_T)
                          chartInstance->c1_FirstCurrentZeroCrossingFlagPositiveToNegative);
        guard1 = true;
      } else {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 44U, chartInstance->c1_sfEvent);
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 42U, chartInstance->c1_sfEvent);
      }
    }
  } else {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 35U, chartInstance->c1_sfEvent);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 42U, chartInstance->c1_sfEvent);
  }

  if (guard1) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 43U, chartInstance->c1_sfEvent);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 41U, chartInstance->c1_sfEvent);
  }

  _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 40U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 11U, chartInstance->c1_sfEvent);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 11U, chartInstance->c1_sfEvent);
}

static const mxArray *c1_sf_marshallOut(void *chartInstanceVoid, void *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  int32_T c1_u;
  const mxArray *c1_y = NULL;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_u = *(int32_T *)c1_inData;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static int32_T c1_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_b_sfEvent, const char_T *c1_identifier)
{
  int32_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_sfEvent),
    &c1_thisId);
  sf_mex_destroy(&c1_b_sfEvent);
  return c1_y;
}

static int32_T c1_b_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  int32_T c1_y;
  int32_T c1_i46;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_i46, 1, 6, 0U, 0, 0U, 0);
  c1_y = c1_i46;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_b_sfEvent;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  int32_T c1_y;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_b_sfEvent = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_sfEvent),
    &c1_thisId);
  sf_mex_destroy(&c1_b_sfEvent);
  *(int32_T *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_b_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  uint8_T c1_u;
  const mxArray *c1_y = NULL;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_u = *(uint8_T *)c1_inData;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static uint8_T c1_c_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_b_tp_Stop_Mode, const char_T *c1_identifier)
{
  uint8_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_tp_Stop_Mode),
    &c1_thisId);
  sf_mex_destroy(&c1_b_tp_Stop_Mode);
  return c1_y;
}

static uint8_T c1_d_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  uint8_T c1_y;
  uint8_T c1_u1;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_u1, 1, 3, 0U, 0, 0U, 0);
  c1_y = c1_u1;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_b_tp_Stop_Mode;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  uint8_T c1_y;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_b_tp_Stop_Mode = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_tp_Stop_Mode),
    &c1_thisId);
  sf_mex_destroy(&c1_b_tp_Stop_Mode);
  *(uint8_T *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_c_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  int16_T c1_u;
  const mxArray *c1_y = NULL;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_u = *(int16_T *)c1_inData;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static int16_T c1_e_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_b_Vs_s, const char_T *c1_identifier)
{
  int16_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_Vs_s), &c1_thisId);
  sf_mex_destroy(&c1_b_Vs_s);
  return c1_y;
}

static int16_T c1_f_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  int16_T c1_y;
  int16_T c1_i47;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_i47, 1, 4, 0U, 0, 0U, 0);
  c1_y = c1_i47;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_b_Vs_s;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  int16_T c1_y;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_b_Vs_s = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_Vs_s), &c1_thisId);
  sf_mex_destroy(&c1_b_Vs_s);
  *(int16_T *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_d_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  c1_ControlFlag c1_u;
  const mxArray *c1_y = NULL;
  int32_T c1_b_u;
  const mxArray *c1_b_y = NULL;
  const mxArray *c1_m3 = NULL;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_u = *(c1_ControlFlag *)c1_inData;
  c1_y = NULL;
  sf_mex_check_enum("ControlFlag", 10, c1_sv0, c1_iv0);
  c1_b_u = (int32_T)c1_u;
  c1_b_y = NULL;
  sf_mex_assign(&c1_b_y, sf_mex_create("y", &c1_b_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_m3, c1_b_y, false);
  sf_mex_assign(&c1_y, sf_mex_create_enum("ControlFlag", c1_m3), false);
  sf_mex_destroy(&c1_m3);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static c1_ControlFlag c1_g_emlrt_marshallIn
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, const mxArray
   *c1_b_controller_flag, const char_T *c1_identifier)
{
  c1_ControlFlag c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_controller_flag),
    &c1_thisId);
  sf_mex_destroy(&c1_b_controller_flag);
  return c1_y;
}

static c1_ControlFlag c1_h_emlrt_marshallIn
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, const mxArray *c1_u,
   const emlrtMsgIdentifier *c1_parentId)
{
  c1_ControlFlag c1_y;
  (void)chartInstance;
  sf_mex_check_enum("ControlFlag", 10, c1_sv0, c1_iv0);
  sf_mex_check_builtin(c1_parentId, c1_u, "ControlFlag", 0, 0U, NULL);
  c1_y = (c1_ControlFlag)sf_mex_get_enum_element(c1_u, 0);
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_b_controller_flag;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  c1_ControlFlag c1_y;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_b_controller_flag = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_controller_flag),
    &c1_thisId);
  sf_mex_destroy(&c1_b_controller_flag);
  *(c1_ControlFlag *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_e_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  c1_struct_F7N0deEp72x1GuUaWKNfKD c1_u;
  const mxArray *c1_y = NULL;
  static const char * c1_sv1[3] = { "SpeedMax", "SpeedVFLimit", "pi" };

  real_T c1_b_u;
  const mxArray *c1_b_y = NULL;
  real_T c1_c_u;
  const mxArray *c1_c_y = NULL;
  real_T c1_d_u;
  const mxArray *c1_d_y = NULL;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_u = *(c1_struct_F7N0deEp72x1GuUaWKNfKD *)c1_inData;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_createstruct("structure", 3, c1_sv1, 2, 1, 1),
                false);
  c1_b_u = c1_u.SpeedMax;
  c1_b_y = NULL;
  sf_mex_assign(&c1_b_y, sf_mex_create("y", &c1_b_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setfieldbynum(c1_y, 0, "SpeedMax", c1_b_y, 0);
  c1_c_u = c1_u.SpeedVFLimit;
  c1_c_y = NULL;
  sf_mex_assign(&c1_c_y, sf_mex_create("y", &c1_c_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setfieldbynum(c1_y, 0, "SpeedVFLimit", c1_c_y, 1);
  c1_d_u = c1_u.pi;
  c1_d_y = NULL;
  sf_mex_assign(&c1_d_y, sf_mex_create("y", &c1_d_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setfieldbynum(c1_y, 0, "pi", c1_d_y, 2);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static c1_struct_F7N0deEp72x1GuUaWKNfKD c1_i_emlrt_marshallIn
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, const mxArray *c1_u,
   const emlrtMsgIdentifier *c1_parentId)
{
  c1_struct_F7N0deEp72x1GuUaWKNfKD c1_y;
  emlrtMsgIdentifier c1_thisId;
  static const char * c1_fieldNames[3] = { "SpeedMax", "SpeedVFLimit", "pi" };

  c1_thisId.fParent = c1_parentId;
  c1_thisId.bParentIsCell = false;
  sf_mex_check_struct(c1_parentId, c1_u, 3, c1_fieldNames, 0U, NULL);
  c1_thisId.fIdentifier = "SpeedMax";
  c1_y.SpeedMax = c1_j_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getfield(c1_u, "SpeedMax", "SpeedMax", 0)), &c1_thisId);
  c1_thisId.fIdentifier = "SpeedVFLimit";
  c1_y.SpeedVFLimit = c1_j_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getfield(c1_u, "SpeedVFLimit", "SpeedVFLimit", 0)), &c1_thisId);
  c1_thisId.fIdentifier = "pi";
  c1_y.pi = c1_j_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield(c1_u,
    "pi", "pi", 0)), &c1_thisId);
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static real_T c1_j_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  real_T c1_y;
  real_T c1_d18;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_d18, 1, 0, 0U, 0, 0U, 0);
  c1_y = c1_d18;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_b_openloop;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  c1_struct_F7N0deEp72x1GuUaWKNfKD c1_y;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_b_openloop = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_openloop),
    &c1_thisId);
  sf_mex_destroy(&c1_b_openloop);
  *(c1_struct_F7N0deEp72x1GuUaWKNfKD *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_f_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  boolean_T c1_u;
  const mxArray *c1_y = NULL;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_u = *(boolean_T *)c1_inData;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_u, 11, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static boolean_T c1_k_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_d_fault, const char_T *c1_identifier)
{
  boolean_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_l_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_d_fault), &c1_thisId);
  sf_mex_destroy(&c1_d_fault);
  return c1_y;
}

static boolean_T c1_l_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  boolean_T c1_y;
  boolean_T c1_b0;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_b0, 1, 11, 0U, 0, 0U, 0);
  c1_y = c1_b0;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_d_fault;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  boolean_T c1_y;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_d_fault = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_l_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_d_fault), &c1_thisId);
  sf_mex_destroy(&c1_d_fault);
  *(boolean_T *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_g_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  c1_ControlModeState c1_u;
  const mxArray *c1_y = NULL;
  static const char * c1_enumNames[6] = { "InitMode", "StopMode",
    "AlignmentMode", "VFMode", "ControlMode", "ErrorMode" };

  static const int32_T c1_enumValues[6] = { 0, 1, 2, 3, 4, 5 };

  int32_T c1_b_u;
  const mxArray *c1_b_y = NULL;
  const mxArray *c1_m4 = NULL;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_u = *(c1_ControlModeState *)c1_inData;
  c1_y = NULL;
  sf_mex_check_enum("ControlModeState", 6, c1_enumNames, c1_enumValues);
  c1_b_u = (int32_T)c1_u;
  c1_b_y = NULL;
  sf_mex_assign(&c1_b_y, sf_mex_create("y", &c1_b_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_m4, c1_b_y, false);
  sf_mex_assign(&c1_y, sf_mex_create_enum("ControlModeState", c1_m4), false);
  sf_mex_destroy(&c1_m4);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static const mxArray *c1_h_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  uint16_T c1_u;
  const mxArray *c1_y = NULL;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_u = *(uint16_T *)c1_inData;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_u, 5, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static uint16_T c1_m_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_c_PWM_OFF, const char_T *c1_identifier)
{
  uint16_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_n_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_c_PWM_OFF),
    &c1_thisId);
  sf_mex_destroy(&c1_c_PWM_OFF);
  return c1_y;
}

static uint16_T c1_n_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  uint16_T c1_y;
  uint16_T c1_u2;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_u2, 1, 5, 0U, 0, 0U, 0);
  c1_y = c1_u2;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_g_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_c_PWM_OFF;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  uint16_T c1_y;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_c_PWM_OFF = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_n_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_c_PWM_OFF),
    &c1_thisId);
  sf_mex_destroy(&c1_c_PWM_OFF);
  *(uint16_T *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_i_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  c1_struct_AnKM5BqWxAKQWowfA30Y4B c1_u;
  const mxArray *c1_y = NULL;
  static const char * c1_sv2[9] = { "EnableSynchronisationTime",
    "EnableSynchronisation", "EnableSynchronisationMaximumVoltageDeviation",
    "BemfKp", "BemfKi", "SpeedKp_M", "SpeedKi_M", "IsKp", "IsKi" };

  real_T c1_b_u;
  const mxArray *c1_b_y = NULL;
  real_T c1_c_u;
  const mxArray *c1_c_y = NULL;
  real_T c1_d_u;
  const mxArray *c1_d_y = NULL;
  real_T c1_e_u;
  const mxArray *c1_e_y = NULL;
  real_T c1_f_u;
  const mxArray *c1_f_y = NULL;
  real_T c1_g_u;
  const mxArray *c1_g_y = NULL;
  real_T c1_h_u;
  const mxArray *c1_h_y = NULL;
  real_T c1_i_u;
  const mxArray *c1_i_y = NULL;
  real_T c1_j_u;
  const mxArray *c1_j_y = NULL;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_u = *(c1_struct_AnKM5BqWxAKQWowfA30Y4B *)c1_inData;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_createstruct("structure", 9, c1_sv2, 2, 1, 1),
                false);
  c1_b_u = c1_u.EnableSynchronisationTime;
  c1_b_y = NULL;
  sf_mex_assign(&c1_b_y, sf_mex_create("y", &c1_b_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setfieldbynum(c1_y, 0, "EnableSynchronisationTime", c1_b_y, 0);
  c1_c_u = c1_u.EnableSynchronisation;
  c1_c_y = NULL;
  sf_mex_assign(&c1_c_y, sf_mex_create("y", &c1_c_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setfieldbynum(c1_y, 0, "EnableSynchronisation", c1_c_y, 1);
  c1_d_u = c1_u.EnableSynchronisationMaximumVoltageDeviation;
  c1_d_y = NULL;
  sf_mex_assign(&c1_d_y, sf_mex_create("y", &c1_d_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setfieldbynum(c1_y, 0, "EnableSynchronisationMaximumVoltageDeviation",
                       c1_d_y, 2);
  c1_e_u = c1_u.BemfKp;
  c1_e_y = NULL;
  sf_mex_assign(&c1_e_y, sf_mex_create("y", &c1_e_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setfieldbynum(c1_y, 0, "BemfKp", c1_e_y, 3);
  c1_f_u = c1_u.BemfKi;
  c1_f_y = NULL;
  sf_mex_assign(&c1_f_y, sf_mex_create("y", &c1_f_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setfieldbynum(c1_y, 0, "BemfKi", c1_f_y, 4);
  c1_g_u = c1_u.SpeedKp_M;
  c1_g_y = NULL;
  sf_mex_assign(&c1_g_y, sf_mex_create("y", &c1_g_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setfieldbynum(c1_y, 0, "SpeedKp_M", c1_g_y, 5);
  c1_h_u = c1_u.SpeedKi_M;
  c1_h_y = NULL;
  sf_mex_assign(&c1_h_y, sf_mex_create("y", &c1_h_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setfieldbynum(c1_y, 0, "SpeedKi_M", c1_h_y, 6);
  c1_i_u = c1_u.IsKp;
  c1_i_y = NULL;
  sf_mex_assign(&c1_i_y, sf_mex_create("y", &c1_i_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setfieldbynum(c1_y, 0, "IsKp", c1_i_y, 7);
  c1_j_u = c1_u.IsKi;
  c1_j_y = NULL;
  sf_mex_assign(&c1_j_y, sf_mex_create("y", &c1_j_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setfieldbynum(c1_y, 0, "IsKi", c1_j_y, 8);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static void c1_o_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId,
  c1_struct_AnKM5BqWxAKQWowfA30Y4B *c1_y)
{
  emlrtMsgIdentifier c1_thisId;
  static const char * c1_fieldNames[9] = { "EnableSynchronisationTime",
    "EnableSynchronisation", "EnableSynchronisationMaximumVoltageDeviation",
    "BemfKp", "BemfKi", "SpeedKp_M", "SpeedKi_M", "IsKp", "IsKi" };

  c1_thisId.fParent = c1_parentId;
  c1_thisId.bParentIsCell = false;
  sf_mex_check_struct(c1_parentId, c1_u, 9, c1_fieldNames, 0U, NULL);
  c1_thisId.fIdentifier = "EnableSynchronisationTime";
  c1_y->EnableSynchronisationTime = c1_j_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getfield(c1_u, "EnableSynchronisationTime",
    "EnableSynchronisationTime", 0)), &c1_thisId);
  c1_thisId.fIdentifier = "EnableSynchronisation";
  c1_y->EnableSynchronisation = c1_j_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getfield(c1_u, "EnableSynchronisation", "EnableSynchronisation", 0)),
    &c1_thisId);
  c1_thisId.fIdentifier = "EnableSynchronisationMaximumVoltageDeviation";
  c1_y->EnableSynchronisationMaximumVoltageDeviation = c1_j_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getfield(c1_u,
       "EnableSynchronisationMaximumVoltageDeviation",
       "EnableSynchronisationMaximumVoltageDeviation", 0)), &c1_thisId);
  c1_thisId.fIdentifier = "BemfKp";
  c1_y->BemfKp = c1_j_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c1_u, "BemfKp", "BemfKp", 0)), &c1_thisId);
  c1_thisId.fIdentifier = "BemfKi";
  c1_y->BemfKi = c1_j_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c1_u, "BemfKi", "BemfKi", 0)), &c1_thisId);
  c1_thisId.fIdentifier = "SpeedKp_M";
  c1_y->SpeedKp_M = c1_j_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getfield(c1_u, "SpeedKp_M", "SpeedKp_M", 0)), &c1_thisId);
  c1_thisId.fIdentifier = "SpeedKi_M";
  c1_y->SpeedKi_M = c1_j_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getfield(c1_u, "SpeedKi_M", "SpeedKi_M", 0)), &c1_thisId);
  c1_thisId.fIdentifier = "IsKp";
  c1_y->IsKp = c1_j_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c1_u, "IsKp", "IsKp", 0)), &c1_thisId);
  c1_thisId.fIdentifier = "IsKi";
  c1_y->IsKi = c1_j_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getfield
    (c1_u, "IsKi", "IsKi", 0)), &c1_thisId);
  sf_mex_destroy(&c1_u);
}

static void c1_h_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_b_closedloop;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  c1_struct_AnKM5BqWxAKQWowfA30Y4B c1_y;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_b_closedloop = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_o_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_closedloop), &c1_thisId,
                        &c1_y);
  sf_mex_destroy(&c1_b_closedloop);
  *(c1_struct_AnKM5BqWxAKQWowfA30Y4B *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_j_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  real_T c1_u;
  const mxArray *c1_y = NULL;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_u = *(real_T *)c1_inData;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static void c1_i_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_b_Reset;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  real_T c1_y;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_b_Reset = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_j_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_Reset), &c1_thisId);
  sf_mex_destroy(&c1_b_Reset);
  *(real_T *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

static uint32_T c1_p_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_b_temporalCounter_i1, const char_T
  *c1_identifier)
{
  uint32_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_q_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_temporalCounter_i1),
    &c1_thisId);
  sf_mex_destroy(&c1_b_temporalCounter_i1);
  return c1_y;
}

static uint32_T c1_q_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  uint32_T c1_y;
  uint32_T c1_u3;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_u3, 1, 7, 0U, 0, 0U, 0);
  c1_y = c1_u3;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static const mxArray *c1_r_emlrt_marshallIn
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, const mxArray
   *c1_b_setSimStateSideEffectsInfo, const char_T *c1_identifier)
{
  const mxArray *c1_y = NULL;
  emlrtMsgIdentifier c1_thisId;
  c1_y = NULL;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  sf_mex_assign(&c1_y, c1_s_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c1_b_setSimStateSideEffectsInfo), &c1_thisId), false);
  sf_mex_destroy(&c1_b_setSimStateSideEffectsInfo);
  return c1_y;
}

static const mxArray *c1_s_emlrt_marshallIn
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, const mxArray *c1_u,
   const emlrtMsgIdentifier *c1_parentId)
{
  const mxArray *c1_y = NULL;
  (void)chartInstance;
  (void)c1_parentId;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_duplicatearraysafe(&c1_u), false);
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_slStringInitializeDynamicBuffers
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *sf_get_hover_data_for_msg
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, int32_T c1_ssid)
{
  (void)chartInstance;
  (void)c1_ssid;
  return NULL;
}

static void c1_init_sf_message_store_memory
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

mxArray *sf_c1_Motor_1pHController_getDebuggerHoverDataFor
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, uint32_T c1_b)
{
  mxArray *c1_c = NULL;
  const mxArray *c1_m5 = NULL;
  const mxArray *c1_m6 = NULL;
  c1_c = NULL;
  switch (c1_b) {
   case 2043U:
    sf_mex_assign(&c1_m5, c1_sfAfterOrElapsed(chartInstance), false);
    sfAppendHoverData(&c1_c, sf_mex_dup(c1_m5), 2043U, "afterOrElapsed", 240U,
                      -1, -1, -1, -1);
    break;

   case 1270U:
    sf_mex_assign(&c1_m6, c1_b_sfAfterOrElapsed(chartInstance), false);
    sfAppendHoverData(&c1_c, sf_mex_dup(c1_m6), 1270U, "afterOrElapsed", 1269U,
                      -1, -1, -1, -1);
    break;
  }

  sf_mex_destroy(&c1_m5);
  sf_mex_destroy(&c1_m6);
  return c1_c;
}

static int32_T c1__s32_add__(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, int32_T c1_b, int32_T c1_c, int32_T c1_EMLOvCount_src_loc,
  uint32_T c1_ssid_src_loc, int32_T c1_offset_src_loc, int32_T c1_length_src_loc)
{
  int32_T c1_a;
  (void)chartInstance;
  (void)c1_EMLOvCount_src_loc;
  c1_a = c1_b + c1_c;
  if (((c1_a ^ c1_b) & (c1_a ^ c1_c)) < 0) {
    _SFD_OVERFLOW_DETECTION(SFDB_OVERFLOW, c1_ssid_src_loc, c1_offset_src_loc,
      c1_length_src_loc);
  }

  return c1_a;
}

static void init_test_point_addr_map(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance)
{
  chartInstance->c1_testPointAddrMap[0] = chartInstance->c1_PWM_CMD2;
}

static void **get_test_point_address_map(SFc1_Motor_1pHControllerInstanceStruct *
  chartInstance)
{
  return &chartInstance->c1_testPointAddrMap[0];
}

static rtwCAPI_ModelMappingInfo *get_test_point_mapping_info
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  return &chartInstance->c1_testPointMappingInfo;
}

static void init_dsm_address_info(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance)
{
  chartInstance->c1_covrtInstance = (CovrtStateflowInstance *)
    sfrtGetCovrtInstance(chartInstance->S);
  chartInstance->c1_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c1_Vs_s = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c1_controller_flag = (c1_ControlFlag *)
    ssGetOutputPortSignal_wrapper(chartInstance->S, 2);
  chartInstance->c1_PWM_CMD1 = (int32_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c1_fault_clear = (boolean_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c1_Direction = (boolean_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c1_controller_mode = (c1_ControlModeState *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 2);
  chartInstance->c1_speed_reference = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c1_voltage_reference = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c1_current_reference = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 5);
  chartInstance->c1_bemf_reference = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 6);
  chartInstance->c1_is_adc = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 7);
  chartInstance->c1_iRshunt1_adc = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 8);
  chartInstance->c1_iRshunt2_adc = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 9);
  chartInstance->c1_PWM_CMD2 = (int32_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c1_PWM_OFF = (uint16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 5);
  chartInstance->c1_fault1 = (boolean_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 10);
  chartInstance->c1_fault2 = (boolean_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 11);
  chartInstance->c1_align_delay = (uint16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 12);
  chartInstance->c1_start_ctr = (boolean_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 13);
  chartInstance->c1_Vs1 = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 14);
  chartInstance->c1_Vs2 = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 15);
  chartInstance->c1_vdc = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 16);
  chartInstance->c1_bemf_vf = (boolean_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 17);
  chartInstance->c1_start_angle_pos = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 18);
  chartInstance->c1_start_angle_neg = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 19);
  chartInstance->c1_fixedPoint_IsKp_int16 = (int16_T *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 20);
  chartInstance->c1_fixedPoint_IsKi_int16 = (int16_T *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 21);
  chartInstance->c1_fixedPoint_BemfKp_int16 = (int16_T *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 22);
  chartInstance->c1_fixedPoint_BemfKi_int16 = (int16_T *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 23);
  chartInstance->c1_fixedPoint_SpeedKp_M_int16 = (int16_T *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 24);
  chartInstance->c1_fixedPoint_SpeedKi_M_int16 = (int16_T *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 25);
  chartInstance->c1_bemf_zcr = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 6);
  chartInstance->c1_SlurpDetectedAppcon = (boolean_T *)
    ssGetOutputPortSignal_wrapper(chartInstance->S, 7);
  chartInstance->c1_ApproxPower = (int32_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 8);
  chartInstance->c1_Bemf_control_fault = (boolean_T *)
    ssGetOutputPortSignal_wrapper(chartInstance->S, 9);
  chartInstance->c1_Speed_control_fault = (boolean_T *)
    ssGetOutputPortSignal_wrapper(chartInstance->S, 10);
  chartInstance->c1_speed_pll = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 11);
  chartInstance->c1_voltage_ref = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 12);
  chartInstance->c1_InvVoltageOut = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 13);
  chartInstance->c1_SlurpDetectedMiele = (boolean_T *)
    ssGetOutputPortSignal_wrapper(chartInstance->S, 14);
  chartInstance->c1_old_InvVoltageOut = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 15);
  chartInstance->c1_testpoint = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 16);
  chartInstance->c1_testpoint2 = (uint16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 17);
  chartInstance->c1_testpoint3 = (uint16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 18);
  chartInstance->c1_b_speed_reference = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 19);
  chartInstance->c1_b_rotor_position_reference = (int16_T *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 26);
  chartInstance->c1_rotor_start_positive = (int16_T *)
    ssGetOutputPortSignal_wrapper(chartInstance->S, 20);
  chartInstance->c1_rotor_start_negative = (int16_T *)
    ssGetOutputPortSignal_wrapper(chartInstance->S, 21);
  chartInstance->c1_Power = (int32_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 22);
  chartInstance->c1_Speed = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 23);
  chartInstance->c1_Slurp_Detected = (boolean_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 27);
  chartInstance->c1_Counter = (int32_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 24);
  chartInstance->c1_Counter_output = (int32_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 28);
  chartInstance->c1_Bemf_Sample = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 25);
  chartInstance->c1_b_Slurp_Detected = (boolean_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 29);
  chartInstance->c1_b_Counter = (int32_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 26);
  chartInstance->c1_b_Counter_output = (int32_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 30);
  chartInstance->c1_Activate = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 27);
  chartInstance->c1_Activation_Counter = (int16_T *)
    ssGetOutputPortSignal_wrapper(chartInstance->S, 28);
  chartInstance->c1_Activation_Counter_output = (int16_T *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 31);
  chartInstance->c1_b_Speed = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 29);
  chartInstance->c1_Speed_Filtered = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 30);
  chartInstance->c1_c_Speed = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 31);
  chartInstance->c1_Speed_reference = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 32);
  chartInstance->c1_c_Counter = (int32_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 33);
  chartInstance->c1_b_Speed_control_fault = (boolean_T *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 32);
  chartInstance->c1_c_Counter_output = (int32_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 33);
  chartInstance->c1_c_speed_reference = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 34);
  chartInstance->c1_control_flag = (c1_ControlFlag *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 34);
  chartInstance->c1_b_start_ctr = (boolean_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 35);
  chartInstance->c1_bemf = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 36);
  chartInstance->c1_Synchronisation_enabled = (boolean_T *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 35);
  chartInstance->c1_Time_State = (int32_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 37);
  chartInstance->c1_Time_state_output = (int32_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 36);
  chartInstance->c1_b_bemf = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 38);
  chartInstance->c1_b_bemf_reference = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 39);
  chartInstance->c1_d_Counter = (int32_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 40);
  chartInstance->c1_b_Bemf_control_fault = (boolean_T *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 37);
  chartInstance->c1_d_Counter_output = (int32_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 38);
  chartInstance->c1_c_fault = (boolean_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 39);
  chartInstance->c1_b_fault1 = (boolean_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 41);
  chartInstance->c1_b_fault2 = (boolean_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 42);
  chartInstance->c1_b_control_flag = (c1_ControlFlag *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 40);
  chartInstance->c1_b_fault_clear = (boolean_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 43);
  chartInstance->c1_b_OL_voltage_state_is = (int16_T *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 41);
  chartInstance->c1_b_OL_voltage_state_bemf = (int16_T *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 42);
  chartInstance->c1_b_OL_speed_state = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 43);
  chartInstance->c1_b_OLvoltage_sat = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 44);
  chartInstance->c1_b_OLspeed_sat = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 45);
  chartInstance->c1_Current = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 44);
  chartInstance->c1_InverterVoltage = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 45);
  chartInstance->c1_Reset = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 46);
  chartInstance->c1_b_Power = (int32_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 46);
  chartInstance->c1_Reference = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 47);
  chartInstance->c1_State_variable_in = (int32_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 48);
  chartInstance->c1_ReferenceOut = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 47);
  chartInstance->c1_State_variable_out = (int32_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 48);
  chartInstance->c1_b_voltage_reference = (int16_T *)
    ssGetOutputPortSignal_wrapper(chartInstance->S, 49);
  chartInstance->c1_is_Rshunt1_adc = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 50);
  chartInstance->c1_is_Rshunt2_adc = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 51);
  chartInstance->c1_b_PWM_OFF = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 52);
  chartInstance->c1_b_is_Rshunt = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 49);
  chartInstance->c1_d_speed_reference = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 53);
  chartInstance->c1_angle_reference = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 54);
  chartInstance->c1_c_rotor_position_reference = (int16_T *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 50);
  chartInstance->c1_soll = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 55);
  chartInstance->c1_ist = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 56);
  chartInstance->c1_kp = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 57);
  chartInstance->c1_ki = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 58);
  chartInstance->c1_I_state_in = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 59);
  chartInstance->c1_limit_low = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 60);
  chartInstance->c1_limit_high = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 61);
  chartInstance->c1_output = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 51);
  chartInstance->c1_I_state_out = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 52);
  chartInstance->c1_c_voltage_reference = (int16_T *)
    ssGetOutputPortSignal_wrapper(chartInstance->S, 62);
  chartInstance->c1_b_PWM_CMD1 = (uint16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 53);
  chartInstance->c1_rotor_angle = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 63);
  chartInstance->c1_b_PWM_CMD2 = (uint16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 54);
  chartInstance->c1_voltage_dc = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 64);
  chartInstance->c1_InverterOutputVoltage = (int16_T *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 55);
  chartInstance->c1_c_PWM_CMD1 = (uint16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 65);
  chartInstance->c1_c_PWM_CMD2 = (uint16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 66);
  chartInstance->c1_Vs = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 56);
  chartInstance->c1_b_voltage_dc = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 67);
}

/* SFunction Glue Code */
static void init_test_point_mapping_info(SimStruct *S);
void sf_c1_Motor_1pHController_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(2968506335U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(1678816098U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2276967289U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(1622635064U);
}

mxArray *sf_c1_Motor_1pHController_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c1_Motor_1pHController_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("testpoint");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("PWM_CMD2");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c1_Motor_1pHController_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c1_Motor_1pHController(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  mxArray *mxVarInfo = sf_mex_decode(
    "eNrN2rtv00AYAHCnhJCCeAuJtyoGQAxAYWFAIpBStVJDIhoFhJCurntprTo+6+4cHuIPQAIkBJF"
    "a8R4Y+AMYGBgY+BMYGBgYGBgYGBm586Mk10BOtoEvkmU+S+fmx3e+++4cIzdZMcRnkzjeHDaMgj"
    "gXxTFkhJ+1UZwTx7HoHF7Pr1wfEQe/4WF5nVFrck6cXbMVxKZ/fdJtkuD+p4xf9y/0uX+u6/7D0"
    "fXw8/1Mqvafl0qyfamrfb5P+w1d7bdE8VnPo+R6jVzD1Ij/n+Lzf/N8CT0TAzzbFI+Mz+FWE1nE"
    "5ZQ4qGn6DjfguL7p5Wmj4pLxpNtuEIeb87jqcyieD89Lavt+nqLikXHtUgWVK2OjIBxGK63jBAj"
    "HR718rFMc6yJHdXwcRr/6pPf8b1ccMp52fOqNYY4tjufE4CaGAjjP//fk41qPq2JjBwMa176myJ"
    "eH8VyfARvGuPAicB0f4Mr3uPJGgyEG4vv/uJd4XJuV8+hNi4JwfG5pzZubFYeMo57lYIqajjkPo"
    "19NPNJ6XrYqHhkTZw6ptQCY56V4RytPw4pLxiwYBzzHgeS5+iixh2PGPWK7oPJz+rGWZ73iWd/t"
    "OQHI8/5Zes9JQJ6F24nXce1wQEAUNzP0FEup2neWg/a1AZ79ikfG50xr8XyrWZ3FlLVNbhO3THy"
    "XY9owHR8D8b0K59eZAb7jik/GZZ9S7PIrmJIyJYzZ7vy4mJ8u4HmBbeM6qRFmy39FbBDeF5l7Y2"
    "WdxHJI3mU971HFK+NxmzKunWQg/fn+3/GuTjIQ790HievjiskWpQ2EY+dDrXljk+KQcXUKhaUX4"
    "yYH0w87WvXxDsWzI/TEU2EgQnIhA2U+vNtJvE5e5bIZnHne6CSuW6pTUfczORxPvpN4n7Y6tZIm"
    "k0PxvF5KvN6s1UfRtOxwDZPa5my87QTDtaxXX+5RXHt699N+1ZaZ7xem9D3R8+1WfLt/58t83zB"
    "tPRn6Lg/wHVJ8h1b5UJg5dFZEPasFGM6Xenncqzj3xvujv10FAcnjmxT72tJ3ETexKBwtjMRwA6"
    "h/vl3SytsuxSXj6RuutUCJa7MgbXW7heMSC47vnZ5vn+KTcaOJxoiLkeI870ZzBAjfreeJ9++C/"
    "W+5SQQpXzVfa72yVvHIOH6zAsJRepo4LzZDF9mC74LKy5Y7WnkpKJ5CtJ8vS3kQjhlHaxzfqThk"
    "TAknFHnBSl9MxDQe0bNxjaRcn4R/f9A4N6K4RsL+ZsqiAiNrFFUC5Kg3UV55q5SN78A/8fWbp4S"
    "vPyzT5yul79104vWl8IW4apZ1/cFM8pWkXuK45RFqOlEdiOyueqmY+/Pv23LK79vWdI2zmBuEGR"
    "bv/n6D3nsP9Xy/IUMueNU8FQa0G45s4ccthb8relj6v+/9j2SS3yT7Bx7FbZv4rG5bi6yrv/4Eq"
    "tkadg=="
    );
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c1_Motor_1pHController_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static const mxArray* sf_opaque_get_hover_data_for_msg(void* chartInstance,
  int32_T msgSSID)
{
  return sf_get_hover_data_for_msg( (SFc1_Motor_1pHControllerInstanceStruct *)
    chartInstance, msgSSID);
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc1_Motor_1pHControllerInstanceStruct *chartInstance =
      (SFc1_Motor_1pHControllerInstanceStruct *)sf_get_chart_instance_ptr(S);
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _Motor_1pHControllerMachineNumber_,
           1,
           34,
           80,
           0,
           157,
           0,
           0,
           0,
           0,
           0,
           &chartInstance->chartNumber,
           &chartInstance->instanceNumber,
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_Motor_1pHControllerMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_Motor_1pHControllerMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _Motor_1pHControllerMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,0,0,0,"BackEmfObersvationCounterValue");
          _SFD_SET_DATA_PROPS(1,0,0,0,
                              "CurrentZeroCrossingFlagNegativeToPositiveCounter");
          _SFD_SET_DATA_PROPS(2,0,0,0,
                              "CurrentZeroCrossingFlagPositiveToNegativeCounter");
          _SFD_SET_DATA_PROPS(3,0,0,0,
                              "FirstCurrentZeroCrossingFlagNegativeToPositive");
          _SFD_SET_DATA_PROPS(4,0,0,0,
                              "FirstCurrentZeroCrossingFlagPositiveToNegative");
          _SFD_SET_DATA_PROPS(5,0,0,0,"MaskFlag");
          _SFD_SET_DATA_PROPS(6,0,0,0,"OL_speed_state");
          _SFD_SET_DATA_PROPS(7,0,0,0,"OL_voltage_state_bemf");
          _SFD_SET_DATA_PROPS(8,0,0,0,"OL_voltage_state_is");
          _SFD_SET_DATA_PROPS(9,0,0,0,"OLspeed_sat");
          _SFD_SET_DATA_PROPS(10,0,0,0,"OLvoltage_sat");
          _SFD_SET_DATA_PROPS(11,0,0,0,"PT1_StateVariable");
          _SFD_SET_DATA_PROPS(12,0,0,0,"SlurpDetectionCounterAppcon");
          _SFD_SET_DATA_PROPS(13,0,0,0,"SlurpDetectionCounterMiele");
          _SFD_SET_DATA_PROPS(14,0,0,0,"SlurpDetection_Appcon_ActivationCounter");
          _SFD_SET_DATA_PROPS(15,0,0,0,"SpeedObersvationCounterValue");
          _SFD_SET_DATA_PROPS(16,0,0,0,"SpeedReference_PT1");
          _SFD_SET_DATA_PROPS(17,0,0,0,"SynchronisationTime_state");
          _SFD_SET_DATA_PROPS(18,0,0,0,"Vf_Done_SynchronisationEnable");
          _SFD_SET_DATA_PROPS(19,0,0,0,"bemf_test");
          _SFD_SET_DATA_PROPS(20,0,0,0,"fault");
          _SFD_SET_DATA_PROPS(21,0,0,0,"is_Rshunt");
          _SFD_SET_DATA_PROPS(22,0,0,0,"old_is");
          _SFD_SET_DATA_PROPS(23,0,0,0,"rotor_position_reference");
          _SFD_SET_DATA_PROPS(24,1,1,0,"fault_clear");
          _SFD_SET_DATA_PROPS(25,1,1,0,"Direction");
          _SFD_SET_DATA_PROPS(26,1,1,0,"controller_mode");
          _SFD_SET_DATA_PROPS(27,1,1,0,"speed_reference");
          _SFD_SET_DATA_PROPS(28,1,1,0,"voltage_reference");
          _SFD_SET_DATA_PROPS(29,1,1,0,"current_reference");
          _SFD_SET_DATA_PROPS(30,1,1,0,"bemf_reference");
          _SFD_SET_DATA_PROPS(31,1,1,0,"is_adc");
          _SFD_SET_DATA_PROPS(32,1,1,0,"iRshunt1_adc");
          _SFD_SET_DATA_PROPS(33,1,1,0,"iRshunt2_adc");
          _SFD_SET_DATA_PROPS(34,1,1,0,"fault1");
          _SFD_SET_DATA_PROPS(35,1,1,0,"fault2");
          _SFD_SET_DATA_PROPS(36,1,1,0,"align_delay");
          _SFD_SET_DATA_PROPS(37,1,1,0,"start_ctr");
          _SFD_SET_DATA_PROPS(38,1,1,0,"Vs1");
          _SFD_SET_DATA_PROPS(39,1,1,0,"Vs2");
          _SFD_SET_DATA_PROPS(40,1,1,0,"vdc");
          _SFD_SET_DATA_PROPS(41,1,1,0,"bemf_vf");
          _SFD_SET_DATA_PROPS(42,1,1,0,"start_angle_pos");
          _SFD_SET_DATA_PROPS(43,1,1,0,"start_angle_neg");
          _SFD_SET_DATA_PROPS(44,1,1,0,"fixedPoint_IsKp_int16");
          _SFD_SET_DATA_PROPS(45,1,1,0,"fixedPoint_IsKi_int16");
          _SFD_SET_DATA_PROPS(46,1,1,0,"fixedPoint_BemfKp_int16");
          _SFD_SET_DATA_PROPS(47,1,1,0,"fixedPoint_BemfKi_int16");
          _SFD_SET_DATA_PROPS(48,1,1,0,"fixedPoint_SpeedKp_M_int16");
          _SFD_SET_DATA_PROPS(49,1,1,0,"fixedPoint_SpeedKi_M_int16");
          _SFD_SET_DATA_PROPS(50,2,0,1,"Vs_s");
          _SFD_SET_DATA_PROPS(51,2,0,1,"controller_flag");
          _SFD_SET_DATA_PROPS(52,2,0,1,"PWM_CMD1");
          _SFD_SET_DATA_PROPS(53,2,0,1,"PWM_CMD2");
          _SFD_SET_DATA_PROPS(54,2,0,1,"PWM_OFF");
          _SFD_SET_DATA_PROPS(55,2,0,1,"bemf_zcr");
          _SFD_SET_DATA_PROPS(56,2,0,1,"SlurpDetectedAppcon");
          _SFD_SET_DATA_PROPS(57,2,0,1,"ApproxPower");
          _SFD_SET_DATA_PROPS(58,2,0,1,"Bemf_control_fault");
          _SFD_SET_DATA_PROPS(59,2,0,1,"Speed_control_fault");
          _SFD_SET_DATA_PROPS(60,2,0,1,"speed_pll");
          _SFD_SET_DATA_PROPS(61,2,0,1,"voltage_ref");
          _SFD_SET_DATA_PROPS(62,2,0,1,"InvVoltageOut");
          _SFD_SET_DATA_PROPS(63,2,0,1,"SlurpDetectedMiele");
          _SFD_SET_DATA_PROPS(64,2,0,1,"old_InvVoltageOut");
          _SFD_SET_DATA_PROPS(65,2,0,1,"testpoint");
          _SFD_SET_DATA_PROPS(66,2,0,1,"testpoint2");
          _SFD_SET_DATA_PROPS(67,2,0,1,"testpoint3");
          _SFD_SET_DATA_PROPS(68,8,0,0,"");
          _SFD_SET_DATA_PROPS(69,8,0,0,"");
          _SFD_SET_DATA_PROPS(70,8,0,0,"");
          _SFD_SET_DATA_PROPS(71,8,0,0,"");
          _SFD_SET_DATA_PROPS(72,8,0,0,"");
          _SFD_SET_DATA_PROPS(73,8,0,0,"");
          _SFD_SET_DATA_PROPS(74,8,0,0,"");
          _SFD_SET_DATA_PROPS(75,8,0,0,"");
          _SFD_SET_DATA_PROPS(76,8,0,0,"");
          _SFD_SET_DATA_PROPS(77,8,0,0,"");
          _SFD_SET_DATA_PROPS(78,8,0,0,"");
          _SFD_SET_DATA_PROPS(79,8,0,0,"");
          _SFD_SET_DATA_PROPS(80,8,0,0,"");
          _SFD_SET_DATA_PROPS(81,8,0,0,"");
          _SFD_SET_DATA_PROPS(82,8,0,0,"");
          _SFD_SET_DATA_PROPS(83,8,0,0,"");
          _SFD_SET_DATA_PROPS(84,8,0,0,"");
          _SFD_SET_DATA_PROPS(85,8,0,0,"");
          _SFD_SET_DATA_PROPS(86,8,0,0,"");
          _SFD_SET_DATA_PROPS(87,8,0,0,"");
          _SFD_SET_DATA_PROPS(88,8,0,0,"");
          _SFD_SET_DATA_PROPS(89,8,0,0,"");
          _SFD_SET_DATA_PROPS(90,8,0,0,"");
          _SFD_SET_DATA_PROPS(91,8,0,0,"");
          _SFD_SET_DATA_PROPS(92,8,0,0,"");
          _SFD_SET_DATA_PROPS(93,8,0,0,"");
          _SFD_SET_DATA_PROPS(94,8,0,0,"");
          _SFD_SET_DATA_PROPS(95,8,0,0,"");
          _SFD_SET_DATA_PROPS(96,8,0,0,"");
          _SFD_SET_DATA_PROPS(97,8,0,0,"");
          _SFD_SET_DATA_PROPS(98,8,0,0,"");
          _SFD_SET_DATA_PROPS(99,8,0,0,"");
          _SFD_SET_DATA_PROPS(100,8,0,0,"");
          _SFD_SET_DATA_PROPS(101,8,0,0,"");
          _SFD_SET_DATA_PROPS(102,8,0,0,"");
          _SFD_SET_DATA_PROPS(103,8,0,0,"");
          _SFD_SET_DATA_PROPS(104,8,0,0,"");
          _SFD_SET_DATA_PROPS(105,8,0,0,"");
          _SFD_SET_DATA_PROPS(106,8,0,0,"");
          _SFD_SET_DATA_PROPS(107,8,0,0,"");
          _SFD_SET_DATA_PROPS(108,8,0,0,"");
          _SFD_SET_DATA_PROPS(109,8,0,0,"");
          _SFD_SET_DATA_PROPS(110,8,0,0,"");
          _SFD_SET_DATA_PROPS(111,8,0,0,"");
          _SFD_SET_DATA_PROPS(112,8,0,0,"");
          _SFD_SET_DATA_PROPS(113,8,0,0,"");
          _SFD_SET_DATA_PROPS(114,8,0,0,"");
          _SFD_SET_DATA_PROPS(115,8,0,0,"");
          _SFD_SET_DATA_PROPS(116,8,0,0,"");
          _SFD_SET_DATA_PROPS(117,9,0,0,"");
          _SFD_SET_DATA_PROPS(118,9,0,0,"");
          _SFD_SET_DATA_PROPS(119,9,0,0,"");
          _SFD_SET_DATA_PROPS(120,9,0,0,"");
          _SFD_SET_DATA_PROPS(121,9,0,0,"");
          _SFD_SET_DATA_PROPS(122,9,0,0,"");
          _SFD_SET_DATA_PROPS(123,9,0,0,"");
          _SFD_SET_DATA_PROPS(124,9,0,0,"");
          _SFD_SET_DATA_PROPS(125,9,0,0,"");
          _SFD_SET_DATA_PROPS(126,9,0,0,"");
          _SFD_SET_DATA_PROPS(127,9,0,0,"");
          _SFD_SET_DATA_PROPS(128,9,0,0,"");
          _SFD_SET_DATA_PROPS(129,9,0,0,"");
          _SFD_SET_DATA_PROPS(130,9,0,0,"");
          _SFD_SET_DATA_PROPS(131,9,0,0,"");
          _SFD_SET_DATA_PROPS(132,9,0,0,"");
          _SFD_SET_DATA_PROPS(133,9,0,0,"");
          _SFD_SET_DATA_PROPS(134,9,0,0,"");
          _SFD_SET_DATA_PROPS(135,9,0,0,"");
          _SFD_SET_DATA_PROPS(136,9,0,0,"");
          _SFD_SET_DATA_PROPS(137,9,0,0,"");
          _SFD_SET_DATA_PROPS(138,9,0,0,"");
          _SFD_SET_DATA_PROPS(139,9,0,0,"");
          _SFD_SET_DATA_PROPS(140,9,0,0,"");
          _SFD_SET_DATA_PROPS(141,9,0,0,"");
          _SFD_SET_DATA_PROPS(142,9,0,0,"");
          _SFD_SET_DATA_PROPS(143,9,0,0,"");
          _SFD_SET_DATA_PROPS(144,9,0,0,"");
          _SFD_SET_DATA_PROPS(145,9,0,0,"");
          _SFD_SET_DATA_PROPS(146,9,0,0,"");
          _SFD_SET_DATA_PROPS(147,9,0,0,"");
          _SFD_SET_DATA_PROPS(148,10,0,0,"PWM_MAX_DUTYCYCLE");
          _SFD_SET_DATA_PROPS(149,10,0,0,"closedloop");
          _SFD_SET_DATA_PROPS(150,10,0,0,"delay");
          _SFD_SET_DATA_PROPS(151,10,0,0,"fixedPoint_SpeedLimitMax");
          _SFD_SET_DATA_PROPS(152,10,0,0,"fixedPoint_SpeedLimitMin");
          _SFD_SET_DATA_PROPS(153,10,0,0,"fixedPoint_VoltageLimitMin");
          _SFD_SET_DATA_PROPS(154,10,0,0,"openloop");
          _SFD_SET_DATA_PROPS(155,10,0,0,"scale");
          _SFD_SET_DATA_PROPS(156,10,0,0,"zerocrossing");
          _SFD_STATE_INFO(4,0,0);
          _SFD_STATE_INFO(5,0,0);
          _SFD_STATE_INFO(6,0,0);
          _SFD_STATE_INFO(7,0,0);
          _SFD_STATE_INFO(8,0,0);
          _SFD_STATE_INFO(9,0,0);
          _SFD_STATE_INFO(10,0,0);
          _SFD_STATE_INFO(24,0,0);
          _SFD_STATE_INFO(0,0,2);
          _SFD_STATE_INFO(1,0,2);
          _SFD_STATE_INFO(2,0,2);
          _SFD_STATE_INFO(3,0,2);
          _SFD_STATE_INFO(11,0,2);
          _SFD_STATE_INFO(12,0,2);
          _SFD_STATE_INFO(13,0,2);
          _SFD_STATE_INFO(14,0,2);
          _SFD_STATE_INFO(15,0,2);
          _SFD_STATE_INFO(16,0,2);
          _SFD_STATE_INFO(17,0,2);
          _SFD_STATE_INFO(19,0,2);
          _SFD_STATE_INFO(20,0,2);
          _SFD_STATE_INFO(21,0,2);
          _SFD_STATE_INFO(22,0,2);
          _SFD_STATE_INFO(23,0,2);
          _SFD_STATE_INFO(25,0,2);
          _SFD_STATE_INFO(26,0,2);
          _SFD_STATE_INFO(27,0,2);
          _SFD_STATE_INFO(28,0,2);
          _SFD_STATE_INFO(30,0,2);
          _SFD_STATE_INFO(31,0,2);
          _SFD_STATE_INFO(32,0,2);
          _SFD_STATE_INFO(33,0,2);
          _SFD_CH_SUBSTATE_COUNT(3);
          _SFD_CH_SUBSTATE_DECOMP(0);
          _SFD_CH_SUBSTATE_INDEX(0,4);
          _SFD_CH_SUBSTATE_INDEX(1,5);
          _SFD_CH_SUBSTATE_INDEX(2,24);
          _SFD_ST_SUBSTATE_COUNT(4,0);
          _SFD_ST_SUBSTATE_COUNT(5,5);
          _SFD_ST_SUBSTATE_INDEX(5,0,6);
          _SFD_ST_SUBSTATE_INDEX(5,1,7);
          _SFD_ST_SUBSTATE_INDEX(5,2,8);
          _SFD_ST_SUBSTATE_INDEX(5,3,9);
          _SFD_ST_SUBSTATE_INDEX(5,4,10);
          _SFD_ST_SUBSTATE_COUNT(6,0);
          _SFD_ST_SUBSTATE_COUNT(7,0);
          _SFD_ST_SUBSTATE_COUNT(8,0);
          _SFD_ST_SUBSTATE_COUNT(9,0);
          _SFD_ST_SUBSTATE_COUNT(10,0);
          _SFD_ST_SUBSTATE_COUNT(24,0);
        }

        _SFD_SET_DATA_COMPILED_PROPS(0,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(1,SF_UINT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_h_sf_marshallOut,(MexInFcnForType)c1_g_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_UINT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_h_sf_marshallOut,(MexInFcnForType)c1_g_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_UINT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_h_sf_marshallOut,(MexInFcnForType)c1_g_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_UINT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_h_sf_marshallOut,(MexInFcnForType)c1_g_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(5,SF_UINT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_h_sf_marshallOut,(MexInFcnForType)c1_g_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(6,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(7,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(8,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(9,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(10,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(11,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(12,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(13,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(14,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(15,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(16,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(17,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(18,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(19,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(20,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(21,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(22,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(23,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(24,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(25,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(26,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_g_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(27,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(28,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(29,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(30,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(31,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(32,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(33,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(34,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(35,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(36,SF_UINT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_h_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(37,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(38,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(39,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(40,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(41,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(42,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(43,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(44,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(45,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(46,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(47,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(48,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(49,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(50,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(51,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_d_sf_marshallOut,(MexInFcnForType)c1_d_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(52,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(53,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(54,SF_UINT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_h_sf_marshallOut,(MexInFcnForType)c1_g_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(55,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(56,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(57,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(58,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(59,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(60,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(61,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(62,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(63,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(64,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(65,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(66,SF_UINT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_h_sf_marshallOut,(MexInFcnForType)c1_g_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(67,SF_UINT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_h_sf_marshallOut,(MexInFcnForType)c1_g_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(68,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(69,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(70,SF_UINT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_h_sf_marshallOut,(MexInFcnForType)c1_g_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(71,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(72,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(73,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(74,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(75,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(76,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(77,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(78,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(79,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(80,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(81,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(82,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(83,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(84,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(85,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(86,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(87,SF_UINT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_h_sf_marshallOut,(MexInFcnForType)c1_g_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(88,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(89,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(90,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(91,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(92,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(93,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(94,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(95,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(96,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(97,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(98,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(99,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(100,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_j_sf_marshallOut,(MexInFcnForType)c1_i_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(101,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(102,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(103,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(104,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(105,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(106,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(107,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(108,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(109,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(110,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(111,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(112,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(113,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(114,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(115,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(116,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(117,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(118,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(119,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(120,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(121,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(122,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(123,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_d_sf_marshallOut,(MexInFcnForType)c1_d_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(124,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(125,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(126,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_d_sf_marshallOut,(MexInFcnForType)c1_d_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(127,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(128,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(129,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(130,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(131,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(132,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(133,SF_UINT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_h_sf_marshallOut,(MexInFcnForType)c1_g_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(134,SF_UINT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_h_sf_marshallOut,(MexInFcnForType)c1_g_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(135,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(136,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(137,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(138,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(139,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(140,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(141,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(142,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(143,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(144,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(145,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(146,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(147,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(148,SF_UINT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_h_sf_marshallOut,(MexInFcnForType)c1_g_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(149,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_i_sf_marshallOut,(MexInFcnForType)c1_h_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(150,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(151,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(152,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(153,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(154,SF_STRUCT,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_e_sf_marshallOut,(MexInFcnForType)c1_e_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(155,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(156,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_VALUE_PTR(68,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(69,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(70,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(71,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(72,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(73,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(74,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(75,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(76,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(77,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(78,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(79,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(80,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(81,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(82,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(83,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(84,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(85,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(86,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(87,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(88,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(89,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(90,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(91,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(92,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(93,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(94,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(95,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(96,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(97,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(98,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(99,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(100,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(101,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(102,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(103,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(104,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(105,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(106,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(107,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(108,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(109,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(110,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(111,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(112,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(113,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(114,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(115,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(116,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(117,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(118,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(119,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(120,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(121,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(122,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(123,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(124,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(125,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(126,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(127,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(128,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(129,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(130,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(131,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(132,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(133,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(134,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(135,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(136,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(137,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(138,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(139,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(140,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(141,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(142,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(143,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(144,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(145,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(146,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(147,(void *)(NULL));
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _Motor_1pHControllerMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc1_Motor_1pHControllerInstanceStruct *chartInstance =
      (SFc1_Motor_1pHControllerInstanceStruct *)sf_get_chart_instance_ptr(S);
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(50U, chartInstance->c1_Vs_s);
        _SFD_SET_DATA_VALUE_PTR(51U, chartInstance->c1_controller_flag);
        _SFD_SET_DATA_VALUE_PTR(52U, chartInstance->c1_PWM_CMD1);
        _SFD_SET_DATA_VALUE_PTR(154U, &chartInstance->c1_openloop);
        _SFD_SET_DATA_VALUE_PTR(20U, &chartInstance->c1_fault);
        _SFD_SET_DATA_VALUE_PTR(24U, chartInstance->c1_fault_clear);
        _SFD_SET_DATA_VALUE_PTR(25U, chartInstance->c1_Direction);
        _SFD_SET_DATA_VALUE_PTR(26U, chartInstance->c1_controller_mode);
        _SFD_SET_DATA_VALUE_PTR(27U, chartInstance->c1_speed_reference);
        _SFD_SET_DATA_VALUE_PTR(28U, chartInstance->c1_voltage_reference);
        _SFD_SET_DATA_VALUE_PTR(29U, chartInstance->c1_current_reference);
        _SFD_SET_DATA_VALUE_PTR(30U, chartInstance->c1_bemf_reference);
        _SFD_SET_DATA_VALUE_PTR(31U, chartInstance->c1_is_adc);
        _SFD_SET_DATA_VALUE_PTR(32U, chartInstance->c1_iRshunt1_adc);
        _SFD_SET_DATA_VALUE_PTR(33U, chartInstance->c1_iRshunt2_adc);
        _SFD_SET_DATA_VALUE_PTR(53U, chartInstance->c1_PWM_CMD2);
        _SFD_SET_DATA_VALUE_PTR(54U, chartInstance->c1_PWM_OFF);
        _SFD_SET_DATA_VALUE_PTR(34U, chartInstance->c1_fault1);
        _SFD_SET_DATA_VALUE_PTR(35U, chartInstance->c1_fault2);
        _SFD_SET_DATA_VALUE_PTR(36U, chartInstance->c1_align_delay);
        _SFD_SET_DATA_VALUE_PTR(37U, chartInstance->c1_start_ctr);
        _SFD_SET_DATA_VALUE_PTR(22U, &chartInstance->c1_old_is);
        _SFD_SET_DATA_VALUE_PTR(4U,
          &chartInstance->c1_FirstCurrentZeroCrossingFlagPositiveToNegative);
        _SFD_SET_DATA_VALUE_PTR(3U,
          &chartInstance->c1_FirstCurrentZeroCrossingFlagNegativeToPositive);
        _SFD_SET_DATA_VALUE_PTR(2U,
          &chartInstance->c1_CurrentZeroCrossingFlagPositiveToNegativeCounter);
        _SFD_SET_DATA_VALUE_PTR(1U,
          &chartInstance->c1_CurrentZeroCrossingFlagNegativeToPositiveCounter);
        _SFD_SET_DATA_VALUE_PTR(38U, chartInstance->c1_Vs1);
        _SFD_SET_DATA_VALUE_PTR(39U, chartInstance->c1_Vs2);
        _SFD_SET_DATA_VALUE_PTR(40U, chartInstance->c1_vdc);
        _SFD_SET_DATA_VALUE_PTR(41U, chartInstance->c1_bemf_vf);
        _SFD_SET_DATA_VALUE_PTR(5U, &chartInstance->c1_MaskFlag);
        _SFD_SET_DATA_VALUE_PTR(149U, &chartInstance->c1_closedloop);
        _SFD_SET_DATA_VALUE_PTR(150U, &chartInstance->c1_delay);
        _SFD_SET_DATA_VALUE_PTR(6U, &chartInstance->c1_OL_speed_state);
        _SFD_SET_DATA_VALUE_PTR(9U, &chartInstance->c1_OLspeed_sat);
        _SFD_SET_DATA_VALUE_PTR(10U, &chartInstance->c1_OLvoltage_sat);
        _SFD_SET_DATA_VALUE_PTR(156U, &chartInstance->c1_zerocrossing);
        _SFD_SET_DATA_VALUE_PTR(8U, &chartInstance->c1_OL_voltage_state_is);
        _SFD_SET_DATA_VALUE_PTR(7U, &chartInstance->c1_OL_voltage_state_bemf);
        _SFD_SET_DATA_VALUE_PTR(155U, &chartInstance->c1_scale);
        _SFD_SET_DATA_VALUE_PTR(151U,
          &chartInstance->c1_fixedPoint_SpeedLimitMax);
        _SFD_SET_DATA_VALUE_PTR(152U,
          &chartInstance->c1_fixedPoint_SpeedLimitMin);
        _SFD_SET_DATA_VALUE_PTR(153U,
          &chartInstance->c1_fixedPoint_VoltageLimitMin);
        _SFD_SET_DATA_VALUE_PTR(42U, chartInstance->c1_start_angle_pos);
        _SFD_SET_DATA_VALUE_PTR(43U, chartInstance->c1_start_angle_neg);
        _SFD_SET_DATA_VALUE_PTR(148U, &chartInstance->c1_PWM_MAX_DUTYCYCLE);
        _SFD_SET_DATA_VALUE_PTR(44U, chartInstance->c1_fixedPoint_IsKp_int16);
        _SFD_SET_DATA_VALUE_PTR(45U, chartInstance->c1_fixedPoint_IsKi_int16);
        _SFD_SET_DATA_VALUE_PTR(46U, chartInstance->c1_fixedPoint_BemfKp_int16);
        _SFD_SET_DATA_VALUE_PTR(47U, chartInstance->c1_fixedPoint_BemfKi_int16);
        _SFD_SET_DATA_VALUE_PTR(48U,
          chartInstance->c1_fixedPoint_SpeedKp_M_int16);
        _SFD_SET_DATA_VALUE_PTR(49U,
          chartInstance->c1_fixedPoint_SpeedKi_M_int16);
        _SFD_SET_DATA_VALUE_PTR(11U, &chartInstance->c1_PT1_StateVariable);
        _SFD_SET_DATA_VALUE_PTR(17U,
          &chartInstance->c1_SynchronisationTime_state);
        _SFD_SET_DATA_VALUE_PTR(18U,
          &chartInstance->c1_Vf_Done_SynchronisationEnable);
        _SFD_SET_DATA_VALUE_PTR(23U, &chartInstance->c1_rotor_position_reference);
        _SFD_SET_DATA_VALUE_PTR(16U, &chartInstance->c1_SpeedReference_PT1);
        _SFD_SET_DATA_VALUE_PTR(55U, chartInstance->c1_bemf_zcr);
        _SFD_SET_DATA_VALUE_PTR(56U, chartInstance->c1_SlurpDetectedAppcon);
        _SFD_SET_DATA_VALUE_PTR(57U, chartInstance->c1_ApproxPower);
        _SFD_SET_DATA_VALUE_PTR(58U, chartInstance->c1_Bemf_control_fault);
        _SFD_SET_DATA_VALUE_PTR(59U, chartInstance->c1_Speed_control_fault);
        _SFD_SET_DATA_VALUE_PTR(60U, chartInstance->c1_speed_pll);
        _SFD_SET_DATA_VALUE_PTR(61U, chartInstance->c1_voltage_ref);
        _SFD_SET_DATA_VALUE_PTR(62U, chartInstance->c1_InvVoltageOut);
        _SFD_SET_DATA_VALUE_PTR(63U, chartInstance->c1_SlurpDetectedMiele);
        _SFD_SET_DATA_VALUE_PTR(0U,
          &chartInstance->c1_BackEmfObersvationCounterValue);
        _SFD_SET_DATA_VALUE_PTR(12U,
          &chartInstance->c1_SlurpDetectionCounterAppcon);
        _SFD_SET_DATA_VALUE_PTR(13U,
          &chartInstance->c1_SlurpDetectionCounterMiele);
        _SFD_SET_DATA_VALUE_PTR(14U,
          &chartInstance->c1_SlurpDetection_Appcon_ActivationCounter);
        _SFD_SET_DATA_VALUE_PTR(15U,
          &chartInstance->c1_SpeedObersvationCounterValue);
        _SFD_SET_DATA_VALUE_PTR(64U, chartInstance->c1_old_InvVoltageOut);
        _SFD_SET_DATA_VALUE_PTR(65U, chartInstance->c1_testpoint);
        _SFD_SET_DATA_VALUE_PTR(66U, chartInstance->c1_testpoint2);
        _SFD_SET_DATA_VALUE_PTR(21U, &chartInstance->c1_is_Rshunt);
        _SFD_SET_DATA_VALUE_PTR(67U, chartInstance->c1_testpoint3);
        _SFD_SET_DATA_VALUE_PTR(19U, &chartInstance->c1_bemf_test);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "sp7ZXWxE4rY5vmIt1CbKwXG";
}

static void sf_opaque_initialize_c1_Motor_1pHController(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc1_Motor_1pHControllerInstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c1_Motor_1pHController
    ((SFc1_Motor_1pHControllerInstanceStruct*) chartInstanceVar);
  initialize_c1_Motor_1pHController((SFc1_Motor_1pHControllerInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c1_Motor_1pHController(void *chartInstanceVar)
{
  enable_c1_Motor_1pHController((SFc1_Motor_1pHControllerInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c1_Motor_1pHController(void *chartInstanceVar)
{
  disable_c1_Motor_1pHController((SFc1_Motor_1pHControllerInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c1_Motor_1pHController(void *chartInstanceVar)
{
  sf_gateway_c1_Motor_1pHController((SFc1_Motor_1pHControllerInstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c1_Motor_1pHController(SimStruct*
  S)
{
  return get_sim_state_c1_Motor_1pHController
    ((SFc1_Motor_1pHControllerInstanceStruct *)sf_get_chart_instance_ptr(S));/* raw sim ctx */
}

static void sf_opaque_set_sim_state_c1_Motor_1pHController(SimStruct* S, const
  mxArray *st)
{
  set_sim_state_c1_Motor_1pHController((SFc1_Motor_1pHControllerInstanceStruct*)
    sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_terminate_c1_Motor_1pHController(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc1_Motor_1pHControllerInstanceStruct*) chartInstanceVar
      )->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_Motor_1pHController_optimization_info();
    }

    finalize_c1_Motor_1pHController((SFc1_Motor_1pHControllerInstanceStruct*)
      chartInstanceVar);
    if (!sim_mode_is_rtw_gen(S)) {
      ssSetModelMappingInfoPtr(S, NULL);
    }

    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc1_Motor_1pHController((SFc1_Motor_1pHControllerInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c1_Motor_1pHController(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    initialize_params_c1_Motor_1pHController
      ((SFc1_Motor_1pHControllerInstanceStruct*)sf_get_chart_instance_ptr(S));
    initSimStructsc1_Motor_1pHController((SFc1_Motor_1pHControllerInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

const char* sf_c1_Motor_1pHController_get_post_codegen_info(void)
{
  return
    (
     "eNrtms1vG0UUwNdpmiaFhrSo6ico4kALB2gKFVRCwo0dUwu7NrWblgppO16P7ZVnd5aZWSdF/AF"
     "ILRKikVrKR3tAAokLhx56AKkHzogDBw499MCBA4fe4MjM7jrZjNf2Zu3SQWIl13num9n3m/f2zZ"
     "vZ0VL5osavWf755YSmTfHvaf6Z0PxreyCnQh//90ntSCCf40q2a5UBARbVBl42sOBZSDFymYntv"
     "N3AkWqm3YAE2gbXdTBh/XqjpuUi027nXNsQ/dHzLdNoVVrYRfVF3hbUSza6zHtzXFbm/WRNAg2W"
     "g7DOWgS7zVYOgea6xYStZFrQaFPXGoRAIau4jjCLFl3ETAfBpVVo5G3KALeYbthWYYDBDFvtiyl"
     "IaaWriC0HmcCOpG0BWoEOH2AGzzl1/m/JZRxKVjNagLBF2AIdSAtm2+sT21Du06T8P2qmDRgmJk"
     "BLFsqIhr22lRG3p4jrEA0YEG7bIoGg7WDTZv39X8lx0iUb1BDMwprb7N9bBb7vCucvm3AFkr7j1"
     "sjgDiSgCUt235t6A7K06nlrPUp61ZhpwWVAThncfxTW+0YvjxxaAdxPsMpb9FODHmSeVonZ4cPb"
     "tzfXyovIHPbIuJbvbDpMzettqQMHeWG9t5xhZwBCtK9aFTsF2IHI6zULGBis5vcarecQ7IAmj9o"
     "6H2QR4hls180eXxguZdjK8HDLFgpneKaINm5DLW8zSBrAgFHPLTVasO4iyEF5ZmLe7SI6rZtUeG"
     "uIVuDTYVoabbh2dgWTNgcd8NhvIAgfDMoPIi8f0zby8pMx8vJM0E6Tvo+G+klF9KOFvoW+E9Kfm"
     "disf1K670T3t6RX6L7p0H13SXZOSvcVenP8s+/7bZ+duffd/QOzP//41zfb35HHTbY/1WN/yvtb"
     "tGu9sLV5cDaQD3cTzno4dnqiQOieDtk1GdH/vlD/c4FMndcuXji/uvQqefdEx8qzhUzt7ZULb3n"
     "93T062N6XJXu7v8+LzHfZ8fIYJUa+HkzQQgauP22J/l8P2Ts1ZDxmgt/96+GbI7V/cD0tx0PUeD"
     "0hjZeQTzk86ayWcXcCCT8Hj43nd59nmP/3SDxCXoRWQzewzQhGegPwwkNTh+vPeH7aJXEJOW93l"
     "jFiYhb3yhkleH69lZbbR/FMSzxCLp8v6plidkEJDs0aleO4Ehy/xfPHDoljR8BRyuXUiKv78Z7/"
     "pyUOIVeQS5wsZHz9Aus8ufFUoM7z/zB5XtvEVTQhggrltT9G8JfD1woRCVuNvHA7LddHUVyTm7g"
     "mtWWqUyXs//vjxHmtJubRDwyiBMcDK9a8+ZTEIeQgshAkegOBphpxdfpmrOdlt8QjZIzqulwLKP"
     "O8TF+J5acZiWvGq9tFHnAQUonnvZuJeRikzNvoUYnnjc9j8eyUeHaGeY4rxPPTV6PzvKIQT+ujx"
     "Ou4jp8QdAIbY+SZTo/Ufu2G1748hOdZiUfIi8BoL1mNUg0S2gFiHymDXbGNtQyQCxXh+9afXy8N"
     "4Tsm8Qk54xICbXYREpwhmFLTbopN9jOwyWE7sIrLmJrirwBbCd7bY+ftUlZxl1wl3hvxeF+SeIW"
     "cMwllsZ2sSDx/8mh4e52sCO/VTxPXx0VA24JNCY7912LNG7MSh5BLBd0vvah4t6ZKHK7Fqo/3Sj"
     "x7fZ7uVOgR6WIho8p8eHUt8Tq5h8uk6szz2lriuqVUCMIPMHV4JtcS79OWCutuAkwVnjvXE683y"
     "9UF3Xvt7r2RrHW3ndTguhGvvjwkcR3avJ+2UVuOfb9wRL4v4vEdlPgO9uMb+77hqPWkz3dhCN8R"
     "ie9ID5/ue04/xaVNqwU1OL+O58fDEufh7v5o31WQIn68O8K+tuA7C4NjTDpPNwrF5w/XY/ntgMQ"
     "l5Mpl22gRbJvUc5s4BtMtsdThuxeP7xmJT8jLDT2LbahLnP65JVX4PryVeP/O2/8Wm0Qq+avsxl"
     "qvbJd4hNx9s6IER/rLxH4xqX6WtlxbKb/MXYnllymJZyrYzxelvBIcl1CsPL5f4hAywQwT3fFW+"
     "nwiJt2MPh6u+RHXJ/79h+W5eYlr3o83IIoKqBsLetGDXHBOZ9bfKo2H77l/hS9qnuJ80WBjfb5G"
     "5LtXSby+5Hw+XGmcdf3zY/FXknqJQcvBBKCgDtTNUL00ndraebxtoTwLmYapZrB+5ymj7JvYZN+"
     "EJha8sp+mhrSbCdiCc7Jp/1zRtfTjfe//4lj8m2T/wCGwY2KXVk2jTaV4TXpOc6vttP/bKdEu9R"
     "+53zjtjHNu+2TCdpmE7ZLauTthu4MJ2+3Rkp1f36r+oHlBk/TnRrjPo9YPf/8DBGxRLg=="
     );
}

static void mdlSetWorkWidths_c1_Motor_1pHController(SimStruct *S)
{
  sf_set_work_widths(S, sf_c1_Motor_1pHController_get_post_codegen_info());
  ssSetChecksum0(S,(2567155735U));
  ssSetChecksum1(S,(3702243406U));
  ssSetChecksum2(S,(3184397849U));
  ssSetChecksum3(S,(1359325175U));
}

static void mdlRTW_c1_Motor_1pHController(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Stateflow");
  }
}

static void mdlStart_c1_Motor_1pHController(SimStruct *S)
{
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)utMalloc(sizeof
    (SFc1_Motor_1pHControllerInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc1_Motor_1pHControllerInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 0;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c1_Motor_1pHController;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c1_Motor_1pHController;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c1_Motor_1pHController;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c1_Motor_1pHController;
  chartInstance->chartInfo.disableChart =
    sf_opaque_disable_c1_Motor_1pHController;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c1_Motor_1pHController;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c1_Motor_1pHController;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c1_Motor_1pHController;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c1_Motor_1pHController;
  chartInstance->chartInfo.mdlStart = mdlStart_c1_Motor_1pHController;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c1_Motor_1pHController;
  chartInstance->chartInfo.callGetHoverDataForMsg =
    sf_opaque_get_hover_data_for_msg;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartEventFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  chartInstance->chartInfo.dispatchToExportedFcn = NULL;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0, NULL, NULL);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
    init_test_point_mapping_info(S);
  }

  chart_debug_initialization(S,1);
  mdl_start_c1_Motor_1pHController(chartInstance);
}

void c1_Motor_1pHController_method_dispatcher(SimStruct *S, int_T method, void
  *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c1_Motor_1pHController(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c1_Motor_1pHController(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c1_Motor_1pHController(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c1_Motor_1pHController_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

static const rtwCAPI_DataTypeMap dataTypeMap[] = {
  /* cName, mwName, numElements, elemMapIndex, dataSize, slDataId, isComplex, isPointer */
  { "int32_T", "int32_T", 0, 0, sizeof(int32_T), SS_INT32, 0, 0 } };

static real_T sfCAPIsampleTimeZero = 0.0;
static const rtwCAPI_SampleTimeMap sampleTimeMap[] = {
  /* *period, *offset, taskId, mode */
  { &sfCAPIsampleTimeZero, &sfCAPIsampleTimeZero, 0, 0 }
};

static const rtwCAPI_DimensionMap dimensionMap[] = {
  /* dataOrientation, dimArrayIndex, numDims*/
  { rtwCAPI_SCALAR, 0, 2 } };

static const rtwCAPI_Signals testPointSignals[] = {
  /* addrMapIndex, sysNum, SFRelativePath, dataName, portNumber, dataTypeIndex, dimIndex, fixPtIdx, sTimeIndex */
  { 0, 0, "StateflowChart/PWM_CMD2", "PWM_CMD2", 0, 0, 0, 0, 0 } };

static const rtwCAPI_FixPtMap fixedPointMap[] = {
  /* *fracSlope, *bias, scaleType, wordLength, exponent, isSigned */
  { NULL, NULL, rtwCAPI_FIX_RESERVED, 64, 0, 0 } };

static const uint_T dimensionArray[] = {
  1, 1 };

static rtwCAPI_ModelMappingStaticInfo testPointMappingStaticInfo = {
  /* block signal monitoring */
  {
    testPointSignals,                  /* Block signals Array  */
    1,                                 /* Num Block IO signals */
    NULL,                              /* Root Inputs Array    */
    0,                                 /* Num root inputs      */
    NULL,                              /* Root Outputs Array */
    0                                  /* Num root outputs   */
  },

  /* parameter tuning */
  {
    NULL,                              /* Block parameters Array    */
    0,                                 /* Num block parameters      */
    NULL,                              /* Variable parameters Array */
    0                                  /* Num variable parameters   */
  },

  /* block states */
  {
    NULL,                              /* Block States array        */
    0                                  /* Num Block States          */
  },

  /* Static maps */
  {
    dataTypeMap,                       /* Data Type Map            */
    dimensionMap,                      /* Data Dimension Map       */
    fixedPointMap,                     /* Fixed Point Map          */
    NULL,                              /* Structure Element map    */
    sampleTimeMap,                     /* Sample Times Map         */
    dimensionArray                     /* Dimension Array          */
  },

  /* Target type */
  "float",

  {
    2567155735U,
    3702243406U,
    3184397849U,
    1359325175U
  }
};

static void init_test_point_mapping_info(SimStruct *S)
{
  rtwCAPI_ModelMappingInfo *testPointMappingInfo;
  void **testPointAddrMap;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance =
    (SFc1_Motor_1pHControllerInstanceStruct *)sf_get_chart_instance_ptr(S);
  init_test_point_addr_map(chartInstance);
  testPointMappingInfo = get_test_point_mapping_info(chartInstance);
  testPointAddrMap = get_test_point_address_map(chartInstance);
  rtwCAPI_SetStaticMap(*testPointMappingInfo, &testPointMappingStaticInfo);
  rtwCAPI_SetLoggingStaticMap(*testPointMappingInfo, NULL);
  rtwCAPI_SetInstanceLoggingInfo(*testPointMappingInfo, NULL);
  rtwCAPI_SetPath(*testPointMappingInfo, "");
  rtwCAPI_SetFullPath(*testPointMappingInfo, NULL);
  rtwCAPI_SetDataAddressMap(*testPointMappingInfo, testPointAddrMap);
  rtwCAPI_SetChildMMIArray(*testPointMappingInfo, NULL);
  rtwCAPI_SetChildMMIArrayLen(*testPointMappingInfo, 0);
  ssSetModelMappingInfoPtr(S, testPointMappingInfo);
}
