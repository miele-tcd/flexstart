#ifndef __c1_Motor_1pHController_h__
#define __c1_Motor_1pHController_h__
#include "sf_runtime/sfc_sdi.h"
#include "rtw_capi.h"
#include "rtw_modelmap.h"

/* Type Definitions */
#ifndef enum_ControlFlag
#define enum_ControlFlag

enum ControlFlag
{
  ControlFlag_Init = 0,                /* Default value */
  ControlFlag_InitDone,
  ControlFlag_Stop,
  ControlFlag_StopDone,
  ControlFlag_Alignment,
  ControlFlag_AlignmentDone,
  ControlFlag_VF,
  ControlFlag_VFDone,
  ControlFlag_Fault,
  ControlFlag_FaultClear
};

#endif                                 /*enum_ControlFlag*/

#ifndef typedef_c1_ControlFlag
#define typedef_c1_ControlFlag

typedef enum ControlFlag c1_ControlFlag;

#endif                                 /*typedef_c1_ControlFlag*/

#ifndef enum_ControlModeState
#define enum_ControlModeState

enum ControlModeState
{
  ControlModeState_InitMode = 0,       /* Default value */
  ControlModeState_StopMode,
  ControlModeState_AlignmentMode,
  ControlModeState_VFMode,
  ControlModeState_ControlMode,
  ControlModeState_ErrorMode
};

#endif                                 /*enum_ControlModeState*/

#ifndef typedef_c1_ControlModeState
#define typedef_c1_ControlModeState

typedef enum ControlModeState c1_ControlModeState;

#endif                                 /*typedef_c1_ControlModeState*/

#ifndef struct_struct_F7N0deEp72x1GuUaWKNfKD_tag
#define struct_struct_F7N0deEp72x1GuUaWKNfKD_tag

struct struct_F7N0deEp72x1GuUaWKNfKD_tag
{
  real_T SpeedMax;
  real_T SpeedVFLimit;
  real_T pi;
};

#endif                                 /*struct_struct_F7N0deEp72x1GuUaWKNfKD_tag*/

#ifndef typedef_c1_struct_F7N0deEp72x1GuUaWKNfKD
#define typedef_c1_struct_F7N0deEp72x1GuUaWKNfKD

typedef struct struct_F7N0deEp72x1GuUaWKNfKD_tag
  c1_struct_F7N0deEp72x1GuUaWKNfKD;

#endif                                 /*typedef_c1_struct_F7N0deEp72x1GuUaWKNfKD*/

#ifndef struct_struct_AnKM5BqWxAKQWowfA30Y4B_tag
#define struct_struct_AnKM5BqWxAKQWowfA30Y4B_tag

struct struct_AnKM5BqWxAKQWowfA30Y4B_tag
{
  real_T EnableSynchronisationTime;
  real_T EnableSynchronisation;
  real_T EnableSynchronisationMaximumVoltageDeviation;
  real_T BemfKp;
  real_T BemfKi;
  real_T SpeedKp_M;
  real_T SpeedKi_M;
  real_T IsKp;
  real_T IsKi;
};

#endif                                 /*struct_struct_AnKM5BqWxAKQWowfA30Y4B_tag*/

#ifndef typedef_c1_struct_AnKM5BqWxAKQWowfA30Y4B
#define typedef_c1_struct_AnKM5BqWxAKQWowfA30Y4B

typedef struct struct_AnKM5BqWxAKQWowfA30Y4B_tag
  c1_struct_AnKM5BqWxAKQWowfA30Y4B;

#endif                                 /*typedef_c1_struct_AnKM5BqWxAKQWowfA30Y4B*/

#ifndef typedef_SFc1_Motor_1pHControllerInstanceStruct
#define typedef_SFc1_Motor_1pHControllerInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c1_sfEvent;
  uint8_T c1_tp_Stop_Mode;
  uint8_T c1_tp_Init_Mode;
  uint8_T c1_tp_Motor_On;
  uint8_T c1_tp_Alignment_Mode;
  uint8_T c1_tp_Error_Mode;
  uint8_T c1_tp_Alignment_Done;
  uint8_T c1_tp_VF_Mode;
  uint8_T c1_tp_ControlMode;
  uint8_T c1_is_active_c1_Motor_1pHController;
  uint8_T c1_is_c1_Motor_1pHController;
  uint8_T c1_is_Motor_On;
  c1_struct_F7N0deEp72x1GuUaWKNfKD c1_openloop;
  boolean_T c1_fault;
  int16_T c1_old_is;
  uint16_T c1_FirstCurrentZeroCrossingFlagPositiveToNegative;
  uint16_T c1_FirstCurrentZeroCrossingFlagNegativeToPositive;
  uint16_T c1_CurrentZeroCrossingFlagPositiveToNegativeCounter;
  uint16_T c1_CurrentZeroCrossingFlagNegativeToPositiveCounter;
  uint16_T c1_MaskFlag;
  c1_struct_AnKM5BqWxAKQWowfA30Y4B c1_closedloop;
  int16_T c1_delay;
  int16_T c1_OL_speed_state;
  int16_T c1_OLspeed_sat;
  int16_T c1_OLvoltage_sat;
  int16_T c1_zerocrossing;
  int16_T c1_OL_voltage_state_is;
  int16_T c1_OL_voltage_state_bemf;
  int16_T c1_scale;
  int16_T c1_fixedPoint_SpeedLimitMax;
  int16_T c1_fixedPoint_SpeedLimitMin;
  int16_T c1_fixedPoint_VoltageLimitMin;
  uint16_T c1_PWM_MAX_DUTYCYCLE;
  int32_T c1_PT1_StateVariable;
  int32_T c1_SynchronisationTime_state;
  boolean_T c1_Vf_Done_SynchronisationEnable;
  int16_T c1_rotor_position_reference;
  int16_T c1_SpeedReference_PT1;
  int32_T c1_BackEmfObersvationCounterValue;
  int32_T c1_SlurpDetectionCounterAppcon;
  int32_T c1_SlurpDetectionCounterMiele;
  int16_T c1_SlurpDetection_Appcon_ActivationCounter;
  int32_T c1_SpeedObersvationCounterValue;
  int16_T c1_is_Rshunt;
  int16_T c1_bemf_test;
  void *c1_RuntimeVar;
  uint32_T c1_temporalCounter_i1;
  uint32_T c1_presentTicks;
  uint32_T c1_elapsedTicks;
  uint32_T c1_previousTicks;
  uint8_T c1_doSetSimStateSideEffects;
  const mxArray *c1_setSimStateSideEffectsInfo;
  uint32_T c1_mlFcnLineNumber;
  rtwCAPI_ModelMappingInfo c1_testPointMappingInfo;
  void *c1_testPointAddrMap[1];
  CovrtStateflowInstance *c1_covrtInstance;
  void *c1_fEmlrtCtx;
  int16_T *c1_Vs_s;
  c1_ControlFlag *c1_controller_flag;
  int32_T *c1_PWM_CMD1;
  boolean_T *c1_fault_clear;
  boolean_T *c1_Direction;
  c1_ControlModeState *c1_controller_mode;
  int16_T *c1_speed_reference;
  int16_T *c1_voltage_reference;
  int16_T *c1_current_reference;
  int16_T *c1_bemf_reference;
  int16_T *c1_is_adc;
  int16_T *c1_iRshunt1_adc;
  int16_T *c1_iRshunt2_adc;
  int32_T *c1_PWM_CMD2;
  uint16_T *c1_PWM_OFF;
  boolean_T *c1_fault1;
  boolean_T *c1_fault2;
  uint16_T *c1_align_delay;
  boolean_T *c1_start_ctr;
  int16_T *c1_Vs1;
  int16_T *c1_Vs2;
  int16_T *c1_vdc;
  boolean_T *c1_bemf_vf;
  int16_T *c1_start_angle_pos;
  int16_T *c1_start_angle_neg;
  int16_T *c1_fixedPoint_IsKp_int16;
  int16_T *c1_fixedPoint_IsKi_int16;
  int16_T *c1_fixedPoint_BemfKp_int16;
  int16_T *c1_fixedPoint_BemfKi_int16;
  int16_T *c1_fixedPoint_SpeedKp_M_int16;
  int16_T *c1_fixedPoint_SpeedKi_M_int16;
  int16_T *c1_bemf_zcr;
  boolean_T *c1_SlurpDetectedAppcon;
  int32_T *c1_ApproxPower;
  boolean_T *c1_Bemf_control_fault;
  boolean_T *c1_Speed_control_fault;
  int16_T *c1_speed_pll;
  int16_T *c1_voltage_ref;
  int16_T *c1_InvVoltageOut;
  boolean_T *c1_SlurpDetectedMiele;
  int16_T *c1_old_InvVoltageOut;
  int16_T *c1_testpoint;
  uint16_T *c1_testpoint2;
  uint16_T *c1_testpoint3;
  int16_T *c1_b_speed_reference;
  int16_T *c1_b_rotor_position_reference;
  int16_T *c1_rotor_start_positive;
  int16_T *c1_rotor_start_negative;
  int32_T *c1_Power;
  int16_T *c1_Speed;
  boolean_T *c1_Slurp_Detected;
  int32_T *c1_Counter;
  int32_T *c1_Counter_output;
  int16_T *c1_Bemf_Sample;
  boolean_T *c1_b_Slurp_Detected;
  int32_T *c1_b_Counter;
  int32_T *c1_b_Counter_output;
  int16_T *c1_Activate;
  int16_T *c1_Activation_Counter;
  int16_T *c1_Activation_Counter_output;
  int16_T *c1_b_Speed;
  int16_T *c1_Speed_Filtered;
  int16_T *c1_c_Speed;
  int16_T *c1_Speed_reference;
  int32_T *c1_c_Counter;
  boolean_T *c1_b_Speed_control_fault;
  int32_T *c1_c_Counter_output;
  int16_T *c1_c_speed_reference;
  c1_ControlFlag *c1_control_flag;
  boolean_T *c1_b_start_ctr;
  int16_T *c1_bemf;
  boolean_T *c1_Synchronisation_enabled;
  int32_T *c1_Time_State;
  int32_T *c1_Time_state_output;
  int16_T *c1_b_bemf;
  int16_T *c1_b_bemf_reference;
  int32_T *c1_d_Counter;
  boolean_T *c1_b_Bemf_control_fault;
  int32_T *c1_d_Counter_output;
  boolean_T *c1_c_fault;
  boolean_T *c1_b_fault1;
  boolean_T *c1_b_fault2;
  c1_ControlFlag *c1_b_control_flag;
  boolean_T *c1_b_fault_clear;
  int16_T *c1_b_OL_voltage_state_is;
  int16_T *c1_b_OL_voltage_state_bemf;
  int16_T *c1_b_OL_speed_state;
  int16_T *c1_b_OLvoltage_sat;
  int16_T *c1_b_OLspeed_sat;
  int16_T *c1_Current;
  int16_T *c1_InverterVoltage;
  real_T *c1_Reset;
  int32_T *c1_b_Power;
  int16_T *c1_Reference;
  int32_T *c1_State_variable_in;
  int16_T *c1_ReferenceOut;
  int32_T *c1_State_variable_out;
  int16_T *c1_b_voltage_reference;
  int16_T *c1_is_Rshunt1_adc;
  int16_T *c1_is_Rshunt2_adc;
  int16_T *c1_b_PWM_OFF;
  int16_T *c1_b_is_Rshunt;
  int16_T *c1_d_speed_reference;
  int16_T *c1_angle_reference;
  int16_T *c1_c_rotor_position_reference;
  int16_T *c1_soll;
  int16_T *c1_ist;
  int16_T *c1_kp;
  int16_T *c1_ki;
  int16_T *c1_I_state_in;
  int16_T *c1_limit_low;
  int16_T *c1_limit_high;
  int16_T *c1_output;
  int16_T *c1_I_state_out;
  int16_T *c1_c_voltage_reference;
  uint16_T *c1_b_PWM_CMD1;
  int16_T *c1_rotor_angle;
  uint16_T *c1_b_PWM_CMD2;
  int16_T *c1_voltage_dc;
  int16_T *c1_InverterOutputVoltage;
  uint16_T *c1_c_PWM_CMD1;
  uint16_T *c1_c_PWM_CMD2;
  int16_T *c1_Vs;
  int16_T *c1_b_voltage_dc;
} SFc1_Motor_1pHControllerInstanceStruct;

#endif                                 /*typedef_SFc1_Motor_1pHControllerInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c1_Motor_1pHController_get_eml_resolved_functions_info
  (void);
extern mxArray *sf_c1_Motor_1pHController_getDebuggerHoverDataFor
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, uint32_T c1_b);

/* Function Definitions */
extern void sf_c1_Motor_1pHController_get_check_sum(mxArray *plhs[]);
extern void c1_Motor_1pHController_method_dispatcher(SimStruct *S, int_T method,
  void *data);

#endif
