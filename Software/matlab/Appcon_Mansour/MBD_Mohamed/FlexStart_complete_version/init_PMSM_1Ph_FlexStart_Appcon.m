% initialization for Library_Example_PMSM_1Ph.slx
% 
% load PMSM and Load data from <simparam.mat>, with the contents:
% - PMSM. <tunable parameter in ref. model>
%    - p                    : Pole pairs
%    - psiF20               : PM-Flux (Sum all Poles)/ Vs
%    - Rstr                 : Resistance (Sum all Poles) / Ohm
%    - Ls                   : Inductance with Saturation (Sum all Poles) / H
%    - Lookup.is_hat        : x-axis Lookup, akt. current / A
%    - Lookup.alpha_el_rad  : y-axis Lookup, akt. Rotorposition / rad
%    - Mrel                 : Reluctance Torque Amp. / Nm
%    - teta0                : Rotor natural angle / rad
%    - Jrotor_kgm2          : Rotor inertia (from CAD) / kg m2
%    - IronLoss.Mv          : Lookup Iron Loss (Torque Losses) / Nm
%    - IronLoss.n           : Lookup axis for Iron Losses / rpm
%    - IronLoss.is_hat      : Lookup axis for Iron Losses / A (Amplitude)
%    - ExtraLoss.Mv         : Lookup Friction Loss (Torque Losses) / Nm
%    - ExtraLoss.n          : Lookup axis for Friction Losses / rpm
% - Load. <tunable parameter outside of ref. model>
%    - M_W2_29mm                : Load W2 impeller 29 mm / Nm
%    - M_W2_33mm                : Load W2 impeller  33 mm / Nm
%    - n                        : x-axis Lookup, Rot. Speed / rpm
%
% Autor: Aryanti Putri (TCD/RD)
%        Tobias Rodeh�ser (SH/ATM)
% Version: 26.5.2021
% MATLAB-Version: R2019a
%
clc;
clear all
close all

%run Elektronik_EFU1301.m % Elektronik TEC, EFU1301
%-----------------Modified by Mansour 28/12/2021%--------------------------
%
%Simulated_model=Electronics.ON;    % Enable electronics on Simulink model
Simulated_model=Electronics.OFF;    % Disable electronics on Simulink model
Motor_model=Electronics.Motor_220; % Carried out Simulation of the 220 V of motor model  
%Motor_model=Electronics.Motor_120;  % Carried out Simulation of the 120 V of motor model 
electronics=Appcon_settings_model(Simulated_model);
run Elektronik_Appcon.m % Elektronik TEC, EFU1301
%--------------------------------------------------------------------------

%% Input
%simparam = 'SimParam_MppXX_3000wdg_0.224mm_20C_Lib.mat';
N1 = 3000; d1 = 0.224; % design 1: var 230 Vrms
N2 = 1800; d2 = 0.254; % var 100 Vrms
%----------------------------Added by Mansour 02/12/2021 %-----------------
N2 = N1;
d2 = d1;
%-----------------------------------End------------------------------------
Kadapt=(N1/N2)*(d1/d2);
Kadapt=(N1/N2);
%----------------------------Added by Mansour 02/12/2021 %-----------------
% N2 = N1;
% d2 = d1;
%-----------------------------------End------------------------------------
tsim = 4; % max. Simulation time / s
Tsample = 1e-6; % Sample Time
fel_rms = 10; % Frequency to create rms values

% alignment
t_align = 0.3; %0.3; %0.3; % time to perform alignment / s ( have to be greater than electric and mechanical time constant)
% open loop
% cramp = 0; % a constant to create voltage Ramp for open loop / V (amplitude), Uramp = 2*pi*fel*cramp (cramp could be time dependent)
% speed_vf_pos=2000;  % [rpm] Speed command during Vf phase 
% speed_vf_neg=-1800;  % [rpm] Speed command during Vf phase 

%----------------------------Added by Mohamed 21/06/2021 %-----------------
% open loop
cramp = 0; % a constant to create voltage Ramp for open loop / V (amplitude), Uramp = 2*pi*fel*cramp (cramp could be time dependent)
speed_vf_pos=750;   % [rpm] Speed command during Vf phase 
speed_vf_neg=-750;  % [rpm] Speed command during Vf phase 
%-----------------------------------End------------------------------------

% control
bemf_soll = 0; % BEMF value (zero --> current and bemf almost in phase)

% Speed
n_soll = [0; ...
        750]; % time / s; speed / rpm
    
% Temperaturen
Tcu = 20; % �C

%% Parameter for Motor / Elektronic
% I_align = 0.5; % Current Amplitude to perform alignment / A
% Uboost_pos = 75; % Boost Command / V (amplitude), boost value of the voltage to perform open loop, Uref = Uboost + Uramp, var 100 Vrms
% Uboost_neg = 85; % Boost Command / V (amplitude), boost value of the voltage to perform open loop, Uref = Uboost + Uramp, var 100 Vrms
% start_angle_pos = 0; % voltage start angle for positive turning direction
% start_angle_neg = -pi; % voltage start angle for negative turning direction

%----------------------------Added by Mohamed 08/07/2021%-----------------%
I_align = 0.02; %0.5; % Current Amplitude to perform alignment / A
Uboost_pos = 325; %85*Kadapt; % Boost Command / V (amplitude), boost value of the voltage to perform open loop, Uref = Uboost + Uramp, var 100 Vrms
Uboost_neg = 325*Kadapt;%72*Kadapt; % Boost Command / V (amplitude), boost value of the voltage to perform open loop, Uref = Uboost + Uramp, var 100 Vrms
start_angle_pos = pi/4; % voltage start angle for positive turning direction
start_angle_neg = 0; % voltage start angle for negative turning direction
%---------------------------------End--------------------------------------
%----------------------------Added by Mansour 08/02/2022 %-----------------
Direction=Flex_Direction.CW;  % Direction during Vf phase
%Direction=Flex_Direction.CCW;  % Direction during Vf phase
%-----------------------------------End------------------------------------

%% Machine Parameter
%----------------------------Commented by Mohamed 25/11/2021%--------------
%load(simparam)
% Auswahl Motor, Parameter manuell �ndern: 
% -I_align, 
% -Uboost,
% -inverter.BusVoltage

%PMSM.cT_wind = 3.93e-3; % Temperatur koeffizient Wicklung
%PMSM.cT_PM = -0.0020; % Temperatur koeffizient Ferrit
%PMSM.alpha0_rotor_rad = deg2rad(7.3); %  Rotor Start position

% Skalierung nach Windungszahl
%PMSM.Rstr = PMSM.Rstr*(N2/N1)*(d1/d2)^2*(1+PMSM.cT_wind*(Tcu-20)); % Resistance (Sum all poles) / Ohm, 
%PMSM.psiF20= PMSM.psiF20*(N2/N1);  % Permanent magnet flux (Sum all poles) Amplitude / Vs
%PMSM.Ls = PMSM.Ls *((N2/N1)^2); % inductance vs rotor angle & current
%PMSM.Lookup.is_hat = PMSM.Lookup.is_hat*(N1/N2); % current amplitude / A
%PMSM.IronLoss.is_hat = PMSM.IronLoss.is_hat*(N1/N2);

%PMSM.psiF20= PMSM.psiF20*0.976; % Schnittanpassung f�r Konstruktion, 17.08.2020
%---------------------------------End--------------------------------------

%----------------------------Added by Mohamed 25/11/2021%------------------
PMSM.p = 4; % Pole pairs           
PMSM.psiF20 = 320e-3; % PM-Flux (Sum all Poles)/ Vs (Amplitude!), between 226e-3 ... 272e-3             %% KONTROLLE: Messung der induzierten Spannung, danach zur�ckrechnen    
PMSM.Rstr = 7.2e3; % Resistance (Sum all Poles) / Ohm, @20�C               
PMSM.Ls = 13; % Inductance (Sum all Poles) / H, Simulation 1,6 H                                       %% KONTROLLE: Messung mit Induktivit�tsmessger�t 
PMSM.Mrel = 17e-3; % Reluctance Torque Amp. / Nm, From FFT 1. Order Cogging Torque / Nm (between 9,5 ... 24 mNm)                 
PMSM.teta0 = deg2rad(42.6); % Rotor natural angle / rad (el.), 42.6� ... 48 � (el.)--> 10.65� ...  12� (mech.)               
PMSM.Jrotor_kgm2 = 1.45e-6; % Rotor inertia (from CAD) / kg m2                                          

PMSM.IronLoss.Mv = [0 0; 0 0]; % Lookup Iron Loss (Torque Losses) / Nm
PMSM.IronLoss.n = [0 1000]; % Lookup axis for Iron Losses / rpm         
PMSM.IronLoss.is_hat = [0 1]; % Lookup axis for Iron Losses / A (Amplitude)
PMSM.ExtraLoss.Mv = [0 0]; % Lookup Friction Loss (Torque Losses) / Nm
PMSM.ExtraLoss.n = [0 1000]; % Lookup axis for Friction Losses / rpm


PMSM.cT_wind = 3.93e-3; % Temperatur koeffizient Wicklung
PMSM.cT_PM = -0.0020; % Temperatur koeffizient Ferrit

PMSM.alpha0_rotor_rad = deg2rad(10.65); %  Rotor Start position / rad (mech.), between 5 ... 15�, Mittelwert 10.65
%---------------------------------End--------------------------------------
%% Load
%----------------------------Commented by Mohamed 25/11/2021%--------------
%Load.Mload = Load.M_W2_29mm;
%---------------------------------End--------------------------------------

%----------------------------Added by Mohamed 25/11/2021%------------------
Load.n = linspace(0,1500,100);
Load.Mload = (1.6e-9*Load.n.^2 + 6.9e-7*Load.n - 0.00072);   %% KONTROLLE: Messung bei unterschiedlichen Drehzahlen bzw. Frequenzen
Load.Mload(Load.Mload < 0) = 0;
%---------------------------------End--------------------------------------

%% Umrichter + Controller
Inverter.SlowInterrupt = 1/HALCPU_IRQ_SYSTICK_FREQUENCY_HZ; % 1 kHz interrupt, dt in s
Inverter.fPWM=HALCPU_PWM3PHASE_FREQ; % Switching frequency 
Inverter.TPWM=1/Inverter.fPWM; % 8 kHz interrupt, dt in s

% Motor and Electronic Parameter
Inverter.HalfBridge.Vdc = 325;  % DC Bus voltage / V 
Inverter.Tdead=2e-6;         % Dead time / s

% Back emf observation
BackEmfObservation.MaxVoltageDeviation = 20; % Maximum voltage deviation
BackEmfObservation.MaxCounter=20000; % 0.5s @ 3000rpm Speed dependent

% Speed observation
SpeedObservation.MaxSpeedDeviation=200;
SpeedObservation.MaxTime=3;

% Slurp Detection
SlurpDetection.Appcon.ActivationDelay=200; % 3000rpm --> 50Hz --> Sampling of Bemf --> 0.01s --> Delay 2s Activation delay after going into controlled state
SlurpDetection.Appcon.ReactivationDelay=100; % 3000rpm --> 50Hz --> Sampling of Bemf --> 0.01s --> Delay 1s Reactivation delay after change in speed reference
SlurpDetection.Appcon.DecrementValue=-0.5; % Decrement value, if bemf is smaller than threshold
SlurpDetection.Appcon.ThresholdforBemfSample1=7.5; % Bemf threshold, if speed is greater than SpeedThreshold1
SlurpDetection.Appcon.ThresholdforBemfSample2=5.5; % Bemf threshold, if speed is smaller than SpeedThreshold1
SlurpDetection.Appcon.SpeedThreshold1=2800; % Speed threshold for switching bemf threshold
SlurpDetection.Appcon.LimitforDetection=1000; % limit for detected slurping. If the increment is greater than this value.
SlurpDetection.Appcon.CounterLimit.Max=SlurpDetection.Appcon.LimitforDetection;
SlurpDetection.Appcon.CounterLimit.Min=0;
SlurpDetection.Appcon.MinSpeed=2450;
SlurpDetection.Appcon.MaxSpeed=4000;
SlurpDetection.Appcon.MaxSpeedDifference=20;
SlurpDetection.Miele.DecrementFactor=-1;
SlurpDetection.Miele.IncrementFactor=2;
SlurpDetection.Miele.LimitforDetection=8000;
SlurpDetection.Miele.CounterLimit.Max=SlurpDetection.Miele.LimitforDetection;
SlurpDetection.Miele.CounterLimit.Min=0;
SlurpDetection.Miele.MinimumSpeedforDetection=2000;

% ------------------------------Inverter-----------------------------------

% PWM
PWM_MAX_DUTYCYCLE = uint16(HALCPU_PWM3PHASE_TPWM_COUNT);

Inverter.HalfBridge.Switch.Ron=2e-3;        % MOSFET Resistance / Ohm
Inverter.HalfBridge.Switch.Lon = 0;       % MOSFET Diode Inductance / H
Inverter.HalfBridge.Switch.Rd = 0.01;       % MOSFET Diode Resistance / Ohm
Inverter.HalfBridge.Switch.Vf = 0.9;       % MOSFET Diode Forward Voltage / V
Inverter.HalfBridge.Switch.Rs = 1e5;       % MOSFET Snubber Resistance / Ohm
Inverter.HalfBridge.Switch.Cs = inf;       % MOSFET Snubber Capacitor / Ohm

Inverter.TzeroCrossing = 5e-3;  % start detect zero crossing after this time / s, calculated from max. speed. Here: 5000rpm / 60 * p = 83.3 Hz --> 12 ms. Sinufunc. has 2 ZeroCrossing --> max. 6 ms)
Inverter.TdelayMeas = 500e-6; % delay before measuring bemf / s, depends on filter time constant, depends on the hardware, determined empirically

%----------------------------Added by Mohamed 21/06/2021%------------------ 
Inverter.Rshunt1= EFU.RShunt; % Ohm
Inverter.Rshunt2= EFU.RShunt; % Ohm
% Filter for i phase Measurement - hardware dependent
Filter.Ri1 = EFU.Ri1_Umotor ;
Filter.Ri2 = EFU.Ri2_Umotor;
Filter.Ri3 = EFU.Ri3_Umotor;
Filter.Ci  = EFU.Ci_Umotor;
%------------------------------------End-----------------------------------

% --------------------------------Filter----------------------------------
% Compare Filter with the per Unit Values!
% Filter for Vs1 und Vs2 Measurement - hardware dependent
Filter.R1=EFU.Rv_Umotor; % Ohm
Filter.R2=EFU.Rm_Umotor ; % Ohm
Filter.C=EFU.Cm_Umotor ; % Farad
% Filter for Vdc Measurement - hardware dependent
Filter.Rdc1=EFU.Rv_Udc; % Ohm
Filter.Rdc2=EFU.Rm_Udc; % Ohm
Filter.Cdc=EFU.Cm_Udc; % Farad
%----------------------------Modified by Mohamed%---------------------------- 

%--------------------------------------------------------------------------
zerocrossing = round(Inverter.TzeroCrossing/Inverter.TPWM); % detect current zero crossing, normed to interrupt 8kHz
delay=round(Inverter.TdelayMeas/Inverter.TPWM); % delay for voltage measurement, normed to interrupt 8kHz

% ------------------------------Alignment----------------------------------
alignment.current=I_align; % Current reference
alignment.VoltageLimit=Inverter.HalfBridge.Vdc; %/sqrt(3);   % Voltage limit
alignment.time=t_align*1e3; % time to perform alignment / ms ( greater than L/R = 9.45 ms)
alignment.delay = alignment.time-(Inverter.SlowInterrupt/1e-3); % alignment time (in Stateflow in ms!)

% --------------------------------Open Loop--------------------------------
openloop.SpeedMax=1250;%5000;          % Maximum speed of the motor / rpm (only in open loop)
openloop.SpeedVFLimit=750;             % Speed limit of the open loop --> control mode
openloop.pi=pi;                        % PI

%----------------------------------Closed Loop-----------------------------
Controller.MovingWindow.Delay = 8;
closedloop.EnableSynchronisationTime = 0.2; % [s] Enable Closed Loop operation after VF has started
closedloop.EnableSynchronisation = ceil(closedloop.EnableSynchronisationTime/((60/2)/abs(speed_vf_pos))); % [-] Amount of zero crossing to allow closed loop operation
closedloop.EnableSynchronisationMaximumVoltageDeviation = fix(Inverter.HalfBridge.Vdc/Controller.MovingWindow.Delay); % [V] Maximum voltage deviation of Bemf measurement in comparison to mean value to allow closed loop operation
Controller.Filter.PT1.Tn =0.1; % [s] Filter time constant for speed reference in closed loop operation

% Slurp Detection 
Slurp_Detection_Time=0.050*HALCPU_PWM3PHASE_FREQ;
Polynom_Coeff=[7.152e-10 -3.32e-6 0.0078 -5.8651];

% Bemf threshold, if speed is greater than SpeedThreshold
Slurp_Lookup_n=300:29:1250;
Slurp_Lookup_P=Polynom_Coeff(1,1).*Slurp_Lookup_n.*Slurp_Lookup_n.*Slurp_Lookup_n+Polynom_Coeff(1,2).*Slurp_Lookup_n.*Slurp_Lookup_n+Polynom_Coeff(1,3).*Slurp_Lookup_n +Polynom_Coeff(1,4);

% Current filter time constant for power approximation
CurrentFilterTimeConstant = 0.05; 

%Output bemf Miele
closedloop.BemfKp=0.1*(N1/N2);    
closedloop.BemfKi=60*20e-3*(N1/N2); % 20 ms, Frequenz bei 50 Hz, ca. Mittelpunkt des Arbeitbereichs

BemfKp=closedloop.BemfKp; % Stateflow
BemfKi=closedloop.BemfKi; % Stateflow
SpeedLimitMin = 300;%1800; % rpm, limit of speed (motor operating area)
SpeedLimitMax = 1250;%5000; % rpm, limit of speed (motor operating area)

%Output Voltage Miele
closedloop.SpeedKp_M=0.000471*(N2/N1);   
closedloop.SpeedKi_M=29*Inverter.TPWM*(N2/N1);
SpeedKp_M=closedloop.SpeedKp_M; % Stateflow
SpeedKi_M=closedloop.SpeedKi_M; % Stateflow
VoltageLimitMin = 32; % V, limit of input voltage (motor operating area), 190 Vrms

%Output Current Miele
closedloop.IsKp=1000*(N2/N1)^2;          
closedloop.IsKi=1000000*Inverter.TPWM*(N2/N1)^2; 
IsKp=closedloop.IsKp; % Stateflow
IsKi=closedloop.IsKi; % Stateflow

%% ------------------------ Per Unit--------------------------
% Depends on the component on the Inverter Board!
Meas.VoltageReg=EFU.ADC_VRef; % V

%Current phase measurment : ADC0-SE1 = Gcurrent*Vsh + Goffset*VoltageReg
Meas.Gcurrent=EFU.I_Verstaerkung;
%Max current : Imax * Rshunt * Gcurrent + offset = VoltageReg
Meas.Rshunt=EFU.RShunt; % Ohm
Meas.offset=EFU.iOffset;
Meas.Imax=(Meas.VoltageReg-Meas.offset)/(Meas.Rshunt*Meas.Gcurrent);

%U phase voltage measurment : ADC0-SE2 = Guvoltage * U-phase-U
Meas.Guvoltage=EFU.Rm_Umotor/(EFU.Rv_Umotor+EFU.Rm_Umotor);
%Max U voltage : Umax * Guvoltage = VoltageReg
Meas.Umax=Meas.VoltageReg/Meas.Guvoltage;

%V phase oltage measurment : ADC1-SE2 = Gvvoltage * U-phase-V
Meas.Gvvoltage=EFU.Rm_Umotor /(EFU.Rv_Umotor +EFU.Rm_Umotor );
%Max V voltage : Vmax * Gvvoltage = VoltageReg
Meas.Vmax=Meas.VoltageReg/Meas.Gvvoltage;

%DC phase Voltage measurment : ADC1-SE8 = Gdcvoltage * UDC1
Meas.Gdcvoltage=EFU.Rm_Udc /(EFU.Rv_Udc+EFU.Rm_Udc);
%Max DC voltage : VDCmax * Gdcvoltage = VoltageReg
Meas.VDCmax=Meas.VoltageReg/Meas.Gdcvoltage;

% Base quantities for normalization
perunit.Ib=Meas.Imax; % Current
perunit.Ub=max(max(Meas.Umax,Meas.Vmax),Meas.VDCmax); % Voltage, U, V and DC
perunit.Wb=openloop.SpeedMax; % Max. Speed
perunit.Wbe=perunit.Wb*((2*pi)/60)*PMSM.p; % Max. electrical frequency
perunit.tetab=openloop.pi;  % Max. angle

% Voltages
Controller.UdcVoltage.Maximum=perunit.Ub*Software.Scale_1V;
Controller.UdcVoltage.Scale=EFU.udcIncrements *Software.Scale_1V;
Controller.UmotorVoltage.Maximum=perunit.Ub*Software.Scale_1V;
Controller.UmotorVoltage.Scale=EFU.umotorIncrements*Software.Scale_1V;
Controller.PhaseCurrent.Maximum=perunit.Ib*Software.Scale_1A;
Controller.PhaseCurrent.Scale=EFU.iIncrements*Software.Scale_1A;
Controller.DCCurrent.Scale=EFU.idcIncrements*Software.Scale_1A;
Controller.Power.Scale=Software.Scale_1W/(Software.Scale_1V*Software.Scale_1A);
%------------------------- Added by Mohamed 08/07/2021   %-----------------
Controller.UgridVoltage.Scale = EFU.ugridIncrements *Software.Scale_1V;
%-------------------------------------End----------------------------------
Controller.BemfController.Kp = BemfKp*(Software.Scale_1rpm/Software.Scale_1V);
Controller.BemfController.Ki = BemfKi*(Software.Scale_1rpm/Software.Scale_1V);
Controller.SpeedController.Kp = SpeedKp_M*(Software.Scale_1V/Software.Scale_1rpm);
Controller.SpeedController.Ki = SpeedKi_M*(Software.Scale_1V/Software.Scale_1rpm);
Controller.CurrentController.Kp = IsKp*(Software.Scale_1V/Software.Scale_1A);
Controller.CurrentController.Ki = IsKi*(Software.Scale_1V/Software.Scale_1A);

%Scaling
%Calculation of scaling factor (Maximum Scaling 2^15)
Controller.UdcVoltage.Scale_u16= 10;%floor(log2(floor(min(EFU.VarMax_u16,EFU.VarMax_u32/Controller.UdcVoltage.Maximum))));
Controller.UdcVoltage.Factor_u16 = floor(Controller.UdcVoltage.Scale*2^Controller.UdcVoltage.Scale_u16);
%----------------------------Added by Mohamed 08/07/2021%------------------     
Controller.UgridVoltage.Scale_u16=10;
Controller.UgridVoltage.Factor_u16 = floor(Controller.UgridVoltage.Scale*2^Controller.UgridVoltage.Scale_u16);
%------------------------------------End-----------------------------------

%----------------------------Added by Mohamed 08/07/2021%------------------     
Controller.UmotorVoltage.Scale_u16= 10;%floor(log2(floor(min(EFU.VarMax_u16,EFU.VarMax_u32/))));
Controller.UmotorVoltage.Factor_u16 = floor(Controller.UmotorVoltage.Scale*2^Controller.UmotorVoltage.Scale_u16);
Controller.PhaseCurrent.Scale_u16= 10;%floor(log2(floor(min(EFU.VarMax_u16,EFU.VarMax_u32/Controller.Currents.Maximum))));
Controller.PhaseCurrent.Factor_u16 = floor(Controller.PhaseCurrent.Scale*2^Controller.PhaseCurrent.Scale_u16);
Controller.DCCurrent.Scale_u16= 10;%floor(log2(floor(min(EFU.VarMax_u16,EFU.VarMax_u32/Controller.Currents.Maximum))));
Controller.DCCurrent.Factor_u16 = floor(Controller.DCCurrent.Scale*2^Controller.DCCurrent.Scale_u16);

%Controller.UdcVoltage.Scale_u16= 12;
%Controller.UdcVoltage.Factor_u16 = 401714;
%Controller.UmotorVoltage.Scale_u16= 12;
%Controller.UmotorVoltage.Factor_u16 = 377300;
%Controller.PhaseCurrent.Scale_u16=15 ;
%Controller.PhaseCurrent.Factor_u16 = 528000; %264000;
%Controller.DCCurrent.Scale_u16= 15;
%Controller.DCCurrent.Factor_u16 = 528000; %264000;
%------------------------------------End-----------------------------------
Controller.closedloop.EnableSynchronisationMaximumVoltageDeviation = closedloop.EnableSynchronisationMaximumVoltageDeviation*Software.Scale_1V; %V
Controller.PowerCalc.Scale_u16 =15;
Controller.PowerCalc.Factor_u16=floor(Software.Scale_1W/(Software.Scale_1V*Software.Scale_1A)*2^Controller.PowerCalc.Scale_u16);
Controller.BackEmfObservation.MaxVoltageDeviation=BackEmfObservation.MaxVoltageDeviation*Software.Scale_1V;
Controller.SpeedObservation.MaxSpeedDeviation=SpeedObservation.MaxSpeedDeviation*Software.Scale_1rpm;
Controller.SpeedObservation.MaxTime=floor(SpeedObservation.MaxTime*Inverter.fPWM);

%% ------------------------Closed Loop | Fixed Point-----------------------
% Controller Board: signed integer 16 Bit, -2^15 ... 2^15-1 (-32768 ... 32767)
scale=2^15; 
scale_uint16 = 2^16;

% ADC 12 Bit: unsigned integer 12 Bit, 0 ... (2^12)-1 (0 ... 4095)
scale_adc_uint = 2^12;
scale_adc_int = 2^11;
scale_adc_offset = scale_adc_uint-scale_adc_int;

% Lookup Table Cosinus 
x_int16 = int16(-scale:256:scale-1);
x_double = -pi:2*pi/length(x_int16):pi-2*pi/length(x_int16);
cos_x_int16 = int16(cos(x_double)*scale);

Controller.CosFcn.x = x_int16;
Controller.CosFcn.y = cos_x_int16;

% Open Loop variables
fixedPoint_SpeedVFLimit=int16(openloop.SpeedVFLimit*Software.Scale_1rpm);

%closed loop variables
%BEMF
fixedPoint_BemfKp_int16=int16(Controller.BemfController.Kp*scale); % Controller.BemfController.Kp = 0.0022
fixedPoint_BemfKi_int16=int16(Controller.BemfController.Ki*scale); % Controller.BemfController.Ki = 0.0265
fixedPoint_SpeedLimitMin=int16(SpeedLimitMin*Software.Scale_1rpm);
fixedPoint_SpeedLimitMax=int16(SpeedLimitMax*Software.Scale_1rpm);

% Speed
fixedPoint_SpeedKp_M_int16=int16(Controller.SpeedController.Kp*scale); % Controller.SpeedController.Kp = 0.0453
fixedPoint_SpeedKi_M_int16=int16(Controller.SpeedController.Ki*scale); % Controller.SpeedController.Ki = 0.8500
fixedPoint_VoltageLimitMin = int16(VoltageLimitMin*Software.Scale_1V);

% Current
fixedPoint_IsKp_int16=int16(Controller.CurrentController.Kp*scale); % Controller.CurrentController.Kp = 0.2055
fixedPoint_IsKi_int16=int16(Controller.CurrentController.Ki*scale); % Controller.CurrentController.Ki = 0.0026

% Slurp 
fixedPoint_Slurp_Lookup_P=Slurp_Lookup_P*Software.Scale_1W;
fixedPoint_Slurp_Lookup_n=Slurp_Lookup_n*Software.Scale_1rpm;

% Scaling
scale_speed = Software.Scale_1rpm;
scale_fel = 1/(scale_speed*((2*pi)/60)*PMSM.p);
scale_current = Software.Scale_1A;
scale_voltage = Software.Scale_1V;
scale_theta = scale/pi;
    
%% Scaling Controller
Controller.Scale.ADC2Fixed.Vphase = scale_adc_uint/Meas.VoltageReg*Controller.UmotorVoltage.Factor_u16/2^Controller.UmotorVoltage.Scale_u16;
%Controller.Scale.ADC2Fixed.Vphase = Controller.UmotorVoltage.Factor_u16/2^Controller.UmotorVoltage.Scale_u16;
Controller.Scale.ADC2Fixed.Vdc = scale_adc_uint/Meas.VoltageReg*Controller.UdcVoltage.Factor_u16/2^Controller.UdcVoltage.Scale_u16;
%Controller.Scale.ADC2Fixed.Vdc = Controller.UdcVoltage.Factor_u16/2^Controller.UdcVoltage.Scale_u16;
%----------------------------Added by Mohamed 08/07/2021 %-----------------
Controller.Scale.ADC2Fixed.Vgrid = scale_adc_uint/Meas.VoltageReg*Controller.UgridVoltage.Factor_u16/2^Controller.UgridVoltage.Scale_u16;
%-------------------------------------End----------------------------------
Controller.Scale.ADC2Fixed.Iphase = Meas.Rshunt*Meas.Gcurrent*scale_adc_uint/Meas.VoltageReg*Controller.PhaseCurrent.Factor_u16/2^Controller.PhaseCurrent.Scale_u16;
%Controller.Scale.ADC2Fixed.Iphase = Meas.Rshunt*scale_adc_uint/Meas.VoltageReg*Controller.PhaseCurrent.Factor_u16/2^Controller.PhaseCurrent.Scale_u16;
Controller.Scale.Product = scale;
Controller.Scale.Float2Fixed.alpha = scale_theta;
Controller.Scale.Float2Fixed.fel = scale_fel;

Controller.MovingWindow.MaxDeviation = int16(closedloop.EnableSynchronisationMaximumVoltageDeviation*scale_voltage);

%% 
%----------------------------Added Mohamed 25/11/2021 %--------------------

gear_ratio = 150*2; % flexstart gear_ratio: 150, between output shaft and rotating body: 2
Jappl_kgm2 = 4.92e-5; % Inertia of rotating body (Drehschieber), kgm2  

%-------------------------------------End----------------------------------
%sim('PMSM_1Ph_FlexStart_Appcon')