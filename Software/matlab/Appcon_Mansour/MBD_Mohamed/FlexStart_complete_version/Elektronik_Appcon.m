% function Elektronik_TEC152
%-------------------------------------------------------------------------------------------------
% (c) Miele & Cie. / Electronic
%
% Datum:       Name:          Aenderung:
% 12.05.2020   T.Rodehueser   Ersterstellung 


global EFU Software

EFU.Name = 'EFU_TEC152';

% Wertebereich der Datentypen
EFU.VarMax_s16          =  2^15 -1;            % Maximalwert s16
EFU.VarMin_s16          = -2^15;               % Minimalwert s16
EFU.VarMax_u16          =  2^16 -1;            % Maximalwert u16
EFU.VarMin_u16          =  0;                  % Minimalwert u16
EFU.VarMax_s32          =  2^31 -1;            % Maximalwert s16
EFU.VarMin_s32          = -2^31;               % Minimalwert s16
EFU.VarMax_u32          =  2^32 -1;            % Maximalwert u16
EFU.VarMin_u32          =  0;    

EFU.ADC_VRef            = 3.3;                 % Referenzspannung ADC in V
EFU.ADC_BIT             = 12;                  % 12 Bit ADC
EFU.CMP_BIT             = 6;                   % 6 Bit CMP


% Skalierung in der Software
Software.Scale_1V = 10;   % 1V --> 1000 Incremente -->(mV]
Software.Scale_1A = 1000000;%800000; %1000;   % 1A --> 1000 Incremente -->(mA]
Software.Scale_1W = 1000;   % 1W --> 1000 Incremente -->(mW]
Software.Scale_1rpm = 1;     % 1rpm --> 1rpm

% Spannungsmessung UDC
% EFU.Rv_Udc                      = 330e3*3;             % Vorwiderstand in Ohm
% EFU.Rm1_Udc                     = 8200;                % Messwiderstand in Ohm
% EFU.Rm2_Udc                     = 39200;                % Messwiderstand in Ohm
% EFU.Rm_Udc                      = (EFU.Rm1_Udc*EFU.Rm2_Udc)/(EFU.Rm1_Udc+EFU.Rm2_Udc); % Resuktierender Messwiderstand
% EFU.Cm_Udc                      = 2.2e-9;              % Filterkondensator

%----------------------------Added by Mohamed 24/08/2021%------------------
% Spannungsmessung UDC
EFU.Rv_Udc                      = 330000*3;             % Vorwiderstand in Ohm
EFU.Rm_Udc                      = 8200;                % Resuktierender Messwiderstand
EFU.Cm_Udc                      = 10e-9;              % Filterkondensator
%-------------------------------------End----------------------------------

% Spannungsmessung UDC
% EFU.Rv_Umotor                   = 69800*2;               % Vorwiderstand in Ohm
% EFU.Rm1_Umotor                  = 13000;                % Messwiderstand in Ohm
% EFU.Rm2_Umotor                  = 1000;                % Messwiderstand in Ohm
% EFU.Rm_Umotor                   = (EFU.Rm1_Umotor*EFU.Rm2_Umotor)/(EFU.Rm1_Umotor+EFU.Rm2_Umotor); % Resuktierender Messwiderstand
% EFU.Cm_Umotor                   = 2.2e-6;                % Filterkondensator

%----------------------------Added by Mohamed 24/08/2021%------------------
EFU.Rv_Umotor                   = 68000*3;  % Vorwiderstand in Ohm
EFU.Rm_Umotor                   = 1800;     % Resuktierender Messwiderstand
EFU.Cm_Umotor                   = 22e-9;    % Filterkondensator
%-------------------------------------End----------------------------------

% Strommessung Phasensstrom
% EFU.RShunt              = 16900;          % Strommess-Shunt in Ohm
% EFU.I_Verstaerkung      = 150e-6;         % Stromverstaerkung: 20V/V

%----------------------------Added by Mohamed 24/08/2021%------------------
EFU.RShunt              =  2;%100;            % Strommess-Shunt in Ohm
EFU.I_Verstaerkung      = 0.5;                % Stromverstaerkung: 20V/V
%-------------------------------------End----------------------------------

% Strommessung Zwischenkreisstrom
EFU.DC_RShunt              = 33e-3;         % Strommess-Shunt in Ohm
EFU.DC_I_Verstaerkung      = 20;            % Stromverstaerkung: 20V/V

%----------------------------Added by Mohamed 21/06/2021%------------------
EFU.Ri1_Umotor = 1000;
EFU.Ri2_Umotor = 1000;
EFU.Ri3_Umotor = 47;
EFU.Ci_Umotor  = 2.2e-9;
%--------------------------------------End---------------------------------

%----------------------------Added by Mohamed 21/06/2021%------------------
EFU.Rv_Ugrid  = 330e3*3;             
EFU.Rm_Ugrid  = 3.9e3;              
%---------------------------------------End--------------------------------

%% Berechnung der Parameter
% Zwischenkreisspannungmessung
EFU.udcSignalconditioning =(EFU.Rv_Udc+EFU.Rm_Udc)/EFU.Rm_Udc;
EFU.udcIncrements         = EFU.ADC_VRef/(2^EFU.ADC_BIT-1)*EFU.udcSignalconditioning;       % Zwischenkreisspannung in V/Inc

% Motorspannungsmessung
EFU.umotorSignalconditioning =(EFU.Rv_Umotor +EFU.Rm_Umotor )/EFU.Rm_Umotor;
EFU.umotorIncrements         = EFU.ADC_VRef/(2^EFU.ADC_BIT-1)*EFU.umotorSignalconditioning;       % Zwischenkreisspannung in V/Inc

% Strommessung
% EFU.iSignalconditioning = EFU.RShunt*EFU.I_Verstaerkung;
% EFU.iIncrements       = EFU.ADC_VRef/(2^EFU.ADC_BIT-1)/EFU.iSignalconditioning; % Normierung des Phasenstroms in A/Inc
% EFU.iOffset   = 0;

%----------------------------Added by Mohamed 24/8/2021%------------------
EFU.iSignalconditioning = EFU.RShunt*EFU.I_Verstaerkung;
EFU.iIncrements       = EFU.ADC_VRef/(2^EFU.ADC_BIT-1)/EFU.iSignalconditioning; % Normierung des Phasenstroms in A/Inc
EFU.iOffset   = (EFU.ADC_VRef/2)/(2^EFU.ADC_BIT-1)/EFU.iSignalconditioning;

%---------------------------------------End--------------------------------

% Strommessung
EFU.idcSignalconditioning = EFU.DC_RShunt*EFU.DC_I_Verstaerkung;
EFU.idcIncrements       = EFU.ADC_VRef/(2^EFU.ADC_BIT-1)/EFU.idcSignalconditioning; % Normierung des Phasenstroms in A/Inc
EFU.idcOffset   = 0;

% Comparator
% EFU.CMPSignalconditioning       =EFU.DC_RShunt*EFU.DC_I_Verstaerkung;
% EFU.CMPIncrements         = (2^EFU.CMP_BIT-1)/EFU.CMPSignalconditioning;  
% EFU.CMPOffset = 0;

%----------------------------Added by Mohamed 24/8/2021%------------------
EFU.CMPSignalconditioning       =EFU.RShunt*EFU.I_Verstaerkung;
EFU.CMPIncrements         = (2^EFU.CMP_BIT-1)/EFU.CMPSignalconditioning;  
EFU.CMPOffset = (EFU.ADC_VRef/2/EFU.ADC_VRef)*(2^EFU.CMP_BIT-1);
%---------------------------------------End--------------------------------

%----------------------------Added by Mohamed 21/06/2021%------------------
EFU.ugridSignalconditioning =(EFU.Rv_Ugrid+EFU.Rm_Ugrid)/EFU.Rm_Ugrid;
EFU.ugridIncrements=EFU.ADC_VRef/(2^EFU.ADC_BIT-1)*EFU.ugridSignalconditioning; 
%--------------------------------------------------------------------------

%% PWM Unit
% Initialisation of PWM Unit
HALCPU_SYSTEM_CLOCK=72e6;     % 72MHz System Clock
HALCPU_PWM3PHASE_FREQ = 8e3;  % 8kHz PWM Freqeuncy
HALCPU_PWM3PHASE_TPWM_COUNT =  (HALCPU_SYSTEM_CLOCK/2)/HALCPU_PWM3PHASE_FREQ;
HALCPU_IRQ_SYSTICK_FREQUENCY_HZ = 1000;
