classdef Electronics < Simulink.IntEnumType
  enumeration
    ON (0)
    OFF(1)
    Motor_220 (2)
    Motor_120 (3)
  end
end