classdef ControlModeState < Simulink.IntEnumType
  enumeration
    InitMode(0)
    StopMode(1)
    AlignmentMode(2)
    VFMode(3)
    ControlMode(4)
    ErrorMode(5)
  end
    
end

