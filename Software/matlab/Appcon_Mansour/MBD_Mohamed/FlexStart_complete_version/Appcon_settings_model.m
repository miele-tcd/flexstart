%% Appcon_settings_model
function [electronics]=Appcon_settings_model(with_electronics)
model = 'PMSM_1Ph_FlexStart_Appcon_complete';
open_system(model)
switch with_electronics
    case 0
        activeConfigObj = getActiveConfigSet(model);
        % get_param(activeConfigObj,'Name')
        set_param(activeConfigObj,'StopTime','tsim');
        set_param(activeConfigObj,'SolverType','Fixed-step');
        CurrentTimeStep = get_param(gcs, 'FixedStep');
        set_param(gcs,'FixedStep','Tsample')
        %set_param('PMSM_1Ph_FlexStart_Appcon_without_electronics/adaptation','commented','on')
        %set_param('PMSM_1Ph_FlexStart_Appcon/Controller_without_electronics','commented','on')
        %set_param('PMSM_1Ph_FlexStart_Appcon_without_electronics/Modulation','commented','off')
        %set_param('PMSM_1Ph_FlexStart_Appcon_without_electronics/MOSFET H-Bridge','commented','off')
        set_param('PMSM_1Ph_FlexStart_Appcon_complete/Modulation + Inverter','commented','off')
        set_param('PMSM_1Ph_FlexStart_Appcon_complete/RT3','commented','off')
        set_param('PMSM_1Ph_FlexStart_Appcon_complete/RT8','commented','off')
        set_param('PMSM_1Ph_FlexStart_Appcon_complete/RT16','commented','off')
        electronics=1;
    case 1
        activeConfigObj = getActiveConfigSet(model);
        % get_param(activeConfigObj,'Name')
        set_param(activeConfigObj,'StopTime','tsim');
        set_param(activeConfigObj,'SolverType','Fixed-step');
        CurrentTimeStep = get_param(gcs, 'FixedStep');
        set_param(gcs,'FixedStep','Inverter.TPWM')
        %set_param('PMSM_1Ph_FlexStart_Appcon_without_electronics/adaptation','commented','off')
        %set_param('PMSM_1Ph_FlexStart_Appcon/Controller_without_electronics','commented','off')
        %set_param('PMSM_1Ph_FlexStart_Appcon_without_electronics/Modulation','commented','on')
        %set_param('PMSM_1Ph_FlexStart_Appcon_without_electronics/MOSFET H-Bridge','commented','on')
        set_param('PMSM_1Ph_FlexStart_Appcon_complete/Modulation + Inverter','commented','on')
        set_param('PMSM_1Ph_FlexStart_Appcon_complete/RT3','commented','on')
        set_param('PMSM_1Ph_FlexStart_Appcon_complete/RT8','commented','on')
        set_param('PMSM_1Ph_FlexStart_Appcon_complete/RT16','commented','on')
        electronics=0;
end
end
