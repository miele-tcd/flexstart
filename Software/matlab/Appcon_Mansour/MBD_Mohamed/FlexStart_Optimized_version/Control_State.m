classdef Control_State < Simulink.IntEnumType
  enumeration
    Init (0)
    InitDone(1)
    Stop (2)
    StopDone(3)
    Alignment(4)
    AlignmentDone(5)
    VF(6)
    VFDone(7)
    Fault(8)
    FaultClear(9)
  end
    
end

