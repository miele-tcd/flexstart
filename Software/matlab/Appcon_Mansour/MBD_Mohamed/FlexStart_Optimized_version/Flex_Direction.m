classdef Flex_Direction < Simulink.IntEnumType
  enumeration
    CCW (0)
    CW (1)
  end
end