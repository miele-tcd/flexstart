% initialization for Library_Example_PMSM_1Ph.slx
% 
% load PMSM and Load data from <simparam.mat>, with the contents:
% - PMSM. <tunable parameter in ref. model>
%    - p                    : Pole pairs
%    - psiF20               : PM-Flux (Sum all Poles)/ Vs
%    - Rstr                 : Resistance (Sum all Poles) / Ohm
%    - Ls                   : Inductance with Saturation (Sum all Poles) / H
%    - Lookup.is_hat        : x-axis Lookup, akt. current / A
%    - Lookup.alpha_el_rad  : y-axis Lookup, akt. Rotorposition / rad
%    - Mrel                 : Reluctance Torque Amp. / Nm
%    - teta0                : Rotor natural angle / rad
%    - Jrotor_kgm2          : Rotor inertia (from CAD) / kg m2
%    - IronLoss.Mv          : Lookup Iron Loss (Torque Losses) / Nm
%    - IronLoss.n           : Lookup axis for Iron Losses / rpm
%    - IronLoss.is_hat      : Lookup axis for Iron Losses / A (Amplitude)
%    - ExtraLoss.Mv         : Lookup Friction Loss (Torque Losses) / Nm
%    - ExtraLoss.n          : Lookup axis for Friction Losses / rpm
% - Load. <tunable parameter outside of ref. model>
%    - M_W2_29mm                : Load W2 impeller 29 mm / Nm
%    - M_W2_33mm                : Load W2 impeller  33 mm / Nm
%    - n                        : x-axis Lookup, Rot. Speed / rpm
%
% Autor: Aryanti Putri (TCD/RD)
%        Tobias Rodeh�ser (SH/ATM)
% Version: 26.5.2021
% MATLAB-Version: R2019a
%
clc;
clear all
close all

%run Elektronik_EFU1301.m % Elektronik TEC, EFU1301
%-----------------Modified by Mansour 28/12/2021%--------------------------
%
Motor_model=Electronics.Motor_220; % Carried out Simulation of the 220 V of motor model  
%Motor_model=Electronics.Motor_120;  % Carried out Simulation of the 120 V of motor model 
run Elektronik_Appcon.m % Elektronik TEC, EFU1301
%--------------------------------------------------------------------------

%% Input
%simparam = 'SimParam_MppXX_3000wdg_0.224mm_20C_Lib.mat';
N1 = 3000; d1 = 0.224; % design 1: var 230 Vrms
N2 = 1800; d2 = 0.254; % var 100 Vrms
%----------------------------Added by Mansour 02/12/2021 %-----------------
N2 = N1;
d2 = d1;
%-----------------------------------End------------------------------------
Kadapt=(N1/N2)*(d1/d2);
Kadapt=(N1/N2);
%----------------------------Added by Mansour 02/12/2021 %-----------------
% N2 = N1;
% d2 = d1;
%-----------------------------------End------------------------------------
tsim = 4; % max. Simulation time / s
Tsample = 1e-6; % Sample Time
fel_rms = 10; % Frequency to create rms values

% alignment
t_align = 0.3; %0.3; %0.3; % time to perform alignment / s ( have to be greater than electric and mechanical time constant)

%----------------------------Added by Mohamed 21/06/2021 %-----------------
% open loop
cramp = 0; % a constant to create voltage Ramp for open loop / V (amplitude), Uramp = 2*pi*fel*cramp (cramp could be time dependent)
speed_vf_pos=750;   % [rpm] Speed command during Vf phase 
speed_vf_neg=-750;  % [rpm] Speed command during Vf phase 
%-----------------------------------End------------------------------------

% control
bemf_soll = 0; % BEMF value (zero --> current and bemf almost in phase)

% Speed
n_soll = [0; ...
        750]; % time / s; speed / rpm
    
% Temperaturen
Tcu = 20; % �C

%% Parameter for Motor / Elektronic
%----------------------------Added by Mohamed 08/07/2021%-----------------%
I_align = 0.02; %0.5; % Current Amplitude to perform alignment / A
Uboost_pos = 325; %85*Kadapt; % Boost Command / V (amplitude), boost value of the voltage to perform open loop, Uref = Uboost + Uramp, var 100 Vrms
Uboost_neg = 325*Kadapt;%72*Kadapt; % Boost Command / V (amplitude), boost value of the voltage to perform open loop, Uref = Uboost + Uramp, var 100 Vrms
start_angle_pos = pi/4; % voltage start angle for positive turning direction
start_angle_neg = 0; % voltage start angle for negative turning direction
%---------------------------------End--------------------------------------

%% Machine Parameter
%----------------------------Added by Mohamed 25/11/2021%------------------
PMSM.p = 4; % Pole pairs           
PMSM.psiF20 = 320e-3; % PM-Flux (Sum all Poles)/ Vs (Amplitude!), between 226e-3 ... 272e-3             %% KONTROLLE: Messung der induzierten Spannung, danach zur�ckrechnen    
PMSM.Rstr = 7.2e3; % Resistance (Sum all Poles) / Ohm, @20�C               
PMSM.Ls = 13; % Inductance (Sum all Poles) / H, Simulation 1,6 H                                       %% KONTROLLE: Messung mit Induktivit�tsmessger�t 
PMSM.Mrel = 17e-3; % Reluctance Torque Amp. / Nm, From FFT 1. Order Cogging Torque / Nm (between 9,5 ... 24 mNm)                 
PMSM.teta0 = deg2rad(42.6); % Rotor natural angle / rad (el.), 42.6� ... 48 � (el.)--> 10.65� ...  12� (mech.)               
PMSM.Jrotor_kgm2 = 1.45e-6; % Rotor inertia (from CAD) / kg m2                                          

PMSM.IronLoss.Mv = [0 0; 0 0]; % Lookup Iron Loss (Torque Losses) / Nm
PMSM.IronLoss.n = [0 1000]; % Lookup axis for Iron Losses / rpm         
PMSM.IronLoss.is_hat = [0 1]; % Lookup axis for Iron Losses / A (Amplitude)
PMSM.ExtraLoss.Mv = [0 0]; % Lookup Friction Loss (Torque Losses) / Nm
PMSM.ExtraLoss.n = [0 1000]; % Lookup axis for Friction Losses / rpm


PMSM.cT_wind = 3.93e-3; % Temperatur koeffizient Wicklung
PMSM.cT_PM = -0.0020; % Temperatur koeffizient Ferrit

PMSM.alpha0_rotor_rad = deg2rad(10.65); %  Rotor Start position / rad (mech.), between 5 ... 15�, Mittelwert 10.65
%---------------------------------End--------------------------------------
%% Load
%----------------------------Commented by Mohamed 25/11/2021%--------------
%Load.Mload = Load.M_W2_29mm;
%---------------------------------End--------------------------------------

%----------------------------Added by Mohamed 25/11/2021%------------------
Load.n = linspace(0,1500,100);
Load.Mload = (1.6e-9*Load.n.^2 + 6.9e-7*Load.n - 0.00072);   %% KONTROLLE: Messung bei unterschiedlichen Drehzahlen bzw. Frequenzen
Load.Mload(Load.Mload < 0) = 0;
%---------------------------------End--------------------------------------

%% Umrichter + Controller
Inverter.SlowInterrupt = 1/HALCPU_IRQ_SYSTICK_FREQUENCY_HZ; % 1 kHz interrupt, dt in s
Inverter.fPWM=HALCPU_PWM3PHASE_FREQ; % Switching frequency 
Inverter.TPWM=1/Inverter.fPWM; % 8 kHz interrupt, dt in s

% Motor and Electronic Parameter
Inverter.HalfBridge.Vdc = 325;  % DC Bus voltage / V 

% PWM
PWM_MAX_DUTYCYCLE = uint16(HALCPU_PWM3PHASE_TPWM_COUNT);


% ------------------------------Alignment----------------------------------
alignment.current=I_align; % Current reference
alignment.voltage=800; % Voltage reference
alignment.VoltageLimit=Inverter.HalfBridge.Vdc; %/sqrt(3);   % Voltage limit
alignment.time=t_align*1e3; % time to perform alignment / ms ( greater than L/R = 9.45 ms)
alignment.delay = alignment.time-(Inverter.SlowInterrupt/1e-3); % alignment time (in Stateflow in ms!)
alignment_voltage=alignment.voltage;
% --------------------------------Open Loop--------------------------------
openloop.SpeedMax=1250;%5000;          % Maximum speed of the motor / rpm (only in open loop)
openloop.SpeedVFLimit=750;             % Speed limit of the open loop --> control mode
openloop.pi=pi;                        % PI

%----------------------------Added by Mansour 08/02/2022 %-----------------
OffTime=uint16(2500);
Direction=Flex_Direction.CW;  % Direction during Vf phase
%Direction=Flex_Direction.CCW;  % Direction during Vf phase
%-----------------------------------End------------------------------------

% Current filter time constant for power approximation
CurrentFilterTimeConstant = 0.05; 


%% ------------------------Closed Loop | Fixed Point-----------------------
% Controller Board: signed integer 16 Bit, -2^15 ... 2^15-1 (-32768 ... 32767)
scale=2^15; 
scale_uint16 = 2^16;

% Lookup Table Cosinus 
x_int16 = int16(-scale:256:scale-1);
x_double = -pi:2*pi/length(x_int16):pi-2*pi/length(x_int16);
cos_x_int16 = int16(cos(x_double)*scale);

Controller.CosFcn.x = x_int16;
Controller.CosFcn.y = cos_x_int16;

% Open Loop variables
fixedPoint_SpeedVFLimit=int16(openloop.SpeedVFLimit*Software.Scale_1rpm);

% Scaling
scale_speed = Software.Scale_1rpm;
scale_fel = 1/(scale_speed*((2*pi)/60)*PMSM.p);
scale_current = Software.Scale_1A;
scale_voltage = Software.Scale_1V;
scale_theta = scale/pi;
    
%% Scaling Controller

Controller.Scale.Product = scale;

%% 
%----------------------------Added Mohamed 25/11/2021 %--------------------

gear_ratio = 150*2; % flexstart gear_ratio: 150, between output shaft and rotating body: 2
Jappl_kgm2 = 4.92e-5; % Inertia of rotating body (Drehschieber), kgm2  

%-------------------------------------End----------------------------------
