/*
 * File: Motor_1pHController.c
 *
 * Code generated for Simulink model 'Motor_1pHController'.
 *
 * Model version                  : 1.467
 * Simulink Coder version         : 9.1 (R2019a) 23-Nov-2018
 * C/C++ source code generated on : Thu Feb 17 15:09:33 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. Traceability
 *    3. Safety precaution
 *    4. RAM efficiency
 * Validation result: Not run
 */

#include "Motor_1pHController.h"
#include "Motor_1pHController_private.h"

/* Named constants for Chart: '<S2>/StateMachine8K' */
#define Motor_1pHCon_IN_NO_ACTIVE_CHILD ((uint8_T)0U)
#define Motor_1pHCont_IN_Alignment_Done ((uint8_T)1U)
#define Motor_1pHCont_IN_Alignment_Mode ((uint8_T)2U)
#define Motor_1pHControll_IN_Error_Mode ((uint8_T)3U)
#define Motor_1pHControlle_IN_Init_Mode ((uint8_T)1U)
#define Motor_1pHControlle_IN_Stop_Mode ((uint8_T)3U)
#define Motor_1pHController_IN_Motor_On ((uint8_T)2U)
#define Motor_1pHController_IN_VF_Mode ((uint8_T)4U)

/* Named constants for Chart: '<S1>/StateMachine1K' */
#define Motor_1pHC_IN_NO_ACTIVE_CHILD_c ((uint8_T)0U)
#define Motor_1pHCo_IN_Alignment_Mode_i ((uint8_T)1U)
#define Motor_1pHContro_IN_Error_Mode_h ((uint8_T)2U)
#define Motor_1pHControl_IN_Init_Mode_o ((uint8_T)1U)
#define Motor_1pHControl_IN_Stop_Mode_l ((uint8_T)3U)
#define Motor_1pHControll_IN_Motor_On_p ((uint8_T)2U)
#define Motor_1pHControlle_IN_VF_Mode_i ((uint8_T)3U)

/* Block signals (default storage) */
B_Motor_1pHController_T Motor_1pHController_B;

/* Block states (default storage) */
DW_Motor_1pHController_T Motor_1pHController_DW;

/* External inputs (root inport signals with default storage) */
ExtU_Motor_1pHController_T Motor_1pHController_U;

/* External outputs (root outports fed by signals with default storage) */
ExtY_Motor_1pHController_T Motor_1pHController_Y;

/* Real-time model */
RT_MODEL_Motor_1pHController_T Motor_1pHController_M_;
RT_MODEL_Motor_1pHController_T *const Motor_1pHController_M =
  &Motor_1pHController_M_;

/* Forward declaration for local functions */
static void Motor_1pHCont_OpenClosedHBridge(int16_T *voltage_dc, int16_T
  *rotor_angle, int16_T *voltage_reference, int16_T *Gain5, int16_T
  rtu_voltage_reference, int16_T rtu_vdc, int16_T *rty_Vs_s, uint16_T
  *rty_PWM_OFF, DW_Interrupt_8k_Motor_1pHCont_T *localDW);
uint32_T plook_u32s16_binckan(int16_T u, const int16_T bp[], uint32_T maxIndex)
{
  uint32_T bpIndex;

  /* Prelookup - Index only
     Index Search method: 'binary'
     Interpolation method: 'Use nearest'
     Extrapolation method: 'Clip'
     Use previous index: 'off'
     Use last breakpoint for index at or above upper limit: 'on'
     Remove protection against out-of-range input in generated code: 'off'
   */
  if (u <= bp[0U]) {
    bpIndex = 0U;
  } else if (u < bp[maxIndex]) {
    bpIndex = binsearch_u32s16(u, bp, maxIndex >> 1U, maxIndex);
    if ((bpIndex < maxIndex) && ((uint16_T)(bp[bpIndex + 1U] - u) <= (uint16_T)
         (u - bp[bpIndex]))) {
      bpIndex++;
    }
  } else {
    bpIndex = maxIndex;
  }

  return bpIndex;
}

uint32_T binsearch_u32s16(int16_T u, const int16_T bp[], uint32_T startIndex,
  uint32_T maxIndex)
{
  uint32_T bpIndex;
  uint32_T iRght;
  uint32_T bpIdx;

  /* Binary Search */
  bpIdx = startIndex;
  bpIndex = 0U;
  iRght = maxIndex;
  while (iRght - bpIndex > 1U) {
    if (u < bp[bpIdx]) {
      iRght = bpIdx;
    } else {
      bpIndex = bpIdx;
    }

    bpIdx = (iRght + bpIndex) >> 1U;
  }

  return bpIndex;
}

void mul_wide_s32(int32_T in0, int32_T in1, uint32_T *ptrOutBitsHi, uint32_T
                  *ptrOutBitsLo)
{
  uint32_T absIn0;
  uint32_T absIn1;
  uint32_T in0Lo;
  uint32_T in0Hi;
  uint32_T in1Hi;
  uint32_T productHiLo;
  uint32_T productLoHi;
  absIn0 = in0 < 0 ? ~(uint32_T)in0 + 1U : (uint32_T)in0;
  absIn1 = in1 < 0 ? ~(uint32_T)in1 + 1U : (uint32_T)in1;
  in0Hi = absIn0 >> 16U;
  in0Lo = absIn0 & 65535U;
  in1Hi = absIn1 >> 16U;
  absIn0 = absIn1 & 65535U;
  productHiLo = in0Hi * absIn0;
  productLoHi = in0Lo * in1Hi;
  absIn0 *= in0Lo;
  absIn1 = 0U;
  in0Lo = (productLoHi << /*MW:OvBitwiseOk*/ 16U) + /*MW:OvCarryOk*/ absIn0;
  if (in0Lo < absIn0) {
    absIn1 = 1U;
  }

  absIn0 = in0Lo;
  in0Lo += /*MW:OvCarryOk*/ productHiLo << /*MW:OvBitwiseOk*/ 16U;
  if (in0Lo < absIn0) {
    absIn1++;
  }

  absIn0 = (((productLoHi >> 16U) + (productHiLo >> 16U)) + in0Hi * in1Hi) +
    absIn1;
  if ((in0 != 0) && ((in1 != 0) && ((in0 > 0) != (in1 > 0)))) {
    absIn0 = ~absIn0;
    in0Lo = ~in0Lo;
    in0Lo++;
    if (in0Lo == 0U) {
      absIn0++;
    }
  }

  *ptrOutBitsHi = absIn0;
  *ptrOutBitsLo = in0Lo;
}

int32_T mul_s32_hiSR(int32_T a, int32_T b, uint32_T aShift)
{
  uint32_T u32_chi;
  uint32_T u32_clo;
  mul_wide_s32(a, b, &u32_chi, &u32_clo);
  return (int32_T)u32_chi >> aShift;
}

int32_T mul_s32_loSR(int32_T a, int32_T b, uint32_T aShift)
{
  uint32_T u32_chi;
  uint32_T u32_clo;
  mul_wide_s32(a, b, &u32_chi, &u32_clo);
  u32_clo = u32_chi << /*MW:OvBitwiseOk*/ (32U - aShift) | u32_clo >> aShift;
  return (int32_T)u32_clo;
}

int32_T div_s32_floor(int32_T numerator, int32_T denominator)
{
  int32_T quotient;
  uint32_T absNumerator;
  uint32_T absDenominator;
  uint32_T tempAbsQuotient;
  boolean_T quotientNeedsNegation;
  if (denominator == 0) {
    quotient = numerator >= 0 ? MAX_int32_T : MIN_int32_T;

    /* Divide by zero handler */
  } else {
    absNumerator = numerator < 0 ? ~(uint32_T)numerator + 1U : (uint32_T)
      numerator;
    absDenominator = denominator < 0 ? ~(uint32_T)denominator + 1U : (uint32_T)
      denominator;
    quotientNeedsNegation = ((numerator < 0) != (denominator < 0));
    tempAbsQuotient = absNumerator / absDenominator;
    if (quotientNeedsNegation) {
      absNumerator %= absDenominator;
      if (absNumerator > 0U) {
        tempAbsQuotient++;
      }
    }

    quotient = quotientNeedsNegation ? -(int32_T)tempAbsQuotient : (int32_T)
      tempAbsQuotient;
  }

  return quotient;
}

/* Output and update for function-call system: '<S8>/VFStart' */
void Motor_1pHController_VFStart(int16_T rtu_speed_reference, int16_T
  rtu_rotor_start_positive, int16_T rtu_rotor_start_negative, int16_T
  *rty_rotor_position_reference)
{
  /* Switch: '<S11>/Switch' */
  if (rtu_speed_reference > 0) {
    *rty_rotor_position_reference = rtu_rotor_start_positive;
  } else {
    *rty_rotor_position_reference = rtu_rotor_start_negative;
  }

  /* End of Switch: '<S11>/Switch' */
}

/* System initialize for function-call system: '<S8>/VFDone_Flag' */
void Motor_1pHC_VFDone_Flag_Init(ControlFlag *rty_control_flag)
{
  /* SystemInitialize for Outport: '<S10>/control_flag' */
  *rty_control_flag = VF;
}

/* Output and update for function-call system: '<S8>/VFDone_Flag' */
void Motor_1pHContro_VFDone_Flag(int16_T rtu_speed_reference, boolean_T
  rtu_start_ctr, ControlFlag *rty_control_flag)
{
  int16_T rtu_speed_reference_0;

  /* Abs: '<S10>/Abs' */
  if (rtu_speed_reference < 0) {
    rtu_speed_reference_0 = (int16_T)-rtu_speed_reference;
  } else {
    rtu_speed_reference_0 = rtu_speed_reference;
  }

  /* End of Abs: '<S10>/Abs' */

  /* Switch: '<S10>/Switch' incorporates:
   *  Constant: '<S17>/Constant'
   *  Constant: '<S18>/Constant'
   *  Constant: '<S19>/Constant'
   *  Logic: '<S10>/Logical Operator1'
   *  RelationalOperator: '<S19>/Compare'
   */
  if ((rtu_speed_reference_0 > 750) && rtu_start_ctr) {
    *rty_control_flag = VFDone;
  } else {
    *rty_control_flag = VF;
  }

  /* End of Switch: '<S10>/Switch' */
}

/* Output and update for function-call system: '<S8>/fault.fault' */
void Motor_1pHControl_faultfault(boolean_T rtu_fault1, boolean_T rtu_fault2,
  boolean_T *rty_fault)
{
  /* Logic: '<S13>/Logical Operator1' incorporates:
   *  Logic: '<S13>/Logical Operator2'
   */
  *rty_fault = ((!rtu_fault1) || rtu_fault2);
}

/* Output and update for function-call system: '<S8>/fault.Protections' */
void Motor_1pHC_faultProtections(boolean_T rtu_fault_clear, ControlFlag
  *rty_control_flag)
{
  /* Switch: '<S12>/Switch' incorporates:
   *  Constant: '<S20>/Constant'
   *  Constant: '<S21>/Constant'
   */
  if (rtu_fault_clear) {
    *rty_control_flag = FaultClear;
  } else {
    *rty_control_flag = Fault;
  }

  /* End of Switch: '<S12>/Switch' */
}

/* Start for function-call system: '<S8>/state_variable_init' */
void M_state_variable_init_Start(int16_T *rty_OL_voltage_state_is, int16_T
  *rty_OL_voltage_state_bemf, int16_T *rty_OL_speed_state, int16_T
  *rty_OLvoltage_sat, int16_T *rty_OLspeed_sat)
{
  /* Start for Constant: '<S15>/Constant1' */
  *rty_OL_voltage_state_is = 0;

  /* Start for Constant: '<S15>/Constant2' */
  *rty_OL_voltage_state_bemf = 0;

  /* Start for Constant: '<S15>/Constant3' */
  *rty_OL_speed_state = 0;

  /* Start for Constant: '<S15>/Constant4' */
  *rty_OLvoltage_sat = 0;

  /* Start for Constant: '<S15>/Constant5' */
  *rty_OLspeed_sat = 0;
}

/* Output and update for function-call system: '<S8>/state_variable_init' */
void Motor_1_state_variable_init(int16_T *rty_OL_voltage_state_is, int16_T
  *rty_OL_voltage_state_bemf, int16_T *rty_OL_speed_state, int16_T
  *rty_OLvoltage_sat, int16_T *rty_OLspeed_sat)
{
  /* Constant: '<S15>/Constant1' */
  *rty_OL_voltage_state_is = 0;

  /* Constant: '<S15>/Constant2' */
  *rty_OL_voltage_state_bemf = 0;

  /* Constant: '<S15>/Constant3' */
  *rty_OL_speed_state = 0;

  /* Constant: '<S15>/Constant4' */
  *rty_OLvoltage_sat = 0;

  /* Constant: '<S15>/Constant5' */
  *rty_OLspeed_sat = 0;
}

/* Output and update for function-call system: '<S8>/SpeedToAngle' */
void Motor_1pHContr_SpeedToAngle(int16_T rtu_speed_reference, int16_T
  rtu_angle_reference, int16_T *rty_rotor_position_reference)
{
  /* Sum: '<S16>/Add1' incorporates:
   *  Gain: '<S16>/Gain1'
   *  Gain: '<S16>/Gain2'
   *  Gain: '<S16>/Gain3'
   */
  *rty_rotor_position_reference = (int16_T)((int16_T)mul_s32_hiSR(274877907,
    mul_s32_loSR(286331153, rtu_speed_reference << 15, 31U), 9U) +
    rtu_angle_reference);
}

/* Output and update for function-call system: '<S8>/modulation' */
void Motor_1pHControl_modulation(int16_T rtu_voltage_reference, int16_T
  rtu_rotor_angle, int16_T rtu_voltage_dc, uint16_T *rty_PWM_CMD1, uint16_T
  *rty_PWM_CMD2, int16_T *rty_InverterOutputVoltage)
{
  int16_T rtb_Gain;

  /* Switch: '<S23>/Switch2' incorporates:
   *  RelationalOperator: '<S23>/LowerRelop1'
   */
  if (rtu_voltage_reference > rtu_voltage_dc) {
    rtb_Gain = rtu_voltage_dc;
  } else {
    /* Gain: '<S22>/Gain' */
    rtb_Gain = (int16_T)-rtu_voltage_dc;

    /* Switch: '<S23>/Switch' incorporates:
     *  RelationalOperator: '<S23>/UpperRelop'
     */
    if (rtu_voltage_reference >= rtb_Gain) {
      rtb_Gain = rtu_voltage_reference;
    }

    /* End of Switch: '<S23>/Switch' */
  }

  /* End of Switch: '<S23>/Switch2' */

  /* Gain: '<S22>/Gain5' incorporates:
   *  Lookup_n-D: '<S22>/1-D Lookup Table'
   *  Product: '<S22>/Product'
   */
  *rty_InverterOutputVoltage = (int16_T)((rtb_Gain *
    Motor_1pHController_ConstP.uDLookupTable_tableData[plook_u32s16_binckan
    (rtu_rotor_angle, Motor_1pHController_ConstP.uDLookupTable_bp01Data, 255U)])
    >> 15);

  /* Gain: '<S22>/Gain2' incorporates:
   *  Product: '<S22>/Divide1'
   *  Product: '<S22>/Product1'
   */
  rtb_Gain = (int16_T)((div_s32_floor(147451500, rtu_voltage_dc) *
                        *rty_InverterOutputVoltage) >> 15);

  /* Switch: '<S22>/Switch' */
  if (rtb_Gain >= 0) {
    /* DataTypeConversion: '<S14>/Data Type Conversion2' */
    *rty_PWM_CMD1 = (uint16_T)rtb_Gain;
  } else {
    /* DataTypeConversion: '<S14>/Data Type Conversion2' incorporates:
     *  Constant: '<S22>/Constant1'
     */
    *rty_PWM_CMD1 = 0U;
  }

  /* End of Switch: '<S22>/Switch' */

  /* Switch: '<S22>/Switch1' */
  if (rtb_Gain > 0) {
    /* DataTypeConversion: '<S14>/Data Type Conversion1' incorporates:
     *  Constant: '<S22>/Constant1'
     */
    *rty_PWM_CMD2 = 0U;
  } else {
    /* DataTypeConversion: '<S14>/Data Type Conversion1' incorporates:
     *  Gain: '<S22>/Gain1'
     */
    *rty_PWM_CMD2 = (uint16_T)-rtb_Gain;
  }

  /* End of Switch: '<S22>/Switch1' */
}

/* Function for Chart: '<S2>/StateMachine8K' */
static void Motor_1pHCont_OpenClosedHBridge(int16_T *voltage_dc, int16_T
  *rotor_angle, int16_T *voltage_reference, int16_T *Gain5, int16_T
  rtu_voltage_reference, int16_T rtu_vdc, int16_T *rty_Vs_s, uint16_T
  *rty_PWM_OFF, DW_Interrupt_8k_Motor_1pHCont_T *localDW)
{
  uint16_T DataTypeConversion2;
  uint16_T DataTypeConversion1;

  /*   && (bemf_vf==1) */
  if (localDW->MaskFlag == 1) {
    /* Chart: '<S2>/StateMachine8K' */
    *rty_PWM_OFF = 1U;
  } else {
    /* Chart: '<S2>/StateMachine8K' */
    *rty_PWM_OFF = 0U;
    *voltage_reference = rtu_voltage_reference;
    *rotor_angle = localDW->rotor_position_reference;

    /* Chart: '<S2>/StateMachine8K' */
    *voltage_dc = rtu_vdc;

    /* Outputs for Function Call SubSystem: '<S8>/modulation' */
    Motor_1pHControl_modulation(*voltage_reference, *rotor_angle, *voltage_dc,
      &DataTypeConversion2, &DataTypeConversion1, Gain5);

    /* End of Outputs for SubSystem: '<S8>/modulation' */

    /* Chart: '<S2>/StateMachine8K' */
    *rty_Vs_s = *Gain5;
  }
}

/* System initialize for function-call system: '<Root>/Interrupt_8k' */
void Motor_1pH_Interrupt_8k_Init(ControlFlag *rty_controller_State, int16_T
  *rty_Vs_s, uint16_T *rty_PWM_OFF, DW_Interrupt_8k_Motor_1pHCont_T *localDW)
{
  ControlFlag Switch_f;
  localDW->is_Motor_On = Motor_1pHCon_IN_NO_ACTIVE_CHILD;
  localDW->temporalCounter_i1 = 0U;
  localDW->is_active_c1_Motor_1pHControlle = 0U;
  localDW->is_c1_Motor_1pHController = Motor_1pHCon_IN_NO_ACTIVE_CHILD;
  localDW->MaskFlag = 0U;
  localDW->rotor_position_reference = 0;
  *rty_controller_State = Init;
  *rty_PWM_OFF = 0U;
  *rty_Vs_s = 0;
  localDW->previousTicks = 0U;

  /* SystemInitialize for Chart: '<S2>/StateMachine8K' incorporates:
   *  SubSystem: '<S8>/VFDone_Flag'
   */
  Motor_1pHC_VFDone_Flag_Init(&Switch_f);
}

/* Enable for function-call system: '<Root>/Interrupt_8k' */
void Motor_1_Interrupt_8k_Enable(RT_MODEL_Motor_1pHController_T * const
  Motor_1pHController_M, DW_Interrupt_8k_Motor_1pHCont_T *localDW)
{
  /* Enable for Chart: '<S2>/StateMachine8K' */
  localDW->previousTicks = Motor_1pHController_M->Timing.clockTick0;
}

/* Start for function-call system: '<Root>/Interrupt_8k' */
void Motor_1p_Interrupt_8k_Start(void)
{
  int16_T Constant1;
  int16_T Constant2;
  int16_T Constant3;
  int16_T Constant4;
  int16_T Constant5;

  /* Start for Chart: '<S2>/StateMachine8K' incorporates:
   *  SubSystem: '<S8>/state_variable_init'
   */
  M_state_variable_init_Start(&Constant1, &Constant2, &Constant3, &Constant4,
    &Constant5);
}

/* Output and update for function-call system: '<Root>/Interrupt_8k' */
void Motor_1pHContr_Interrupt_8k(RT_MODEL_Motor_1pHController_T * const
  Motor_1pHController_M, boolean_T rtu_fault_clear, boolean_T rtu_Direction,
  ControlModeState rtu_controller_mode, int16_T rtu_speed_reference, int16_T
  rtu_voltage_reference, uint16_T rtu_align_delay, boolean_T rtu_start_ctr,
  int16_T rtu_vdc, int16_T rtu_start_angle_pos, int16_T rtu_start_angle_neg,
  boolean_T rtu_fault1, boolean_T rtu_fault2, ControlFlag *rty_controller_State,
  int16_T *rty_Vs_s, uint16_T *rty_PWM_OFF, DW_Interrupt_8k_Motor_1pHCont_T
  *localDW)
{
  int16_T Constant5;
  uint32_T elapsedTicks;
  int16_T voltage_dc;
  int16_T rotor_angle;
  int16_T voltage_reference;
  boolean_T LogicalOperator1;
  int16_T Gain5;
  uint16_T DataTypeConversion2;
  uint16_T DataTypeConversion1;
  int32_T tmp;

  /* Chart: '<S2>/StateMachine8K' */
  elapsedTicks = Motor_1pHController_M->Timing.clockTick0 -
    localDW->previousTicks;
  localDW->previousTicks = Motor_1pHController_M->Timing.clockTick0;
  if (localDW->temporalCounter_i1 + elapsedTicks <= 524287U) {
    localDW->temporalCounter_i1 += elapsedTicks;
  } else {
    localDW->temporalCounter_i1 = 524287U;
  }

  if (localDW->is_active_c1_Motor_1pHControlle == 0U) {
    localDW->is_active_c1_Motor_1pHControlle = 1U;
    localDW->is_c1_Motor_1pHController = Motor_1pHControlle_IN_Init_Mode;
    *rty_controller_State = Init;

    /*  flag */
    localDW->rotor_position_reference = 0;
    *rty_PWM_OFF = 1U;

    /* Outputs for Function Call SubSystem: '<S8>/state_variable_init' */
    Motor_1_state_variable_init(&voltage_dc, &rotor_angle, &voltage_reference,
      &Gain5, &Constant5);

    /* End of Outputs for SubSystem: '<S8>/state_variable_init' */
    *rty_controller_State = InitDone;

    /*  flag */
  } else {
    switch (localDW->is_c1_Motor_1pHController) {
     case Motor_1pHControlle_IN_Init_Mode:
      if (rtu_controller_mode == StopMode) {
        localDW->is_c1_Motor_1pHController = Motor_1pHControlle_IN_Stop_Mode;
        *rty_controller_State = Stop;

        /*  flag */
        *rty_PWM_OFF = 1U;
        localDW->rotor_position_reference = 0;
      }
      break;

     case Motor_1pHController_IN_Motor_On:
      if (rtu_controller_mode == StopMode) {
        /* Outputs for Function Call SubSystem: '<S8>/state_variable_init' */
        Motor_1_state_variable_init(&voltage_dc, &rotor_angle,
          &voltage_reference, &Gain5, &Constant5);

        /* End of Outputs for SubSystem: '<S8>/state_variable_init' */
        /* controller_flag=Init; */
        if (localDW->is_Motor_On == Motor_1pHController_IN_VF_Mode) {
          /*  nur in vf durchgef�hrt */
          localDW->is_Motor_On = Motor_1pHCon_IN_NO_ACTIVE_CHILD;
        } else {
          localDW->is_Motor_On = Motor_1pHCon_IN_NO_ACTIVE_CHILD;
        }

        localDW->is_c1_Motor_1pHController = Motor_1pHControlle_IN_Stop_Mode;
        *rty_controller_State = Stop;

        /*  flag */
        *rty_PWM_OFF = 1U;
        localDW->rotor_position_reference = 0;
      } else {
        switch (localDW->is_Motor_On) {
         case Motor_1pHCont_IN_Alignment_Done:
          if (((rtu_controller_mode == VFMode) & (localDW->temporalCounter_i1 >=
                20000U)) != 0) {
            localDW->is_Motor_On = Motor_1pHController_IN_VF_Mode;

            /* Outputs for Function Call SubSystem: '<S8>/VFStart' */
            /* rotor_position_reference = VFStart(speed_reference,start_angle_pos,start_angle_neg); % initialisierung f�r modulation */
            Motor_1pHController_VFStart((int16_T)rtu_Direction,
              rtu_start_angle_pos, rtu_start_angle_neg,
              &localDW->rotor_position_reference);

            /* End of Outputs for SubSystem: '<S8>/VFStart' */
            /*  initialisierung f�r modulation */
            localDW->MaskFlag = 0U;

            /* controller_flag = VFDone_Flag(abs(speed_reference),Vf_Done_SynchronisationEnable);      % Commented by Mohamed    */
            if (rtu_speed_reference < 0) {
              tmp = -rtu_speed_reference;
              if (tmp > 32767) {
                tmp = 32767;
              }

              voltage_dc = (int16_T)tmp;
            } else {
              voltage_dc = rtu_speed_reference;
            }

            /* Outputs for Function Call SubSystem: '<S8>/VFDone_Flag' */
            Motor_1pHContro_VFDone_Flag(voltage_dc, rtu_start_ctr,
              rty_controller_State);

            /* End of Outputs for SubSystem: '<S8>/VFDone_Flag' */
            /*  Added by Mohamed  */
            if (rtu_speed_reference < 0) {
              tmp = -rtu_speed_reference;
              if (tmp > 32767) {
                tmp = 32767;
              }

              voltage_dc = (int16_T)tmp;
            } else {
              voltage_dc = rtu_speed_reference;
            }

            /* Outputs for Function Call SubSystem: '<S8>/SpeedToAngle' */
            Motor_1pHContr_SpeedToAngle(voltage_dc,
              localDW->rotor_position_reference,
              &localDW->rotor_position_reference);

            /* End of Outputs for SubSystem: '<S8>/SpeedToAngle' */
            /* [SpeedReference_PT1,PT1_StateVariable] = SpeedRateLimit(0,abs(speed_reference)*scale); % PT1 Filter
               VF */
            Motor_1pHCont_OpenClosedHBridge(&voltage_dc, &rotor_angle,
              &voltage_reference, &Gain5, rtu_voltage_reference, rtu_vdc,
              rty_Vs_s, rty_PWM_OFF, localDW);
          } else {
            *rty_controller_State = AlignmentDone;

            /*  flag  */
            *rty_Vs_s = 0;
            *rty_PWM_OFF = 1U;
          }
          break;

         case Motor_1pHCont_IN_Alignment_Mode:
          if (localDW->temporalCounter_i1 >= (uint32_T)(rtu_align_delay << 3)) {
            localDW->is_Motor_On = Motor_1pHCont_IN_Alignment_Done;
            localDW->temporalCounter_i1 = 0U;
            *rty_controller_State = AlignmentDone;

            /*  flag  */
            *rty_Vs_s = 0;
            *rty_PWM_OFF = 1U;
          } else {
            /* Outputs for Function Call SubSystem: '<S8>/fault.fault' */
            Motor_1pHControl_faultfault(rtu_fault1, rtu_fault2,
              &LogicalOperator1);

            /* End of Outputs for SubSystem: '<S8>/fault.fault' */
            if (((rtu_controller_mode == ErrorMode) | LogicalOperator1) != 0) {
              localDW->is_Motor_On = Motor_1pHControll_IN_Error_Mode;
              *rty_PWM_OFF = 1U;

              /* Outputs for Function Call SubSystem: '<S8>/fault.Protections' */
              Motor_1pHC_faultProtections(rtu_fault_clear, rty_controller_State);

              /* End of Outputs for SubSystem: '<S8>/fault.Protections' */
            } else {
              /* Outputs for Function Call SubSystem: '<S8>/modulation' */
              /* is_adc =  iRshunt1_adc;
                 [voltage_ref,OL_voltage_state_is] = PI_Controller(current_reference,is_adc,fixedPoint_IsKp_int16,fixedPoint_IsKi_int16,OL_voltage_state_is,-vdc,vdc);
                 [PWM_CMD1,PWM_CMD2,InvVoltageOut] = modulation(voltage_ref,0,vdc); */
              Motor_1pHControl_modulation(800, 0, rtu_vdc, &DataTypeConversion2,
                &DataTypeConversion1, rty_Vs_s);

              /* End of Outputs for SubSystem: '<S8>/modulation' */
            }
          }
          break;

         case Motor_1pHControll_IN_Error_Mode:
          /* Outputs for Function Call SubSystem: '<S8>/fault.Protections' */
          Motor_1pHC_faultProtections(rtu_fault_clear, rty_controller_State);

          /* End of Outputs for SubSystem: '<S8>/fault.Protections' */
          break;

         default:
          /* Outputs for Function Call SubSystem: '<S8>/fault.fault' */
          /* case IN_VF_Mode: */
          Motor_1pHControl_faultfault(rtu_fault1, rtu_fault2, &LogicalOperator1);

          /* End of Outputs for SubSystem: '<S8>/fault.fault' */
          if (((rtu_controller_mode == ErrorMode) | LogicalOperator1) != 0) {
            /*  nur in vf durchgef�hrt */
            localDW->is_Motor_On = Motor_1pHControll_IN_Error_Mode;
            *rty_PWM_OFF = 1U;

            /* Outputs for Function Call SubSystem: '<S8>/fault.Protections' */
            Motor_1pHC_faultProtections(rtu_fault_clear, rty_controller_State);

            /* End of Outputs for SubSystem: '<S8>/fault.Protections' */
          } else {
            /* controller_flag = VFDone_Flag(abs(speed_reference),Vf_Done_SynchronisationEnable);      % Commented by Mohamed    */
            if (rtu_speed_reference < 0) {
              tmp = -rtu_speed_reference;
              if (tmp > 32767) {
                tmp = 32767;
              }

              voltage_dc = (int16_T)tmp;
            } else {
              voltage_dc = rtu_speed_reference;
            }

            /* Outputs for Function Call SubSystem: '<S8>/VFDone_Flag' */
            Motor_1pHContro_VFDone_Flag(voltage_dc, rtu_start_ctr,
              rty_controller_State);

            /* End of Outputs for SubSystem: '<S8>/VFDone_Flag' */
            /*  Added by Mohamed  */
            if (rtu_speed_reference < 0) {
              tmp = -rtu_speed_reference;
              if (tmp > 32767) {
                tmp = 32767;
              }

              voltage_dc = (int16_T)tmp;
            } else {
              voltage_dc = rtu_speed_reference;
            }

            /* Outputs for Function Call SubSystem: '<S8>/SpeedToAngle' */
            Motor_1pHContr_SpeedToAngle(voltage_dc,
              localDW->rotor_position_reference,
              &localDW->rotor_position_reference);

            /* End of Outputs for SubSystem: '<S8>/SpeedToAngle' */
            /* [SpeedReference_PT1,PT1_StateVariable] = SpeedRateLimit(0,abs(speed_reference)*scale); % PT1 Filter
               VF */
            Motor_1pHCont_OpenClosedHBridge(&voltage_dc, &rotor_angle,
              &voltage_reference, &Gain5, rtu_voltage_reference, rtu_vdc,
              rty_Vs_s, rty_PWM_OFF, localDW);

            /*  auch in PI.bemf und PI.voltage upgedated */
          }
          break;
        }
      }
      break;

     default:
      /* case IN_Stop_Mode: */
      if (rtu_controller_mode == AlignmentMode) {
        localDW->is_c1_Motor_1pHController = Motor_1pHController_IN_Motor_On;
        localDW->is_Motor_On = Motor_1pHCont_IN_Alignment_Mode;
        localDW->temporalCounter_i1 = 0U;
        *rty_controller_State = Alignment;

        /*  flag */
        *rty_PWM_OFF = 0U;

        /* Outputs for Function Call SubSystem: '<S8>/modulation' */
        /* is_adc =  iRshunt1_adc;
           [voltage_ref,OL_voltage_state_is] = PI_Controller(current_reference,is_adc,fixedPoint_IsKp_int16,fixedPoint_IsKi_int16,OL_voltage_state_is,-vdc,vdc);
           [PWM_CMD1,PWM_CMD2,InvVoltageOut] = modulation(voltage_ref,0,vdc); */
        Motor_1pHControl_modulation(800, 0, rtu_vdc, &DataTypeConversion2,
          &DataTypeConversion1, rty_Vs_s);

        /* End of Outputs for SubSystem: '<S8>/modulation' */
      }
      break;
    }
  }

  /* End of Chart: '<S2>/StateMachine8K' */
}

/* System initialize for function-call system: '<S3>/alignment_1k' */
void Motor_1pH_alignment_1k_Init(int16_T *rty_voltage_reference, int16_T
  *rty_bemf_reference)
{
  /* SystemInitialize for Outport: '<S4>/voltage_reference' */
  *rty_voltage_reference = 0;

  /* SystemInitialize for Outport: '<S4>/bemf_reference' */
  *rty_bemf_reference = 0;
}

/* Start for function-call system: '<S3>/alignment_1k' */
void Motor_1p_alignment_1k_Start(int16_T *rty_speed_reference)
{
  /* Start for Constant: '<S4>/Constant2' */
  *rty_speed_reference = 0;
}

/* Output and update for function-call system: '<S3>/alignment_1k' */
void Motor_1pHContr_alignment_1k(int16_T rtu_speed_command, int16_T
  rtu_current_command, int16_T *rty_speed_reference, int16_T
  *rty_voltage_reference, int32_T *rty_current_reference, int16_T
  *rty_bemf_reference)
{
  /* Constant: '<S4>/Constant2' */
  *rty_speed_reference = 0;

  /* Constant: '<S4>/Constant1' */
  *rty_voltage_reference = 0;

  /* Switch: '<S4>/Switch' incorporates:
   *  Gain: '<S4>/Gain'
   */
  if (rtu_speed_command > 0) {
    *rty_current_reference = rtu_current_command << 15;
  } else {
    *rty_current_reference = -32768 * rtu_current_command;
  }

  /* End of Switch: '<S4>/Switch' */

  /* Constant: '<S4>/Constant3' */
  *rty_bemf_reference = 0;
}

/* System initialize for function-call system: '<S3>/vf_1k' */
void Motor_1pHControl_vf_1k_Init(int16_T *rty_current_reference, int16_T
  *rty_bemf_reference)
{
  /* SystemInitialize for Outport: '<S5>/current_reference' */
  *rty_current_reference = 0;

  /* SystemInitialize for Outport: '<S5>/bemf_reference' */
  *rty_bemf_reference = 0;
}

/* Output and update for function-call system: '<S3>/vf_1k' */
void Motor_1pHController_vf_1k(int16_T rtu_speed_command_ramp, int16_T
  rtu_speed_command_ramp_vf_pos, int16_T rtu_speed_command_ramp_vf_neg, int16_T
  rtu_ramp_command, int16_T rtu_boost_command_pos, int16_T rtu_boost_command_neg,
  int16_T rtu_vdc, int16_T *rty_speed_reference, int16_T *rty_voltage_reference,
  int16_T *rty_current_reference, int16_T *rty_bemf_reference)
{
  int16_T rtb_Add1;
  int16_T rtb_Gain;

  /* Abs: '<S5>/Abs' */
  if (rtu_speed_command_ramp < 0) {
    rtb_Add1 = (int16_T)-rtu_speed_command_ramp;
  } else {
    rtb_Add1 = rtu_speed_command_ramp;
  }

  /* End of Abs: '<S5>/Abs' */

  /* Switch: '<S5>/Switch1' */
  if (rtu_speed_command_ramp >= 0) {
    rtb_Gain = rtu_boost_command_pos;
  } else {
    rtb_Gain = rtu_boost_command_neg;
  }

  /* End of Switch: '<S5>/Switch1' */

  /* Sum: '<S7>/Add1' incorporates:
   *  Gain: '<S7>/Gain'
   *  Gain: '<S7>/Gain1'
   *  Product: '<S7>/Product1'
   *  Product: '<S7>/Product3'
   */
  rtb_Add1 = (int16_T)(((((rtb_Add1 * 13726) >> 15) * rtu_ramp_command) >> 15) +
                       rtb_Gain);

  /* Switch: '<S6>/Switch2' incorporates:
   *  RelationalOperator: '<S6>/LowerRelop1'
   */
  if (rtb_Add1 > rtu_vdc) {
    *rty_voltage_reference = rtu_vdc;
  } else {
    /* Gain: '<S5>/Gain' */
    rtb_Gain = (int16_T)-rtu_vdc;

    /* Switch: '<S6>/Switch' incorporates:
     *  RelationalOperator: '<S6>/UpperRelop'
     */
    if (rtb_Add1 < rtb_Gain) {
      *rty_voltage_reference = rtb_Gain;
    } else {
      *rty_voltage_reference = rtb_Add1;
    }

    /* End of Switch: '<S6>/Switch' */
  }

  /* End of Switch: '<S6>/Switch2' */

  /* Switch: '<S5>/Switch' */
  if (rtu_speed_command_ramp >= 0) {
    *rty_speed_reference = rtu_speed_command_ramp_vf_pos;
  } else {
    *rty_speed_reference = rtu_speed_command_ramp_vf_neg;
  }

  /* End of Switch: '<S5>/Switch' */

  /* Constant: '<S5>/Constant1' */
  *rty_current_reference = 0;

  /* Constant: '<S5>/Constant2' */
  *rty_bemf_reference = 0;
}

/* System initialize for function-call system: '<Root>/Interrupt_1k' */
void Motor_1pH_Interrupt_1k_Init(ControlModeState *rty_controller_mode, int16_T *
  rty_speed_reference, int16_T *rty_voltage_reference, int16_T
  *rty_current_reference, DW_Interrupt_1k_Motor_1pHCont_T *localDW)
{
  int16_T Constant1_d;
  int16_T Constant3;
  localDW->is_Motor_On = Motor_1pHC_IN_NO_ACTIVE_CHILD_c;
  localDW->is_active_c3_Motor_1pHControlle = 0U;
  localDW->is_c3_Motor_1pHController = Motor_1pHC_IN_NO_ACTIVE_CHILD_c;
  *rty_controller_mode = InitMode;
  *rty_speed_reference = 0;
  *rty_voltage_reference = 0;
  *rty_current_reference = 0;

  /* SystemInitialize for Chart: '<S1>/StateMachine1K' incorporates:
   *  SubSystem: '<S3>/alignment_1k'
   */
  Motor_1pH_alignment_1k_Init(&Constant1_d, &Constant3);

  /* SystemInitialize for Chart: '<S1>/StateMachine1K' incorporates:
   *  SubSystem: '<S3>/vf_1k'
   */
  Motor_1pHControl_vf_1k_Init(&Constant1_d, &Constant3);
}

/* Start for function-call system: '<Root>/Interrupt_1k' */
void Motor_1p_Interrupt_1k_Start(void)
{
  int16_T Constant2_p;

  /* Start for Chart: '<S1>/StateMachine1K' incorporates:
   *  SubSystem: '<S3>/alignment_1k'
   */
  Motor_1p_alignment_1k_Start(&Constant2_p);
}

/* Output and update for function-call system: '<Root>/Interrupt_1k' */
void Motor_1pHContr_Interrupt_1k(boolean_T rtu_motor_on, int16_T
  rtu_speed_command, int16_T rtu_current_command, int16_T rtu_ramp_command,
  int16_T rtu_boost_command_pos, int16_T rtu_boost_command_neg, ControlFlag
  rtu_controller_State, int16_T rtu_vdc, int16_T rtu_speed_command_vf_pos,
  int16_T rtu_speed_command_vf_neg, ControlModeState *rty_controller_mode,
  int16_T *rty_speed_reference, int16_T *rty_voltage_reference, int16_T
  *rty_current_reference, DW_Interrupt_1k_Motor_1pHCont_T *localDW)
{
  int16_T Constant3;
  int32_T Switch;

  /* Chart: '<S1>/StateMachine1K' */
  if (localDW->is_active_c3_Motor_1pHControlle == 0U) {
    localDW->is_active_c3_Motor_1pHControlle = 1U;
    localDW->is_c3_Motor_1pHController = Motor_1pHControl_IN_Init_Mode_o;

    /*  execute at t = 0 (start) */
    *rty_controller_mode = InitMode;

    /*  flag   */
    *rty_speed_reference = 0;
    *rty_current_reference = 0;
    *rty_voltage_reference = 0;
  } else {
    switch (localDW->is_c3_Motor_1pHController) {
     case Motor_1pHControl_IN_Init_Mode_o:
      *rty_controller_mode = InitMode;
      if (rtu_controller_State == InitDone) {
        localDW->is_c3_Motor_1pHController = Motor_1pHControl_IN_Stop_Mode_l;
        *rty_controller_mode = StopMode;

        /*  flag */
        *rty_speed_reference = 0;
        *rty_current_reference = 0;
        *rty_voltage_reference = 0;
      }
      break;

     case Motor_1pHControll_IN_Motor_On_p:
      if (((((rtu_speed_command == 0) | !rtu_motor_on) & (rtu_controller_State
             != Fault)) | (rtu_controller_State == Init) | (rtu_controller_State
            == Stop)) != 0) {
        localDW->is_Motor_On = Motor_1pHC_IN_NO_ACTIVE_CHILD_c;
        localDW->is_c3_Motor_1pHController = Motor_1pHControl_IN_Stop_Mode_l;
        *rty_controller_mode = StopMode;

        /*  flag */
        *rty_speed_reference = 0;
        *rty_current_reference = 0;
        *rty_voltage_reference = 0;
      } else {
        switch (localDW->is_Motor_On) {
         case Motor_1pHCo_IN_Alignment_Mode_i:
          *rty_controller_mode = AlignmentMode;
          switch (rtu_controller_State) {
           case Fault:
            localDW->is_Motor_On = Motor_1pHContro_IN_Error_Mode_h;
            *rty_controller_mode = ErrorMode;

            /*  flag */
            *rty_current_reference = 0;
            *rty_voltage_reference = 0;
            *rty_speed_reference = 0;
            break;

           case AlignmentDone:
            localDW->is_Motor_On = Motor_1pHControlle_IN_VF_Mode_i;

            /*  open loop (OL), voltage foward (VF) */
            *rty_controller_mode = VFMode;

            /* Outputs for Function Call SubSystem: '<S3>/vf_1k' */
            /*  flag
               speed_command_ramp = Speed_Ramp(speed_command,3000,speed_command_ramp); */
            Motor_1pHController_vf_1k(rtu_speed_command,
              rtu_speed_command_vf_pos, rtu_speed_command_vf_neg,
              rtu_ramp_command, rtu_boost_command_pos, rtu_boost_command_neg,
              rtu_vdc, rty_speed_reference, rty_voltage_reference,
              rty_current_reference, &Constant3);

            /* End of Outputs for SubSystem: '<S3>/vf_1k' */
            break;

           default:
            /* Outputs for Function Call SubSystem: '<S3>/alignment_1k' */
            /*  flag */
            Motor_1pHContr_alignment_1k(rtu_speed_command, rtu_current_command,
              rty_speed_reference, rty_voltage_reference, &Switch, &Constant3);

            /* End of Outputs for SubSystem: '<S3>/alignment_1k' */
            Switch >>= 15;
            if (Switch > 32767) {
              Switch = 32767;
            } else {
              if (Switch < -32768) {
                Switch = -32768;
              }
            }

            *rty_current_reference = (int16_T)Switch;
            break;
          }
          break;

         case Motor_1pHContro_IN_Error_Mode_h:
          *rty_controller_mode = ErrorMode;
          if (rtu_controller_State == FaultClear) {
            localDW->is_Motor_On = Motor_1pHC_IN_NO_ACTIVE_CHILD_c;
            localDW->is_c3_Motor_1pHController = Motor_1pHControl_IN_Stop_Mode_l;
            *rty_controller_mode = StopMode;

            /*  flag */
            *rty_speed_reference = 0;
            *rty_current_reference = 0;
            *rty_voltage_reference = 0;
          } else {
            /*  flag */
            *rty_current_reference = 0;
            *rty_voltage_reference = 0;
            *rty_speed_reference = 0;
          }
          break;

         default:
          /* case IN_VF_Mode: */
          *rty_controller_mode = VFMode;
          if (rtu_controller_State == Fault) {
            localDW->is_Motor_On = Motor_1pHContro_IN_Error_Mode_h;
            *rty_controller_mode = ErrorMode;

            /*  flag */
            *rty_current_reference = 0;
            *rty_voltage_reference = 0;
            *rty_speed_reference = 0;
          } else {
            /* Outputs for Function Call SubSystem: '<S3>/vf_1k' */
            /*  flag
               speed_command_ramp = Speed_Ramp(speed_command,3000,speed_command_ramp); */
            Motor_1pHController_vf_1k(rtu_speed_command,
              rtu_speed_command_vf_pos, rtu_speed_command_vf_neg,
              rtu_ramp_command, rtu_boost_command_pos, rtu_boost_command_neg,
              rtu_vdc, rty_speed_reference, rty_voltage_reference,
              rty_current_reference, &Constant3);

            /* End of Outputs for SubSystem: '<S3>/vf_1k' */
          }
          break;
        }
      }
      break;

     default:
      /* case IN_Stop_Mode: */
      *rty_controller_mode = StopMode;
      if (((rtu_speed_command != 0) & rtu_motor_on) != 0) {
        localDW->is_c3_Motor_1pHController = Motor_1pHControll_IN_Motor_On_p;
        localDW->is_Motor_On = Motor_1pHCo_IN_Alignment_Mode_i;
        *rty_controller_mode = AlignmentMode;

        /* Outputs for Function Call SubSystem: '<S3>/alignment_1k' */
        /*  flag */
        Motor_1pHContr_alignment_1k(rtu_speed_command, rtu_current_command,
          rty_speed_reference, rty_voltage_reference, &Switch, &Constant3);

        /* End of Outputs for SubSystem: '<S3>/alignment_1k' */
        Switch >>= 15;
        if (Switch > 32767) {
          Switch = 32767;
        } else {
          if (Switch < -32768) {
            Switch = -32768;
          }
        }

        *rty_current_reference = (int16_T)Switch;
      }
      break;
    }
  }

  /* End of Chart: '<S1>/StateMachine1K' */
}

/* Model step function for TID0 */
void Motor_1pHController_step0(void)   /* Sample time: [0.000125s, 0.0s] */
{
  /* Update the flag to indicate when data transfers from
   *  Sample time: [0.000125s, 0.0s] to Sample time: [0.001s, 0.0s]  */
  (Motor_1pHController_M->Timing.RateInteraction.TID0_1)++;
  if ((Motor_1pHController_M->Timing.RateInteraction.TID0_1) > 7) {
    Motor_1pHController_M->Timing.RateInteraction.TID0_1 = 0;
  }

  /* RateTransition: '<Root>/RT19' */
  if (Motor_1pHController_M->Timing.RateInteraction.TID0_1 == 1) {
    Motor_1pHController_B.controller_mode_m =
      Motor_1pHController_DW.RT19_Buffer0;

    /* RateTransition: '<Root>/RT3' */
    Motor_1pHController_B.speed_reference_c = Motor_1pHController_DW.RT3_Buffer0;

    /* RateTransition: '<Root>/RT2' */
    Motor_1pHController_B.voltage_reference_k =
      Motor_1pHController_DW.RT2_Buffer0;
  }

  /* End of RateTransition: '<Root>/RT19' */

  /* S-Function (fcncallgen): '<Root>/FastLoop' incorporates:
   *  SubSystem: '<Root>/Interrupt_8k'
   */
  /* Inport: '<Root>/fault_clear' incorporates:
   *  Inport: '<Root>/Direction'
   *  Inport: '<Root>/align_delay'
   *  Inport: '<Root>/fault1'
   *  Inport: '<Root>/fault2 '
   *  Inport: '<Root>/start_angle_neg'
   *  Inport: '<Root>/start_angle_pos'
   *  Inport: '<Root>/start_ctr'
   *  Inport: '<Root>/vdc_adc'
   *  Outport: '<Root>/PWM_OFF'
   *  Outport: '<Root>/Vs_s'
   */
  Motor_1pHContr_Interrupt_8k(Motor_1pHController_M,
    Motor_1pHController_U.fault_clear, Motor_1pHController_U.Direction,
    Motor_1pHController_B.controller_mode_m,
    Motor_1pHController_B.speed_reference_c,
    Motor_1pHController_B.voltage_reference_k, Motor_1pHController_U.align_delay,
    Motor_1pHController_U.start_ctr, Motor_1pHController_U.vdc_adc,
    Motor_1pHController_U.start_angle_pos, Motor_1pHController_U.start_angle_neg,
    Motor_1pHController_U.fault1, Motor_1pHController_U.fault2,
    &Motor_1pHController_B.controller_flag, &Motor_1pHController_Y.Vs_s,
    &Motor_1pHController_Y.PWM_OFF, &Motor_1pHController_DW.Interrupt_8k);

  /* End of Outputs for S-Function (fcncallgen): '<Root>/FastLoop' */

  /* RateTransition: '<Root>/RT9' */
  if (Motor_1pHController_M->Timing.RateInteraction.TID0_1 == 1) {
    Motor_1pHController_DW.RT9_Buffer = Motor_1pHController_B.controller_flag;

    /* RateTransition: '<Root>/RT20' incorporates:
     *  Inport: '<Root>/vdc_adc'
     */
    Motor_1pHController_DW.RT20_Buffer = Motor_1pHController_U.vdc_adc;
  }

  /* End of RateTransition: '<Root>/RT9' */

  /* Update absolute time */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The resolution of this integer timer is 0.000125, which is the step size
   * of the task. Size of "clockTick0" ensures timer will not overflow during the
   * application lifespan selected.
   */
  Motor_1pHController_M->Timing.clockTick0++;
}

/* Model step function for TID1 */
void Motor_1pHController_step1(void)   /* Sample time: [0.001s, 0.0s] */
{
  /* S-Function (fcncallgen): '<Root>/SlowLoop' incorporates:
   *  SubSystem: '<Root>/Interrupt_1k'
   */

  /* Inport: '<Root>/motor_on' incorporates:
   *  Inport: '<Root>/boost_command_neg'
   *  Inport: '<Root>/boost_command_pos'
   *  Inport: '<Root>/current_command'
   *  Inport: '<Root>/ramp_command'
   *  Inport: '<Root>/speed_command'
   *  Inport: '<Root>/speed_command_vf_neg'
   *  Inport: '<Root>/speed_command_vf_pos'
   *  RateTransition: '<Root>/RT20'
   *  RateTransition: '<Root>/RT9'
   */
  Motor_1pHContr_Interrupt_1k(Motor_1pHController_U.motor_on,
    Motor_1pHController_U.speed_command, Motor_1pHController_U.current_command,
    Motor_1pHController_U.ramp_command, Motor_1pHController_U.boost_command_pos,
    Motor_1pHController_U.boost_command_neg, Motor_1pHController_DW.RT9_Buffer,
    Motor_1pHController_DW.RT20_Buffer,
    Motor_1pHController_U.speed_command_vf_pos,
    Motor_1pHController_U.speed_command_vf_neg,
    &Motor_1pHController_B.controller_mode,
    &Motor_1pHController_B.speed_reference,
    &Motor_1pHController_B.voltage_reference,
    &Motor_1pHController_B.current_reference_g,
    &Motor_1pHController_DW.Interrupt_1k);

  /* End of Outputs for S-Function (fcncallgen): '<Root>/SlowLoop' */

  /* Update for RateTransition: '<Root>/RT19' */
  Motor_1pHController_DW.RT19_Buffer0 = Motor_1pHController_B.controller_mode;

  /* Update for RateTransition: '<Root>/RT3' */
  Motor_1pHController_DW.RT3_Buffer0 = Motor_1pHController_B.speed_reference;

  /* Update for RateTransition: '<Root>/RT2' */
  Motor_1pHController_DW.RT2_Buffer0 = Motor_1pHController_B.voltage_reference;
}

/* Model initialize function */
void Motor_1pHController_initialize(void)
{
  /* Registration code */

  /* initialize real-time model */
  (void) memset((void *)Motor_1pHController_M, 0,
                sizeof(RT_MODEL_Motor_1pHController_T));

  /* block I/O */
  (void) memset(((void *) &Motor_1pHController_B), 0,
                sizeof(B_Motor_1pHController_T));

  {
    Motor_1pHController_B.controller_mode_m = InitMode;
    Motor_1pHController_B.controller_mode = InitMode;
    Motor_1pHController_B.controller_flag = Init;
  }

  /* states (dwork) */
  (void) memset((void *)&Motor_1pHController_DW, 0,
                sizeof(DW_Motor_1pHController_T));

  /* external inputs */
  (void)memset(&Motor_1pHController_U, 0, sizeof(ExtU_Motor_1pHController_T));

  /* external outputs */
  (void) memset((void *)&Motor_1pHController_Y, 0,
                sizeof(ExtY_Motor_1pHController_T));

  /* Start for S-Function (fcncallgen): '<Root>/FastLoop' incorporates:
   *  SubSystem: '<Root>/Interrupt_8k'
   */
  Motor_1p_Interrupt_8k_Start();

  /* End of Start for S-Function (fcncallgen): '<Root>/FastLoop' */

  /* Start for S-Function (fcncallgen): '<Root>/SlowLoop' incorporates:
   *  SubSystem: '<Root>/Interrupt_1k'
   */
  Motor_1p_Interrupt_1k_Start();

  /* End of Start for S-Function (fcncallgen): '<Root>/SlowLoop' */

  /* SystemInitialize for S-Function (fcncallgen): '<Root>/FastLoop' incorporates:
   *  SubSystem: '<Root>/Interrupt_8k'
   */

  /* Outport: '<Root>/Vs_s' incorporates:
   *  Outport: '<Root>/PWM_OFF'
   */
  Motor_1pH_Interrupt_8k_Init(&Motor_1pHController_B.controller_flag,
    &Motor_1pHController_Y.Vs_s, &Motor_1pHController_Y.PWM_OFF,
    &Motor_1pHController_DW.Interrupt_8k);

  /* End of SystemInitialize for S-Function (fcncallgen): '<Root>/FastLoop' */

  /* SystemInitialize for S-Function (fcncallgen): '<Root>/SlowLoop' incorporates:
   *  SubSystem: '<Root>/Interrupt_1k'
   */
  Motor_1pH_Interrupt_1k_Init(&Motor_1pHController_B.controller_mode,
    &Motor_1pHController_B.speed_reference,
    &Motor_1pHController_B.voltage_reference,
    &Motor_1pHController_B.current_reference_g,
    &Motor_1pHController_DW.Interrupt_1k);

  /* End of SystemInitialize for S-Function (fcncallgen): '<Root>/SlowLoop' */

  /* Enable for S-Function (fcncallgen): '<Root>/FastLoop' incorporates:
   *  SubSystem: '<Root>/Interrupt_8k'
   */
  Motor_1_Interrupt_8k_Enable(Motor_1pHController_M,
    &Motor_1pHController_DW.Interrupt_8k);

  /* End of Enable for S-Function (fcncallgen): '<Root>/FastLoop' */
}

/* Model terminate function */
void Motor_1pHController_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
