function CodeDefine() { 
this.def = new Array();
this.def["rt_OneStep"] = {file: "ert_main_c.html",line:37,type:"fcn"};
this.def["main"] = {file: "ert_main_c.html",line:123,type:"fcn"};
this.def["Motor_1pHController_B"] = {file: "Motor_1pHController_c.html",line:43,type:"var"};
this.def["Motor_1pHController_DW"] = {file: "Motor_1pHController_c.html",line:46,type:"var"};
this.def["Motor_1pHController_U"] = {file: "Motor_1pHController_c.html",line:49,type:"var"};
this.def["Motor_1pHController_Y"] = {file: "Motor_1pHController_c.html",line:52,type:"var"};
this.def["Motor_1pHController_M_"] = {file: "Motor_1pHController_c.html",line:55,type:"var"};
this.def["Motor_1pHController_M"] = {file: "Motor_1pHController_c.html",line:56,type:"var"};
this.def["plook_u32s16_binckan"] = {file: "Motor_1pHController_c.html",line:64,type:"fcn"};
this.def["binsearch_u32s16"] = {file: "Motor_1pHController_c.html",line:91,type:"fcn"};
this.def["mul_wide_s32"] = {file: "Motor_1pHController_c.html",line:115,type:"fcn"};
this.def["mul_s32_hiSR"] = {file: "Motor_1pHController_c.html",line:161,type:"fcn"};
this.def["mul_s32_loSR"] = {file: "Motor_1pHController_c.html",line:169,type:"fcn"};
this.def["div_s32_floor"] = {file: "Motor_1pHController_c.html",line:178,type:"fcn"};
this.def["Motor_1pHController_VFStart"] = {file: "Motor_1pHController_c.html",line:211,type:"fcn"};
this.def["Motor_1pHC_VFDone_Flag_Init"] = {file: "Motor_1pHController_c.html",line:226,type:"fcn"};
this.def["Motor_1pHContro_VFDone_Flag"] = {file: "Motor_1pHController_c.html",line:233,type:"fcn"};
this.def["Motor_1pHControl_faultfault"] = {file: "Motor_1pHController_c.html",line:264,type:"fcn"};
this.def["Motor_1pHC_faultProtections"] = {file: "Motor_1pHController_c.html",line:274,type:"fcn"};
this.def["M_state_variable_init_Start"] = {file: "Motor_1pHController_c.html",line:291,type:"fcn"};
this.def["Motor_1_state_variable_init"] = {file: "Motor_1pHController_c.html",line:312,type:"fcn"};
this.def["Motor_1pHContr_SpeedToAngle"] = {file: "Motor_1pHController_c.html",line:333,type:"fcn"};
this.def["Motor_1pHControl_modulation"] = {file: "Motor_1pHController_c.html",line:347,type:"fcn"};
this.def["Motor_1pHController.c:Motor_1pHCont_OpenClosedHBridge"] = {file: "Motor_1pHController_c.html",line:420,type:"fcn"};
this.def["Motor_1pH_Interrupt_8k_Init"] = {file: "Motor_1pHController_c.html",line:453,type:"fcn"};
this.def["Motor_1_Interrupt_8k_Enable"] = {file: "Motor_1pHController_c.html",line:475,type:"fcn"};
this.def["Motor_1p_Interrupt_8k_Start"] = {file: "Motor_1pHController_c.html",line:483,type:"fcn"};
this.def["Motor_1pHContr_Interrupt_8k"] = {file: "Motor_1pHController_c.html",line:499,type:"fcn"};
this.def["Motor_1pH_alignment_1k_Init"] = {file: "Motor_1pHController_c.html",line:781,type:"fcn"};
this.def["Motor_1p_alignment_1k_Start"] = {file: "Motor_1pHController_c.html",line:792,type:"fcn"};
this.def["Motor_1pHContr_alignment_1k"] = {file: "Motor_1pHController_c.html",line:799,type:"fcn"};
this.def["Motor_1pHControl_vf_1k_Init"] = {file: "Motor_1pHController_c.html",line:826,type:"fcn"};
this.def["Motor_1pHController_vf_1k"] = {file: "Motor_1pHController_c.html",line:837,type:"fcn"};
this.def["Motor_1pH_Interrupt_1k_Init"] = {file: "Motor_1pHController_c.html",line:913,type:"fcn"};
this.def["Motor_1p_Interrupt_1k_Start"] = {file: "Motor_1pHController_c.html",line:939,type:"fcn"};
this.def["Motor_1pHContr_Interrupt_1k"] = {file: "Motor_1pHController_c.html",line:950,type:"fcn"};
this.def["Motor_1pHController_step0"] = {file: "Motor_1pHController_c.html",line:1134,type:"fcn"};
this.def["Motor_1pHController_step1"] = {file: "Motor_1pHController_c.html",line:1208,type:"fcn"};
this.def["Motor_1pHController_initialize"] = {file: "Motor_1pHController_c.html",line:1251,type:"fcn"};
this.def["Motor_1pHController_terminate"] = {file: "Motor_1pHController_c.html",line:1328,type:"fcn"};
this.def["B_Interrupt_8k_Motor_1pHContr_T"] = {file: "Motor_1pHController_h.html",line:51,type:"type"};
this.def["DW_Interrupt_8k_Motor_1pHCont_T"] = {file: "Motor_1pHController_h.html",line:62,type:"type"};
this.def["DW_Interrupt_1k_Motor_1pHCont_T"] = {file: "Motor_1pHController_h.html",line:69,type:"type"};
this.def["B_Motor_1pHController_T"] = {file: "Motor_1pHController_h.html",line:82,type:"type"};
this.def["DW_Motor_1pHController_T"] = {file: "Motor_1pHController_h.html",line:93,type:"type"};
this.def["ConstP_Motor_1pHController_T"] = {file: "Motor_1pHController_h.html",line:106,type:"type"};
this.def["ExtU_Motor_1pHController_T"] = {file: "Motor_1pHController_h.html",line:127,type:"type"};
this.def["ExtY_Motor_1pHController_T"] = {file: "Motor_1pHController_h.html",line:133,type:"type"};
this.def["ControlFlag"] = {file: "Motor_1pHController_types_h.html",line:37,type:"type"};
this.def["ControlModeState"] = {file: "Motor_1pHController_types_h.html",line:51,type:"type"};
this.def["struct_F7N0deEp72x1GuUaWKNfKD"] = {file: "Motor_1pHController_types_h.html",line:62,type:"type"};
this.def["RT_MODEL_Motor_1pHController_T"] = {file: "Motor_1pHController_types_h.html",line:67,type:"type"};
this.def["Motor_1pHController_ConstP"] = {file: "Motor_1pHController_data_c.html",line:24,type:"var"};
this.def["int8_T"] = {file: "rtwtypes_h.html",line:51,type:"type"};
this.def["uint8_T"] = {file: "rtwtypes_h.html",line:52,type:"type"};
this.def["int16_T"] = {file: "rtwtypes_h.html",line:53,type:"type"};
this.def["uint16_T"] = {file: "rtwtypes_h.html",line:54,type:"type"};
this.def["int32_T"] = {file: "rtwtypes_h.html",line:55,type:"type"};
this.def["uint32_T"] = {file: "rtwtypes_h.html",line:56,type:"type"};
this.def["real32_T"] = {file: "rtwtypes_h.html",line:57,type:"type"};
this.def["real64_T"] = {file: "rtwtypes_h.html",line:58,type:"type"};
this.def["real_T"] = {file: "rtwtypes_h.html",line:64,type:"type"};
this.def["time_T"] = {file: "rtwtypes_h.html",line:65,type:"type"};
this.def["boolean_T"] = {file: "rtwtypes_h.html",line:66,type:"type"};
this.def["int_T"] = {file: "rtwtypes_h.html",line:67,type:"type"};
this.def["uint_T"] = {file: "rtwtypes_h.html",line:68,type:"type"};
this.def["ulong_T"] = {file: "rtwtypes_h.html",line:69,type:"type"};
this.def["char_T"] = {file: "rtwtypes_h.html",line:70,type:"type"};
this.def["uchar_T"] = {file: "rtwtypes_h.html",line:71,type:"type"};
this.def["byte_T"] = {file: "rtwtypes_h.html",line:72,type:"type"};
this.def["creal32_T"] = {file: "rtwtypes_h.html",line:82,type:"type"};
this.def["creal64_T"] = {file: "rtwtypes_h.html",line:87,type:"type"};
this.def["creal_T"] = {file: "rtwtypes_h.html",line:92,type:"type"};
this.def["cint8_T"] = {file: "rtwtypes_h.html",line:99,type:"type"};
this.def["cuint8_T"] = {file: "rtwtypes_h.html",line:106,type:"type"};
this.def["cint16_T"] = {file: "rtwtypes_h.html",line:113,type:"type"};
this.def["cuint16_T"] = {file: "rtwtypes_h.html",line:120,type:"type"};
this.def["cint32_T"] = {file: "rtwtypes_h.html",line:127,type:"type"};
this.def["cuint32_T"] = {file: "rtwtypes_h.html",line:134,type:"type"};
this.def["pointer_T"] = {file: "rtwtypes_h.html",line:152,type:"type"};
}
CodeDefine.instance = new CodeDefine();
var testHarnessInfo = {OwnerFileName: "", HarnessOwner: "", HarnessName: "", IsTestHarness: "0"};
var relPathToBuildDir = "../ert_main.c";
var fileSep = "\\";
var isPC = true;
function Html2SrcLink() {
	this.html2SrcPath = new Array;
	this.html2Root = new Array;
	this.html2SrcPath["ert_main_c.html"] = "../ert_main.c";
	this.html2Root["ert_main_c.html"] = "ert_main_c.html";
	this.html2SrcPath["Motor_1pHController_c.html"] = "../Motor_1pHController.c";
	this.html2Root["Motor_1pHController_c.html"] = "Motor_1pHController_c.html";
	this.html2SrcPath["Motor_1pHController_h.html"] = "../Motor_1pHController.h";
	this.html2Root["Motor_1pHController_h.html"] = "Motor_1pHController_h.html";
	this.html2SrcPath["Motor_1pHController_private_h.html"] = "../Motor_1pHController_private.h";
	this.html2Root["Motor_1pHController_private_h.html"] = "Motor_1pHController_private_h.html";
	this.html2SrcPath["Motor_1pHController_types_h.html"] = "../Motor_1pHController_types.h";
	this.html2Root["Motor_1pHController_types_h.html"] = "Motor_1pHController_types_h.html";
	this.html2SrcPath["Motor_1pHController_data_c.html"] = "../Motor_1pHController_data.c";
	this.html2Root["Motor_1pHController_data_c.html"] = "Motor_1pHController_data_c.html";
	this.html2SrcPath["rtwtypes_h.html"] = "../rtwtypes.h";
	this.html2Root["rtwtypes_h.html"] = "rtwtypes_h.html";
	this.getLink2Src = function (htmlFileName) {
		 if (this.html2SrcPath[htmlFileName])
			 return this.html2SrcPath[htmlFileName];
		 else
			 return null;
	}
	this.getLinkFromRoot = function (htmlFileName) {
		 if (this.html2Root[htmlFileName])
			 return this.html2Root[htmlFileName];
		 else
			 return null;
	}
}
Html2SrcLink.instance = new Html2SrcLink();
var fileList = [
"ert_main_c.html","Motor_1pHController_c.html","Motor_1pHController_h.html","Motor_1pHController_private_h.html","Motor_1pHController_types_h.html","Motor_1pHController_data_c.html","rtwtypes_h.html"];
