/*
 * File: Motor_1pHController_private.h
 *
 * Code generated for Simulink model 'Motor_1pHController'.
 *
 * Model version                  : 1.467
 * Simulink Coder version         : 9.1 (R2019a) 23-Nov-2018
 * C/C++ source code generated on : Thu Feb 17 15:09:33 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. Traceability
 *    3. Safety precaution
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_Motor_1pHController_private_h_
#define RTW_HEADER_Motor_1pHController_private_h_
#include "rtwtypes.h"
#include "Motor_1pHController.h"
#ifndef UCHAR_MAX
#include <limits.h>
#endif

#if ( UCHAR_MAX != (0xFFU) ) || ( SCHAR_MAX != (0x7F) )
#error Code was generated for compiler with different sized uchar/char. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

#if ( USHRT_MAX != (0xFFFFU) ) || ( SHRT_MAX != (0x7FFF) )
#error Code was generated for compiler with different sized ushort/short. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

#if ( UINT_MAX != (0xFFFFFFFFU) ) || ( INT_MAX != (0x7FFFFFFF) )
#error Code was generated for compiler with different sized uint/int. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

#if ( ULONG_MAX != (0xFFFFFFFFU) ) || ( LONG_MAX != (0x7FFFFFFF) )
#error Code was generated for compiler with different sized ulong/long. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

extern uint32_T plook_u32s16_binckan(int16_T u, const int16_T bp[], uint32_T
  maxIndex);
extern uint32_T binsearch_u32s16(int16_T u, const int16_T bp[], uint32_T
  startIndex, uint32_T maxIndex);
extern void mul_wide_s32(int32_T in0, int32_T in1, uint32_T *ptrOutBitsHi,
  uint32_T *ptrOutBitsLo);
extern int32_T mul_s32_hiSR(int32_T a, int32_T b, uint32_T aShift);
extern int32_T mul_s32_loSR(int32_T a, int32_T b, uint32_T aShift);
extern int32_T div_s32_floor(int32_T numerator, int32_T denominator);
extern void Motor_1pHController_VFStart(int16_T rtu_speed_reference, int16_T
  rtu_rotor_start_positive, int16_T rtu_rotor_start_negative, int16_T
  *rty_rotor_position_reference);
extern void Motor_1pHC_VFDone_Flag_Init(ControlFlag *rty_control_flag);
extern void Motor_1pHContro_VFDone_Flag(int16_T rtu_speed_reference, boolean_T
  rtu_start_ctr, ControlFlag *rty_control_flag);
extern void Motor_1pHControl_faultfault(boolean_T rtu_fault1, boolean_T
  rtu_fault2, boolean_T *rty_fault);
extern void Motor_1pHC_faultProtections(boolean_T rtu_fault_clear, ControlFlag
  *rty_control_flag);
extern void M_state_variable_init_Start(int16_T *rty_OL_voltage_state_is,
  int16_T *rty_OL_voltage_state_bemf, int16_T *rty_OL_speed_state, int16_T
  *rty_OLvoltage_sat, int16_T *rty_OLspeed_sat);
extern void Motor_1_state_variable_init(int16_T *rty_OL_voltage_state_is,
  int16_T *rty_OL_voltage_state_bemf, int16_T *rty_OL_speed_state, int16_T
  *rty_OLvoltage_sat, int16_T *rty_OLspeed_sat);
extern void Motor_1pHContr_SpeedToAngle(int16_T rtu_speed_reference, int16_T
  rtu_angle_reference, int16_T *rty_rotor_position_reference);
extern void Motor_1pHControl_modulation(int16_T rtu_voltage_reference, int16_T
  rtu_rotor_angle, int16_T rtu_voltage_dc, uint16_T *rty_PWM_CMD1, uint16_T
  *rty_PWM_CMD2, int16_T *rty_InverterOutputVoltage);
extern void Motor_1pH_Interrupt_8k_Init(ControlFlag *rty_controller_State,
  int16_T *rty_Vs_s, uint16_T *rty_PWM_OFF, DW_Interrupt_8k_Motor_1pHCont_T
  *localDW);
extern void Motor_1_Interrupt_8k_Enable(RT_MODEL_Motor_1pHController_T * const
  Motor_1pHController_M, DW_Interrupt_8k_Motor_1pHCont_T *localDW);
extern void Motor_1p_Interrupt_8k_Start(void);
extern void Motor_1pHContr_Interrupt_8k(RT_MODEL_Motor_1pHController_T * const
  Motor_1pHController_M, boolean_T rtu_fault_clear, boolean_T rtu_Direction,
  ControlModeState rtu_controller_mode, int16_T rtu_speed_reference, int16_T
  rtu_voltage_reference, uint16_T rtu_align_delay, boolean_T rtu_start_ctr,
  int16_T rtu_vdc, int16_T rtu_start_angle_pos, int16_T rtu_start_angle_neg,
  boolean_T rtu_fault1, boolean_T rtu_fault2, ControlFlag *rty_controller_State,
  int16_T *rty_Vs_s, uint16_T *rty_PWM_OFF, DW_Interrupt_8k_Motor_1pHCont_T
  *localDW);
extern void Motor_1pH_alignment_1k_Init(int16_T *rty_voltage_reference, int16_T *
  rty_bemf_reference);
extern void Motor_1p_alignment_1k_Start(int16_T *rty_speed_reference);
extern void Motor_1pHContr_alignment_1k(int16_T rtu_speed_command, int16_T
  rtu_current_command, int16_T *rty_speed_reference, int16_T
  *rty_voltage_reference, int32_T *rty_current_reference, int16_T
  *rty_bemf_reference);
extern void Motor_1pHControl_vf_1k_Init(int16_T *rty_current_reference, int16_T *
  rty_bemf_reference);
extern void Motor_1pHController_vf_1k(int16_T rtu_speed_command_ramp, int16_T
  rtu_speed_command_ramp_vf_pos, int16_T rtu_speed_command_ramp_vf_neg, int16_T
  rtu_ramp_command, int16_T rtu_boost_command_pos, int16_T rtu_boost_command_neg,
  int16_T rtu_vdc, int16_T *rty_speed_reference, int16_T *rty_voltage_reference,
  int16_T *rty_current_reference, int16_T *rty_bemf_reference);
extern void Motor_1pH_Interrupt_1k_Init(ControlModeState *rty_controller_mode,
  int16_T *rty_speed_reference, int16_T *rty_voltage_reference, int16_T
  *rty_current_reference, DW_Interrupt_1k_Motor_1pHCont_T *localDW);
extern void Motor_1p_Interrupt_1k_Start(void);
extern void Motor_1pHContr_Interrupt_1k(boolean_T rtu_motor_on, int16_T
  rtu_speed_command, int16_T rtu_current_command, int16_T rtu_ramp_command,
  int16_T rtu_boost_command_pos, int16_T rtu_boost_command_neg, ControlFlag
  rtu_controller_State, int16_T rtu_vdc, int16_T rtu_speed_command_vf_pos,
  int16_T rtu_speed_command_vf_neg, ControlModeState *rty_controller_mode,
  int16_T *rty_speed_reference, int16_T *rty_voltage_reference, int16_T
  *rty_current_reference, DW_Interrupt_1k_Motor_1pHCont_T *localDW);

#endif                           /* RTW_HEADER_Motor_1pHController_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
