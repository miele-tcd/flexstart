/*
 * File: Motor_1pHController_types.h
 *
 * Code generated for Simulink model 'Motor_1pHController'.
 *
 * Model version                  : 1.467
 * Simulink Coder version         : 9.1 (R2019a) 23-Nov-2018
 * C/C++ source code generated on : Thu Feb 17 15:09:33 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. Traceability
 *    3. Safety precaution
 *    4. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_Motor_1pHController_types_h_
#define RTW_HEADER_Motor_1pHController_types_h_
#include "rtwtypes.h"
#ifndef DEFINED_TYPEDEF_FOR_ControlFlag_
#define DEFINED_TYPEDEF_FOR_ControlFlag_

typedef enum {
  Init = 0,                            /* Default value */
  InitDone,
  Stop,
  StopDone,
  Alignment,
  AlignmentDone,
  VF,
  VFDone,
  Fault,
  FaultClear
} ControlFlag;

#endif

#ifndef DEFINED_TYPEDEF_FOR_ControlModeState_
#define DEFINED_TYPEDEF_FOR_ControlModeState_

typedef enum {
  InitMode = 0,                        /* Default value */
  StopMode,
  AlignmentMode,
  VFMode,
  ControlMode,
  ErrorMode
} ControlModeState;

#endif

#ifndef DEFINED_TYPEDEF_FOR_struct_F7N0deEp72x1GuUaWKNfKD_
#define DEFINED_TYPEDEF_FOR_struct_F7N0deEp72x1GuUaWKNfKD_

typedef struct {
  real_T SpeedMax;
  real_T SpeedVFLimit;
  real_T pi;
} struct_F7N0deEp72x1GuUaWKNfKD;

#endif

/* Forward declaration for rtModel */
typedef struct tag_RTM_Motor_1pHController_T RT_MODEL_Motor_1pHController_T;

#endif                             /* RTW_HEADER_Motor_1pHController_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
