#ifndef __c1_Motor_1pHController_h__
#define __c1_Motor_1pHController_h__
#include "sf_runtime/sfc_sdi.h"
#include "rtw_capi.h"
#include "rtw_modelmap.h"

/* Type Definitions */
#ifndef enum_ControlFlag
#define enum_ControlFlag

enum ControlFlag
{
  ControlFlag_Init = 0,                /* Default value */
  ControlFlag_InitDone,
  ControlFlag_Stop,
  ControlFlag_StopDone,
  ControlFlag_Alignment,
  ControlFlag_AlignmentDone,
  ControlFlag_VF,
  ControlFlag_VFDone,
  ControlFlag_Fault,
  ControlFlag_FaultClear
};

#endif                                 /*enum_ControlFlag*/

#ifndef typedef_c1_ControlFlag
#define typedef_c1_ControlFlag

typedef enum ControlFlag c1_ControlFlag;

#endif                                 /*typedef_c1_ControlFlag*/

#ifndef enum_ControlModeState
#define enum_ControlModeState

enum ControlModeState
{
  ControlModeState_InitMode = 0,       /* Default value */
  ControlModeState_StopMode,
  ControlModeState_AlignmentMode,
  ControlModeState_VFMode,
  ControlModeState_ControlMode,
  ControlModeState_ErrorMode
};

#endif                                 /*enum_ControlModeState*/

#ifndef typedef_c1_ControlModeState
#define typedef_c1_ControlModeState

typedef enum ControlModeState c1_ControlModeState;

#endif                                 /*typedef_c1_ControlModeState*/

#ifndef typedef_SFc1_Motor_1pHControllerInstanceStruct
#define typedef_SFc1_Motor_1pHControllerInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c1_sfEvent;
  uint8_T c1_tp_Stop_Mode;
  uint8_T c1_tp_Motor_On;
  uint8_T c1_tp_Alignment_Mode;
  uint8_T c1_tp_Error_Mode;
  uint8_T c1_tp_Alignment_Done;
  uint8_T c1_tp_VF_Mode;
  uint8_T c1_tp_Init_Mode;
  uint8_T c1_is_active_c1_Motor_1pHController;
  uint8_T c1_is_c1_Motor_1pHController;
  uint8_T c1_is_Motor_On;
  boolean_T c1_fault;
  uint16_T c1_MaskFlag;
  int16_T c1_OL_speed_state;
  int16_T c1_OLspeed_sat;
  int16_T c1_OLvoltage_sat;
  int16_T c1_OL_voltage_state_is;
  int16_T c1_OL_voltage_state_bemf;
  int16_T c1_scale;
  uint16_T c1_PWM_MAX_DUTYCYCLE;
  int32_T c1_PT1_StateVariable;
  int32_T c1_SynchronisationTime_state;
  boolean_T c1_Vf_Done_SynchronisationEnable;
  int16_T c1_rotor_position_reference;
  int16_T c1_SpeedReference_PT1;
  int16_T c1_alignment_voltage;
  uint16_T c1_OffTime;
  void *c1_RuntimeVar;
  uint32_T c1_temporalCounter_i1;
  uint32_T c1_presentTicks;
  uint32_T c1_elapsedTicks;
  uint32_T c1_previousTicks;
  uint8_T c1_doSetSimStateSideEffects;
  const mxArray *c1_setSimStateSideEffectsInfo;
  uint32_T c1_mlFcnLineNumber;
  rtwCAPI_ModelMappingInfo c1_testPointMappingInfo;
  void *c1_testPointAddrMap[1];
  CovrtStateflowInstance *c1_covrtInstance;
  void *c1_fEmlrtCtx;
  c1_ControlFlag *c1_controller_State;
  int32_T *c1_PWM_CMD1;
  boolean_T *c1_fault_clear;
  boolean_T *c1_Direction;
  c1_ControlModeState *c1_controller_mode;
  int16_T *c1_speed_reference;
  int16_T *c1_voltage_reference;
  int16_T *c1_current_reference;
  int32_T *c1_PWM_CMD2;
  uint16_T *c1_PWM_OFF;
  uint16_T *c1_align_delay;
  boolean_T *c1_start_ctr;
  int16_T *c1_vdc;
  int16_T *c1_start_angle_pos;
  int16_T *c1_start_angle_neg;
  boolean_T *c1_fault1;
  boolean_T *c1_fault2;
  int16_T *c1_InvVoltageOut;
  int16_T *c1_b_speed_reference;
  int16_T *c1_b_rotor_position_reference;
  int16_T *c1_rotor_start_positive;
  int16_T *c1_rotor_start_negative;
  int16_T *c1_c_speed_reference;
  c1_ControlFlag *c1_control_flag;
  boolean_T *c1_b_start_ctr;
  boolean_T *c1_c_fault;
  boolean_T *c1_b_fault1;
  boolean_T *c1_b_fault2;
  c1_ControlFlag *c1_b_control_flag;
  boolean_T *c1_b_fault_clear;
  int16_T *c1_b_OL_voltage_state_is;
  int16_T *c1_b_OL_voltage_state_bemf;
  int16_T *c1_b_OL_speed_state;
  int16_T *c1_b_OLvoltage_sat;
  int16_T *c1_b_OLspeed_sat;
  int16_T *c1_d_speed_reference;
  int16_T *c1_angle_reference;
  int16_T *c1_c_rotor_position_reference;
  int16_T *c1_b_voltage_reference;
  uint16_T *c1_b_PWM_CMD1;
  int16_T *c1_rotor_angle;
  uint16_T *c1_b_PWM_CMD2;
  int16_T *c1_voltage_dc;
  int16_T *c1_InverterOutputVoltage;
} SFc1_Motor_1pHControllerInstanceStruct;

#endif                                 /*typedef_SFc1_Motor_1pHControllerInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c1_Motor_1pHController_get_eml_resolved_functions_info
  (void);
extern mxArray *sf_c1_Motor_1pHController_getDebuggerHoverDataFor
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, uint32_T c1_b);

/* Function Definitions */
extern void sf_c1_Motor_1pHController_get_check_sum(mxArray *plhs[]);
extern void c1_Motor_1pHController_method_dispatcher(SimStruct *S, int_T method,
  void *data);

#endif
