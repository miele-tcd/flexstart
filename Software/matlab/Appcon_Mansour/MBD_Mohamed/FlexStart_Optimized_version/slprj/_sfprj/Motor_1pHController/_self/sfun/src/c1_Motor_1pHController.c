/* Include files */

#include "Motor_1pHController_sfun.h"
#include "c1_Motor_1pHController.h"
#include "mwmathutil.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "Motor_1pHController_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(S);
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);
static const mxArray* sf_opaque_get_hover_data_for_msg(void *chartInstance,
  int32_T msgSSID);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)
#define c1_IN_NO_ACTIVE_CHILD          ((uint8_T)0U)
#define c1_IN_Init_Mode                ((uint8_T)1U)
#define c1_IN_Motor_On                 ((uint8_T)2U)
#define c1_IN_Stop_Mode                ((uint8_T)3U)
#define c1_IN_Alignment_Done           ((uint8_T)1U)
#define c1_IN_Alignment_Mode           ((uint8_T)2U)
#define c1_IN_Error_Mode               ((uint8_T)3U)
#define c1_IN_VF_Mode                  ((uint8_T)4U)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c1_sv0[10] = { "Init", "InitDone", "Stop", "StopDone",
  "Alignment", "AlignmentDone", "VF", "VFDone", "Fault", "FaultClear" };

static const int32_T c1_iv0[10] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

/* Function Declarations */
static void initialize_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void initialize_params_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void enable_c1_Motor_1pHController(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance);
static void disable_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void c1_update_debugger_state_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void ext_mode_exec_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static const mxArray *get_sim_state_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void set_sim_state_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, const mxArray *c1_st);
static void c1_set_sim_state_side_effects_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void finalize_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void sf_gateway_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void mdl_start_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void initSimStructsc1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void c1_enter_atomic_Stop_Mode(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance);
static void c1_Motor_On(SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void c1_VF_Mode(SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void c1_exit_atomic_VF_Mode(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance);
static void init_script_number_translation(uint32_T c1_machineNumber, uint32_T
  c1_chartNumber, uint32_T c1_instanceNumber);
static const mxArray *c1_sfAfterOrElapsed(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance);
static const mxArray *c1_b_sfAfterOrElapsed
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static c1_ControlFlag c1_VFDone_Flag(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, int16_T c1_e_speed_reference, boolean_T c1_c_start_ctr);
static boolean_T c1_b_fault(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, boolean_T c1_c_fault1, boolean_T c1_c_fault2);
static c1_ControlFlag c1_Protections(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, boolean_T c1_c_fault_clear);
static void c1_state_variable_init(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, int16_T *c1_c_OL_voltage_state_is, int16_T
  *c1_c_OL_voltage_state_bemf, int16_T *c1_c_OL_speed_state, int16_T
  *c1_c_OLvoltage_sat, int16_T *c1_c_OLspeed_sat);
static int16_T c1_SpeedToAngle(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, int16_T c1_e_speed_reference, int16_T c1_b_angle_reference);
static void c1_modulation(SFc1_Motor_1pHControllerInstanceStruct *chartInstance,
  int16_T c1_c_voltage_reference, int16_T c1_b_rotor_angle, int16_T
  c1_b_voltage_dc, uint16_T *c1_c_PWM_CMD1, uint16_T *c1_c_PWM_CMD2, int16_T
  *c1_b_InverterOutputVoltage);
static void c1_OpenClosedHBridge(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance);
static const mxArray *c1_sf_marshallOut(void *chartInstanceVoid, void *c1_inData);
static int32_T c1_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_b_sfEvent, const char_T *c1_identifier);
static int32_T c1_b_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_b_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static uint8_T c1_c_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_b_tp_Stop_Mode, const char_T *c1_identifier);
static uint8_T c1_d_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_c_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static c1_ControlFlag c1_e_emlrt_marshallIn
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, const mxArray
   *c1_b_controller_State, const char_T *c1_identifier);
static c1_ControlFlag c1_f_emlrt_marshallIn
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, const mxArray *c1_u,
   const emlrtMsgIdentifier *c1_parentId);
static void c1_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_d_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static boolean_T c1_g_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_d_fault, const char_T *c1_identifier);
static boolean_T c1_h_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_e_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static const mxArray *c1_f_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static const mxArray *c1_g_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static uint16_T c1_i_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_b_PWM_OFF, const char_T *c1_identifier);
static uint16_T c1_j_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static int16_T c1_k_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_c_OL_speed_state, const char_T
  *c1_identifier);
static int16_T c1_l_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static uint32_T c1_m_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_b_temporalCounter_i1, const char_T
  *c1_identifier);
static uint32_T c1_n_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static const mxArray *c1_o_emlrt_marshallIn
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, const mxArray
   *c1_b_setSimStateSideEffectsInfo, const char_T *c1_identifier);
static const mxArray *c1_p_emlrt_marshallIn
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, const mxArray *c1_u,
   const emlrtMsgIdentifier *c1_parentId);
static void c1_slStringInitializeDynamicBuffers
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static const mxArray *sf_get_hover_data_for_msg
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, int32_T c1_ssid);
static void c1_init_sf_message_store_memory
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void init_test_point_addr_map(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance);
static void **get_test_point_address_map(SFc1_Motor_1pHControllerInstanceStruct *
  chartInstance);
static rtwCAPI_ModelMappingInfo *get_test_point_mapping_info
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance);
static void init_dsm_address_info(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance);
static void init_simulink_io_address(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  setLegacyDebuggerFlag(chartInstance->S, true);
  setDebuggerFlag(chartInstance->S, true);
  c1_slStringInitializeDynamicBuffers(chartInstance);
  if (sf_is_first_init_cond(chartInstance->S)) {
    initSimStructsc1_Motor_1pHController(chartInstance);
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  sim_mode_is_external(chartInstance->S);
  chartInstance->c1_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c1_doSetSimStateSideEffects = 0U;
  chartInstance->c1_setSimStateSideEffectsInfo = NULL;
  chartInstance->c1_tp_Init_Mode = 0U;
  chartInstance->c1_is_Motor_On = c1_IN_NO_ACTIVE_CHILD;
  chartInstance->c1_tp_Motor_On = 0U;
  chartInstance->c1_tp_Alignment_Done = 0U;
  chartInstance->c1_temporalCounter_i1 = 0U;
  chartInstance->c1_tp_Alignment_Mode = 0U;
  chartInstance->c1_temporalCounter_i1 = 0U;
  chartInstance->c1_tp_Error_Mode = 0U;
  chartInstance->c1_tp_VF_Mode = 0U;
  chartInstance->c1_tp_Stop_Mode = 0U;
  chartInstance->c1_is_active_c1_Motor_1pHController = 0U;
  chartInstance->c1_is_c1_Motor_1pHController = c1_IN_NO_ACTIVE_CHILD;
  chartInstance->c1_fault = false;
  chartInstance->c1_MaskFlag = 0U;
  chartInstance->c1_OL_speed_state = 0;
  chartInstance->c1_OLspeed_sat = 0;
  chartInstance->c1_OLvoltage_sat = 0;
  chartInstance->c1_OL_voltage_state_is = 0;
  chartInstance->c1_OL_voltage_state_bemf = 0;
  chartInstance->c1_PT1_StateVariable = 0;
  chartInstance->c1_SynchronisationTime_state = 0;
  chartInstance->c1_Vf_Done_SynchronisationEnable = false;
  chartInstance->c1_rotor_position_reference = 0;
  chartInstance->c1_SpeedReference_PT1 = 0;
  if (sf_get_output_port_reusable(chartInstance->S, 1) == 0) {
    *chartInstance->c1_controller_State = ControlFlag_Init;
  }

  if (sf_get_output_port_reusable(chartInstance->S, 2) == 0) {
    *chartInstance->c1_PWM_CMD1 = 0;
  }

  if (sf_get_output_port_reusable(chartInstance->S, 3) == 0) {
    *chartInstance->c1_PWM_CMD2 = 0;
  }

  if (sf_get_output_port_reusable(chartInstance->S, 4) == 0) {
    *chartInstance->c1_PWM_OFF = 0U;
  }

  if (sf_get_output_port_reusable(chartInstance->S, 5) == 0) {
    *chartInstance->c1_InvVoltageOut = 0;
  }

  chartInstance->c1_presentTicks = 0U;
  chartInstance->c1_elapsedTicks = 0U;
  chartInstance->c1_previousTicks = 0U;
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_fault, 10U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 10U, (real_T)
                    chartInstance->c1_fault);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_MaskFlag, 0U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 0U, (real_T)
                    chartInstance->c1_MaskFlag);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OL_speed_state, 1U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 1U, (real_T)
                    chartInstance->c1_OL_speed_state);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OLspeed_sat, 4U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 4U, (real_T)
                    chartInstance->c1_OLspeed_sat);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OLvoltage_sat, 5U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 5U, (real_T)
                    chartInstance->c1_OLvoltage_sat);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OL_voltage_state_is, 3U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 3U, (real_T)
                    chartInstance->c1_OL_voltage_state_is);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OL_voltage_state_bemf, 2U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 2U, (real_T)
                    chartInstance->c1_OL_voltage_state_bemf);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_PT1_StateVariable, 6U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 6U, (real_T)
                    chartInstance->c1_PT1_StateVariable);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_SynchronisationTime_state, 8U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 8U, (real_T)
                    chartInstance->c1_SynchronisationTime_state);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_Vf_Done_SynchronisationEnable,
                        9U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 9U, (real_T)
                    chartInstance->c1_Vf_Done_SynchronisationEnable);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_rotor_position_reference, 11U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 11U, (real_T)
                    chartInstance->c1_rotor_position_reference);
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_SpeedReference_PT1, 7U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 7U, (real_T)
                    chartInstance->c1_SpeedReference_PT1);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD1, 26U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 26U, (real_T)
                    *chartInstance->c1_PWM_CMD1);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD2, 27U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 27U, (real_T)
                    *chartInstance->c1_PWM_CMD2);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_OFF, 28U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 28U, (real_T)
                    *chartInstance->c1_PWM_OFF);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_InvVoltageOut, 29U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 29U, (real_T)
                    *chartInstance->c1_InvVoltageOut);
}

static void initialize_params_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  real_T c1_d0;
  real_T c1_d1;
  int16_T c1_i0;
  real_T c1_d2;
  real_T c1_d3;
  uint16_T c1_u0;
  real_T c1_d4;
  real_T c1_d5;
  int16_T c1_i1;
  real_T c1_d6;
  real_T c1_d7;
  uint16_T c1_u1;
  sf_mex_import_named("scale", sf_mex_get_sfun_param(chartInstance->S, 3U, 0U),
                      &c1_d0, 0, 0, 0U, 0, 0U, 0);
  c1_d1 = c1_d0;
  if (c1_d1 < 32768.0) {
    if (c1_d1 >= -32768.0) {
      c1_i0 = (int16_T)c1_d1;
    } else {
      c1_i0 = MIN_int16_T;
    }
  } else if (c1_d1 >= 32768.0) {
    c1_i0 = MAX_int16_T;
  } else {
    c1_i0 = 0;
  }

  chartInstance->c1_scale = c1_i0;
  sf_mex_import_named("PWM_MAX_DUTYCYCLE", sf_mex_get_sfun_param
                      (chartInstance->S, 1U, 0U), &c1_d2, 0, 0, 0U, 0, 0U, 0);
  c1_d3 = c1_d2;
  if (c1_d3 < 65536.0) {
    if (c1_d3 >= 0.0) {
      c1_u0 = (uint16_T)c1_d3;
    } else {
      c1_u0 = 0U;
    }
  } else if (c1_d3 >= 65536.0) {
    c1_u0 = MAX_uint16_T;
  } else {
    c1_u0 = 0U;
  }

  chartInstance->c1_PWM_MAX_DUTYCYCLE = c1_u0;
  sf_mex_import_named("alignment_voltage", sf_mex_get_sfun_param
                      (chartInstance->S, 2U, 0U), &c1_d4, 0, 0, 0U, 0, 0U, 0);
  c1_d5 = c1_d4;
  if (c1_d5 < 32768.0) {
    if (c1_d5 >= -32768.0) {
      c1_i1 = (int16_T)c1_d5;
    } else {
      c1_i1 = MIN_int16_T;
    }
  } else if (c1_d5 >= 32768.0) {
    c1_i1 = MAX_int16_T;
  } else {
    c1_i1 = 0;
  }

  chartInstance->c1_alignment_voltage = c1_i1;
  sf_mex_import_named("OffTime", sf_mex_get_sfun_param(chartInstance->S, 0U, 0U),
                      &c1_d6, 0, 0, 0U, 0, 0U, 0);
  c1_d7 = c1_d6;
  if (c1_d7 < 65536.0) {
    if (c1_d7 >= 0.0) {
      c1_u1 = (uint16_T)c1_d7;
    } else {
      c1_u1 = 0U;
    }
  } else if (c1_d7 >= 65536.0) {
    c1_u1 = MAX_uint16_T;
  } else {
    c1_u1 = 0U;
  }

  chartInstance->c1_OffTime = c1_u1;
}

static void enable_c1_Motor_1pHController(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
  sf_call_output_fcn_enable(chartInstance->S, 0, "VFStart", 0);
  sf_call_output_fcn_enable(chartInstance->S, 1, "VFDone_Flag", 0);
  sf_call_output_fcn_enable(chartInstance->S, 2, "fault", 0);
  sf_call_output_fcn_enable(chartInstance->S, 3, "Protections", 0);
  sf_call_output_fcn_enable(chartInstance->S, 4, "state_variable_init", 0);
  sf_call_output_fcn_enable(chartInstance->S, 5, "SpeedToAngle", 0);
  sf_call_output_fcn_enable(chartInstance->S, 8, "modulation", 0);
  chartInstance->c1_presentTicks = (uint32_T)muDoubleScalarFloor((_sfTime_ -
    sf_get_start_time(chartInstance->S)) / 0.000125 + 0.5);
  chartInstance->c1_previousTicks = chartInstance->c1_presentTicks;
}

static void disable_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c1_presentTicks = (uint32_T)muDoubleScalarFloor((_sfTime_ -
    sf_get_start_time(chartInstance->S)) / 0.000125 + 0.5);
  chartInstance->c1_elapsedTicks = chartInstance->c1_presentTicks -
    chartInstance->c1_previousTicks;
  chartInstance->c1_previousTicks = chartInstance->c1_presentTicks;
  if (chartInstance->c1_temporalCounter_i1 + chartInstance->c1_elapsedTicks <=
      524287U) {
    chartInstance->c1_temporalCounter_i1 += chartInstance->c1_elapsedTicks;
  } else {
    chartInstance->c1_temporalCounter_i1 = 524287U;
  }

  sf_call_output_fcn_disable(chartInstance->S, 0, "VFStart", 0);
  sf_call_output_fcn_disable(chartInstance->S, 1, "VFDone_Flag", 0);
  sf_call_output_fcn_disable(chartInstance->S, 2, "fault", 0);
  sf_call_output_fcn_disable(chartInstance->S, 3, "Protections", 0);
  sf_call_output_fcn_disable(chartInstance->S, 4, "state_variable_init", 0);
  sf_call_output_fcn_disable(chartInstance->S, 5, "SpeedToAngle", 0);
  sf_call_output_fcn_disable(chartInstance->S, 8, "modulation", 0);
}

static void c1_update_debugger_state_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  uint32_T c1_prevAniVal;
  c1_prevAniVal = _SFD_GET_ANIMATION();
  _SFD_SET_ANIMATION(0U);
  _SFD_SET_HONOR_BREAKPOINTS(0U);
  if (chartInstance->c1_is_active_c1_Motor_1pHController == 1U) {
    _SFD_CC_CALL(CHART_ACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_c1_Motor_1pHController == c1_IN_Stop_Mode) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 8U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 8U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_c1_Motor_1pHController == c1_IN_Motor_On) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_Motor_On == c1_IN_Alignment_Mode) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_Motor_On == c1_IN_Error_Mode) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_Motor_On == c1_IN_Alignment_Done) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_Motor_On == c1_IN_VF_Mode) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 5U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, chartInstance->c1_sfEvent);
  }

  if (chartInstance->c1_is_c1_Motor_1pHController == c1_IN_Init_Mode) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
  }

  _SFD_SET_ANIMATION(c1_prevAniVal);
  _SFD_SET_HONOR_BREAKPOINTS(1U);
  _SFD_ANIMATE();
}

static void ext_mode_exec_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  const mxArray *c1_st;
  const mxArray *c1_y = NULL;
  int16_T c1_hoistedGlobal;
  const mxArray *c1_b_y = NULL;
  int32_T c1_b_hoistedGlobal;
  const mxArray *c1_c_y = NULL;
  int32_T c1_c_hoistedGlobal;
  const mxArray *c1_d_y = NULL;
  uint16_T c1_d_hoistedGlobal;
  const mxArray *c1_e_y = NULL;
  c1_ControlFlag c1_e_hoistedGlobal;
  const mxArray *c1_f_y = NULL;
  int32_T c1_u;
  const mxArray *c1_g_y = NULL;
  const mxArray *c1_m0 = NULL;
  uint16_T c1_f_hoistedGlobal;
  const mxArray *c1_h_y = NULL;
  int16_T c1_g_hoistedGlobal;
  const mxArray *c1_i_y = NULL;
  int16_T c1_h_hoistedGlobal;
  const mxArray *c1_j_y = NULL;
  int16_T c1_i_hoistedGlobal;
  const mxArray *c1_k_y = NULL;
  int16_T c1_j_hoistedGlobal;
  const mxArray *c1_l_y = NULL;
  int16_T c1_k_hoistedGlobal;
  const mxArray *c1_m_y = NULL;
  int32_T c1_l_hoistedGlobal;
  const mxArray *c1_n_y = NULL;
  int16_T c1_m_hoistedGlobal;
  const mxArray *c1_o_y = NULL;
  int32_T c1_n_hoistedGlobal;
  const mxArray *c1_p_y = NULL;
  boolean_T c1_o_hoistedGlobal;
  const mxArray *c1_q_y = NULL;
  boolean_T c1_p_hoistedGlobal;
  const mxArray *c1_r_y = NULL;
  int16_T c1_q_hoistedGlobal;
  const mxArray *c1_s_y = NULL;
  uint8_T c1_r_hoistedGlobal;
  const mxArray *c1_t_y = NULL;
  uint8_T c1_s_hoistedGlobal;
  const mxArray *c1_u_y = NULL;
  uint8_T c1_t_hoistedGlobal;
  const mxArray *c1_v_y = NULL;
  uint32_T c1_u_hoistedGlobal;
  const mxArray *c1_w_y = NULL;
  uint32_T c1_v_hoistedGlobal;
  const mxArray *c1_x_y = NULL;
  c1_st = NULL;
  c1_st = NULL;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_createcellmatrix(22, 1), false);
  c1_hoistedGlobal = *chartInstance->c1_InvVoltageOut;
  c1_b_y = NULL;
  sf_mex_assign(&c1_b_y, sf_mex_create("y", &c1_hoistedGlobal, 4, 0U, 0U, 0U, 0),
                false);
  sf_mex_setcell(c1_y, 0, c1_b_y);
  c1_b_hoistedGlobal = *chartInstance->c1_PWM_CMD1;
  c1_c_y = NULL;
  sf_mex_assign(&c1_c_y, sf_mex_create("y", &c1_b_hoistedGlobal, 6, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 1, c1_c_y);
  c1_c_hoistedGlobal = *chartInstance->c1_PWM_CMD2;
  c1_d_y = NULL;
  sf_mex_assign(&c1_d_y, sf_mex_create("y", &c1_c_hoistedGlobal, 6, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 2, c1_d_y);
  c1_d_hoistedGlobal = *chartInstance->c1_PWM_OFF;
  c1_e_y = NULL;
  sf_mex_assign(&c1_e_y, sf_mex_create("y", &c1_d_hoistedGlobal, 5, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 3, c1_e_y);
  c1_e_hoistedGlobal = *chartInstance->c1_controller_State;
  c1_f_y = NULL;
  sf_mex_check_enum("ControlFlag", 10, c1_sv0, c1_iv0);
  c1_u = (int32_T)c1_e_hoistedGlobal;
  c1_g_y = NULL;
  sf_mex_assign(&c1_g_y, sf_mex_create("y", &c1_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_m0, c1_g_y, false);
  sf_mex_assign(&c1_f_y, sf_mex_create_enum("ControlFlag", c1_m0), false);
  sf_mex_destroy(&c1_m0);
  sf_mex_setcell(c1_y, 4, c1_f_y);
  c1_f_hoistedGlobal = chartInstance->c1_MaskFlag;
  c1_h_y = NULL;
  sf_mex_assign(&c1_h_y, sf_mex_create("y", &c1_f_hoistedGlobal, 5, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 5, c1_h_y);
  c1_g_hoistedGlobal = chartInstance->c1_OL_speed_state;
  c1_i_y = NULL;
  sf_mex_assign(&c1_i_y, sf_mex_create("y", &c1_g_hoistedGlobal, 4, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 6, c1_i_y);
  c1_h_hoistedGlobal = chartInstance->c1_OL_voltage_state_bemf;
  c1_j_y = NULL;
  sf_mex_assign(&c1_j_y, sf_mex_create("y", &c1_h_hoistedGlobal, 4, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 7, c1_j_y);
  c1_i_hoistedGlobal = chartInstance->c1_OL_voltage_state_is;
  c1_k_y = NULL;
  sf_mex_assign(&c1_k_y, sf_mex_create("y", &c1_i_hoistedGlobal, 4, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 8, c1_k_y);
  c1_j_hoistedGlobal = chartInstance->c1_OLspeed_sat;
  c1_l_y = NULL;
  sf_mex_assign(&c1_l_y, sf_mex_create("y", &c1_j_hoistedGlobal, 4, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 9, c1_l_y);
  c1_k_hoistedGlobal = chartInstance->c1_OLvoltage_sat;
  c1_m_y = NULL;
  sf_mex_assign(&c1_m_y, sf_mex_create("y", &c1_k_hoistedGlobal, 4, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 10, c1_m_y);
  c1_l_hoistedGlobal = chartInstance->c1_PT1_StateVariable;
  c1_n_y = NULL;
  sf_mex_assign(&c1_n_y, sf_mex_create("y", &c1_l_hoistedGlobal, 6, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 11, c1_n_y);
  c1_m_hoistedGlobal = chartInstance->c1_SpeedReference_PT1;
  c1_o_y = NULL;
  sf_mex_assign(&c1_o_y, sf_mex_create("y", &c1_m_hoistedGlobal, 4, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 12, c1_o_y);
  c1_n_hoistedGlobal = chartInstance->c1_SynchronisationTime_state;
  c1_p_y = NULL;
  sf_mex_assign(&c1_p_y, sf_mex_create("y", &c1_n_hoistedGlobal, 6, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 13, c1_p_y);
  c1_o_hoistedGlobal = chartInstance->c1_Vf_Done_SynchronisationEnable;
  c1_q_y = NULL;
  sf_mex_assign(&c1_q_y, sf_mex_create("y", &c1_o_hoistedGlobal, 11, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 14, c1_q_y);
  c1_p_hoistedGlobal = chartInstance->c1_fault;
  c1_r_y = NULL;
  sf_mex_assign(&c1_r_y, sf_mex_create("y", &c1_p_hoistedGlobal, 11, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 15, c1_r_y);
  c1_q_hoistedGlobal = chartInstance->c1_rotor_position_reference;
  c1_s_y = NULL;
  sf_mex_assign(&c1_s_y, sf_mex_create("y", &c1_q_hoistedGlobal, 4, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 16, c1_s_y);
  c1_r_hoistedGlobal = chartInstance->c1_is_active_c1_Motor_1pHController;
  c1_t_y = NULL;
  sf_mex_assign(&c1_t_y, sf_mex_create("y", &c1_r_hoistedGlobal, 3, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 17, c1_t_y);
  c1_s_hoistedGlobal = chartInstance->c1_is_c1_Motor_1pHController;
  c1_u_y = NULL;
  sf_mex_assign(&c1_u_y, sf_mex_create("y", &c1_s_hoistedGlobal, 3, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 18, c1_u_y);
  c1_t_hoistedGlobal = chartInstance->c1_is_Motor_On;
  c1_v_y = NULL;
  sf_mex_assign(&c1_v_y, sf_mex_create("y", &c1_t_hoistedGlobal, 3, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 19, c1_v_y);
  c1_u_hoistedGlobal = chartInstance->c1_temporalCounter_i1;
  c1_w_y = NULL;
  sf_mex_assign(&c1_w_y, sf_mex_create("y", &c1_u_hoistedGlobal, 7, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 20, c1_w_y);
  c1_v_hoistedGlobal = chartInstance->c1_previousTicks;
  c1_x_y = NULL;
  sf_mex_assign(&c1_x_y, sf_mex_create("y", &c1_v_hoistedGlobal, 7, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c1_y, 21, c1_x_y);
  sf_mex_assign(&c1_st, c1_y, false);
  return c1_st;
}

static void set_sim_state_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, const mxArray *c1_st)
{
  const mxArray *c1_u;
  c1_u = sf_mex_dup(c1_st);
  *chartInstance->c1_InvVoltageOut = c1_k_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 0)), "InvVoltageOut");
  *chartInstance->c1_PWM_CMD1 = c1_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c1_u, 1)), "PWM_CMD1");
  *chartInstance->c1_PWM_CMD2 = c1_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c1_u, 2)), "PWM_CMD2");
  *chartInstance->c1_PWM_OFF = c1_i_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c1_u, 3)), "PWM_OFF");
  *chartInstance->c1_controller_State = c1_e_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 4)), "controller_State");
  chartInstance->c1_MaskFlag = c1_i_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c1_u, 5)), "MaskFlag");
  chartInstance->c1_OL_speed_state = c1_k_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 6)), "OL_speed_state");
  chartInstance->c1_OL_voltage_state_bemf = c1_k_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 7)), "OL_voltage_state_bemf");
  chartInstance->c1_OL_voltage_state_is = c1_k_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 8)), "OL_voltage_state_is");
  chartInstance->c1_OLspeed_sat = c1_k_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 9)), "OLspeed_sat");
  chartInstance->c1_OLvoltage_sat = c1_k_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 10)), "OLvoltage_sat");
  chartInstance->c1_PT1_StateVariable = c1_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 11)), "PT1_StateVariable");
  chartInstance->c1_SpeedReference_PT1 = c1_k_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 12)), "SpeedReference_PT1");
  chartInstance->c1_SynchronisationTime_state = c1_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c1_u, 13)),
     "SynchronisationTime_state");
  chartInstance->c1_Vf_Done_SynchronisationEnable = c1_g_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c1_u, 14)),
     "Vf_Done_SynchronisationEnable");
  chartInstance->c1_fault = c1_g_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c1_u, 15)), "fault");
  chartInstance->c1_rotor_position_reference = c1_k_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c1_u, 16)),
     "rotor_position_reference");
  chartInstance->c1_is_active_c1_Motor_1pHController = c1_c_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c1_u, 17)),
     "is_active_c1_Motor_1pHController");
  chartInstance->c1_is_c1_Motor_1pHController = c1_c_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c1_u, 18)),
     "is_c1_Motor_1pHController");
  chartInstance->c1_is_Motor_On = c1_c_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 19)), "is_Motor_On");
  chartInstance->c1_temporalCounter_i1 = c1_m_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 20)), "temporalCounter_i1");
  chartInstance->c1_previousTicks = c1_m_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c1_u, 21)), "previousTicks");
  sf_mex_assign(&chartInstance->c1_setSimStateSideEffectsInfo,
                c1_o_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell
    (c1_u, 22)), "setSimStateSideEffectsInfo"), true);
  sf_mex_destroy(&c1_u);
  chartInstance->c1_doSetSimStateSideEffects = 1U;
  c1_update_debugger_state_c1_Motor_1pHController(chartInstance);
  sf_mex_destroy(&c1_st);
}

static void c1_set_sim_state_side_effects_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  if (chartInstance->c1_doSetSimStateSideEffects != 0) {
    if (chartInstance->c1_is_c1_Motor_1pHController == c1_IN_Init_Mode) {
      chartInstance->c1_tp_Init_Mode = 1U;
    } else {
      chartInstance->c1_tp_Init_Mode = 0U;
    }

    if (chartInstance->c1_is_c1_Motor_1pHController == c1_IN_Motor_On) {
      chartInstance->c1_tp_Motor_On = 1U;
    } else {
      chartInstance->c1_tp_Motor_On = 0U;
    }

    if (chartInstance->c1_is_Motor_On == c1_IN_Alignment_Done) {
      chartInstance->c1_tp_Alignment_Done = 1U;
      if (sf_mex_sub(chartInstance->c1_setSimStateSideEffectsInfo,
                     "setSimStateSideEffectsInfo", 1, 4) == 0.0) {
        chartInstance->c1_temporalCounter_i1 = 0U;
      }
    } else {
      chartInstance->c1_tp_Alignment_Done = 0U;
    }

    if (chartInstance->c1_is_Motor_On == c1_IN_Alignment_Mode) {
      chartInstance->c1_tp_Alignment_Mode = 1U;
      if (sf_mex_sub(chartInstance->c1_setSimStateSideEffectsInfo,
                     "setSimStateSideEffectsInfo", 1, 5) == 0.0) {
        chartInstance->c1_temporalCounter_i1 = 0U;
      }
    } else {
      chartInstance->c1_tp_Alignment_Mode = 0U;
    }

    if (chartInstance->c1_is_Motor_On == c1_IN_Error_Mode) {
      chartInstance->c1_tp_Error_Mode = 1U;
    } else {
      chartInstance->c1_tp_Error_Mode = 0U;
    }

    if (chartInstance->c1_is_Motor_On == c1_IN_VF_Mode) {
      chartInstance->c1_tp_VF_Mode = 1U;
    } else {
      chartInstance->c1_tp_VF_Mode = 0U;
    }

    if (chartInstance->c1_is_c1_Motor_1pHController == c1_IN_Stop_Mode) {
      chartInstance->c1_tp_Stop_Mode = 1U;
    } else {
      chartInstance->c1_tp_Stop_Mode = 0U;
    }

    chartInstance->c1_doSetSimStateSideEffects = 0U;
  }
}

static void finalize_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  sfListenerLightTerminate(chartInstance->c1_RuntimeVar);
  sf_mex_destroy(&chartInstance->c1_setSimStateSideEffectsInfo);
  covrtDeleteStateflowInstanceData(chartInstance->c1_covrtInstance);
}

static void sf_gateway_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  boolean_T c1_out;
  boolean_T c1_b_out;
  uint16_T c1_c_PWM_CMD1;
  uint16_T c1_c_PWM_CMD2;
  int16_T c1_b_InverterOutputVoltage;
  int16_T c1_c_OL_voltage_state_is;
  int16_T c1_c_OL_voltage_state_bemf;
  int16_T c1_c_OL_speed_state;
  int16_T c1_c_OLvoltage_sat;
  int16_T c1_c_OLspeed_sat;
  c1_set_sim_state_side_effects_c1_Motor_1pHController(chartInstance);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c1_presentTicks = (uint32_T)muDoubleScalarFloor((_sfTime_ -
    sf_get_start_time(chartInstance->S)) / 0.000125 + 0.5);
  chartInstance->c1_elapsedTicks = chartInstance->c1_presentTicks -
    chartInstance->c1_previousTicks;
  chartInstance->c1_previousTicks = chartInstance->c1_presentTicks;
  if (chartInstance->c1_temporalCounter_i1 + chartInstance->c1_elapsedTicks <=
      524287U) {
    chartInstance->c1_temporalCounter_i1 += chartInstance->c1_elapsedTicks;
  } else {
    chartInstance->c1_temporalCounter_i1 = 524287U;
  }

  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 0, chartInstance->c1_sfEvent);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_fault2, 24U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 24U, (real_T)
                    *chartInstance->c1_fault2);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_fault1, 23U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 23U, (real_T)
                    *chartInstance->c1_fault1);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_start_angle_neg, 22U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 22U, (real_T)
                    *chartInstance->c1_start_angle_neg);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_start_angle_pos, 21U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 21U, (real_T)
                    *chartInstance->c1_start_angle_pos);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_vdc, 20U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 20U, (real_T)
                    *chartInstance->c1_vdc);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_start_ctr, 19U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 19U, (real_T)
                    *chartInstance->c1_start_ctr);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_align_delay, 18U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 18U, (real_T)
                    *chartInstance->c1_align_delay);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_current_reference, 17U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 17U, (real_T)
                    *chartInstance->c1_current_reference);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_voltage_reference, 16U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 16U, (real_T)
                    *chartInstance->c1_voltage_reference);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_speed_reference, 15U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 15U, (real_T)
                    *chartInstance->c1_speed_reference);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_Direction, 13U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 13U, (real_T)
                    *chartInstance->c1_Direction);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_fault_clear, 12U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 12U, (real_T)
                    *chartInstance->c1_fault_clear);
  chartInstance->c1_sfEvent = CALL_EVENT;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
  if (chartInstance->c1_is_active_c1_Motor_1pHController == 0U) {
    _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
    chartInstance->c1_is_active_c1_Motor_1pHController = 1U;
    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 5U, chartInstance->c1_sfEvent);
    chartInstance->c1_is_c1_Motor_1pHController = c1_IN_Init_Mode;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
    chartInstance->c1_tp_Init_Mode = 1U;
    *chartInstance->c1_controller_State = ControlFlag_Init;
    chartInstance->c1_rotor_position_reference = 0;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_rotor_position_reference,
                          11U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 11U, (real_T)
                      chartInstance->c1_rotor_position_reference);
    chartInstance->c1_fault = false;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_fault, 10U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 10U, (real_T)
                      chartInstance->c1_fault);
    *chartInstance->c1_PWM_CMD1 = (uint16_T)((uint32_T)
      chartInstance->c1_PWM_MAX_DUTYCYCLE >> 1);
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD1, 26U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 26U, (real_T)
                      *chartInstance->c1_PWM_CMD1);
    *chartInstance->c1_PWM_CMD2 = (uint16_T)((uint32_T)
      chartInstance->c1_PWM_MAX_DUTYCYCLE >> 1);
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD2, 27U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 27U, (real_T)
                      *chartInstance->c1_PWM_CMD2);
    *chartInstance->c1_PWM_OFF = 1U;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_OFF, 28U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 28U, (real_T)
                      *chartInstance->c1_PWM_OFF);
    c1_state_variable_init(chartInstance, &c1_c_OL_voltage_state_is,
      &c1_c_OL_voltage_state_bemf, &c1_c_OL_speed_state, &c1_c_OLvoltage_sat,
      &c1_c_OLspeed_sat);
    chartInstance->c1_OL_voltage_state_is = c1_c_OL_voltage_state_is;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OL_voltage_state_is, 3U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 3U, (real_T)
                      chartInstance->c1_OL_voltage_state_is);
    chartInstance->c1_OL_voltage_state_bemf = c1_c_OL_voltage_state_bemf;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OL_voltage_state_bemf, 2U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 2U, (real_T)
                      chartInstance->c1_OL_voltage_state_bemf);
    chartInstance->c1_OL_speed_state = c1_c_OL_speed_state;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OL_speed_state, 1U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 1U, (real_T)
                      chartInstance->c1_OL_speed_state);
    chartInstance->c1_OLvoltage_sat = c1_c_OLvoltage_sat;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OLvoltage_sat, 5U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 5U, (real_T)
                      chartInstance->c1_OLvoltage_sat);
    chartInstance->c1_OLspeed_sat = c1_c_OLspeed_sat;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OLspeed_sat, 4U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 4U, (real_T)
                      chartInstance->c1_OLspeed_sat);
    *chartInstance->c1_controller_State = ControlFlag_InitDone;
  } else {
    switch (chartInstance->c1_is_c1_Motor_1pHController) {
     case c1_IN_Init_Mode:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 1U, 0, 0,
                        c1_IN_Init_Mode);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 4U,
                   chartInstance->c1_sfEvent);
      c1_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance, 5U,
        4U, 0U, covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 4U, 0, (*
        chartInstance->c1_controller_mode == ControlModeState_StopMode) != 0U)
        != 0U);
      if (c1_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
        chartInstance->c1_tp_Init_Mode = 0U;
        chartInstance->c1_is_c1_Motor_1pHController = c1_IN_NO_ACTIVE_CHILD;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
        chartInstance->c1_is_c1_Motor_1pHController = c1_IN_Stop_Mode;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 8U, chartInstance->c1_sfEvent);
        chartInstance->c1_tp_Stop_Mode = 1U;
        c1_enter_atomic_Stop_Mode(chartInstance);
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U,
                     chartInstance->c1_sfEvent);
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
      break;

     case c1_IN_Motor_On:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 1U, 0, 0,
                        c1_IN_Motor_On);
      c1_Motor_On(chartInstance);
      break;

     case c1_IN_Stop_Mode:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 1U, 0, 0,
                        c1_IN_Stop_Mode);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 1U,
                   chartInstance->c1_sfEvent);
      c1_b_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance, 5U,
        1U, 0U, covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 1U, 0, (*
        chartInstance->c1_controller_mode == ControlModeState_AlignmentMode) !=
        0U) != 0U);
      if (c1_b_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
        chartInstance->c1_tp_Stop_Mode = 0U;
        chartInstance->c1_is_c1_Motor_1pHController = c1_IN_NO_ACTIVE_CHILD;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 8U, chartInstance->c1_sfEvent);
        chartInstance->c1_is_c1_Motor_1pHController = c1_IN_Motor_On;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
        chartInstance->c1_tp_Motor_On = 1U;
        chartInstance->c1_is_Motor_On = c1_IN_Alignment_Mode;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, chartInstance->c1_sfEvent);
        chartInstance->c1_temporalCounter_i1 = 0U;
        chartInstance->c1_tp_Alignment_Mode = 1U;
        *chartInstance->c1_controller_State = ControlFlag_Alignment;
        *chartInstance->c1_PWM_OFF = 0U;
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_OFF, 28U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 28U, (real_T)
                          *chartInstance->c1_PWM_OFF);
        c1_modulation(chartInstance, chartInstance->c1_alignment_voltage, 0,
                      *chartInstance->c1_vdc, &c1_c_PWM_CMD1, &c1_c_PWM_CMD2,
                      &c1_b_InverterOutputVoltage);
        *chartInstance->c1_PWM_CMD1 = c1_c_PWM_CMD1;
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD1, 26U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 26U, (real_T)
                          *chartInstance->c1_PWM_CMD1);
        *chartInstance->c1_PWM_CMD2 = c1_c_PWM_CMD2;
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD2, 27U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 27U, (real_T)
                          *chartInstance->c1_PWM_CMD2);
        *chartInstance->c1_InvVoltageOut = c1_b_InverterOutputVoltage;
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_InvVoltageOut, 29U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 29U, (real_T)
                          *chartInstance->c1_InvVoltageOut);
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 8U,
                     chartInstance->c1_sfEvent);
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 8U, chartInstance->c1_sfEvent);
      break;

     default:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 1U, 0, 0, 0);

      /* Unreachable state, for coverage only */
      chartInstance->c1_is_c1_Motor_1pHController = c1_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
      break;
    }
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
}

static void mdl_start_c1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  static const uint32_T c1_decisionTxtStartIdx = 0U;
  static const uint32_T c1_decisionTxtEndIdx = 0U;
  static const uint32_T c1_b_decisionTxtStartIdx = 0U;
  static const uint32_T c1_b_decisionTxtEndIdx = 0U;
  static const uint32_T c1_c_decisionTxtStartIdx = 0U;
  static const uint32_T c1_c_decisionTxtEndIdx = 0U;
  static const uint32_T c1_d_decisionTxtStartIdx = 0U;
  static const uint32_T c1_d_decisionTxtEndIdx = 0U;
  static const uint32_T c1_e_decisionTxtStartIdx = 0U;
  static const uint32_T c1_e_decisionTxtEndIdx = 0U;
  static const uint32_T c1_f_decisionTxtStartIdx = 0U;
  static const uint32_T c1_f_decisionTxtEndIdx = 0U;
  static const uint32_T c1_saturationTxtStartIdx[2] = { 414U, 553U };

  static const uint32_T c1_saturationTxtEndIdx[2] = { 434U, 573U };

  static const uint32_T c1_g_decisionTxtStartIdx = 0U;
  static const uint32_T c1_g_decisionTxtEndIdx = 0U;
  static const uint32_T c1_h_decisionTxtStartIdx = 0U;
  static const uint32_T c1_h_decisionTxtEndIdx = 0U;
  static const uint32_T c1_i_decisionTxtStartIdx = 0U;
  static const uint32_T c1_i_decisionTxtEndIdx = 0U;
  static const uint32_T c1_j_decisionTxtStartIdx = 0U;
  static const uint32_T c1_j_decisionTxtEndIdx = 0U;
  static const uint32_T c1_k_decisionTxtStartIdx = 0U;
  static const uint32_T c1_k_decisionTxtEndIdx = 0U;
  static const uint32_T c1_l_decisionTxtStartIdx = 0U;
  static const uint32_T c1_l_decisionTxtEndIdx = 0U;
  static const uint32_T c1_m_decisionTxtStartIdx = 0U;
  static const uint32_T c1_m_decisionTxtEndIdx = 0U;
  static const uint32_T c1_n_decisionTxtStartIdx = 0U;
  static const uint32_T c1_n_decisionTxtEndIdx = 0U;
  static const uint32_T c1_o_decisionTxtStartIdx = 0U;
  static const uint32_T c1_o_decisionTxtEndIdx = 0U;
  static const uint32_T c1_transitionTxtStartIdx[1] = { 1U };

  static const uint32_T c1_transitionTxtEndIdx[1] = { 26U };

  static const int32_T c1_postfixPredicateTree[1] = { 0 };

  static const uint32_T c1_b_transitionTxtStartIdx[1] = { 1U };

  static const uint32_T c1_b_transitionTxtEndIdx[1] = { 31U };

  static const int32_T c1_b_postfixPredicateTree[1] = { 0 };

  static const uint32_T c1_c_transitionTxtStartIdx[1] = { 1U };

  static const uint32_T c1_c_transitionTxtEndIdx[1] = { 26U };

  static const int32_T c1_c_postfixPredicateTree[1] = { 0 };

  static const uint32_T c1_d_transitionTxtStartIdx[1] = { 1U };

  static const uint32_T c1_d_transitionTxtEndIdx[1] = { 56U };

  static const int32_T c1_d_postfixPredicateTree[1] = { 0 };

  static const int32_T c1_relopTxtStartIdx[1] = { 1 };

  static const int32_T c1_relopTxtEndIdx[1] = { 56 };

  static const int32_T c1_relationalopEps[1] = { 0 };

  static const int32_T c1_relationalopType[4] = { 1, 1, 1, 1 };

  static const uint32_T c1_e_transitionTxtStartIdx[1] = { 1U };

  static const uint32_T c1_e_transitionTxtEndIdx[1] = { 24U };

  static const int32_T c1_e_postfixPredicateTree[1] = { 0 };

  static const uint32_T c1_b_saturationTxtStartIdx[1] = { 1U };

  static const uint32_T c1_b_saturationTxtEndIdx[1] = { 6U };

  static const uint32_T c1_f_transitionTxtStartIdx[1] = { 1U };

  static const uint32_T c1_f_transitionTxtEndIdx[1] = { 48U };

  static const int32_T c1_f_postfixPredicateTree[1] = { 0 };

  static const int32_T c1_b_relopTxtStartIdx[1] = { 1 };

  static const int32_T c1_b_relopTxtEndIdx[1] = { 48 };

  static const int32_T c1_b_relationalopEps[1] = { 0 };

  static const int32_T c1_b_relationalopType[4] = { 1, 1, 1, 1 };

  static const uint32_T c1_c_saturationTxtStartIdx[1] = { 28U };

  static const uint32_T c1_c_saturationTxtEndIdx[1] = { 33U };

  static const uint32_T c1_g_transitionTxtStartIdx[1] = { 1U };

  static const uint32_T c1_g_transitionTxtEndIdx[1] = { 55U };

  static const int32_T c1_g_postfixPredicateTree[1] = { 0 };

  static const int32_T c1_c_relopTxtStartIdx[1] = { 1 };

  static const int32_T c1_c_relopTxtEndIdx[1] = { 55 };

  static const int32_T c1_c_relationalopEps[1] = { 0 };

  static const int32_T c1_c_relationalopType[4] = { 1, 1, 1, 1 };

  static const uint32_T c1_h_transitionTxtStartIdx[1] = { 3U };

  static const uint32_T c1_h_transitionTxtEndIdx[1] = { 14U };

  static const int32_T c1_h_postfixPredicateTree[1] = { 0 };

  static const int32_T c1_d_relopTxtStartIdx[1] = { 3 };

  static const int32_T c1_d_relopTxtEndIdx[1] = { 14 };

  static const int32_T c1_d_relationalopEps[1] = { 0 };

  static const int32_T c1_d_relationalopType[4] = { 0, 0, 0, 0 };

  chartInstance->c1_RuntimeVar = sfListenerCacheSimStruct(chartInstance->S);
  sfListenerRegisterHover(chartInstance->c1_RuntimeVar, (void *)
    sf_c1_Motor_1pHController_getDebuggerHoverDataFor);
  c1_init_sf_message_store_memory(chartInstance);
  sim_mode_is_external(chartInstance->S);
  covrtCreateStateflowInstanceData(chartInstance->c1_covrtInstance, 16U, 0U, 14U,
    155U);
  covrtChartInitFcn(chartInstance->c1_covrtInstance, 3U, true, false, false);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 0U, 0U, false, false, false,
                    0U, &c1_decisionTxtStartIdx, &c1_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 1U, 4U, true, true, false,
                    0U, &c1_b_decisionTxtStartIdx, &c1_b_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 2U, 0U, false, false, false,
                    0U, &c1_c_decisionTxtStartIdx, &c1_c_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 3U, 0U, false, false, false,
                    0U, &c1_d_decisionTxtStartIdx, &c1_d_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 4U, 0U, false, false, false,
                    0U, &c1_e_decisionTxtStartIdx, &c1_e_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 5U, 0U, false, false, false,
                    0U, &c1_f_decisionTxtStartIdx, &c1_f_decisionTxtEndIdx);
  covrtSaturationInitFcn(chartInstance->c1_covrtInstance, 4U, 5U, 2U,
    c1_saturationTxtStartIdx, c1_saturationTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 8U, 0U, false, false, false,
                    0U, &c1_g_decisionTxtStartIdx, &c1_g_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 6U, 0U, false, false, false,
                    0U, &c1_h_decisionTxtStartIdx, &c1_h_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 7U, 0U, false, false, false,
                    0U, &c1_i_decisionTxtStartIdx, &c1_i_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 9U, 0U, false, false, false,
                    0U, &c1_j_decisionTxtStartIdx, &c1_j_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 10U, 0U, false, false,
                    false, 0U, &c1_k_decisionTxtStartIdx,
                    &c1_k_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 12U, 0U, false, false,
                    false, 0U, &c1_l_decisionTxtStartIdx,
                    &c1_l_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 13U, 0U, false, false,
                    false, 0U, &c1_m_decisionTxtStartIdx,
                    &c1_m_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 14U, 0U, false, false,
                    false, 0U, &c1_n_decisionTxtStartIdx,
                    &c1_n_decisionTxtEndIdx);
  covrtStateInitFcn(chartInstance->c1_covrtInstance, 15U, 0U, false, false,
                    false, 0U, &c1_o_decisionTxtStartIdx,
                    &c1_o_decisionTxtEndIdx);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 5U, 0, NULL, NULL, 0U, NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 4U, 1,
                    c1_transitionTxtStartIdx, c1_transitionTxtEndIdx, 1U,
                    c1_postfixPredicateTree);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 1U, 1,
                    c1_b_transitionTxtStartIdx, c1_b_transitionTxtEndIdx, 1U,
                    c1_b_postfixPredicateTree);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 0U, 1,
                    c1_c_transitionTxtStartIdx, c1_c_transitionTxtEndIdx, 1U,
                    c1_c_postfixPredicateTree);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 2U, 1,
                    c1_d_transitionTxtStartIdx, c1_d_transitionTxtEndIdx, 1U,
                    c1_d_postfixPredicateTree);
  covrtRelationalopInitFcn(chartInstance->c1_covrtInstance, 2U, 1,
    c1_relopTxtStartIdx, c1_relopTxtEndIdx, c1_relationalopEps,
    c1_relationalopType);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 13U, 1,
                    c1_e_transitionTxtStartIdx, c1_e_transitionTxtEndIdx, 1U,
                    c1_e_postfixPredicateTree);
  covrtSaturationInitFcn(chartInstance->c1_covrtInstance, 5U, 13U, 1U,
    c1_b_saturationTxtStartIdx, c1_b_saturationTxtEndIdx);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 12U, 1,
                    c1_f_transitionTxtStartIdx, c1_f_transitionTxtEndIdx, 1U,
                    c1_f_postfixPredicateTree);
  covrtRelationalopInitFcn(chartInstance->c1_covrtInstance, 12U, 1,
    c1_b_relopTxtStartIdx, c1_b_relopTxtEndIdx, c1_b_relationalopEps,
    c1_b_relationalopType);
  covrtSaturationInitFcn(chartInstance->c1_covrtInstance, 5U, 12U, 1U,
    c1_c_saturationTxtStartIdx, c1_c_saturationTxtEndIdx);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 3U, 1,
                    c1_g_transitionTxtStartIdx, c1_g_transitionTxtEndIdx, 1U,
                    c1_g_postfixPredicateTree);
  covrtRelationalopInitFcn(chartInstance->c1_covrtInstance, 3U, 1,
    c1_c_relopTxtStartIdx, c1_c_relopTxtEndIdx, c1_c_relationalopEps,
    c1_c_relationalopType);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 6U, 0, NULL, NULL, 0U, NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 9U, 1,
                    c1_h_transitionTxtStartIdx, c1_h_transitionTxtEndIdx, 1U,
                    c1_h_postfixPredicateTree);
  covrtRelationalopInitFcn(chartInstance->c1_covrtInstance, 9U, 1,
    c1_d_relopTxtStartIdx, c1_d_relopTxtEndIdx, c1_d_relationalopEps,
    c1_d_relationalopType);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 7U, 0, NULL, NULL, 0U, NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 10U, 0, NULL, NULL, 0U,
                    NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 8U, 0, NULL, NULL, 0U, NULL);
  covrtTransInitFcn(chartInstance->c1_covrtInstance, 11U, 0, NULL, NULL, 0U,
                    NULL);
}

static void initSimStructsc1_Motor_1pHController
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c1_enter_atomic_Stop_Mode(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance)
{
  *chartInstance->c1_controller_State = ControlFlag_Stop;
  *chartInstance->c1_PWM_CMD1 = (uint16_T)((uint32_T)
    chartInstance->c1_PWM_MAX_DUTYCYCLE >> 1);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD1, 26U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 26U, (real_T)
                    *chartInstance->c1_PWM_CMD1);
  *chartInstance->c1_PWM_CMD2 = (uint16_T)((uint32_T)
    chartInstance->c1_PWM_MAX_DUTYCYCLE >> 1);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD2, 27U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 27U, (real_T)
                    *chartInstance->c1_PWM_CMD2);
  *chartInstance->c1_PWM_OFF = 1U;
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_OFF, 28U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 28U, (real_T)
                    *chartInstance->c1_PWM_OFF);
  chartInstance->c1_rotor_position_reference = 0;
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_rotor_position_reference, 11U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 11U, (real_T)
                    chartInstance->c1_rotor_position_reference);
  chartInstance->c1_PT1_StateVariable = 0;
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_PT1_StateVariable, 6U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 6U, (real_T)
                    chartInstance->c1_PT1_StateVariable);
}

static void c1_Motor_On(SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  boolean_T c1_out;
  int16_T c1_c_OL_voltage_state_is;
  int16_T c1_c_OL_voltage_state_bemf;
  int16_T c1_c_OL_speed_state;
  int16_T c1_c_OLvoltage_sat;
  int16_T c1_c_OLspeed_sat;
  int32_T c1_i2;
  int32_T c1_i3;
  boolean_T c1_covSaturation;
  boolean_T c1_b_covSaturation;
  int32_T c1_i4;
  boolean_T c1_b_out;
  boolean_T c1_c_out;
  int32_T c1_i5;
  boolean_T c1_d_out;
  uint16_T c1_c_PWM_CMD1;
  uint16_T c1_c_PWM_CMD2;
  int16_T c1_b_InverterOutputVoltage;
  boolean_T c1_hoistedGlobal;
  int16_T c1_b_hoistedGlobal;
  int16_T c1_c_hoistedGlobal;
  int16_T c1_e_speed_reference;
  int16_T c1_b_rotor_start_positive;
  int16_T c1_b_rotor_start_negative;
  int16_T c1_d_rotor_position_reference;
  int16_T c1_i6;
  int16_T c1_i7;
  int32_T c1_i8;
  boolean_T c1_c_covSaturation;
  int16_T c1_i9;
  int16_T c1_i10;
  int32_T c1_i11;
  boolean_T c1_d_covSaturation;
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 0U, chartInstance->c1_sfEvent);
  c1_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance, 5U, 0U,
    0U, covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 0U, 0,
    (*chartInstance->c1_controller_mode == ControlModeState_StopMode) != 0U) !=
    0U);
  if (c1_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 0U, chartInstance->c1_sfEvent);
    c1_state_variable_init(chartInstance, &c1_c_OL_voltage_state_is,
      &c1_c_OL_voltage_state_bemf, &c1_c_OL_speed_state, &c1_c_OLvoltage_sat,
      &c1_c_OLspeed_sat);
    chartInstance->c1_OL_voltage_state_is = c1_c_OL_voltage_state_is;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OL_voltage_state_is, 3U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 3U, (real_T)
                      chartInstance->c1_OL_voltage_state_is);
    chartInstance->c1_OL_voltage_state_bemf = c1_c_OL_voltage_state_bemf;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OL_voltage_state_bemf, 2U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 2U, (real_T)
                      chartInstance->c1_OL_voltage_state_bemf);
    chartInstance->c1_OL_speed_state = c1_c_OL_speed_state;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OL_speed_state, 1U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 1U, (real_T)
                      chartInstance->c1_OL_speed_state);
    chartInstance->c1_OLvoltage_sat = c1_c_OLvoltage_sat;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OLvoltage_sat, 5U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 5U, (real_T)
                      chartInstance->c1_OLvoltage_sat);
    chartInstance->c1_OLspeed_sat = c1_c_OLspeed_sat;
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OLspeed_sat, 4U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 4U, (real_T)
                      chartInstance->c1_OLspeed_sat);
    switch (chartInstance->c1_is_Motor_On) {
     case c1_IN_Alignment_Done:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 4U, 1, 1,
                        c1_IN_Alignment_Done);
      chartInstance->c1_tp_Alignment_Done = 0U;
      chartInstance->c1_is_Motor_On = c1_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c1_sfEvent);
      break;

     case c1_IN_Alignment_Mode:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 4U, 1, 1,
                        c1_IN_Alignment_Mode);
      chartInstance->c1_tp_Alignment_Mode = 0U;
      chartInstance->c1_is_Motor_On = c1_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c1_sfEvent);
      break;

     case c1_IN_Error_Mode:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 4U, 1, 1,
                        c1_IN_Error_Mode);
      chartInstance->c1_tp_Error_Mode = 0U;
      chartInstance->c1_is_Motor_On = c1_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
      break;

     case c1_IN_VF_Mode:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 4U, 1, 1, c1_IN_VF_Mode);
      chartInstance->c1_tp_VF_Mode = 0U;
      c1_exit_atomic_VF_Mode(chartInstance);
      chartInstance->c1_is_Motor_On = c1_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, chartInstance->c1_sfEvent);
      break;

     default:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 4U, 1, 1, 0);
      chartInstance->c1_is_Motor_On = c1_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c1_sfEvent);
      break;
    }

    chartInstance->c1_tp_Motor_On = 0U;
    chartInstance->c1_is_c1_Motor_1pHController = c1_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, chartInstance->c1_sfEvent);
    chartInstance->c1_is_c1_Motor_1pHController = c1_IN_Stop_Mode;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 8U, chartInstance->c1_sfEvent);
    chartInstance->c1_tp_Stop_Mode = 1U;
    c1_enter_atomic_Stop_Mode(chartInstance);
  } else {
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c1_sfEvent);
    switch (chartInstance->c1_is_Motor_On) {
     case c1_IN_Alignment_Done:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 4U, 1, 0,
                        c1_IN_Alignment_Done);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 12U,
                   chartInstance->c1_sfEvent);
      c1_i2 = chartInstance->c1_OffTime << 3;
      c1_covSaturation = false;
      if (c1_i2 < 0) {
        c1_covSaturation = true;
        c1_i2 = 0;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1270U, 28U, 5U);
      }

      c1_i4 = chartInstance->c1_OffTime << 3;
      if (c1_i4 < 0) {
        c1_covSaturation = true;
        c1_i4 = 0;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 1270U, 28U, 5U);
      }

      covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 5, 12, 0, 0,
        c1_covSaturation);
      c1_c_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance, 5U,
        12U, 0U, covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 12U, 0,
        (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance,
        5U, 12U, 0U, (real_T)((*chartInstance->c1_controller_mode ==
        ControlModeState_VFMode) & (chartInstance->c1_temporalCounter_i1 >=
        (uint32_T)c1_i2)), 0.0, 0, 1U, ((*chartInstance->c1_controller_mode ==
        ControlModeState_VFMode) & (chartInstance->c1_temporalCounter_i1 >=
        (uint32_T)c1_i4)) != 0) != 0U) != 0U);
      sf_temporal_value_range_check_min(chartInstance->S, 1270U, (real_T)
        chartInstance->c1_OffTime, 0.0);
      if (c1_c_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 12U, chartInstance->c1_sfEvent);
        chartInstance->c1_tp_Alignment_Done = 0U;
        chartInstance->c1_is_Motor_On = c1_IN_NO_ACTIVE_CHILD;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c1_sfEvent);
        chartInstance->c1_is_Motor_On = c1_IN_VF_Mode;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 5U, chartInstance->c1_sfEvent);
        chartInstance->c1_tp_VF_Mode = 1U;
        c1_hoistedGlobal = *chartInstance->c1_Direction;
        c1_b_hoistedGlobal = *chartInstance->c1_start_angle_pos;
        c1_c_hoistedGlobal = *chartInstance->c1_start_angle_neg;
        c1_e_speed_reference = (int16_T)c1_hoistedGlobal;
        c1_b_rotor_start_positive = c1_b_hoistedGlobal;
        c1_b_rotor_start_negative = c1_c_hoistedGlobal;
        _SFD_SET_DATA_VALUE_PTR(44U, &c1_d_rotor_position_reference);
        _SFD_SET_DATA_VALUE_PTR(41U, &c1_b_rotor_start_negative);
        _SFD_SET_DATA_VALUE_PTR(39U, &c1_b_rotor_start_positive);
        _SFD_SET_DATA_VALUE_PTR(34U, &c1_e_speed_reference);
        _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 10U, chartInstance->c1_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH(4U, 0U);
        _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("speed_reference",
          &c1_e_speed_reference, c1_f_sf_marshallOut, c1_f_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("rotor_start_positive",
          &c1_b_rotor_start_positive, c1_f_sf_marshallOut, c1_f_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("rotor_start_negative",
          &c1_b_rotor_start_negative, c1_f_sf_marshallOut, c1_f_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("rotor_position_reference",
          &c1_d_rotor_position_reference, c1_f_sf_marshallOut,
          c1_f_sf_marshallIn);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 10U,
                     chartInstance->c1_sfEvent);
        *chartInstance->c1_b_speed_reference = c1_e_speed_reference;
        *chartInstance->c1_rotor_start_positive = c1_b_rotor_start_positive;
        *chartInstance->c1_rotor_start_negative = c1_b_rotor_start_negative;
        sf_call_output_fcn_call(chartInstance->S, 0, "VFStart", 0);
        c1_d_rotor_position_reference =
          *chartInstance->c1_b_rotor_position_reference;
        _SFD_SYMBOL_SCOPE_POP();
        _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 10U, chartInstance->c1_sfEvent);
        _SFD_UNSET_DATA_VALUE_PTR(44U);
        _SFD_UNSET_DATA_VALUE_PTR(41U);
        _SFD_UNSET_DATA_VALUE_PTR(39U);
        _SFD_UNSET_DATA_VALUE_PTR(34U);
        _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 10U, chartInstance->c1_sfEvent);
        chartInstance->c1_rotor_position_reference =
          c1_d_rotor_position_reference;
        _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_rotor_position_reference,
                              11U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 11U, (real_T)
                          chartInstance->c1_rotor_position_reference);
        chartInstance->c1_MaskFlag = 0U;
        _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_MaskFlag, 0U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 0U, (real_T)
                          chartInstance->c1_MaskFlag);
        c1_i6 = *chartInstance->c1_speed_reference;
        if (c1_i6 < 0) {
          c1_i8 = -c1_i6;
          c1_c_covSaturation = false;
          if (c1_i8 > 32767) {
            c1_c_covSaturation = true;
            c1_i8 = 32767;
            _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 414U, 20U);
          } else {
            if (c1_i8 < -32768) {
              c1_c_covSaturation = true;
              c1_i8 = -32768;
              _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 414U, 20U);
            }
          }

          covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 4, 5, 0, 0,
            c1_c_covSaturation);
          c1_i7 = (int16_T)c1_i8;
        } else {
          c1_i7 = c1_i6;
        }

        *chartInstance->c1_controller_State = c1_VFDone_Flag(chartInstance,
          c1_i7, *chartInstance->c1_start_ctr);
        c1_i9 = *chartInstance->c1_speed_reference;
        if (c1_i9 < 0) {
          c1_i11 = -c1_i9;
          c1_d_covSaturation = false;
          if (c1_i11 > 32767) {
            c1_d_covSaturation = true;
            c1_i11 = 32767;
            _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 553U, 20U);
          } else {
            if (c1_i11 < -32768) {
              c1_d_covSaturation = true;
              c1_i11 = -32768;
              _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 553U, 20U);
            }
          }

          covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 4, 5, 1, 0,
            c1_d_covSaturation);
          c1_i10 = (int16_T)c1_i11;
        } else {
          c1_i10 = c1_i9;
        }

        chartInstance->c1_rotor_position_reference = c1_SpeedToAngle
          (chartInstance, c1_i10, chartInstance->c1_rotor_position_reference);
        _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_rotor_position_reference,
                              11U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 11U, (real_T)
                          chartInstance->c1_rotor_position_reference);
        c1_OpenClosedHBridge(chartInstance);
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U,
                     chartInstance->c1_sfEvent);
        *chartInstance->c1_controller_State = ControlFlag_AlignmentDone;
        *chartInstance->c1_InvVoltageOut = 0;
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_InvVoltageOut, 29U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 29U, (real_T)
                          *chartInstance->c1_InvVoltageOut);
        *chartInstance->c1_PWM_CMD1 = (uint16_T)((uint32_T)
          chartInstance->c1_PWM_MAX_DUTYCYCLE >> 1);
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD1, 26U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 26U, (real_T)
                          *chartInstance->c1_PWM_CMD1);
        *chartInstance->c1_PWM_CMD2 = (uint16_T)((uint32_T)
          chartInstance->c1_PWM_MAX_DUTYCYCLE >> 1);
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD2, 27U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 27U, (real_T)
                          *chartInstance->c1_PWM_CMD2);
        *chartInstance->c1_PWM_OFF = 1U;
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_OFF, 28U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 28U, (real_T)
                          *chartInstance->c1_PWM_OFF);
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U, chartInstance->c1_sfEvent);
      break;

     case c1_IN_Alignment_Mode:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 4U, 1, 0,
                        c1_IN_Alignment_Mode);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 13U,
                   chartInstance->c1_sfEvent);
      c1_i3 = *chartInstance->c1_align_delay << 3;
      c1_b_covSaturation = false;
      if (c1_i3 < 0) {
        c1_b_covSaturation = true;
        c1_i3 = 0;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 2043U, 1U, 5U);
      }

      covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 5, 13, 0, 0,
        c1_b_covSaturation);
      c1_b_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance, 5U,
        13U, 0U, covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 13U, 0,
        (chartInstance->c1_temporalCounter_i1 >= (uint32_T)c1_i3) != 0U) != 0U);
      sf_temporal_value_range_check_min(chartInstance->S, 2043U, (real_T)
        *chartInstance->c1_align_delay, 0.0);
      if (c1_b_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 13U, chartInstance->c1_sfEvent);
        chartInstance->c1_tp_Alignment_Mode = 0U;
        chartInstance->c1_is_Motor_On = c1_IN_NO_ACTIVE_CHILD;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c1_sfEvent);
        chartInstance->c1_is_Motor_On = c1_IN_Alignment_Done;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, chartInstance->c1_sfEvent);
        chartInstance->c1_temporalCounter_i1 = 0U;
        chartInstance->c1_tp_Alignment_Done = 1U;
        *chartInstance->c1_controller_State = ControlFlag_AlignmentDone;
        *chartInstance->c1_InvVoltageOut = 0;
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_InvVoltageOut, 29U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 29U, (real_T)
                          *chartInstance->c1_InvVoltageOut);
        *chartInstance->c1_PWM_CMD1 = (uint16_T)((uint32_T)
          chartInstance->c1_PWM_MAX_DUTYCYCLE >> 1);
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD1, 26U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 26U, (real_T)
                          *chartInstance->c1_PWM_CMD1);
        *chartInstance->c1_PWM_CMD2 = (uint16_T)((uint32_T)
          chartInstance->c1_PWM_MAX_DUTYCYCLE >> 1);
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD2, 27U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 27U, (real_T)
                          *chartInstance->c1_PWM_CMD2);
        *chartInstance->c1_PWM_OFF = 1U;
        _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_OFF, 28U);
        covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 28U, (real_T)
                          *chartInstance->c1_PWM_OFF);
      } else {
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 2U,
                     chartInstance->c1_sfEvent);
        c1_i5 = (*chartInstance->c1_controller_mode ==
                 ControlModeState_ErrorMode) | (int32_T)c1_b_fault(chartInstance,
          *chartInstance->c1_fault1, *chartInstance->c1_fault2);
        c1_d_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance,
          5U, 2U, 0U, covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 2U,
          0, (boolean_T)covrtRelationalopUpdateFcn
          (chartInstance->c1_covrtInstance, 5U, 2U, 0U, (real_T)c1_i5, 0.0, 0,
           1U, c1_i5 != 0) != 0U) != 0U);
        if (c1_d_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 2U, chartInstance->c1_sfEvent);
          chartInstance->c1_tp_Alignment_Mode = 0U;
          chartInstance->c1_is_Motor_On = c1_IN_NO_ACTIVE_CHILD;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, chartInstance->c1_sfEvent);
          chartInstance->c1_is_Motor_On = c1_IN_Error_Mode;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
          chartInstance->c1_tp_Error_Mode = 1U;
          *chartInstance->c1_PWM_CMD1 = (uint16_T)((uint32_T)
            chartInstance->c1_PWM_MAX_DUTYCYCLE >> 1);
          _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD1, 26U);
          covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 26U, (real_T)
                            *chartInstance->c1_PWM_CMD1);
          *chartInstance->c1_PWM_CMD2 = (uint16_T)((uint32_T)
            chartInstance->c1_PWM_MAX_DUTYCYCLE >> 1);
          _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD2, 27U);
          covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 27U, (real_T)
                            *chartInstance->c1_PWM_CMD2);
          *chartInstance->c1_PWM_OFF = 1U;
          _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_OFF, 28U);
          covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 28U, (real_T)
                            *chartInstance->c1_PWM_OFF);
          *chartInstance->c1_controller_State = c1_Protections(chartInstance,
            *chartInstance->c1_fault_clear);
        } else {
          _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 3U,
                       chartInstance->c1_sfEvent);
          c1_modulation(chartInstance, chartInstance->c1_alignment_voltage, 0,
                        *chartInstance->c1_vdc, &c1_c_PWM_CMD1, &c1_c_PWM_CMD2,
                        &c1_b_InverterOutputVoltage);
          *chartInstance->c1_PWM_CMD1 = c1_c_PWM_CMD1;
          _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD1, 26U);
          covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 26U, (real_T)
                            *chartInstance->c1_PWM_CMD1);
          *chartInstance->c1_PWM_CMD2 = c1_c_PWM_CMD2;
          _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD2, 27U);
          covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 27U, (real_T)
                            *chartInstance->c1_PWM_CMD2);
          *chartInstance->c1_InvVoltageOut = c1_b_InverterOutputVoltage;
          _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_InvVoltageOut, 29U);
          covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 29U, (real_T)
                            *chartInstance->c1_InvVoltageOut);
        }
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U, chartInstance->c1_sfEvent);
      break;

     case c1_IN_Error_Mode:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 4U, 1, 0,
                        c1_IN_Error_Mode);
      _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 4U,
                   chartInstance->c1_sfEvent);
      *chartInstance->c1_controller_State = c1_Protections(chartInstance,
        *chartInstance->c1_fault_clear);
      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 4U, chartInstance->c1_sfEvent);
      break;

     case c1_IN_VF_Mode:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 4U, 1, 0, c1_IN_VF_Mode);
      c1_VF_Mode(chartInstance);
      break;

     default:
      covrtDecUpdateFcn(chartInstance->c1_covrtInstance, 4U, 1, 0, 0);

      /* Unreachable state, for coverage only */
      chartInstance->c1_is_Motor_On = c1_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, chartInstance->c1_sfEvent);
      break;
    }
  }

  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c1_sfEvent);
}

static void c1_VF_Mode(SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  int32_T c1_i12;
  boolean_T c1_out;
  int16_T c1_i13;
  int16_T c1_i14;
  int32_T c1_i15;
  boolean_T c1_covSaturation;
  int16_T c1_i16;
  int16_T c1_i17;
  int32_T c1_i18;
  boolean_T c1_b_covSaturation;
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 3U, chartInstance->c1_sfEvent);
  c1_i12 = (*chartInstance->c1_controller_mode == ControlModeState_ErrorMode) |
    (int32_T)c1_b_fault(chartInstance, *chartInstance->c1_fault1,
                        *chartInstance->c1_fault2);
  c1_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance, 5U, 3U,
    0U, covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 3U, 0,
    (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance, 5U,
    3U, 0U, (real_T)c1_i12, 0.0, 0, 1U, c1_i12 != 0) != 0U) != 0U);
  if (c1_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 3U, chartInstance->c1_sfEvent);
    chartInstance->c1_tp_VF_Mode = 0U;
    c1_exit_atomic_VF_Mode(chartInstance);
    chartInstance->c1_is_Motor_On = c1_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, chartInstance->c1_sfEvent);
    chartInstance->c1_is_Motor_On = c1_IN_Error_Mode;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, chartInstance->c1_sfEvent);
    chartInstance->c1_tp_Error_Mode = 1U;
    *chartInstance->c1_PWM_CMD1 = (uint16_T)((uint32_T)
      chartInstance->c1_PWM_MAX_DUTYCYCLE >> 1);
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD1, 26U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 26U, (real_T)
                      *chartInstance->c1_PWM_CMD1);
    *chartInstance->c1_PWM_CMD2 = (uint16_T)((uint32_T)
      chartInstance->c1_PWM_MAX_DUTYCYCLE >> 1);
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD2, 27U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 27U, (real_T)
                      *chartInstance->c1_PWM_CMD2);
    *chartInstance->c1_PWM_OFF = 1U;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_OFF, 28U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 28U, (real_T)
                      *chartInstance->c1_PWM_OFF);
    *chartInstance->c1_controller_State = c1_Protections(chartInstance,
      *chartInstance->c1_fault_clear);
  } else {
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 5U, chartInstance->c1_sfEvent);
    c1_i13 = *chartInstance->c1_speed_reference;
    if (c1_i13 < 0) {
      c1_i15 = -c1_i13;
      c1_covSaturation = false;
      if (c1_i15 > 32767) {
        c1_covSaturation = true;
        c1_i15 = 32767;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 414U, 20U);
      } else {
        if (c1_i15 < -32768) {
          c1_covSaturation = true;
          c1_i15 = -32768;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 414U, 20U);
        }
      }

      covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 4, 5, 0, 0,
        c1_covSaturation);
      c1_i14 = (int16_T)c1_i15;
    } else {
      c1_i14 = c1_i13;
    }

    *chartInstance->c1_controller_State = c1_VFDone_Flag(chartInstance, c1_i14, *
      chartInstance->c1_start_ctr);
    c1_i16 = *chartInstance->c1_speed_reference;
    if (c1_i16 < 0) {
      c1_i18 = -c1_i16;
      c1_b_covSaturation = false;
      if (c1_i18 > 32767) {
        c1_b_covSaturation = true;
        c1_i18 = 32767;
        _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 553U, 20U);
      } else {
        if (c1_i18 < -32768) {
          c1_b_covSaturation = true;
          c1_i18 = -32768;
          _SFD_OVERFLOW_DETECTION(SFDB_SATURATE, 80U, 553U, 20U);
        }
      }

      covrtSaturationUpdateFcn(chartInstance->c1_covrtInstance, 4, 5, 1, 0,
        c1_b_covSaturation);
      c1_i17 = (int16_T)c1_i18;
    } else {
      c1_i17 = c1_i16;
    }

    chartInstance->c1_rotor_position_reference = c1_SpeedToAngle(chartInstance,
      c1_i17, chartInstance->c1_rotor_position_reference);
    _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_rotor_position_reference,
                          11U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 11U, (real_T)
                      chartInstance->c1_rotor_position_reference);
    c1_OpenClosedHBridge(chartInstance);
  }

  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 5U, chartInstance->c1_sfEvent);
}

static void c1_exit_atomic_VF_Mode(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance)
{
  _SFD_CS_CALL(STATE_ENTER_EXIT_FUNCTION_TAG, 5U, chartInstance->c1_sfEvent);
  chartInstance->c1_OLspeed_sat = chartInstance->c1_SpeedReference_PT1;
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OLspeed_sat, 4U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 4U, (real_T)
                    chartInstance->c1_OLspeed_sat);
  chartInstance->c1_OLvoltage_sat = *chartInstance->c1_voltage_reference;
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OLvoltage_sat, 5U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 5U, (real_T)
                    chartInstance->c1_OLvoltage_sat);
  chartInstance->c1_OL_speed_state = chartInstance->c1_SpeedReference_PT1;
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OL_speed_state, 1U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 1U, (real_T)
                    chartInstance->c1_OL_speed_state);
  chartInstance->c1_OL_voltage_state_bemf = *chartInstance->c1_voltage_reference;
  _SFD_DATA_RANGE_CHECK((real_T)chartInstance->c1_OL_voltage_state_bemf, 2U);
  covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 2U, (real_T)
                    chartInstance->c1_OL_voltage_state_bemf);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 5U, chartInstance->c1_sfEvent);
}

static void init_script_number_translation(uint32_T c1_machineNumber, uint32_T
  c1_chartNumber, uint32_T c1_instanceNumber)
{
  (void)(c1_machineNumber);
  (void)(c1_chartNumber);
  (void)(c1_instanceNumber);
}

const mxArray *sf_c1_Motor_1pHController_get_eml_resolved_functions_info(void)
{
  const mxArray *c1_nameCaptureInfo = NULL;
  c1_nameCaptureInfo = NULL;
  sf_mex_assign(&c1_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c1_nameCaptureInfo;
}

static const mxArray *c1_sfAfterOrElapsed(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance)
{
  const mxArray *c1_b;
  real_T c1_d8;
  real_T c1_d9;
  c1_b = NULL;
  c1_b = NULL;
  if (chartInstance->c1_is_Motor_On == c1_IN_Alignment_Mode) {
    c1_d9 = 0.000125 * (real_T)chartInstance->c1_temporalCounter_i1;
    sf_mex_assign(&c1_b, sf_mex_create("unnamed temp", &c1_d9, 0, 0U, 0U, 0U, 0),
                  false);
  } else {
    c1_d8 = -1.0;
    sf_mex_assign(&c1_b, sf_mex_create("unnamed temp", &c1_d8, 0, 0U, 0U, 0U, 0),
                  false);
  }

  return c1_b;
}

static const mxArray *c1_b_sfAfterOrElapsed
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  const mxArray *c1_b;
  real_T c1_d10;
  real_T c1_d11;
  c1_b = NULL;
  c1_b = NULL;
  if (chartInstance->c1_is_Motor_On == c1_IN_Alignment_Done) {
    c1_d11 = 0.000125 * (real_T)chartInstance->c1_temporalCounter_i1;
    sf_mex_assign(&c1_b, sf_mex_create("unnamed temp", &c1_d11, 0, 0U, 0U, 0U, 0),
                  false);
  } else {
    c1_d10 = -1.0;
    sf_mex_assign(&c1_b, sf_mex_create("unnamed temp", &c1_d10, 0, 0U, 0U, 0U, 0),
                  false);
  }

  return c1_b;
}

static c1_ControlFlag c1_VFDone_Flag(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, int16_T c1_e_speed_reference, boolean_T c1_c_start_ctr)
{
  c1_ControlFlag c1_c_control_flag;
  _SFD_SET_DATA_VALUE_PTR(43U, &c1_c_control_flag);
  _SFD_SET_DATA_VALUE_PTR(40U, &c1_c_start_ctr);
  _SFD_SET_DATA_VALUE_PTR(30U, &c1_e_speed_reference);
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 9U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(3U, 0U);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("speed_reference", &c1_e_speed_reference,
    c1_f_sf_marshallOut, c1_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("start_ctr", &c1_c_start_ctr,
    c1_d_sf_marshallOut, c1_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("control_flag", &c1_c_control_flag,
    c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 9U, chartInstance->c1_sfEvent);
  *chartInstance->c1_c_speed_reference = c1_e_speed_reference;
  *chartInstance->c1_b_start_ctr = c1_c_start_ctr;
  sf_call_output_fcn_call(chartInstance->S, 1, "VFDone_Flag", 0);
  c1_c_control_flag = *chartInstance->c1_control_flag;
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 9U, chartInstance->c1_sfEvent);
  _SFD_UNSET_DATA_VALUE_PTR(43U);
  _SFD_UNSET_DATA_VALUE_PTR(40U);
  _SFD_UNSET_DATA_VALUE_PTR(30U);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 9U, chartInstance->c1_sfEvent);
  return c1_c_control_flag;
}

static boolean_T c1_b_fault(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, boolean_T c1_c_fault1, boolean_T c1_c_fault2)
{
  boolean_T c1_d_fault;
  _SFD_SET_DATA_VALUE_PTR(45U, &c1_d_fault);
  _SFD_SET_DATA_VALUE_PTR(36U, &c1_c_fault2);
  _SFD_SET_DATA_VALUE_PTR(35U, &c1_c_fault1);
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 13U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(3U, 0U);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("fault1", &c1_c_fault1, c1_d_sf_marshallOut,
    c1_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("fault2", &c1_c_fault2, c1_d_sf_marshallOut,
    c1_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("fault", &c1_d_fault, c1_d_sf_marshallOut,
    c1_d_sf_marshallIn);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 13U, chartInstance->c1_sfEvent);
  *chartInstance->c1_b_fault1 = c1_c_fault1;
  *chartInstance->c1_b_fault2 = c1_c_fault2;
  sf_call_output_fcn_call(chartInstance->S, 2, "fault", 0);
  c1_d_fault = *chartInstance->c1_c_fault;
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 13U, chartInstance->c1_sfEvent);
  _SFD_UNSET_DATA_VALUE_PTR(45U);
  _SFD_UNSET_DATA_VALUE_PTR(36U);
  _SFD_UNSET_DATA_VALUE_PTR(35U);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 13U, chartInstance->c1_sfEvent);
  return c1_d_fault;
}

static c1_ControlFlag c1_Protections(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, boolean_T c1_c_fault_clear)
{
  c1_ControlFlag c1_c_control_flag;
  _SFD_SET_DATA_VALUE_PTR(47U, &c1_c_control_flag);
  _SFD_SET_DATA_VALUE_PTR(33U, &c1_c_fault_clear);
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 12U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(2U, 0U);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("fault_clear", &c1_c_fault_clear,
    c1_d_sf_marshallOut, c1_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("control_flag", &c1_c_control_flag,
    c1_c_sf_marshallOut, c1_c_sf_marshallIn);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 12U, chartInstance->c1_sfEvent);
  *chartInstance->c1_b_fault_clear = c1_c_fault_clear;
  sf_call_output_fcn_call(chartInstance->S, 3, "Protections", 0);
  c1_c_control_flag = *chartInstance->c1_b_control_flag;
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 12U, chartInstance->c1_sfEvent);
  _SFD_UNSET_DATA_VALUE_PTR(47U);
  _SFD_UNSET_DATA_VALUE_PTR(33U);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 12U, chartInstance->c1_sfEvent);
  return c1_c_control_flag;
}

static void c1_state_variable_init(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, int16_T *c1_c_OL_voltage_state_is, int16_T
  *c1_c_OL_voltage_state_bemf, int16_T *c1_c_OL_speed_state, int16_T
  *c1_c_OLvoltage_sat, int16_T *c1_c_OLspeed_sat)
{
  _SFD_SET_DATA_VALUE_PTR(55U, c1_c_OLspeed_sat);
  _SFD_SET_DATA_VALUE_PTR(54U, c1_c_OLvoltage_sat);
  _SFD_SET_DATA_VALUE_PTR(53U, c1_c_OL_speed_state);
  _SFD_SET_DATA_VALUE_PTR(51U, c1_c_OL_voltage_state_bemf);
  _SFD_SET_DATA_VALUE_PTR(46U, c1_c_OL_voltage_state_is);
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 15U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(5U, 0U);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("OL_voltage_state_is",
    c1_c_OL_voltage_state_is, c1_f_sf_marshallOut, c1_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("OL_voltage_state_bemf",
    c1_c_OL_voltage_state_bemf, c1_f_sf_marshallOut, c1_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("OL_speed_state", c1_c_OL_speed_state,
    c1_f_sf_marshallOut, c1_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("OLvoltage_sat", c1_c_OLvoltage_sat,
    c1_f_sf_marshallOut, c1_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("OLspeed_sat", c1_c_OLspeed_sat,
    c1_f_sf_marshallOut, c1_f_sf_marshallIn);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 15U, chartInstance->c1_sfEvent);
  sf_call_output_fcn_call(chartInstance->S, 4, "state_variable_init", 0);
  *c1_c_OL_voltage_state_is = *chartInstance->c1_b_OL_voltage_state_is;
  *c1_c_OL_voltage_state_bemf = *chartInstance->c1_b_OL_voltage_state_bemf;
  *c1_c_OL_speed_state = *chartInstance->c1_b_OL_speed_state;
  *c1_c_OLvoltage_sat = *chartInstance->c1_b_OLvoltage_sat;
  *c1_c_OLspeed_sat = *chartInstance->c1_b_OLspeed_sat;
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 15U, chartInstance->c1_sfEvent);
  _SFD_UNSET_DATA_VALUE_PTR(55U);
  _SFD_UNSET_DATA_VALUE_PTR(54U);
  _SFD_UNSET_DATA_VALUE_PTR(53U);
  _SFD_UNSET_DATA_VALUE_PTR(51U);
  _SFD_UNSET_DATA_VALUE_PTR(46U);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 15U, chartInstance->c1_sfEvent);
}

static int16_T c1_SpeedToAngle(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, int16_T c1_e_speed_reference, int16_T c1_b_angle_reference)
{
  int16_T c1_d_rotor_position_reference;
  _SFD_SET_DATA_VALUE_PTR(49U, &c1_d_rotor_position_reference);
  _SFD_SET_DATA_VALUE_PTR(37U, &c1_b_angle_reference);
  _SFD_SET_DATA_VALUE_PTR(32U, &c1_e_speed_reference);
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 7U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(3U, 0U);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("speed_reference", &c1_e_speed_reference,
    c1_f_sf_marshallOut, c1_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("angle_reference", &c1_b_angle_reference,
    c1_f_sf_marshallOut, c1_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("rotor_position_reference",
    &c1_d_rotor_position_reference, c1_f_sf_marshallOut, c1_f_sf_marshallIn);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 7U, chartInstance->c1_sfEvent);
  *chartInstance->c1_d_speed_reference = c1_e_speed_reference;
  *chartInstance->c1_angle_reference = c1_b_angle_reference;
  sf_call_output_fcn_call(chartInstance->S, 5, "SpeedToAngle", 0);
  c1_d_rotor_position_reference = *chartInstance->c1_c_rotor_position_reference;
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 7U, chartInstance->c1_sfEvent);
  _SFD_UNSET_DATA_VALUE_PTR(49U);
  _SFD_UNSET_DATA_VALUE_PTR(37U);
  _SFD_UNSET_DATA_VALUE_PTR(32U);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 7U, chartInstance->c1_sfEvent);
  return c1_d_rotor_position_reference;
}

static void c1_modulation(SFc1_Motor_1pHControllerInstanceStruct *chartInstance,
  int16_T c1_c_voltage_reference, int16_T c1_b_rotor_angle, int16_T
  c1_b_voltage_dc, uint16_T *c1_c_PWM_CMD1, uint16_T *c1_c_PWM_CMD2, int16_T
  *c1_b_InverterOutputVoltage)
{
  _SFD_SET_DATA_VALUE_PTR(52U, c1_b_InverterOutputVoltage);
  _SFD_SET_DATA_VALUE_PTR(50U, c1_c_PWM_CMD2);
  _SFD_SET_DATA_VALUE_PTR(48U, c1_c_PWM_CMD1);
  _SFD_SET_DATA_VALUE_PTR(42U, &c1_b_voltage_dc);
  _SFD_SET_DATA_VALUE_PTR(38U, &c1_b_rotor_angle);
  _SFD_SET_DATA_VALUE_PTR(31U, &c1_c_voltage_reference);
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 14U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(6U, 0U);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("voltage_reference", &c1_c_voltage_reference,
    c1_f_sf_marshallOut, c1_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("rotor_angle", &c1_b_rotor_angle,
    c1_f_sf_marshallOut, c1_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("voltage_dc", &c1_b_voltage_dc,
    c1_f_sf_marshallOut, c1_f_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("PWM_CMD1", c1_c_PWM_CMD1,
    c1_g_sf_marshallOut, c1_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("PWM_CMD2", c1_c_PWM_CMD2,
    c1_g_sf_marshallOut, c1_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_IMPORTABLE("InverterOutputVoltage",
    c1_b_InverterOutputVoltage, c1_f_sf_marshallOut, c1_f_sf_marshallIn);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 14U, chartInstance->c1_sfEvent);
  *chartInstance->c1_b_voltage_reference = c1_c_voltage_reference;
  *chartInstance->c1_rotor_angle = c1_b_rotor_angle;
  *chartInstance->c1_voltage_dc = c1_b_voltage_dc;
  sf_call_output_fcn_call(chartInstance->S, 8, "modulation", 0);
  *c1_c_PWM_CMD1 = *chartInstance->c1_b_PWM_CMD1;
  *c1_c_PWM_CMD2 = *chartInstance->c1_b_PWM_CMD2;
  *c1_b_InverterOutputVoltage = *chartInstance->c1_InverterOutputVoltage;
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 14U, chartInstance->c1_sfEvent);
  _SFD_UNSET_DATA_VALUE_PTR(52U);
  _SFD_UNSET_DATA_VALUE_PTR(50U);
  _SFD_UNSET_DATA_VALUE_PTR(48U);
  _SFD_UNSET_DATA_VALUE_PTR(42U);
  _SFD_UNSET_DATA_VALUE_PTR(38U);
  _SFD_UNSET_DATA_VALUE_PTR(31U);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 14U, chartInstance->c1_sfEvent);
}

static void c1_OpenClosedHBridge(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance)
{
  boolean_T c1_out;
  uint16_T c1_c_PWM_CMD1;
  uint16_T c1_c_PWM_CMD2;
  int16_T c1_b_InverterOutputVoltage;
  _SFD_CS_CALL(FUNCTION_ACTIVE_TAG, 6U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 6U, chartInstance->c1_sfEvent);
  _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 6U, chartInstance->c1_sfEvent);
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 9U, chartInstance->c1_sfEvent);
  c1_out = covrtTransitionDecUpdateFcn(chartInstance->c1_covrtInstance, 5U, 9U,
    0U, covrtCondUpdateFcn(chartInstance->c1_covrtInstance, 5U, 9U, 0,
    (boolean_T)covrtRelationalopUpdateFcn(chartInstance->c1_covrtInstance, 5U,
    9U, 0U, (real_T)chartInstance->c1_MaskFlag, 1.0, 0, 0U,
    chartInstance->c1_MaskFlag == 1) != 0U) != 0U);
  if (c1_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 9U, chartInstance->c1_sfEvent);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 10U, chartInstance->c1_sfEvent);
    *chartInstance->c1_PWM_OFF = 1U;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_OFF, 28U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 28U, (real_T)
                      *chartInstance->c1_PWM_OFF);
    *chartInstance->c1_PWM_CMD1 = (uint16_T)((uint32_T)
      chartInstance->c1_PWM_MAX_DUTYCYCLE >> 1);
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD1, 26U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 26U, (real_T)
                      *chartInstance->c1_PWM_CMD1);
    *chartInstance->c1_PWM_CMD2 = (uint16_T)((uint32_T)
      chartInstance->c1_PWM_MAX_DUTYCYCLE >> 1);
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD2, 27U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 27U, (real_T)
                      *chartInstance->c1_PWM_CMD2);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 8U, chartInstance->c1_sfEvent);
  } else {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 7U, chartInstance->c1_sfEvent);
    *chartInstance->c1_PWM_OFF = 0U;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_OFF, 28U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 28U, (real_T)
                      *chartInstance->c1_PWM_OFF);
    c1_modulation(chartInstance, *chartInstance->c1_voltage_reference,
                  chartInstance->c1_rotor_position_reference,
                  *chartInstance->c1_vdc, &c1_c_PWM_CMD1, &c1_c_PWM_CMD2,
                  &c1_b_InverterOutputVoltage);
    *chartInstance->c1_PWM_CMD1 = c1_c_PWM_CMD1;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD1, 26U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 26U, (real_T)
                      *chartInstance->c1_PWM_CMD1);
    *chartInstance->c1_PWM_CMD2 = c1_c_PWM_CMD2;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_PWM_CMD2, 27U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 27U, (real_T)
                      *chartInstance->c1_PWM_CMD2);
    *chartInstance->c1_InvVoltageOut = c1_b_InverterOutputVoltage;
    _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c1_InvVoltageOut, 29U);
    covrtSigUpdateFcn(chartInstance->c1_covrtInstance, 29U, (real_T)
                      *chartInstance->c1_InvVoltageOut);
  }

  _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 11U, chartInstance->c1_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CS_CALL(FUNCTION_INACTIVE_TAG, 6U, chartInstance->c1_sfEvent);
  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 6U, chartInstance->c1_sfEvent);
}

static const mxArray *c1_sf_marshallOut(void *chartInstanceVoid, void *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  int32_T c1_u;
  const mxArray *c1_y = NULL;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_u = *(int32_T *)c1_inData;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static int32_T c1_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_b_sfEvent, const char_T *c1_identifier)
{
  int32_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_sfEvent),
    &c1_thisId);
  sf_mex_destroy(&c1_b_sfEvent);
  return c1_y;
}

static int32_T c1_b_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  int32_T c1_y;
  int32_T c1_i19;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_i19, 1, 6, 0U, 0, 0U, 0);
  c1_y = c1_i19;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_b_sfEvent;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  int32_T c1_y;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_b_sfEvent = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_sfEvent),
    &c1_thisId);
  sf_mex_destroy(&c1_b_sfEvent);
  *(int32_T *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_b_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  uint8_T c1_u;
  const mxArray *c1_y = NULL;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_u = *(uint8_T *)c1_inData;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static uint8_T c1_c_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_b_tp_Stop_Mode, const char_T *c1_identifier)
{
  uint8_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_tp_Stop_Mode),
    &c1_thisId);
  sf_mex_destroy(&c1_b_tp_Stop_Mode);
  return c1_y;
}

static uint8_T c1_d_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  uint8_T c1_y;
  uint8_T c1_u2;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_u2, 1, 3, 0U, 0, 0U, 0);
  c1_y = c1_u2;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_b_tp_Stop_Mode;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  uint8_T c1_y;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_b_tp_Stop_Mode = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_tp_Stop_Mode),
    &c1_thisId);
  sf_mex_destroy(&c1_b_tp_Stop_Mode);
  *(uint8_T *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_c_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  c1_ControlFlag c1_u;
  const mxArray *c1_y = NULL;
  int32_T c1_b_u;
  const mxArray *c1_b_y = NULL;
  const mxArray *c1_m1 = NULL;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_u = *(c1_ControlFlag *)c1_inData;
  c1_y = NULL;
  sf_mex_check_enum("ControlFlag", 10, c1_sv0, c1_iv0);
  c1_b_u = (int32_T)c1_u;
  c1_b_y = NULL;
  sf_mex_assign(&c1_b_y, sf_mex_create("y", &c1_b_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_m1, c1_b_y, false);
  sf_mex_assign(&c1_y, sf_mex_create_enum("ControlFlag", c1_m1), false);
  sf_mex_destroy(&c1_m1);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static c1_ControlFlag c1_e_emlrt_marshallIn
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, const mxArray
   *c1_b_controller_State, const char_T *c1_identifier)
{
  c1_ControlFlag c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_controller_State),
    &c1_thisId);
  sf_mex_destroy(&c1_b_controller_State);
  return c1_y;
}

static c1_ControlFlag c1_f_emlrt_marshallIn
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, const mxArray *c1_u,
   const emlrtMsgIdentifier *c1_parentId)
{
  c1_ControlFlag c1_y;
  (void)chartInstance;
  sf_mex_check_enum("ControlFlag", 10, c1_sv0, c1_iv0);
  sf_mex_check_builtin(c1_parentId, c1_u, "ControlFlag", 0, 0U, NULL);
  c1_y = (c1_ControlFlag)sf_mex_get_enum_element(c1_u, 0);
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_b_controller_State;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  c1_ControlFlag c1_y;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_b_controller_State = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_controller_State),
    &c1_thisId);
  sf_mex_destroy(&c1_b_controller_State);
  *(c1_ControlFlag *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_d_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  boolean_T c1_u;
  const mxArray *c1_y = NULL;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_u = *(boolean_T *)c1_inData;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_u, 11, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static boolean_T c1_g_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_d_fault, const char_T *c1_identifier)
{
  boolean_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_d_fault), &c1_thisId);
  sf_mex_destroy(&c1_d_fault);
  return c1_y;
}

static boolean_T c1_h_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  boolean_T c1_y;
  boolean_T c1_b0;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_b0, 1, 11, 0U, 0, 0U, 0);
  c1_y = c1_b0;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_d_fault;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  boolean_T c1_y;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_d_fault = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_d_fault), &c1_thisId);
  sf_mex_destroy(&c1_d_fault);
  *(boolean_T *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_e_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  c1_ControlModeState c1_u;
  const mxArray *c1_y = NULL;
  static const char * c1_enumNames[6] = { "InitMode", "StopMode",
    "AlignmentMode", "VFMode", "ControlMode", "ErrorMode" };

  static const int32_T c1_enumValues[6] = { 0, 1, 2, 3, 4, 5 };

  int32_T c1_b_u;
  const mxArray *c1_b_y = NULL;
  const mxArray *c1_m2 = NULL;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_u = *(c1_ControlModeState *)c1_inData;
  c1_y = NULL;
  sf_mex_check_enum("ControlModeState", 6, c1_enumNames, c1_enumValues);
  c1_b_u = (int32_T)c1_u;
  c1_b_y = NULL;
  sf_mex_assign(&c1_b_y, sf_mex_create("y", &c1_b_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_m2, c1_b_y, false);
  sf_mex_assign(&c1_y, sf_mex_create_enum("ControlModeState", c1_m2), false);
  sf_mex_destroy(&c1_m2);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static const mxArray *c1_f_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  int16_T c1_u;
  const mxArray *c1_y = NULL;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_u = *(int16_T *)c1_inData;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_u, 4, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static const mxArray *c1_g_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData;
  uint16_T c1_u;
  const mxArray *c1_y = NULL;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_mxArrayOutData = NULL;
  c1_u = *(uint16_T *)c1_inData;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_u, 5, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static uint16_T c1_i_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_b_PWM_OFF, const char_T *c1_identifier)
{
  uint16_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_j_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_PWM_OFF),
    &c1_thisId);
  sf_mex_destroy(&c1_b_PWM_OFF);
  return c1_y;
}

static uint16_T c1_j_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  uint16_T c1_y;
  uint16_T c1_u3;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_u3, 1, 5, 0U, 0, 0U, 0);
  c1_y = c1_u3;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_b_PWM_OFF;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  uint16_T c1_y;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_b_PWM_OFF = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_j_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_PWM_OFF),
    &c1_thisId);
  sf_mex_destroy(&c1_b_PWM_OFF);
  *(uint16_T *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

static int16_T c1_k_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_c_OL_speed_state, const char_T
  *c1_identifier)
{
  int16_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_l_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_c_OL_speed_state),
    &c1_thisId);
  sf_mex_destroy(&c1_c_OL_speed_state);
  return c1_y;
}

static int16_T c1_l_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  int16_T c1_y;
  int16_T c1_i20;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_i20, 1, 4, 0U, 0, 0U, 0);
  c1_y = c1_i20;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_c_OL_speed_state;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  int16_T c1_y;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)chartInstanceVoid;
  c1_c_OL_speed_state = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_l_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_c_OL_speed_state),
    &c1_thisId);
  sf_mex_destroy(&c1_c_OL_speed_state);
  *(int16_T *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

static uint32_T c1_m_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_b_temporalCounter_i1, const char_T
  *c1_identifier)
{
  uint32_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_n_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_temporalCounter_i1),
    &c1_thisId);
  sf_mex_destroy(&c1_b_temporalCounter_i1);
  return c1_y;
}

static uint32_T c1_n_emlrt_marshallIn(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance, const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  uint32_T c1_y;
  uint32_T c1_u4;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_u4, 1, 7, 0U, 0, 0U, 0);
  c1_y = c1_u4;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static const mxArray *c1_o_emlrt_marshallIn
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, const mxArray
   *c1_b_setSimStateSideEffectsInfo, const char_T *c1_identifier)
{
  const mxArray *c1_y = NULL;
  emlrtMsgIdentifier c1_thisId;
  c1_y = NULL;
  c1_thisId.fIdentifier = (const char *)c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  sf_mex_assign(&c1_y, c1_p_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c1_b_setSimStateSideEffectsInfo), &c1_thisId), false);
  sf_mex_destroy(&c1_b_setSimStateSideEffectsInfo);
  return c1_y;
}

static const mxArray *c1_p_emlrt_marshallIn
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, const mxArray *c1_u,
   const emlrtMsgIdentifier *c1_parentId)
{
  const mxArray *c1_y = NULL;
  (void)chartInstance;
  (void)c1_parentId;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_duplicatearraysafe(&c1_u), false);
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_slStringInitializeDynamicBuffers
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *sf_get_hover_data_for_msg
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, int32_T c1_ssid)
{
  (void)chartInstance;
  (void)c1_ssid;
  return NULL;
}

static void c1_init_sf_message_store_memory
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

mxArray *sf_c1_Motor_1pHController_getDebuggerHoverDataFor
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance, uint32_T c1_b)
{
  mxArray *c1_c = NULL;
  const mxArray *c1_m3 = NULL;
  const mxArray *c1_m4 = NULL;
  c1_c = NULL;
  switch (c1_b) {
   case 2043U:
    sf_mex_assign(&c1_m3, c1_sfAfterOrElapsed(chartInstance), false);
    sfAppendHoverData(&c1_c, sf_mex_dup(c1_m3), 2043U, "afterOrElapsed", 240U,
                      -1, -1, -1, -1);
    break;

   case 1270U:
    sf_mex_assign(&c1_m4, c1_b_sfAfterOrElapsed(chartInstance), false);
    sfAppendHoverData(&c1_c, sf_mex_dup(c1_m4), 1270U, "afterOrElapsed", 1269U,
                      -1, -1, -1, -1);
    break;
  }

  sf_mex_destroy(&c1_m3);
  sf_mex_destroy(&c1_m4);
  return c1_c;
}

static void init_test_point_addr_map(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance)
{
  chartInstance->c1_testPointAddrMap[0] = chartInstance->c1_PWM_CMD2;
}

static void **get_test_point_address_map(SFc1_Motor_1pHControllerInstanceStruct *
  chartInstance)
{
  return &chartInstance->c1_testPointAddrMap[0];
}

static rtwCAPI_ModelMappingInfo *get_test_point_mapping_info
  (SFc1_Motor_1pHControllerInstanceStruct *chartInstance)
{
  return &chartInstance->c1_testPointMappingInfo;
}

static void init_dsm_address_info(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc1_Motor_1pHControllerInstanceStruct
  *chartInstance)
{
  chartInstance->c1_covrtInstance = (CovrtStateflowInstance *)
    sfrtGetCovrtInstance(chartInstance->S);
  chartInstance->c1_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c1_controller_State = (c1_ControlFlag *)
    ssGetOutputPortSignal_wrapper(chartInstance->S, 1);
  chartInstance->c1_PWM_CMD1 = (int32_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c1_fault_clear = (boolean_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c1_Direction = (boolean_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c1_controller_mode = (c1_ControlModeState *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 2);
  chartInstance->c1_speed_reference = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c1_voltage_reference = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c1_current_reference = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 5);
  chartInstance->c1_PWM_CMD2 = (int32_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c1_PWM_OFF = (uint16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c1_align_delay = (uint16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 6);
  chartInstance->c1_start_ctr = (boolean_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 7);
  chartInstance->c1_vdc = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 8);
  chartInstance->c1_start_angle_pos = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 9);
  chartInstance->c1_start_angle_neg = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 10);
  chartInstance->c1_fault1 = (boolean_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 11);
  chartInstance->c1_fault2 = (boolean_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 12);
  chartInstance->c1_InvVoltageOut = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 5);
  chartInstance->c1_b_speed_reference = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 6);
  chartInstance->c1_b_rotor_position_reference = (int16_T *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 13);
  chartInstance->c1_rotor_start_positive = (int16_T *)
    ssGetOutputPortSignal_wrapper(chartInstance->S, 7);
  chartInstance->c1_rotor_start_negative = (int16_T *)
    ssGetOutputPortSignal_wrapper(chartInstance->S, 8);
  chartInstance->c1_c_speed_reference = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 9);
  chartInstance->c1_control_flag = (c1_ControlFlag *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 14);
  chartInstance->c1_b_start_ctr = (boolean_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 10);
  chartInstance->c1_c_fault = (boolean_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 15);
  chartInstance->c1_b_fault1 = (boolean_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 11);
  chartInstance->c1_b_fault2 = (boolean_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 12);
  chartInstance->c1_b_control_flag = (c1_ControlFlag *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 16);
  chartInstance->c1_b_fault_clear = (boolean_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 13);
  chartInstance->c1_b_OL_voltage_state_is = (int16_T *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 17);
  chartInstance->c1_b_OL_voltage_state_bemf = (int16_T *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 18);
  chartInstance->c1_b_OL_speed_state = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 19);
  chartInstance->c1_b_OLvoltage_sat = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 20);
  chartInstance->c1_b_OLspeed_sat = (int16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 21);
  chartInstance->c1_d_speed_reference = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 14);
  chartInstance->c1_angle_reference = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 15);
  chartInstance->c1_c_rotor_position_reference = (int16_T *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 22);
  chartInstance->c1_b_voltage_reference = (int16_T *)
    ssGetOutputPortSignal_wrapper(chartInstance->S, 26);
  chartInstance->c1_b_PWM_CMD1 = (uint16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 26);
  chartInstance->c1_rotor_angle = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 27);
  chartInstance->c1_b_PWM_CMD2 = (uint16_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 27);
  chartInstance->c1_voltage_dc = (int16_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 28);
  chartInstance->c1_InverterOutputVoltage = (int16_T *)
    ssGetInputPortSignal_wrapper(chartInstance->S, 28);
}

/* SFunction Glue Code */
static void init_test_point_mapping_info(SimStruct *S);
void sf_c1_Motor_1pHController_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(2582631766U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(1096412713U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3228724179U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(3583446808U);
}

mxArray *sf_c1_Motor_1pHController_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c1_Motor_1pHController_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("testpoint");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("PWM_CMD2");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c1_Motor_1pHController_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c1_Motor_1pHController(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  mxArray *mxVarInfo = sf_mex_decode(
    "eNrN2MFu0zAYAGC3dF2niWmsGhoHpIoDB06UEzcidVSrtKoVrcrReJk7rCV25DgVewMOO6Ct0o5"
    "7CQ4cOOwhOHDkATjwCNh1WoIV0aiJqCNZ0W/pT/3Ffxw3oNTpAnnsyPaxDkBVnmuylYE+NuK4JN"
    "vD+Kz7K4v+hmziIsCqP+Ru51SeKfJnMYo+dOiYza7/Evy5fjXl+qXE9bfifn38epUr/+eNo/KdR"
    "H4lJf9+In83jjt0MmKeQGe4F4n4Ps3v19o8324dMz/NUzM8Ku6/7cJW97BphQP4eR0vrHB8zzYf"
    "m4ZjM3b02m076uqHn+k52TUcqrmMCs48D3M4EEjgYjw1J1f+5dXK9dVF4XnbQ2dWOA6uM83LjuF"
    "Qce8YhgHGpzBUs1LU+pXT82k6yz9a4tk3PPvaM9HrsRbBE+yPgSWuy2yuuuGqp7lIuPCs3QWmme"
    "pv23Btz1xx+SFhj6cyXXk/0DteTBMStng+32SquweGR8X9YVMv2CPECTrxcMKzdteXbK49w6Xig"
    "aq6N3iMOaYuhpJpkeurdvWXuB4ZLhUPLqj7njNKZPkRRofEx/Ol3R7fXTbfY8On4tEYHjKKoeF8"
    "TePatMLXjzLtKzYMn4rHKPKsWTfeeZmerwPDoWLOBOMwYCFR0wP5/EkrxtXI+b7Sv7+s/hqGS8U"
    "khMgVZIKh24TdGbIZHLUWu9tifE/+iy9t/ZC+dFih60dO391g5f2G9GlcjxboeVrIfK3yHhPYDx"
    "hHXotFVMg/VyTxHquV/v29pmR8r7kXx2psWAAWAlckx/d8yfjKf42vDNQGyJyn6pK8rdimD+ro7"
    "xnXznq/QzwrZH5X2U8GHE8Ii8Ihcc/DRL3+BunG5TM="
    );
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c1_Motor_1pHController_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static const mxArray* sf_opaque_get_hover_data_for_msg(void* chartInstance,
  int32_T msgSSID)
{
  return sf_get_hover_data_for_msg( (SFc1_Motor_1pHControllerInstanceStruct *)
    chartInstance, msgSSID);
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc1_Motor_1pHControllerInstanceStruct *chartInstance =
      (SFc1_Motor_1pHControllerInstanceStruct *)sf_get_chart_instance_ptr(S);
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _Motor_1pHControllerMachineNumber_,
           1,
           16,
           14,
           0,
           60,
           0,
           0,
           0,
           0,
           0,
           &chartInstance->chartNumber,
           &chartInstance->instanceNumber,
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_Motor_1pHControllerMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_Motor_1pHControllerMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _Motor_1pHControllerMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,0,0,0,"MaskFlag");
          _SFD_SET_DATA_PROPS(1,0,0,0,"OL_speed_state");
          _SFD_SET_DATA_PROPS(2,0,0,0,"OL_voltage_state_bemf");
          _SFD_SET_DATA_PROPS(3,0,0,0,"OL_voltage_state_is");
          _SFD_SET_DATA_PROPS(4,0,0,0,"OLspeed_sat");
          _SFD_SET_DATA_PROPS(5,0,0,0,"OLvoltage_sat");
          _SFD_SET_DATA_PROPS(6,0,0,0,"PT1_StateVariable");
          _SFD_SET_DATA_PROPS(7,0,0,0,"SpeedReference_PT1");
          _SFD_SET_DATA_PROPS(8,0,0,0,"SynchronisationTime_state");
          _SFD_SET_DATA_PROPS(9,0,0,0,"Vf_Done_SynchronisationEnable");
          _SFD_SET_DATA_PROPS(10,0,0,0,"fault");
          _SFD_SET_DATA_PROPS(11,0,0,0,"rotor_position_reference");
          _SFD_SET_DATA_PROPS(12,1,1,0,"fault_clear");
          _SFD_SET_DATA_PROPS(13,1,1,0,"Direction");
          _SFD_SET_DATA_PROPS(14,1,1,0,"controller_mode");
          _SFD_SET_DATA_PROPS(15,1,1,0,"speed_reference");
          _SFD_SET_DATA_PROPS(16,1,1,0,"voltage_reference");
          _SFD_SET_DATA_PROPS(17,1,1,0,"current_reference");
          _SFD_SET_DATA_PROPS(18,1,1,0,"align_delay");
          _SFD_SET_DATA_PROPS(19,1,1,0,"start_ctr");
          _SFD_SET_DATA_PROPS(20,1,1,0,"vdc");
          _SFD_SET_DATA_PROPS(21,1,1,0,"start_angle_pos");
          _SFD_SET_DATA_PROPS(22,1,1,0,"start_angle_neg");
          _SFD_SET_DATA_PROPS(23,1,1,0,"fault1");
          _SFD_SET_DATA_PROPS(24,1,1,0,"fault2");
          _SFD_SET_DATA_PROPS(25,2,0,1,"controller_State");
          _SFD_SET_DATA_PROPS(26,2,0,1,"PWM_CMD1");
          _SFD_SET_DATA_PROPS(27,2,0,1,"PWM_CMD2");
          _SFD_SET_DATA_PROPS(28,2,0,1,"PWM_OFF");
          _SFD_SET_DATA_PROPS(29,2,0,1,"InvVoltageOut");
          _SFD_SET_DATA_PROPS(30,8,0,0,"");
          _SFD_SET_DATA_PROPS(31,8,0,0,"");
          _SFD_SET_DATA_PROPS(32,8,0,0,"");
          _SFD_SET_DATA_PROPS(33,8,0,0,"");
          _SFD_SET_DATA_PROPS(34,8,0,0,"");
          _SFD_SET_DATA_PROPS(35,8,0,0,"");
          _SFD_SET_DATA_PROPS(36,8,0,0,"");
          _SFD_SET_DATA_PROPS(37,8,0,0,"");
          _SFD_SET_DATA_PROPS(38,8,0,0,"");
          _SFD_SET_DATA_PROPS(39,8,0,0,"");
          _SFD_SET_DATA_PROPS(40,8,0,0,"");
          _SFD_SET_DATA_PROPS(41,8,0,0,"");
          _SFD_SET_DATA_PROPS(42,8,0,0,"");
          _SFD_SET_DATA_PROPS(43,9,0,0,"");
          _SFD_SET_DATA_PROPS(44,9,0,0,"");
          _SFD_SET_DATA_PROPS(45,9,0,0,"");
          _SFD_SET_DATA_PROPS(46,9,0,0,"");
          _SFD_SET_DATA_PROPS(47,9,0,0,"");
          _SFD_SET_DATA_PROPS(48,9,0,0,"");
          _SFD_SET_DATA_PROPS(49,9,0,0,"");
          _SFD_SET_DATA_PROPS(50,9,0,0,"");
          _SFD_SET_DATA_PROPS(51,9,0,0,"");
          _SFD_SET_DATA_PROPS(52,9,0,0,"");
          _SFD_SET_DATA_PROPS(53,9,0,0,"");
          _SFD_SET_DATA_PROPS(54,9,0,0,"");
          _SFD_SET_DATA_PROPS(55,9,0,0,"");
          _SFD_SET_DATA_PROPS(56,10,0,0,"OffTime");
          _SFD_SET_DATA_PROPS(57,10,0,0,"PWM_MAX_DUTYCYCLE");
          _SFD_SET_DATA_PROPS(58,10,0,0,"alignment_voltage");
          _SFD_SET_DATA_PROPS(59,10,0,0,"scale");
          _SFD_STATE_INFO(0,0,0);
          _SFD_STATE_INFO(1,0,0);
          _SFD_STATE_INFO(2,0,0);
          _SFD_STATE_INFO(3,0,0);
          _SFD_STATE_INFO(4,0,0);
          _SFD_STATE_INFO(5,0,0);
          _SFD_STATE_INFO(8,0,0);
          _SFD_STATE_INFO(6,0,2);
          _SFD_STATE_INFO(7,0,2);
          _SFD_STATE_INFO(9,0,2);
          _SFD_STATE_INFO(10,0,2);
          _SFD_STATE_INFO(12,0,2);
          _SFD_STATE_INFO(13,0,2);
          _SFD_STATE_INFO(14,0,2);
          _SFD_STATE_INFO(15,0,2);
          _SFD_CH_SUBSTATE_COUNT(3);
          _SFD_CH_SUBSTATE_DECOMP(0);
          _SFD_CH_SUBSTATE_INDEX(0,0);
          _SFD_CH_SUBSTATE_INDEX(1,1);
          _SFD_CH_SUBSTATE_INDEX(2,8);
          _SFD_ST_SUBSTATE_COUNT(0,0);
          _SFD_ST_SUBSTATE_COUNT(1,4);
          _SFD_ST_SUBSTATE_INDEX(1,0,2);
          _SFD_ST_SUBSTATE_INDEX(1,1,3);
          _SFD_ST_SUBSTATE_INDEX(1,2,4);
          _SFD_ST_SUBSTATE_INDEX(1,3,5);
          _SFD_ST_SUBSTATE_COUNT(2,0);
          _SFD_ST_SUBSTATE_COUNT(3,0);
          _SFD_ST_SUBSTATE_COUNT(4,0);
          _SFD_ST_SUBSTATE_COUNT(5,0);
          _SFD_ST_SUBSTATE_COUNT(8,0);
        }

        _SFD_SET_DATA_COMPILED_PROPS(0,SF_UINT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_g_sf_marshallOut,(MexInFcnForType)c1_e_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(1,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(5,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(6,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(7,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(8,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(9,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_d_sf_marshallOut,(MexInFcnForType)c1_d_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(10,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_d_sf_marshallOut,(MexInFcnForType)c1_d_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(11,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(12,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_d_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(13,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_d_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(14,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_e_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(15,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(16,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(17,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(18,SF_UINT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_g_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(19,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_d_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(20,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(21,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(22,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(23,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_d_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(24,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_d_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(25,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(26,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(27,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(28,SF_UINT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_g_sf_marshallOut,(MexInFcnForType)c1_e_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(29,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(30,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(31,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(32,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(33,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_d_sf_marshallOut,(MexInFcnForType)c1_d_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(34,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(35,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_d_sf_marshallOut,(MexInFcnForType)c1_d_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(36,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_d_sf_marshallOut,(MexInFcnForType)c1_d_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(37,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(38,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(39,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(40,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_d_sf_marshallOut,(MexInFcnForType)c1_d_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(41,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(42,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(43,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(44,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(45,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_d_sf_marshallOut,(MexInFcnForType)c1_d_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(46,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(47,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_c_sf_marshallOut,(MexInFcnForType)c1_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(48,SF_UINT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_g_sf_marshallOut,(MexInFcnForType)c1_e_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(49,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(50,SF_UINT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_g_sf_marshallOut,(MexInFcnForType)c1_e_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(51,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(52,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(53,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(54,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(55,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(56,SF_UINT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_g_sf_marshallOut,(MexInFcnForType)c1_e_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(57,SF_UINT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_g_sf_marshallOut,(MexInFcnForType)c1_e_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(58,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(59,SF_INT16,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_f_sf_marshallOut,(MexInFcnForType)c1_f_sf_marshallIn);
        _SFD_SET_DATA_VALUE_PTR(30,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(31,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(32,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(33,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(34,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(35,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(36,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(37,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(38,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(39,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(40,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(41,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(42,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(43,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(44,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(45,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(46,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(47,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(48,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(49,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(50,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(51,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(52,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(53,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(54,(void *)(NULL));
        _SFD_SET_DATA_VALUE_PTR(55,(void *)(NULL));
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _Motor_1pHControllerMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc1_Motor_1pHControllerInstanceStruct *chartInstance =
      (SFc1_Motor_1pHControllerInstanceStruct *)sf_get_chart_instance_ptr(S);
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(25U, chartInstance->c1_controller_State);
        _SFD_SET_DATA_VALUE_PTR(26U, chartInstance->c1_PWM_CMD1);
        _SFD_SET_DATA_VALUE_PTR(10U, &chartInstance->c1_fault);
        _SFD_SET_DATA_VALUE_PTR(12U, chartInstance->c1_fault_clear);
        _SFD_SET_DATA_VALUE_PTR(13U, chartInstance->c1_Direction);
        _SFD_SET_DATA_VALUE_PTR(14U, chartInstance->c1_controller_mode);
        _SFD_SET_DATA_VALUE_PTR(15U, chartInstance->c1_speed_reference);
        _SFD_SET_DATA_VALUE_PTR(16U, chartInstance->c1_voltage_reference);
        _SFD_SET_DATA_VALUE_PTR(17U, chartInstance->c1_current_reference);
        _SFD_SET_DATA_VALUE_PTR(27U, chartInstance->c1_PWM_CMD2);
        _SFD_SET_DATA_VALUE_PTR(28U, chartInstance->c1_PWM_OFF);
        _SFD_SET_DATA_VALUE_PTR(18U, chartInstance->c1_align_delay);
        _SFD_SET_DATA_VALUE_PTR(19U, chartInstance->c1_start_ctr);
        _SFD_SET_DATA_VALUE_PTR(20U, chartInstance->c1_vdc);
        _SFD_SET_DATA_VALUE_PTR(0U, &chartInstance->c1_MaskFlag);
        _SFD_SET_DATA_VALUE_PTR(1U, &chartInstance->c1_OL_speed_state);
        _SFD_SET_DATA_VALUE_PTR(4U, &chartInstance->c1_OLspeed_sat);
        _SFD_SET_DATA_VALUE_PTR(5U, &chartInstance->c1_OLvoltage_sat);
        _SFD_SET_DATA_VALUE_PTR(3U, &chartInstance->c1_OL_voltage_state_is);
        _SFD_SET_DATA_VALUE_PTR(2U, &chartInstance->c1_OL_voltage_state_bemf);
        _SFD_SET_DATA_VALUE_PTR(59U, &chartInstance->c1_scale);
        _SFD_SET_DATA_VALUE_PTR(21U, chartInstance->c1_start_angle_pos);
        _SFD_SET_DATA_VALUE_PTR(22U, chartInstance->c1_start_angle_neg);
        _SFD_SET_DATA_VALUE_PTR(23U, chartInstance->c1_fault1);
        _SFD_SET_DATA_VALUE_PTR(24U, chartInstance->c1_fault2);
        _SFD_SET_DATA_VALUE_PTR(57U, &chartInstance->c1_PWM_MAX_DUTYCYCLE);
        _SFD_SET_DATA_VALUE_PTR(6U, &chartInstance->c1_PT1_StateVariable);
        _SFD_SET_DATA_VALUE_PTR(8U, &chartInstance->c1_SynchronisationTime_state);
        _SFD_SET_DATA_VALUE_PTR(9U,
          &chartInstance->c1_Vf_Done_SynchronisationEnable);
        _SFD_SET_DATA_VALUE_PTR(11U, &chartInstance->c1_rotor_position_reference);
        _SFD_SET_DATA_VALUE_PTR(7U, &chartInstance->c1_SpeedReference_PT1);
        _SFD_SET_DATA_VALUE_PTR(29U, chartInstance->c1_InvVoltageOut);
        _SFD_SET_DATA_VALUE_PTR(58U, &chartInstance->c1_alignment_voltage);
        _SFD_SET_DATA_VALUE_PTR(56U, &chartInstance->c1_OffTime);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "sdv4PdumdPmlzOAUs31BjBG";
}

static void sf_opaque_initialize_c1_Motor_1pHController(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc1_Motor_1pHControllerInstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c1_Motor_1pHController
    ((SFc1_Motor_1pHControllerInstanceStruct*) chartInstanceVar);
  initialize_c1_Motor_1pHController((SFc1_Motor_1pHControllerInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c1_Motor_1pHController(void *chartInstanceVar)
{
  enable_c1_Motor_1pHController((SFc1_Motor_1pHControllerInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c1_Motor_1pHController(void *chartInstanceVar)
{
  disable_c1_Motor_1pHController((SFc1_Motor_1pHControllerInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c1_Motor_1pHController(void *chartInstanceVar)
{
  sf_gateway_c1_Motor_1pHController((SFc1_Motor_1pHControllerInstanceStruct*)
    chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c1_Motor_1pHController(SimStruct*
  S)
{
  return get_sim_state_c1_Motor_1pHController
    ((SFc1_Motor_1pHControllerInstanceStruct *)sf_get_chart_instance_ptr(S));/* raw sim ctx */
}

static void sf_opaque_set_sim_state_c1_Motor_1pHController(SimStruct* S, const
  mxArray *st)
{
  set_sim_state_c1_Motor_1pHController((SFc1_Motor_1pHControllerInstanceStruct*)
    sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_terminate_c1_Motor_1pHController(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc1_Motor_1pHControllerInstanceStruct*) chartInstanceVar
      )->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_Motor_1pHController_optimization_info();
    }

    finalize_c1_Motor_1pHController((SFc1_Motor_1pHControllerInstanceStruct*)
      chartInstanceVar);
    if (!sim_mode_is_rtw_gen(S)) {
      ssSetModelMappingInfoPtr(S, NULL);
    }

    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc1_Motor_1pHController((SFc1_Motor_1pHControllerInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c1_Motor_1pHController(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    initialize_params_c1_Motor_1pHController
      ((SFc1_Motor_1pHControllerInstanceStruct*)sf_get_chart_instance_ptr(S));
    initSimStructsc1_Motor_1pHController((SFc1_Motor_1pHControllerInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

const char* sf_c1_Motor_1pHController_get_post_codegen_info(void)
{
  return
    (
     "eNrtmU9v40QUwJ3SdlstrQplYZG2UsQBECe6cEBCgmyTho2UkGjTdiUu3qk9qYeMZ8z8SVtOHDn"
     "sAbGVeuTAV0CCA0J75sJlDxw5IsEB8QmYsZ00O3ES1wlbI2HJtZ773vP7zXt+M55YhVrDUse6Or"
     "8oWtayuq6oc8GKjqVYLgyd0f1F641Y3ldKRPotwIDPrYkHAT68BznFUiBKaqRDE9UQ6UAGiaN0A"
     "8rEOG8c+RIj0q1K4mh//L6HHK/tUYndHWUL3CbBp8pbIEVL+akgBh1RhdAVHqPyyKticDSImInj"
     "sgedLpf+JAQORVsGOizekFigAMPdE+jUCBdARcwvYmsLIGBZnIzF1KS83VekfoARIIm0HuBtGKg"
     "BFnA/cNXfphQKylRzPMDEDvRAD/I66oY+KYGmT8TVPw4RAYIyBPCuj8vacDS2FlbxNKgL8YQBUb"
     "HtMAi6AUVEjM9/u6pIdwk4xLACD+XReG9t+JnUyT9A8BiysePWKdMeZOAINsnYh4YDsnsSZmtQJ"
     "aNqAvnwALA7jsofh+7Y6lWVw9tA5QnuKYtxajCErPE9hnpqeMd6k35NV+a0V0b6UbL5NLXQ224P"
     "TsrCwFvVIWWAMR+rtkeDOuxBHHqtAAEmq0Vek/UCRgNwpKrWVYOsS7xMiYtGcuFILqhfVuVWqdc"
     "/Vp0iObgLtRoRkHWAA5PeW+540JUYKlDVmUT4uASnLuI6W1O04pxO07J4R5LKMWVdBTrhtb9A0D"
     "mY1B90X37buujLz6foy4uxnWVc3xzyU0jwYw1dtX5rSH914Wn9LeO5C/17k47Yb2nI75oRx6LhV"
     "+ttqLP4+y9/f7v6/QdP3v/kJ+uPn8/NcTHjK4zEV7D6z3/vpcvNc+uxfKvfUAbl1hvJsta9OxTX"
     "YoL/V4b8b8Qyd3vvtlzpuy0ff968s8/f2d75dOej0N+Xm5PjfdmIt3+/qDvbaRD2Kc6cmhtPwFo"
     "GMpqWwvEYind5ynisxvej468PZ7L/87xk1kPSeK0Z46XlGukdUCx09w+nweE6vzKeJ9+UTPsknh"
     "WDR8ut+w273Khs54LD8mfluJ0Ljl/T5eOawXEt5mhWq/moq9/8VO/JhsGhT4cSwSjGkNnhKnM+P"
     "Culmewffp25vhqAd/WqPRccNx+lysu6waHlZt3mgVpO2lxnZV79a0aer85KaeavGwbPjYinF/Xj"
     "iMg+hH7HygnXw3RcmwbXZhIX4iPrqivjss5S1d91g+t6yBWXHxD54Vk8y7weaNYHaQIiLzzfnae"
     "quxcMHi239rajhh1+MalPj4T1/JVx/ZCO60WDS8ttXXX3YLy9YyvMHHH9GHG1pnC9anBpuX1KHI"
     "9Ror4m9ceh3h7ot/b88D1Ox7dl8Gn5oGNXKIG2wRnt5+SFryVTrSuWDD4td4DEuekbD3Cq9+umw"
     "aFlRgVldkB5uEdhs/6bNh+u4ozz1ei+QhJX0eAqhvt9NnAE6kHb2bYbIeR2cLc8WN3Oh++1Z8KX"
     "1D8UXzLYXPvHjHyP25nXG4ovgos3a+fD8/pc8pVlHhPQDygDuEyl3oS00dA8tlK43P7Sc7GsY4P"
     "CotxyxLj9v6T4Fp6Kb8HSCyAzT8tT7FZjtnhftxTtZzwqXe0+xFtzyW+W9WTAYA9RyfeQ0+VGvW"
     "bdd7ysnfW/XS7sCv+R580zzjS/M2xltLuV0S5rnKsZ7dYy2i1l/L3lsvqT5gXL0N+Y4Tn/tv7w9"
     "R8/uBvV"
     );
}

static void mdlSetWorkWidths_c1_Motor_1pHController(SimStruct *S)
{
  sf_set_work_widths(S, sf_c1_Motor_1pHController_get_post_codegen_info());
  ssSetChecksum0(S,(4073711648U));
  ssSetChecksum1(S,(1052182947U));
  ssSetChecksum2(S,(3193584596U));
  ssSetChecksum3(S,(2546658048U));
}

static void mdlRTW_c1_Motor_1pHController(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Stateflow");
  }
}

static void mdlStart_c1_Motor_1pHController(SimStruct *S)
{
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance;
  chartInstance = (SFc1_Motor_1pHControllerInstanceStruct *)utMalloc(sizeof
    (SFc1_Motor_1pHControllerInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc1_Motor_1pHControllerInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 0;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c1_Motor_1pHController;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c1_Motor_1pHController;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c1_Motor_1pHController;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c1_Motor_1pHController;
  chartInstance->chartInfo.disableChart =
    sf_opaque_disable_c1_Motor_1pHController;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c1_Motor_1pHController;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c1_Motor_1pHController;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c1_Motor_1pHController;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c1_Motor_1pHController;
  chartInstance->chartInfo.mdlStart = mdlStart_c1_Motor_1pHController;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c1_Motor_1pHController;
  chartInstance->chartInfo.callGetHoverDataForMsg =
    sf_opaque_get_hover_data_for_msg;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartEventFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  chartInstance->chartInfo.dispatchToExportedFcn = NULL;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0, NULL, NULL);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
    init_test_point_mapping_info(S);
  }

  chart_debug_initialization(S,1);
  mdl_start_c1_Motor_1pHController(chartInstance);
}

void c1_Motor_1pHController_method_dispatcher(SimStruct *S, int_T method, void
  *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c1_Motor_1pHController(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c1_Motor_1pHController(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c1_Motor_1pHController(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c1_Motor_1pHController_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}

static const rtwCAPI_DataTypeMap dataTypeMap[] = {
  /* cName, mwName, numElements, elemMapIndex, dataSize, slDataId, isComplex, isPointer */
  { "int32_T", "int32_T", 0, 0, sizeof(int32_T), SS_INT32, 0, 0 } };

static real_T sfCAPIsampleTimeZero = 0.0;
static const rtwCAPI_SampleTimeMap sampleTimeMap[] = {
  /* *period, *offset, taskId, mode */
  { &sfCAPIsampleTimeZero, &sfCAPIsampleTimeZero, 0, 0 }
};

static const rtwCAPI_DimensionMap dimensionMap[] = {
  /* dataOrientation, dimArrayIndex, numDims*/
  { rtwCAPI_SCALAR, 0, 2 } };

static const rtwCAPI_Signals testPointSignals[] = {
  /* addrMapIndex, sysNum, SFRelativePath, dataName, portNumber, dataTypeIndex, dimIndex, fixPtIdx, sTimeIndex */
  { 0, 0, "StateflowChart/PWM_CMD2", "PWM_CMD2", 0, 0, 0, 0, 0 } };

static const rtwCAPI_FixPtMap fixedPointMap[] = {
  /* *fracSlope, *bias, scaleType, wordLength, exponent, isSigned */
  { NULL, NULL, rtwCAPI_FIX_RESERVED, 64, 0, 0 } };

static const uint_T dimensionArray[] = {
  1, 1 };

static rtwCAPI_ModelMappingStaticInfo testPointMappingStaticInfo = {
  /* block signal monitoring */
  {
    testPointSignals,                  /* Block signals Array  */
    1,                                 /* Num Block IO signals */
    NULL,                              /* Root Inputs Array    */
    0,                                 /* Num root inputs      */
    NULL,                              /* Root Outputs Array */
    0                                  /* Num root outputs   */
  },

  /* parameter tuning */
  {
    NULL,                              /* Block parameters Array    */
    0,                                 /* Num block parameters      */
    NULL,                              /* Variable parameters Array */
    0                                  /* Num variable parameters   */
  },

  /* block states */
  {
    NULL,                              /* Block States array        */
    0                                  /* Num Block States          */
  },

  /* Static maps */
  {
    dataTypeMap,                       /* Data Type Map            */
    dimensionMap,                      /* Data Dimension Map       */
    fixedPointMap,                     /* Fixed Point Map          */
    NULL,                              /* Structure Element map    */
    sampleTimeMap,                     /* Sample Times Map         */
    dimensionArray                     /* Dimension Array          */
  },

  /* Target type */
  "float",

  {
    4073711648U,
    1052182947U,
    3193584596U,
    2546658048U
  }
};

static void init_test_point_mapping_info(SimStruct *S)
{
  rtwCAPI_ModelMappingInfo *testPointMappingInfo;
  void **testPointAddrMap;
  SFc1_Motor_1pHControllerInstanceStruct *chartInstance =
    (SFc1_Motor_1pHControllerInstanceStruct *)sf_get_chart_instance_ptr(S);
  init_test_point_addr_map(chartInstance);
  testPointMappingInfo = get_test_point_mapping_info(chartInstance);
  testPointAddrMap = get_test_point_address_map(chartInstance);
  rtwCAPI_SetStaticMap(*testPointMappingInfo, &testPointMappingStaticInfo);
  rtwCAPI_SetLoggingStaticMap(*testPointMappingInfo, NULL);
  rtwCAPI_SetInstanceLoggingInfo(*testPointMappingInfo, NULL);
  rtwCAPI_SetPath(*testPointMappingInfo, "");
  rtwCAPI_SetFullPath(*testPointMappingInfo, NULL);
  rtwCAPI_SetDataAddressMap(*testPointMappingInfo, testPointAddrMap);
  rtwCAPI_SetChildMMIArray(*testPointMappingInfo, NULL);
  rtwCAPI_SetChildMMIArrayLen(*testPointMappingInfo, 0);
  ssSetModelMappingInfoPtr(S, testPointMappingInfo);
}
