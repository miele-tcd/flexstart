% function Elektronik_TEC152
%-------------------------------------------------------------------------------------------------
% (c) Miele & Cie. / Electronic
%
% Datum:       Name:          Aenderung:
% 12.05.2020   T.Rodehueser   Ersterstellung 


global EFU Software

EFU.Name = 'EFU_TEC152';

% Wertebereich der Datentypen
EFU.VarMax_s16          =  2^15 -1;            % Maximalwert s16
EFU.VarMin_s16          = -2^15;               % Minimalwert s16
EFU.VarMax_u16          =  2^16 -1;            % Maximalwert u16
EFU.VarMin_u16          =  0;                  % Minimalwert u16
EFU.VarMax_s32          =  2^31 -1;            % Maximalwert s16
EFU.VarMin_s32          = -2^31;               % Minimalwert s16
EFU.VarMax_u32          =  2^32 -1;            % Maximalwert u16
EFU.VarMin_u32          =  0;    

EFU.ADC_VRef            = 3.3;                 % Referenzspannung ADC in V
EFU.ADC_BIT             = 12;                  % 12 Bit ADC
EFU.CMP_BIT             = 6;                   % 6 Bit CMP


% Skalierung in der Software
Software.Scale_1V = 1000;   % 1V --> 1000 Incremente -->(mV]
Software.Scale_1A = 1000;   % 1A --> 1000 Incremente -->(mA]
Software.Scale_1W = 1000;   % 1W --> 1000 Incremente -->(mW]
Software.Scale_1rpm = 1;     % 1rpm --> 1rpm

% Spannungsmessung UDC
EFU.Rv_Udc                      = 10000;                % Vorwiderstand in Ohm
EFU.Rm_Udc                      = 2740;                 % Messwiderstand in Ohm
EFU.Cm_Udc                      = 10e-9;                % Filterkondensator
% Spannungsmessung UDC
EFU.Rv_Umotor                   = 10000;               % Vorwiderstand in Ohm
EFU.Rm_Umotor                   = 2740;                % Messwiderstand in Ohm
EFU.Cm_Umotor                      = 10e-9;                % Filterkondensator

% Strommessung Phasensstrom
EFU.RShunt              = 0.005;               % Strommess-Shunt in Ohm
EFU.I_Verstaerkung      = 20;                  % Stromverstaerkung: 20V/V


%% Berechnung der Parameter
% Zwischenkreisspannungmessung
EFU.udcSignalconditioning =(EFU.Rv_Udc+EFU.Rm_Udc)/EFU.Rm_Udc;
EFU.udcIncrements         = EFU.ADC_VRef/(2^EFU.ADC_BIT-1)*EFU.udcSignalconditioning;       % Zwischenkreisspannung in V/Inc

% Motorspannungsmessung
EFU.umotorSignalconditioning =(EFU.Rv_Umotor +EFU.Rm_Umotor )/EFU.Rm_Umotor;
EFU.umotorIncrements         = EFU.ADC_VRef/(2^EFU.ADC_BIT-1)*EFU.umotorSignalconditioning;       % Zwischenkreisspannung in V/Inc

% Strommessung
EFU.iSignalconditioning = EFU.RShunt*EFU.I_Verstaerkung;
EFU.iIncrements       = EFU.ADC_VRef/(2^EFU.ADC_BIT-1)/EFU.iSignalconditioning; % Normierung des Phasenstroms in A/Inc
EFU.iOffset   = (EFU.ADC_VRef/2)/(2^EFU.ADC_BIT-1)/EFU.iSignalconditioning;

% Comparator
EFU.CMPSignalconditioning       =EFU.RShunt*EFU.I_Verstaerkung;
EFU.CMPIncrements         = (2^EFU.CMP_BIT-1)/EFU.CMPSignalconditioning;  
EFU.CMPOffset = (EFU.ADC_VRef/2/EFU.ADC_VRef)*(2^EFU.CMP_BIT-1);


%% PWM Unit
% Initialisation of PWM Unit
HALCPU_SYSTEM_CLOCK=72e6;     % 72MHz System Clock
HALCPU_PWM3PHASE_FREQ = 8e3;  % 8kHz PWM Freqeuncy
HALCPU_PWM3PHASE_TPWM_COUNT =  (HALCPU_SYSTEM_CLOCK/2)/HALCPU_PWM3PHASE_FREQ;
HALCPU_IRQ_SYSTICK_FREQUENCY_HZ = 1000;
